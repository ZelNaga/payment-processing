USE `process`;
DROP procedure IF EXISTS `report_for_zhek_by_banks`;

DELIMITER $$
USE `process`$$
CREATE DEFINER=`root`@`%` PROCEDURE `report_for_zhek_by_banks`(
	IN acceptorOrgId INT, processedDate DATE
)
	COMMENT 'Build report for the zhek in the context of banks !'
	BEGIN

		DECLARE EXIT HANDLER FOR SQLEXCEPTION
		BEGIN
			DROP TABLE IF EXISTS payments, commissions_1, commissions_2, zhek_commissions, result;
			SELECT NULL LIMIT 0;
		END;

		CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
			SELECT o_p.service_id,
				p_p.acceptor_org_id,
				p_p.payment_id,
				p_p.start_value,
				p_p.transfer_value,
				(SELECT main_new.get_balance_org_id(p_p.organization_id) ) AS balance_org_id
			FROM `process`.`processed_payments` AS p_p
				LEFT JOIN `process`.`processed_days` AS p_d
					ON p_p.processed_day_id = p_d.id
				LEFT JOIN `online`.`tbl_payment` AS o_p
					ON p_p.payment_id = o_p.ID
			WHERE p_p.acceptor_org_id = acceptorOrgId
						AND p_d.processed_date = processedDate
		);

		CREATE TEMPORARY TABLE IF NOT EXISTS commissions_1 AS (
			SELECT p.balance_org_id, o.type_id, SUM(c.comission) AS sum
			FROM `process`.`payments_commissions` AS c
				LEFT JOIN `online`.`tbl_organization` AS o
					ON c.comission_acceptor_org_id = o.id
				RIGHT JOIN payments AS p
					ON c.payment_id = p.payment_id
			WHERE c.formula_id != 0
			GROUP BY o.type_id, p.balance_org_id
		);

		CREATE TEMPORARY TABLE IF NOT EXISTS commissions_2 AS (
			SELECT p.balance_org_id, o.type_id, SUM(c.comission) AS sum
			FROM `process`.`payments_commissions` AS c
				LEFT JOIN `online`.`tbl_organization` AS o
					ON c.comission_acceptor_org_id = o.id
				RIGHT JOIN payments AS p
					ON c.payment_id = p.payment_id
			WHERE c.formula_id != 0
			GROUP BY o.type_id, p.balance_org_id
		);

		CREATE TEMPORARY TABLE IF NOT EXISTS zhek_commissions AS (
			SELECT (SELECT main_new.get_balance_org_id(p.organization_id)) AS balance_org_id,
						 a.short_name AS balance_org_name,
				c.service_id,
						 s.short_name AS service_name,
				p.acceptor_org_id,
						 (SELECT short_name FROM main_new.m_org_attributes WHERE org_id = p.acceptor_org_id) AS acceptor_org_name,
						 0.00 AS start_value,
						 SUM(c.comission) AS transfer_value
			FROM process.payments_commissions AS c
				JOIN process.processed_payments AS p
					ON c.payment_id = p.payment_id
				JOIN process.processed_days AS d
					ON p.processed_day_id = d.id
				JOIN main_new.m_org_attributes AS a
					ON p.main_org_id = a.org_id
				JOIN main_new.m_service AS s
					ON c.service_id = s.id
			WHERE c.comission_acceptor_org_id = acceptorOrgId
						AND d.processed_date = processedDate
			GROUP BY balance_org_id, c.service_id, p.acceptor_org_id
		);

		SET @sql = (SELECT CONCAT(
				'SELECT ',
				'p.balance_org_id AS balance_org_id, ',
				'		a.short_name AS balance_org_name, ',
				'		p.service_id, ',
				'		s.short_name AS service_name, ',
				'		p.acceptor_org_id, ',
				'		(SELECT short_name FROM main_new.m_org_attributes WHERE org_id = p.acceptor_org_id) AS acceptor_org_name, ',
				'		ROUND(SUM(p.start_value), 2) AS start_value, ',
				'		ROUND(SUM(p.transfer_value), 2) AS transfer_value, ',
				GROUP_CONCAT(DISTINCT '(CASE c1.type_id WHEN ', type_id, ' THEN c1.sum ELSE 0 END) AS `', type_id SEPARATOR '`, '),
				'` FROM payments AS p ',
				'LEFT JOIN commissions_1 AS c1 ',
				'ON p.balance_org_id = c1.balance_org_id ',
				'LEFT JOIN main_new.m_org_attributes AS a ',
				'ON p.balance_org_id = a.org_id ',
				'LEFT JOIN main_new.m_service AS s ',
				'ON p.service_id = s.id ',
				'GROUP BY p.balance_org_id, p.service_id ',
				#'ORDER BY p.balance_org_id ',
				'UNION ALL ',
				'SELECT c.*, ',
				GROUP_CONCAT(DISTINCT '(CASE c2.type_id WHEN ', type_id, ' THEN c2.sum ELSE 0 END) AS `', type_id SEPARATOR '`, '),
				'` FROM zhek_commissions AS c ',
				'LEFT JOIN commissions_2 AS c2 ',
				'ON c.acceptor_org_id = c2.balance_org_id ',
				'ORDER BY balance_org_id, service_id'
		)
								FROM commissions_1);

		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		#SELECT @sql;

		DROP TABLE IF EXISTS payments, commissions_1, commissions_2, zhek_commissions, result;
	END$$

DELIMITER ;

