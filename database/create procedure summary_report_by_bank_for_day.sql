USE `process`;
DROP procedure IF EXISTS `summary_report_by_bank_for_day`;

DELIMITER $$
USE `process`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `summary_report_by_bank_for_day`(
	IN parent_id INT
)
    COMMENT 'Build summary report by bank for day !'
BEGIN
	
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS commissions, payments;
		SELECT NULL LIMIT 0;
    END;

	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
    SELECT p.acceptor_org_id, o.type_id, SUM(c.comission ) AS sum
	FROM process.payments_commissions AS c
	LEFT JOIN `online`.`tbl_organization` AS o
	ON c.comission_acceptor_org_id = o.id
	LEFT JOIN process.processed_payments AS p
	ON c.payment_id = p.payment_id
	LEFT JOIN main_new.m_organization AS m_o
	ON p.organization_id = m_o.id    
	WHERE c.formula_id != 0 AND m_o.parent_id = parent_id
	GROUP BY p.acceptor_org_id, o.type_id);

	CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
	SELECT organization_name.name AS acceptor_name,
			payments.acceptor_org_id AS acceptor_id,
			payments.mfo AS mfo, 
			payments.main_account AS account, 
			SUM(payments.start_value) AS start_value, 
			SUM(payments.transfer_value) AS transfer_value,
			COUNT(payments.payment_id) AS total_payments
	FROM process.processed_payments AS payments
	LEFT JOIN main_new.m_organization AS organization
	ON payments.organization_id = organization.id
	LEFT JOIN `online`.`tbl_organization` AS organization_name
	ON payments.acceptor_org_id = organization_name.id
	WHERE organization.parent_id = parent_id
	GROUP BY payments.acceptor_org_id);

	SET @sql = (SELECT CONCAT(
		'SELECT p.acceptor_name AS `name`, ',
        'p.mfo AS `mfo`, ',
        'p.account AS `account`, ',
        'p.start_value AS `start_value`, ',
        'p.transfer_value AS `transfer_value`, ',
		GROUP_CONCAT(DISTINCT 'SUM(CASE c.type_id WHEN ', type_id, ' THEN c.sum ELSE 0 END) AS `', type_id SEPARATOR '`, '),
        '`, p.total_payments AS `total_payments` ',
		'FROM payments AS p ',
		'LEFT JOIN commissions AS c ',
		'ON p.acceptor_id = c.acceptor_org_id ',
		'GROUP BY p.acceptor_id;')
	FROM commissions);	
    
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    DROP TABLE IF EXISTS commissions, payments;  

END$$

DELIMITER ;


