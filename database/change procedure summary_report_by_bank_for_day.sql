USE `process`;
DROP procedure IF EXISTS `summary_report_by_bank_for_day`;

DELIMITER $$
USE `process`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `summary_report_by_bank_for_day`(
  IN parentId INT, processedDate DATE
)
  COMMENT 'Build summary report by bank for day !'
  BEGIN

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      DROP TABLE IF EXISTS commissions1, commissions2, commissions_cache, payments, acceptor_org_id_cache;
      SELECT NULL LIMIT 0;
    END;

    CREATE TEMPORARY TABLE IF NOT EXISTS commissions1 AS (
      SELECT p.acceptor_org_id, o.type_id, SUM(c.comission ) AS sum
      FROM process.payments_commissions AS c
        LEFT JOIN `online`.`tbl_organization` AS o
          ON c.comission_acceptor_org_id = o.id
        LEFT JOIN process.processed_payments AS p
          ON c.payment_id = p.payment_id
        LEFT JOIN process.processed_days AS d
          ON p.processed_day_id = d.id
        LEFT JOIN main_new.m_organization AS m_o
          ON p.organization_id = m_o.id
      WHERE c.formula_id != 0 AND m_o.parent_id = parentId
            AND d.processed_date = processedDate
      GROUP BY p.acceptor_org_id, o.type_id
    );

    CREATE TEMPORARY TABLE IF NOT EXISTS commissions2 AS (
      SELECT p.acceptor_org_id, o.type_id, SUM(c.comission ) AS sum
      FROM process.payments_commissions AS c
        LEFT JOIN `online`.`tbl_organization` AS o
          ON c.comission_acceptor_org_id = o.id
        LEFT JOIN process.processed_payments AS p
          ON c.payment_id = p.payment_id
        LEFT JOIN process.processed_days AS d
          ON p.processed_day_id = d.id
        LEFT JOIN main_new.m_organization AS m_o
          ON p.organization_id = m_o.id
      WHERE c.formula_id != 0 AND m_o.parent_id = parentId
            AND d.processed_date = processedDate
      GROUP BY p.acceptor_org_id, o.type_id
    );

    CREATE TEMPORARY TABLE IF NOT EXISTS commissions_cache AS (
      SELECT c.comission_acceptor_org_id AS acceptor_org_id, c.comission_acceptor_type AS type_id, c.comission AS comission
      FROM process.payments_commissions AS c
        LEFT JOIN process.processed_payments AS p
          ON c.payment_id = p.payment_id
        LEFT JOIN process.processed_days AS d
          ON p.processed_day_id = d.id
        LEFT JOIN main_new.m_organization AS m_o
          ON p.organization_id = m_o.id
      WHERE c.formula_id = 0 AND m_o.parent_id = parentId
            AND d.processed_date = processedDate
    );

    CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
      SELECT  org_cmsu.supplier_id AS supplier_id,
              organization_name.name AS acceptor_name,
              payments.acceptor_org_id AS acceptor_id,
              payments.mfo AS mfo,
              payments.main_account AS account,
              ROUND(SUM(payments.start_value), 2) AS start_value,
              ROUND(SUM(payments.transfer_value)
                    + CASE WHEN (@debt := (SELECT SUM(b.organization_credit)
                                           FROM process.organizations_debts AS d
                                             LEFT JOIN process.organizations_debts_balance AS b
                                               ON d.id = b.organization_debt_id
                                           WHERE payments.acceptor_org_id = d.acceptor_id
                                                 AND b.processed_date = processedDate))
                THEN @debt
                      ELSE 0 END
                    + CASE WHEN (@commission := (SELECT SUM(c_com.comission ) AS sum
                                                 FROM commissions_cache AS c_com
                                                 WHERE c_com.acceptor_org_id = payments.acceptor_org_id))
                THEN @commission
                      ELSE 0 END, 2)
                AS transfer_value,
              COUNT(payments.payment_id) AS total_payments
      FROM process.processed_payments AS payments
        LEFT JOIN main_new.m_organization AS organization
          ON payments.organization_id = organization.id
        LEFT JOIN process.processed_days AS p_d
          ON payments.processed_day_id = p_d.id
        LEFT JOIN `online`.`tbl_organization` AS organization_name
          ON payments.acceptor_org_id = organization_name.id
        LEFT JOIN main_new.m_org_cmsu AS org_cmsu
          ON payments.acceptor_org_id = org_cmsu.org_id
      WHERE organization.parent_id = parentId
            AND p_d.processed_date = processedDate
      GROUP BY payments.main_account
    );

    CREATE TEMPORARY TABLE IF NOT EXISTS acceptor_org_id_cache AS (
      SELECT p.acceptor_org_id
      FROM process.processed_payments AS p
        LEFT JOIN process.processed_days AS d
          ON p.processed_day_id = d.id
        LEFT JOIN main_new.m_organization AS m_o
          ON p.organization_id = m_o.id
      WHERE m_o.parent_id = parentId
            AND d.processed_date = processedDate
    );

    SET @sql = (SELECT CONCAT(
        'SELECT p.supplier_id AS supplier_id, p.acceptor_name AS `name`, ',
        'p.mfo AS `mfo`, ',
        'p.account AS `account`, ',
        'p.start_value AS `start_value`, ',
        'p.transfer_value AS `transfer_value`, ',
        GROUP_CONCAT(DISTINCT 'SUM(CASE c1.type_id WHEN ', commission_type_id, ' THEN c1.sum ELSE 0 END) AS `', commission_type_id SEPARATOR '`, '),
        '`, p.total_payments AS `total_payments` ',
        'FROM payments AS p ',
        'LEFT JOIN commissions1 AS c1 ',
        'ON p.acceptor_id = c1.acceptor_org_id ',
        'GROUP BY p.account ',
        'UNION '
        'SELECT org_cmsu.supplier_id AS supplier_id, ',
        'organization_name.name AS acceptor_name, ',
        'a.mfo AS mfo, ',
        'a.account AS account, ',
        '0 AS start_value, ',
        'SUM(c_cache.comission) AS transfer_value, ',
        GROUP_CONCAT(DISTINCT 'SUM(CASE c2.type_id WHEN ', commission_type_id, ' THEN c2.sum ELSE 0 END) AS `', commission_type_id SEPARATOR '`, '),
        '`, 0 AS total_payments ',
        'FROM commissions_cache AS c_cache ',
        'LEFT JOIN commissions2 AS c2 ',
        'ON c_cache.acceptor_org_id = c2.acceptor_org_id ',
        'LEFT JOIN main_new.m_org_accounts AS a ',
        'ON c_cache.acceptor_org_id = a.org_id '
        'LEFT JOIN `online`.`tbl_organization` AS organization_name ',
        'ON c_cache.acceptor_org_id = organization_name.id ',
        'LEFT JOIN main_new.m_org_cmsu AS org_cmsu ',
        'ON c_cache.acceptor_org_id = org_cmsu.org_id	',
        'WHERE c_cache.acceptor_org_id NOT IN (SELECT * FROM acceptor_org_id_cache) ',
        'AND a.priority = 0 '
        'GROUP BY c_cache.acceptor_org_id;')
                FROM main_new.report_commissions_types);

    PREPARE stmt FROM @sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

    DROP TABLE IF EXISTS commissions1, commissions2, commissions_cache, payments, acceptor_org_id_cache;

  END$$

DELIMITER ;

