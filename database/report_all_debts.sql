USE `process`;
DROP procedure IF EXISTS `report_all_debts`;

DELIMITER $$
USE `process`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `report_all_debts`(
	IN dateBegin DATE, dateEnd DATE
)
BEGIN

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS debts, balance, balance_sum;
		SELECT NULL LIMIT 0;
    END;
    
    CREATE TEMPORARY TABLE IF NOT EXISTS debts AS (
		SELECT DISTINCT a.name, 
						d.reason, 
                        d.acceptor_id,
						d.id AS debt_id
		FROM process.organizations_debts AS d
		LEFT JOIN main_new.m_org_attributes AS a
		ON d.organization_id = a.org_id
		WHERE d.acceptor_id != 9999 # мають бути відображені усі борги які списані на користь інших організацій окрім ЦМСУ 
		AND d.processing_date BETWEEN dateBegin AND dateEnd 
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS balance AS (
		SELECT b.organization_debt_id, 
			   b.organization_credit AS credit,
			   DAY(b.processed_date) AS day
		FROM process.organizations_debts_balance AS b
		JOIN debts AS d 
		ON b.organization_debt_id = d.debt_id
		WHERE b.organization_credit != 0.00 # нульове значення кредиту означає що данний борг вперше списується
		AND b.processed_date BETWEEN dateBegin AND dateEnd
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS balance_sum AS (
		SELECT b.organization_debt_id, 
			   SUM(credit) AS sum
		FROM balance AS b
		GROUP BY b.organization_debt_id
	);

	SET @sql = (
					SELECT CONCAT(
									'SELECT d.name AS supplier_name, ',
									'		d.reason, ',
                                    '		a.name AS acceptor_name, ',
									GROUP_CONCAT(DISTINCT 'SUM((CASE b.day WHEN ', day, ' THEN b.credit ELSE 0.00 END)) AS `', day SEPARATOR '`, '),
									'`, s.sum ',
									'FROM debts AS d ',
									'JOIN balance AS b ',
									'ON d.debt_id = b.organization_debt_id ',
									'JOIN  balance_sum AS s ',
									'ON d.debt_id = s.organization_debt_id ',
                                    'LEFT JOIN main_new.m_org_attributes AS a ',
									'ON d.acceptor_id = a.org_id ',
									'GROUP BY d.name, a.name;'
								 )
					FROM balance
				);

	PREPARE stmt FROM @sql;    
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

	DROP TABLE IF EXISTS debts, balance, balance_sum;

END$$

DELIMITER ;


