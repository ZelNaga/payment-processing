USE `process`;
DROP procedure IF EXISTS `dbf_bank`;

DELIMITER $$
USE `process`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `dbf_bank`(
	IN balanceId INT, periodBegin DATE, periodEnd DATE
)
    COMMENT 'Build summary report by bank for day'
BEGIN
	DECLARE empty_decimal_value DECIMAL(1, 1) DEFAULT 0.0;
    DECLARE empty_char_value CHAR(1) DEFAULT "";
    DECLARE error_msg VARCHAR(45) DEFAULT "!!! No value !!!";
	
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS commissions, renta_commissions, payments, acceptor_org_id_cache;
		SELECT NULL LIMIT 0;
    END;

	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
		SELECT  p.acceptor_org_id, 
				p.main_account,
                p.service_id,
                c.comission_acceptor_type AS type_id, 
                SUM(c.comission ) AS sum
		FROM process.payments_commissions AS c
		LEFT JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id   
		WHERE c.formula_id != 0 AND p.balance_org_id = balanceId 
		AND d.processed_date BETWEEN periodBegin AND periodEnd
		GROUP BY p.acceptor_org_id, p.main_account, p.service_id, type_id
    );
    
    CREATE TEMPORARY TABLE IF NOT EXISTS renta_commissions AS (
		SELECT c.comission_acceptor_org_id AS acceptor_org_id,
				a.mfo,
                a.account,
                o.edrpou,
                p.service_id,
				c.comission_acceptor_type AS type_id,
				c.comission AS comission
		FROM process.payments_commissions AS c
		LEFT JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id
        LEFT JOIN main_new.m_org_accounts AS a
        ON c.comission_acceptor_org_id = a.org_id
        LEFT JOIN main_new.m_organization AS o
		ON c.comission_acceptor_org_id = o.id
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id		   
		WHERE c.formula_id = 0 
        AND p.balance_org_id = balanceId
        AND a.priority = 0
		AND d.processed_date BETWEEN periodBegin 
        AND periodEnd
        GROUP BY c.comission_acceptor_org_id, a.account, p.service_id
    ); 

	CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
		SELECT  org_cmsu.supplier_id AS supplier_id,
				organization_name.name AS acceptor_name,
				payments.acceptor_org_id AS acceptor_id,
				payments.mfo AS mfo, 
				payments.main_account AS account,
                o.edrpou,
                payments.service_id,
                payments.zhek,
				ROUND(SUM(payments.start_value), 2) AS start_value,
                ROUND(SUM(payments.transfer_value)
						+ CASE WHEN (@debt := (	
													SELECT SUM(b.organization_credit) 
													FROM process.organizations_debts AS d
													LEFT JOIN process.organizations_debts_balance AS b
													ON d.id = b.organization_debt_id
													WHERE payments.acceptor_org_id = d.acceptor_id 
													AND b.processed_date BETWEEN periodBegin AND periodEnd
											   )
									) 
							THEN @debt
							ELSE 0 END
						+ CASE WHEN (@commission := (
														SELECT SUM(c_com.comission ) AS sum
														FROM renta_commissions AS c_com
														WHERE c_com.acceptor_org_id = payments.acceptor_org_id
													 )
									)
							THEN @commission
							ELSE 0	 END
					  , 2) AS transfer_value,			
				COUNT(payments.payment_id) AS total_payments
		FROM process.processed_payments AS payments		
		LEFT JOIN process.processed_days AS d
		ON payments.processed_day_id = d.id
		LEFT JOIN main_new.m_org_attributes AS organization_name
		ON payments.acceptor_org_id = organization_name.org_id
		LEFT JOIN main_new.m_org_cmsu AS org_cmsu
		ON payments.acceptor_org_id = org_cmsu.org_id
        LEFT JOIN main_new.m_organization AS o
		ON payments.acceptor_org_id = o.id
		WHERE payments.balance_org_id = balanceId
        AND organization_name.enabled = true
		AND d.processed_date BETWEEN periodBegin AND periodEnd
		GROUP BY payments.acceptor_org_id, payments.main_account, payments.service_id
    );
    
    CREATE TEMPORARY TABLE IF NOT EXISTS acceptor_org_id_cache AS (
		SELECT p.acceptor_org_id
		FROM process.processed_payments AS p
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		WHERE p.balance_org_id = balanceId
		AND d.processed_date BETWEEN periodBegin AND periodEnd
    );
    
    SELECT  IFNULL(CAST(p.acceptor_name AS CHAR(40)), error_msg) AS `DESTORG`,
			IFNULL(CAST((SELECT name FROM main_new.m_service WHERE id = p.service_id) AS CHAR(40)), error_msg) AS `NAME`,
			empty_char_value AS `PDV`,
			IFNULL(CAST(p.mfo AS CHAR(6)), error_msg) AS `MFO`,
			IFNULL(CAST(p.account AS CHAR(14)), error_msg) AS `ACCOUNT`,
			IFNULL(CAST(p.edrpou AS CHAR(10)), error_msg) AS `ZKPO`,
			IFNULL(CAST(p.transfer_value AS DECIMAL(14, 2)), empty_decimal_value) AS `WITHOUTTAX`,
			IFNULL(CAST(p.start_value AS DECIMAL(14, 2)), empty_decimal_value) AS `PAYMENT`,
			IFNULL(CAST(SUM(c.sum) AS DECIMAL(9, 2)), empty_decimal_value) AS `TAX`,
			IFNULL(CAST(SUM(CASE c.type_id WHEN 2 THEN c.sum ELSE empty_decimal_value END) AS DECIMAL(9, 2)), empty_decimal_value) AS TAXBANK,
			IFNULL(CAST(SUM(CASE c.type_id WHEN 1 THEN c.sum ELSE empty_decimal_value END) AS DECIMAL(9, 2)), empty_decimal_value) AS TAXCC,
			IFNULL(CAST(p.total_payments AS DECIMAL(4, 0)), empty_decimal_value) AS `PAYMENTNO`,
			empty_char_value AS OTHER,
			IFNULL(CAST(p.zhek AS CHAR(2)), empty_decimal_value) AS NOMJ,
			IFNULL(CAST(SUM(CASE c.type_id WHEN 4 THEN c.sum ELSE empty_decimal_value END) AS DECIMAL(9, 2)), empty_decimal_value) AS TAXJEK,
			empty_char_value AS TAXAV,
			empty_char_value AS TAXAKT,
			IFNULL(CAST(p.service_id AS DECIMAL(2, 0)), empty_decimal_value) AS `KODP`,
			IFNULL(CAST(p.supplier_id AS CHAR(2)), empty_decimal_value) AS SOURORGID,
			IFNULL(CAST(p.acceptor_id AS  CHAR(2)), empty_decimal_value) AS DESTORGID,
			empty_char_value AS LREMAP
	FROM payments AS p
	LEFT JOIN commissions AS c
	ON p.acceptor_id = c.acceptor_org_id AND p.account = c.main_account AND p.service_id = c.service_id 
	GROUP BY p.acceptor_id, p.account, p.service_id 
	UNION 
	SELECT  IFNULL(CAST(organization_name.name AS CHAR(40)), error_msg) AS DESTORG,
			IFNULL(CAST((SELECT name FROM main_new.m_service WHERE id = c_cache.service_id) AS CHAR(40)), error_msg) `NAME`,
			empty_char_value AS `PDV`,
			IFNULL(CAST(c_cache.mfo AS CHAR(6)), error_msg) AS MFO,
			IFNULL(CAST(c_cache.account AS CHAR(14)), error_msg) AS ACCOUNT,
			IFNULL(CAST(c_cache.edrpou AS CHAR(10)), error_msg) AS `ZKPO`,
			IFNULL(CAST(SUM(c_cache.comission) AS DECIMAL(14, 2)), empty_decimal_value) AS `WITHOUTTAX`,
			empty_decimal_value AS PAYMENT,
			empty_decimal_value AS TAX,
			empty_decimal_value AS TAXBANK,
			empty_decimal_value AS TAXCC,
			0 AS PAYMENTNO,
			empty_char_value AS OTHER,
			empty_char_value AS NOMJ,
			empty_decimal_value AS TAXJEK,
			empty_char_value AS TAXAV,
			empty_char_value AS TAXAKT,
			IFNULL(CAST(c_cache.service_id  AS DECIMAL(2, 0)), empty_decimal_value) AS `KODP`,
			IFNULL(CAST(c_cache.acceptor_org_id AS CHAR(2)), empty_decimal_value) AS SOURORGID,
			IFNULL(CAST(c_cache.acceptor_org_id AS CHAR(2)), empty_decimal_value) AS DESTORGID,
			empty_char_value AS LREMAP
	FROM renta_commissions AS c_cache
	LEFT JOIN main_new.m_org_attributes AS organization_name
	ON c_cache.acceptor_org_id = organization_name.org_id		  
	WHERE c_cache.acceptor_org_id NOT IN (SELECT * FROM acceptor_org_id_cache)
	AND organization_name.enabled = true
	GROUP BY c_cache.acceptor_org_id, c_cache.account, c_cache.service_id;
    
    DROP TABLE IF EXISTS commissions, renta_commissions, payments, acceptor_org_id_cache;  

END$$

DELIMITER ;


