DROP PROCEDURE IF EXISTS `process`.`report_bank_for_day`;
DROP PROCEDURE IF EXISTS `process`.`report_money_for_cmsu`;
DROP PROCEDURE IF EXISTS `process`.`report_osbb_by_banks_and_accounts`;
DROP PROCEDURE IF EXISTS `process`.`report_supplier_by_banks_and_zheks_for_day`;
DROP PROCEDURE IF EXISTS `process`.`report_supplier_by_zheks_for_day`;
DROP PROCEDURE IF EXISTS `process`.`report_zhek_by_banks`;
DROP PROCEDURE IF EXISTS `process`.`report_zhek_by_banks_and_services`;

DROP FUNCTION IF EXISTS `process`.`get_home_address`;
DROP FUNCTION IF EXISTS `process`.`get_home_owner`;
DROP FUNCTION IF EXISTS `process`.`get_zhek_id`;

CREATE DATABASE  IF NOT EXISTS `process` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `process`;
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: 127.0.0.1    Database: process
-- ------------------------------------------------------
-- Server version	5.6.30-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping events for database 'process'
--

--
-- Dumping routines for database 'process'
--
/*!50003 DROP FUNCTION IF EXISTS `get_home_address` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_home_address`(
	home_id INTEGER UNSIGNED
) RETURNS varchar(255) CHARSET utf8
BEGIN

	DECLARE home_address VARCHAR(255);
    SET home_address = (SELECT CONCAT(street.full_name, ' ', house.house_no, '/', home.flat_no) 
						FROM main_new.m_home AS home
						JOIN main_new.m_house AS house
						ON home.house_id = house.id
						JOIN main_new.m_street AS street
						ON house.street_id = street.id
						WHERE home.id  = home_id
                        LIMIT 1);

RETURN home_address;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_home_owner` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_home_owner`(
	home_id INTEGER UNSIGNED
) RETURNS varchar(255) CHARSET utf8
BEGIN

	DECLARE home_owner VARCHAR(255);
    SET home_owner = (SELECT CONCAT(person_attributes.last_name, ' ', SUBSTRING(person_attributes.first_name, 1, 1), '. ', SUBSTRING(person_attributes.middle_name, 1, 1), '.') 
					  FROM main_new.m_home_owner AS home_owner
					  JOIN main_new.m_person_attributes AS person_attributes
					  ON home_owner.person_id = person_attributes.person_id
					  WHERE home_owner.home_id = home_id 
                      LIMIT 1);

RETURN home_owner;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_zhek_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_zhek_id`(	
    zhek_code INTEGER UNSIGNED,
    account INTEGER UNSIGNED
) RETURNS int(11)
    COMMENT 'Ця функція створена для того, щоб опрацьовувати крайні випадки (костилі) при визначенні ідентифікатора жека'
BEGIN
	DECLARE zhek_id INT;
    IF (zhek_code = 0) THEN # неіснуючий жек (різні платежі по Водоканалу)
		SET zhek_id = 0;
	ELSEIF (zhek_code = 7) THEN # для даного кода жека є більше ніж один запис, повертається id головної організації
		SET zhek_id = 8699;
	ELSEIF (zhek_code = 14) THEN # для даного кода жека є більше ніж один запис, повертається id головної організації
		SET zhek_id = 3286;
	ELSEIF (zhek_code = 16) THEN # для даного кода жека є більше ніж один запис, повертається id головної організації
		SET zhek_id = 9369;
	ELSEIF (zhek_code = 90) THEN # усі ОСББ віднесені до цього коду жека, тому необхідний окремий алгоритм для визначення id організації
		SET zhek_id = (SELECT s.supplier_org_id 
						FROM main_new.m_supplier AS s
						JOIN main_new.m_account AS ma
						ON s.account_id = ma.id
						WHERE ma.zhek_code = zhek_code
						AND ma.account = account
						AND s.service_id = 1);
	ELSE # усі інші коди жеків опрацьовуються тут (так має опрацьовуватися все, але із-за ``костилів`` маємо таку ситуацію)
		SET zhek_id = (SELECT org_id 
						FROM main_new.m_org_cmsu 
						WHERE zhek = zhek_code);
	END IF;

RETURN zhek_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `report_bank` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `report_bank`(
	IN balanceId INT, periodBegin DATE, periodEnd DATE
)
    COMMENT 'Build summary report by bank for day !'
BEGIN
	
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS commissions, renta_commissions, payments, acceptor_org_id_cache;
		SELECT NULL LIMIT 0;
    END;

	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
		SELECT p.acceptor_org_id, p.main_account, c.comission_acceptor_type AS type_id, SUM(c.comission ) AS sum
		FROM process.payments_commissions AS c
		LEFT JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		LEFT JOIN main_new.m_organization AS o
		ON p.organization_id = o.id    
		WHERE c.formula_id != 0 AND p.balance_org_id = balanceId 
		AND d.processed_date BETWEEN periodBegin AND periodEnd
		GROUP BY p.acceptor_org_id, p.main_account, type_id
    );
    
    CREATE TEMPORARY TABLE IF NOT EXISTS renta_commissions AS (
		SELECT c.comission_acceptor_org_id AS acceptor_org_id,
				c.comission_acceptor_type AS type_id,
				c.comission AS comission
		FROM process.payments_commissions AS c
		LEFT JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id 
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id		   
		WHERE c.formula_id = 0 AND p.balance_org_id = balanceId
		AND d.processed_date BETWEEN periodBegin AND periodEnd
    ); 

	CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
		SELECT  org_cmsu.supplier_id AS supplier_id,
				organization_name.name AS acceptor_name,
				payments.acceptor_org_id AS acceptor_id,
				payments.mfo AS mfo, 
				payments.main_account AS account, 
				ROUND(SUM(payments.start_value), 2) AS start_value,
                ROUND(SUM(payments.transfer_value)
						+ CASE WHEN (@debt := (	
													SELECT SUM(b.organization_credit) 
													FROM process.organizations_debts AS d
													LEFT JOIN process.organizations_debts_balance AS b
													ON d.id = b.organization_debt_id
													WHERE payments.acceptor_org_id = d.acceptor_id 
													AND b.processed_date BETWEEN periodBegin AND periodEnd
											   )
									) 
							THEN @debt
							ELSE 0 END
						+ CASE WHEN (@commission := (
														SELECT SUM(c_com.comission ) AS sum
														FROM renta_commissions AS c_com
														WHERE c_com.acceptor_org_id = payments.acceptor_org_id
													 )
									)
							THEN @commission
							ELSE 0 END
					  , 2) AS transfer_value,			
				COUNT(payments.payment_id) AS total_payments
		FROM process.processed_payments AS payments		
		LEFT JOIN process.processed_days AS d
		ON payments.processed_day_id = d.id
		LEFT JOIN main_new.m_org_attributes AS organization_name
		ON payments.acceptor_org_id = organization_name.org_id
		LEFT JOIN main_new.m_org_cmsu AS org_cmsu
		ON payments.acceptor_org_id = org_cmsu.org_id
		WHERE payments.balance_org_id = balanceId
        AND organization_name.enabled = true
		AND d.processed_date BETWEEN periodBegin AND periodEnd
        
		GROUP BY payments.main_account
    );
    
    CREATE TEMPORARY TABLE IF NOT EXISTS acceptor_org_id_cache AS (
		SELECT p.acceptor_org_id
		FROM process.processed_payments AS p
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		WHERE p.balance_org_id = balanceId
		AND d.processed_date BETWEEN periodBegin AND periodEnd
    );

	SET @sql = (SELECT CONCAT(
				'SELECT p.supplier_id AS supplier_id, p.acceptor_name AS `name`, ',
					'p.mfo AS `mfo`, ',
					'p.account AS `account`, ',
					'p.start_value AS `start_value`, ',
					'p.transfer_value AS `transfer_value`, ',
					GROUP_CONCAT(DISTINCT 'SUM(CASE c.type_id WHEN ', commission_type_id, ' THEN c.sum ELSE 0 END) AS `', commission_type_id SEPARATOR '`, '),
					'`, p.total_payments AS `total_payments` ',
				'FROM payments AS p ',
				'LEFT JOIN commissions AS c ',
				'ON p.acceptor_id = c.acceptor_org_id AND p.account = c.main_account ',
				'GROUP BY p.account ',
                'UNION '
                'SELECT org_cmsu.supplier_id AS supplier_id, ',
					'organization_name.name AS acceptor_name, ',
					'a.mfo AS mfo, ',
					'a.account AS account, ',
					'0 AS start_value, ',
					'SUM(c_cache.comission) AS transfer_value, ',
                    GROUP_CONCAT(DISTINCT 'SUM(CASE c_cache.type_id WHEN ', commission_type_id, ' THEN c_cache.comission ELSE 0 END) AS `', commission_type_id SEPARATOR '`, '),
					'`, 0 AS total_payments ',
				'FROM renta_commissions AS c_cache ',                
                'LEFT JOIN main_new.m_org_accounts AS a ',
                'ON c_cache.acceptor_org_id = a.org_id '
				'LEFT JOIN main_new.m_org_attributes AS organization_name ',
				'ON c_cache.acceptor_org_id = organization_name.org_id ',
				'LEFT JOIN main_new.m_org_cmsu AS org_cmsu ',
				'ON c_cache.acceptor_org_id = org_cmsu.org_id	',			  
				'WHERE c_cache.acceptor_org_id NOT IN (SELECT * FROM acceptor_org_id_cache) ',
                'AND a.priority = 0 ',
                'AND organization_name.enabled = true ',
				'GROUP BY c_cache.acceptor_org_id;')
                FROM main_new.report_commissions_types);	
    
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    DROP TABLE IF EXISTS commissions, renta_commissions, payments, acceptor_org_id_cache;  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `report_money_for_cmsu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `report_money_for_cmsu`(
	IN orgId INT, IN dateBegin DATE, dateEnd DATE
)
BEGIN

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS debts, balance, balance_sum;
		SELECT NULL LIMIT 0;
    END;
    
    CREATE TEMPORARY TABLE IF NOT EXISTS debts AS (
		SELECT DISTINCT a.name, 
						d.reason, 
						d.id AS debt_id
		FROM process.organizations_debts AS d
		LEFT JOIN main_new.m_org_attributes AS a
		ON d.organization_id = a.org_id
		WHERE d.acceptor_id = orgId 
		AND d.processing_date BETWEEN dateBegin AND dateEnd 
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS balance AS (
		SELECT b.organization_debt_id, 
			   b.organization_credit AS credit,
			   DAY(b.processed_date) AS day
		FROM process.organizations_debts_balance AS b
		JOIN debts AS d 
		ON b.organization_debt_id = d.debt_id
		WHERE b.organization_credit != 0.00 # нульове значення кредиту означає що данний борг вперше списується 
		AND b.processed_date BETWEEN dateBegin AND dateEnd
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS balance_sum AS (
		SELECT b.organization_debt_id, 
			   SUM(credit) AS sum
		FROM balance AS b
		GROUP BY b.organization_debt_id
	);

	SET @sql = (
					SELECT CONCAT(
									'SELECT d.name, ',
									'd.reason, ',
									GROUP_CONCAT(DISTINCT 'SUM((CASE b.day WHEN ', day, ' THEN b.credit ELSE 0.00 END)) AS `', day SEPARATOR '`, '),
									'`, s.sum ',
									'FROM debts AS d ',
									'JOIN balance AS b ',
									'ON d.debt_id = b.organization_debt_id ',
									'JOIN  balance_sum AS s ',
									'ON d.debt_id = s.organization_debt_id '
									'GROUP BY d.name;'
								 )
					FROM balance
				);

	PREPARE stmt FROM @sql;    
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

	DROP TABLE IF EXISTS debts, balance, balance_sum;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `report_osbb_by_banks_and_accounts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `report_osbb_by_banks_and_accounts`(
	IN osbbId INT, zhek INT, startDate DATE, endDate DATE
)
BEGIN

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS payments, commissions, total_commissions, accounts;
		SELECT NULL LIMIT 0;
    END;
    
    CREATE TEMPORARY TABLE IF NOT EXISTS accounts AS (
		SELECT DISTINCT a.account 
        FROM main_new.m_supplier AS s
		JOIN main_new.m_account AS a
		ON s.account_id = a.id
		WHERE s.supplier_org_id = osbbId 
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
		SELECT  p.account,
				p.home_id,
				p.service_id,
				p.payment_id,
				p.start_value, 
				p.transfer_value, 
				p.balance_org_id
		FROM `process`.`processed_payments` AS p
		JOIN `process`.`processed_days` AS d
		ON p.processed_day_id = d.id
        JOIN accounts AS a
        ON p.account = a.account
		WHERE p.zhek = zhek
        AND p.supplier_org_id NOT IN (553, 4269, 6589) # з цими постачальниками послуг помешкання розраховуються на пряму.
		AND d.processed_date BETWEEN startDate AND endDate
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
		SELECT p.balance_org_id, p.account, p.service_id, c.comission_acceptor_type AS type_id, SUM(c.comission) AS sum
		FROM `process`.`payments_commissions` AS c		
		RIGHT JOIN payments AS p
		ON c.payment_id = p.payment_id       
		WHERE c.formula_id != 0
		GROUP BY p.account, p.service_id, c.comission_acceptor_type, p.balance_org_id
	);
    
    CREATE TEMPORARY TABLE IF NOT EXISTS total_commissions AS (
		SELECT c.balance_org_id, c.account, c.service_id, SUM(c.sum) AS sum
		FROM commissions AS c
		GROUP BY c.balance_org_id, c.account, c.service_id 
	);
    
	SET @sql = (SELECT CONCAT('SELECT a.short_name AS balance_org_name, ',
								'	  p.account, ',
                                '	  (SELECT process.get_home_address(p.home_id)) AS address, ',
                                '	  (SELECT process.get_home_owner(p.home_id)) AS owner, ',
                                '	  s.short_name AS service_name, ',                             
								'	  ROUND(SUM(p.start_value), 2) AS start_value, ',
								'	  ROUND(SUM(p.transfer_value), 2) AS transfer_value, ',
                                '     (CASE WHEN tc.sum THEN tc.sum ELSE 0.00 END) AS total_commission, ',
									  GROUP_CONCAT(DISTINCT 'ROUND((CASE c.type_id WHEN ', commission_type_id, ' THEN c.sum ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'` FROM payments AS p ',
								'LEFT JOIN commissions AS c ',
								'ON p.balance_org_id = c.balance_org_id AND p.account = c.account AND p.service_id = c.service_id ',
                                'LEFT JOIN total_commissions AS tc ',
                                'ON p.balance_org_id = tc.balance_org_id AND p.account = tc.account AND p.service_id = tc.service_id ',
                                'LEFT JOIN main_new.m_org_attributes AS a ',
                                'ON p.balance_org_id = a.org_id ',
                                'LEFT JOIN main_new.m_service AS s ',
                                'ON p.service_id = s.id ',
								'GROUP BY p.balance_org_id, p.account, p.service_id;'
							)
					FROM main_new.report_commissions_types);	
		
	PREPARE stmt FROM @sql;    
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

	DROP TABLE IF EXISTS payments, commissions, total_commissions, accounts;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `report_supplier_by_banks_and_zheks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `report_supplier_by_banks_and_zheks`(
	IN supplierOrgId INT, startDate DATE, endDate DATE
)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS payments, commissions, total_commissions;
		SELECT NULL LIMIT 0;
    END;
    
    CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
	SELECT  payments.payment_id,  
			payments.balance_org_id,
			payments.zhek, 
			(SELECT name FROM main_new.m_org_attributes WHERE org_id = payments.balance_org_id AND enabled = true) AS bank_name,		
			(SELECT process.get_zhek_id(payments.zhek, payments.account)) AS zhek_id,
			payments.start_value AS start_value,
			payments.transfer_value AS transfer_value,
            payments.service_id
		FROM process.processed_payments AS payments
		JOIN process.processed_days AS d
		ON payments.processed_day_id = d.id
		WHERE payments.supplier_org_id = supplierOrgId
		AND d.processed_date BETWEEN startDate AND endDate
	);
		
	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
		SELECT p.balance_org_id, p.zhek, p.zhek_id, c.comission_acceptor_type AS type_id, SUM(c.comission) AS sum
		FROM process.payments_commissions AS c		
		JOIN payments AS p
		ON c.payment_id = p.payment_id
		WHERE c.formula_id != 0
		GROUP BY p.balance_org_id, p.zhek, p.zhek_id, c.comission_acceptor_type 
	);   
    CREATE TEMPORARY TABLE IF NOT EXISTS total_commissions AS (
		SELECT c.balance_org_id, c.zhek, c.zhek_id, SUM(c.sum) AS sum
		FROM commissions AS c
		GROUP BY c.balance_org_id, c.zhek, c.zhek_id 
	);
    
    SET @sql = (SELECT CONCAT('SELECT p.bank_name, ',
                                '	  (SELECT name FROM main_new.m_org_attributes WHERE org_id = p.zhek_id AND enabled = true LIMIT 1) AS zhek_name, ',
                                '     (SELECT name FROM main_new.m_service WHERE id = p.service_id) AS service_name, ',
								'	  SUM(p.start_value) AS start_value, ',
								'	  SUM(p.transfer_value) AS transfer_value, ',
                                '     (CASE WHEN tc.sum THEN tc.sum ELSE 0.00 END) AS total_commission, ',
									  GROUP_CONCAT(DISTINCT 'ROUND((CASE c.type_id WHEN ', commission_type_id, ' THEN c.sum ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'`, COUNT(p.payment_id) AS total_payments '
                                'FROM payments AS p ',
								'LEFT JOIN commissions AS c ',
								'ON p.balance_org_id = c.balance_org_id AND p.zhek = c.zhek AND p.zhek_id = c.zhek_id ',
                                'LEFT JOIN total_commissions AS tc ',
                                'ON p.balance_org_id = tc.balance_org_id AND p.zhek = tc.zhek AND p.zhek_id = tc.zhek_id ',
                                'GROUP BY p.bank_name, zhek_name;'
							)
					FROM main_new.report_commissions_types);	
		
	PREPARE stmt FROM @sql;    
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    DROP TABLE IF EXISTS payments, commissions, total_commissions;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `report_supplier_by_zheks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `report_supplier_by_zheks`(
	IN supplierOrgId INT, startDate DATE, endDate DATE
)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS payments, commissions, total_commissions;
		SELECT NULL LIMIT 0;
    END;
    
    CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
	SELECT  payments.payment_id,
			payments.zhek,		
			(SELECT process.get_zhek_id(payments.zhek, payments.account)) AS zhek_id,
			payments.start_value AS start_value,
			payments.transfer_value AS transfer_value
		FROM process.processed_payments AS payments
		JOIN process.processed_days AS d
		ON payments.processed_day_id = d.id
		WHERE payments.supplier_org_id = supplierOrgId
		AND d.processed_date BETWEEN startDate AND endDate
	);
		
	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
		SELECT p.zhek, p.zhek_id, c.comission_acceptor_type AS type_id, SUM(c.comission) AS sum
		FROM process.payments_commissions AS c		
		JOIN payments AS p
		ON c.payment_id = p.payment_id
		WHERE c.formula_id != 0
		GROUP BY p.zhek, p.zhek_id, c.comission_acceptor_type 
	);   
    CREATE TEMPORARY TABLE IF NOT EXISTS total_commissions AS (
		SELECT c.zhek, c.zhek_id, SUM(c.sum) AS sum
		FROM commissions AS c
		GROUP BY c.zhek, c.zhek_id 
	);
    
    SET @sql = (SELECT CONCAT('SELECT (SELECT name FROM main_new.m_org_attributes WHERE org_id = p.zhek_id AND enabled = true LIMIT 1) AS zhek_name, ',
                                '	  SUM(p.start_value) AS start_value, ',
								'	  SUM(p.transfer_value) AS transfer_value, ',
                                '     (CASE WHEN tc.sum THEN tc.sum ELSE 0.00 END) AS total_commission, ',
									  GROUP_CONCAT(DISTINCT 'ROUND((CASE c.type_id WHEN ', commission_type_id, ' THEN c.sum ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'`, COUNT(p.payment_id) AS total_payments '
                                'FROM payments AS p ',
								'LEFT JOIN commissions AS c ',
								'ON p.zhek = c.zhek AND p.zhek_id = c.zhek_id ',
                                'LEFT JOIN total_commissions AS tc ',
                                'ON p.zhek = tc.zhek AND p.zhek_id = tc.zhek_id ',
                                'GROUP BY p.zhek, p.zhek_id, zhek_name;'
							)
					FROM main_new.report_commissions_types);	
		
	PREPARE stmt FROM @sql;    
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    DROP TABLE IF EXISTS payments, commissions, total_commissions;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `report_zhek_by_banks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `report_zhek_by_banks`(
	IN acceptorOrgId INT, startDate DATE, endDate DATE
)
    COMMENT 'Build report for the zhek in the context of banks !'
BEGIN

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS payments, commissions, zhek_commissions, total_commissions;
		SELECT NULL LIMIT 0;
    END;

	CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
		SELECT p.service_id, 
				p.acceptor_org_id, 
				p.payment_id,
				p.start_value, 
				p.transfer_value, 
				p.balance_org_id
		FROM `process`.`processed_payments` AS p
		LEFT JOIN `process`.`processed_days` AS d
		ON p.processed_day_id = d.id
		WHERE p.acceptor_org_id = acceptorOrgId
		AND d.processed_date BETWEEN startDate AND endDate
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
		SELECT p.balance_org_id, c.comission_acceptor_type AS type_id, SUM(c.comission) AS sum
		FROM `process`.`payments_commissions` AS c		
		RIGHT JOIN payments AS p
		ON c.payment_id = p.payment_id       
		WHERE c.formula_id != 0
		GROUP BY c.comission_acceptor_type, p.balance_org_id
	);
    
    CREATE TEMPORARY TABLE IF NOT EXISTS total_commissions AS (
		SELECT c.balance_org_id, SUM(c.sum) AS sum
		FROM commissions AS c
		GROUP BY c.balance_org_id 
	);
      
    CREATE TEMPORARY TABLE IF NOT EXISTS zhek_commissions AS (
		SELECT p.balance_org_id AS balance_org_id,
				(SELECT short_name FROM main_new.m_org_attributes WHERE org_id = balance_org_id) AS balance_org_name,
				p.service_id,
				s.short_name AS service_name,
				p.acceptor_org_id,
				(SELECT short_name FROM main_new.m_org_attributes WHERE org_id = p.acceptor_org_id) AS acceptor_org_name,
				0.00 AS start_value,
				SUM(c.comission) AS transfer_value,
                c.comission_acceptor_type AS type_id
		FROM process.payments_commissions AS c
		JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id
		JOIN process.processed_days AS d
		ON p.processed_day_id = d.id		
		JOIN main_new.m_service AS s
		ON p.service_id = s.id        
		WHERE c.comission_acceptor_org_id = acceptorOrgId
		AND d.processed_date = startDate AND endDate
		GROUP BY balance_org_id, p.service_id, p.acceptor_org_id
	);
	
	SET @sql = (SELECT CONCAT('SELECT a.short_name AS balance_org_name, ',
                                '	  s.short_name AS service_name, ',
                                '	  (SELECT short_name FROM main_new.m_org_attributes WHERE org_id = p.acceptor_org_id) AS acceptor_org_name, ',                                
								'	  ROUND(SUM(p.start_value), 2) AS start_value, ',
								'	  ROUND(SUM(p.transfer_value), 2) AS transfer_value, ',
                                '     (CASE WHEN tc.sum THEN tc.sum ELSE 0.00 END) AS total_commission, ',
									  GROUP_CONCAT(DISTINCT 'ROUND((CASE c.type_id WHEN ', commission_type_id, ' THEN c.sum ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'` FROM payments AS p ',
								'LEFT JOIN commissions AS c ',
								'ON p.balance_org_id = c.balance_org_id ',
                                'LEFT JOIN total_commissions AS tc ',
                                'ON p.balance_org_id = tc.balance_org_id '
                                'LEFT JOIN main_new.m_org_attributes AS a ',
                                'ON p.balance_org_id = a.org_id ',
                                'LEFT JOIN main_new.m_service AS s ',
                                'ON p.service_id = s.id ',
								'GROUP BY p.balance_org_id, p.service_id ',
                                'UNION ALL ',
								'SELECT zc.balance_org_name, ',
                                '		zc.service_name, ',
                                '		zc.acceptor_org_name, ',                                
                                '		zc.start_value, ',
                                '		zc.transfer_value, ',
                                '		zc.transfer_value AS total_commission, ',
										GROUP_CONCAT(DISTINCT 'ROUND((CASE zc.type_id WHEN ', commission_type_id, ' THEN zc.transfer_value ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'` FROM zhek_commissions AS zc ',
                                'ORDER BY balance_org_name, service_name;'
							)
					FROM main_new.report_commissions_types);	
		
	PREPARE stmt FROM @sql;    
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;    
    
	DROP TABLE IF EXISTS payments, commissions, zhek_commissions, total_commissions;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `report_zhek_by_banks_and_services` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `report_zhek_by_banks_and_services`(
	IN zhekCode INT, startDate DATE, endDate DATE
)
BEGIN

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS payments, commissions, /*zhek_commissions,*/ total_commissions;
		SELECT NULL LIMIT 0;
    END;

	CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
		SELECT p.service_id, 
				p.acceptor_org_id, 
				p.payment_id,
				p.start_value, 
				p.transfer_value, 
				p.balance_org_id
		FROM `process`.`processed_payments` AS p
		LEFT JOIN `process`.`processed_days` AS d
		ON p.processed_day_id = d.id
		WHERE p.zhek = zhekCode
        AND p.service_id IN (1, 8, 12, 55)
		AND d.processed_date BETWEEN startDate AND endDate
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
		SELECT p.balance_org_id, c.comission_acceptor_type AS type_id, SUM(c.comission) AS sum
		FROM `process`.`payments_commissions` AS c		
		RIGHT JOIN payments AS p
		ON c.payment_id = p.payment_id       
		WHERE c.formula_id != 0
		GROUP BY c.comission_acceptor_type, p.balance_org_id
	);
    
    CREATE TEMPORARY TABLE IF NOT EXISTS total_commissions AS (
		SELECT c.balance_org_id, SUM(c.sum) AS sum
		FROM commissions AS c
		GROUP BY c.balance_org_id 
	);
      
    /*CREATE TEMPORARY TABLE IF NOT EXISTS zhek_commissions AS (
		SELECT p.balance_org_id AS balance_org_id,
				(SELECT short_name FROM main_new.m_org_attributes WHERE org_id = balance_org_id) AS balance_org_name,
				p.service_id,
				s.short_name AS service_name,
				p.acceptor_org_id,
				(SELECT short_name FROM main_new.m_org_attributes WHERE org_id = p.acceptor_org_id) AS acceptor_org_name,
				0.00 AS start_value,
				SUM(c.comission) AS transfer_value,
                c.comission_acceptor_type AS type_id
		FROM process.payments_commissions AS c
		JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id
		JOIN process.processed_days AS d
		ON p.processed_day_id = d.id		
		JOIN main_new.m_service AS s
		ON p.service_id = s.id        
		WHERE p.zhek = zhekCode
        AND p.service_id IN (1, 8, 12, 55)
		AND d.processed_date BETWEEN startDate AND endDate
		GROUP BY balance_org_id, p.service_id, p.acceptor_org_id
	);*/
	
	SET @sql = (SELECT CONCAT('SELECT a.short_name AS balance_org_name, ',
                                '	  s.short_name AS service_name, ',
                                '	  (SELECT short_name FROM main_new.m_org_attributes WHERE org_id = p.acceptor_org_id) AS acceptor_org_name, ',                                
								'	  ROUND(SUM(p.start_value), 2) AS start_value, ',
								'	  ROUND(SUM(p.transfer_value), 2) AS transfer_value, ',
                                '     (CASE WHEN tc.sum THEN tc.sum ELSE 0.00 END) AS total_commission, ',
									  GROUP_CONCAT(DISTINCT 'ROUND((CASE c.type_id WHEN ', commission_type_id, ' THEN c.sum ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'` FROM payments AS p ',
								'LEFT JOIN commissions AS c ',
								'ON p.balance_org_id = c.balance_org_id ',
                                'LEFT JOIN total_commissions AS tc ',
                                'ON p.balance_org_id = tc.balance_org_id '
                                'LEFT JOIN main_new.m_org_attributes AS a ',
                                'ON p.balance_org_id = a.org_id ',
                                'LEFT JOIN main_new.m_service AS s ',
                                'ON p.service_id = s.id ',
								'GROUP BY p.balance_org_id, p.service_id;'/*,
                                'UNION ALL ',
								'SELECT zc.balance_org_name, ',
                                '		zc.service_name, ',
                                '		zc.acceptor_org_name, ',                                
                                '		zc.start_value, ',
                                '		zc.transfer_value, ',
                                '		zc.transfer_value AS total_commission, ',
										GROUP_CONCAT(DISTINCT 'ROUND((CASE zc.type_id WHEN ', commission_type_id, ' THEN zc.transfer_value ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'` FROM zhek_commissions AS zc ',
                                'ORDER BY balance_org_name, service_name;'*/
							)
					FROM main_new.report_commissions_types);	
		
	PREPARE stmt FROM @sql;    
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    #SELECT @sql;

	DROP TABLE IF EXISTS payments, commissions, /*zhek_commissions,*/ total_commissions;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-23 12:58:56
