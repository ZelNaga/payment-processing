CREATE DATABASE  IF NOT EXISTS `main_new` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `main_new`;
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: 127.0.0.1    Database: main_new
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `report_commissions_types`
--

DROP TABLE IF EXISTS `report_commissions_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_commissions_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commission_type_id` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_commissions_types`
--

LOCK TABLES `report_commissions_types` WRITE;
/*!40000 ALTER TABLE `report_commissions_types` DISABLE KEYS */;
INSERT INTO `report_commissions_types` VALUES (1,1),(2,2),(3,4);
/*!40000 ALTER TABLE `report_commissions_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'main_new'
--

--
-- Dumping routines for database 'main_new'
--
/*!50003 DROP FUNCTION IF EXISTS `api_key_is_valid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `api_key_is_valid`(
    p_key_id INT(11) UNSIGNED
) RETURNS tinyint(1)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Проверяет, является ли активным ключ'
BEGIN
    DECLARE v_existing_key TINYINT(1) UNSIGNED;

    SET v_existing_key = (
        SELECT
                m_api_key.id
        FROM m_api_key
        WHERE
                m_api_key.id = p_key_id
            AND m_api_key.expired_on > NOW()
    );

    IF v_existing_key IS NULL THEN
        RETURN 0;
    ELSE
        RETURN 1;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `create_home_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `create_home_id`(`p_district_id` int(11) unsigned, `p_town_id` int(11) unsigned, `p_street_id` int(11) unsigned, `p_house_no` varchar(16), `p_flat_no` varchar(16), `p_gen_area` decimal(10,2), `p_heat_area` decimal(10,2), `p_private_id` int(11), `p_rooms` int(11), `p_in_date` date, `p_out_period` tinyint, `p_cancelled` tinyint, `p_user_id` int(11) unsigned) RETURNS int(11) unsigned
BEGIN
	DECLARE EXID INT DEFAULT 0;
	DECLARE NEWHOUSE INT DEFAULT 0;
	DECLARE MAXID INT DEFAULT 0;
	DECLARE COUNT_RECORDS INT DEFAULT 0;
	DECLARE OLD_CODE VARCHAR(3);

	select id, count(*) INTO NEWHOUSE,COUNT_RECORDS FROM m_house WHERE district_id = p_district_id and p_town_id = town_id and p_street_id = street_id and p_house_no = house_no GROUP BY id;
	IF COUNT_RECORDS = 0 THEN 
		 SELECT `create_house_id`(p_town_id,p_district_id,p_street_id,p_house_no,p_in_date) INTO NEWHOUSE FROM DUAL;
	END IF;
	SET COUNT_RECORDS = 0;
	select home_id, count(*) INTO EXID,COUNT_RECORDS FROM m_home WHERE house_id = NEWHOUSE and p_flat_no = flat_no GROUP BY home_id;

	IF COUNT_RECORDS > 0 THEN 
		IF EXID IS NULL THEN 
			RETURN 0;
		END IF;
		RETURN 0;
	END IF;


	INSERT INTO `m_home` (`home_id`, `house_id`, `flat_no`, `gen_area`, `heat_area`, `private_id`, `rooms`, `in_date`, `out_date`, `out_period`, `cancelled`, `dt_creation`, `user_id`)
	      VALUES (get_newhome(), NEWHOUSE, p_flat_no, p_gen_area, p_heat_area, p_private_id, p_rooms, p_in_date, '0000-00-00', p_out_period, p_cancelled, NOW(), p_user_id); 
	
	
	RETURN MAXID;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `create_house_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` FUNCTION `create_house_id`(`p_district_id` int(11) unsigned, `p_town_id` int(11) unsigned, `p_street_id` int(11) unsigned, `p_house_no` varchar(16), `p_in_date` date) RETURNS int(11) unsigned
BEGIN
	DECLARE EXID INT DEFAULT 0;
	DECLARE MAXID INT DEFAULT 0;
	DECLARE COUNT_RECORDS INT DEFAULT 0;
	DECLARE OLD_CODE VARCHAR(3);

	select id, count(*) INTO EXID,COUNT_RECORDS FROM m_house WHERE district_id = p_district_id and p_town_id = town_id and p_street_id = street_id and p_house_no = house_no GROUP BY id;

	IF COUNT_RECORDS > 0 THEN 
		RETURN 0;
	END IF;

	select old_street INTO OLD_CODE FROM m_house WHERE district_id = p_district_id and p_town_id = town_id and p_street_id = street_id GROUP BY old_street;
	select MAX(id)+1 INTO MAXID FROM m_house;

	INSERT INTO `m_house` (`id`, `street_id`, `town_id`, `district_id`, `house_no`, `in_date`, `out_date`, `dt_creation`, `cancelled`, `old_street`)
		VALUES (MAXID, p_street_id, p_town_id, p_district_id, p_house_no, p_in_date, '0000-00-00', NOW(), '0', OLD_CODE); 
	
	RETURN MAXID;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `create_rand_home_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `create_rand_home_id`(
`p_house_id` INT( 11 ) UNSIGNED,
`p_flat_no` VARCHAR( 16 )
) RETURNS int(11) unsigned
    READS SQL DATA
    DETERMINISTIC
BEGIN DECLARE NEWHOMEID INT DEFAULT 0;

SELECT id
INTO NEWHOMEID
FROM m_home_id
ORDER BY RAND( ) 
LIMIT 1 ;

INSERT INTO  `m_home` (  `id` ,  `house_id` ,  `flat_no` ) 
VALUES (
NEWHOMEID, p_house_id, p_flat_no
);

DELETE FROM m_home_id WHERE id = NEWHOMEID;

RETURN NEWHOMEID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FIRST_DAY` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FIRST_DAY`(
    p_date DATE
) RETURNS date
    NO SQL
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Возвращает первый день месяца'
BEGIN
    RETURN DATE_ADD(
        LAST_DAY(
            DATE_SUB(
                LAST_DAY(p_date), INTERVAL 1 MONTH
            )
        ), INTERVAL 1 DAY
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_balance_org_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_balance_org_id`(`p_org_id` int) RETURNS int(11)
    COMMENT 'get balance organization id'
BEGIN
	DECLARE v_org_id INT DEFAULT 0;
	DECLARE v_parent_id INT DEFAULT p_org_id;
	DECLARE v_is_balance BOOLEAN DEFAULT false;

	WHILE (v_is_balance = false AND v_org_id != v_parent_id) DO
		SET v_is_balance = true;
		SELECT org.isBalance, org.id, org.parent_id 
        INTO v_is_balance, v_org_id, v_parent_id
		FROM main_new.m_organization AS org
		WHERE org.id = v_parent_id;
	END WHILE;

RETURN v_org_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_home_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_home_id`(
    p_zhek_code DECIMAL(2, 0) UNSIGNED,
    p_account DECIMAL(10, 0) UNSIGNED,
    p_dt DATE
) RETURNS int(11) unsigned
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Возвращает home_id по указанному коду ЖЭК и учётной записи'
BEGIN
    SET @home_id = (
        SELECT
                m_account.home_id
        FROM m_account
        WHERE
                m_account.zhek_code = p_zhek_code
            AND m_account.account = p_account
        LIMIT 1
    );

    RETURN @home_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_home_owner_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_home_owner_id`(`p_home_id` int) RETURNS int(11)
BEGIN
	DECLARE v_person_id INT;

	select own.person_id into v_person_id
		from main_new.m_home_owner own
			inner join main_new.m_history h on h.id = own.history_id
		where own.home_id = p_home_id and h.cancelled = false 
		limit 1;

RETURN v_person_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_house_Id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_house_Id`(`p_zhek` int, `p_account` int) RETURNS int(11)
BEGIN
	DECLARE r_house_id INT;
	select
		h.house_id INTO r_house_id
	from m_account a, 
	     m_home    h	     
	where
		zhek_code = p_zhek and 
		account = p_account and
		a.home_id = h.id
	LIMIT 1;
	RETURN r_house_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_main_org_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_main_org_id`(`p_org_id` int) RETURNS int(11)
BEGIN
	DECLARE v_org_id INT;
	DECLARE v_is_main boolean default false;
	DECLARE v_is_realy_org boolean default false;

	select (count(*) > 0) into v_is_realy_org from main_new.m_organization where id = p_org_id;
	if (v_is_realy_org = false) then
		call throw_error(concat("Organization with id = ", p_org_id, " is not found."));
	END if; 

	set	v_org_id = p_org_id;

	while (v_is_main = false) do
		select (org.id = org.parent_id), org.parent_id into v_is_main, v_org_id
			from main_new.m_organization org
		where org.id = v_org_id;
	END while;

RETURN v_org_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_newhome` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_newhome`() RETURNS int(10) unsigned
BEGIN
	DECLARE INEW INT;
	DECLARE SOMENEW INT;
	DECLARE COUNT_RECORDS INT;
	
	found_new_home: LOOP
		SELECT CAST(AVG(ID) AS UNSIGNED INTEGER) INTO INEW FROM m_home;
		SELECT ((INEW*1664525 +1013904223) % 4294967296)>>12 INTO SOMENEW FROM DUAL;
		SELECT count(*) INTO COUNT_RECORDS FROM m_home WHERE id = SOMENEW;
		IF (COUNT_RECORDS = 0) THEN 
			 LEAVE found_new_home;
		END IF;
	END LOOP found_new_home;
	RETURN SOMENEW;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_zhek_main_org_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_zhek_main_org_id`(`p_zhek` int) RETURNS int(11)
BEGIN
	DECLARE v_org_id INT;

	select max(org_id) into v_org_id 
		from main_new.m_org_cmsu 
	where zhek = p_zhek;

	if (v_org_id is not null) then
		set v_org_id = get_main_org_id(v_org_id);
	END if;

RETURN v_org_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_zhek_main_org_id_by_home_and_account` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_zhek_main_org_id_by_home_and_account`(`p_zhek` int, `p_home_id` int, `p_account` int) RETURNS int(11)
BEGIN
	DECLARE v_org_id INT;
	DECLARE v_one_zhek_org boolean default false;

	SELECT SQL_CALC_FOUND_ROWS org_id into v_org_id FROM main_new.m_org_cmsu where zhek = p_zhek limit 1;
	SELECT FOUND_ROWS() = 1 into v_one_zhek_org;
	if (v_one_zhek_org = false) then
		select supp.supplier_org_id into v_org_id from main_new.m_supplier supp
			inner join main_new.m_account acc on acc.id = supp.account_id
			inner join main_new.m_history as supp_h on supp_h.id = supp.history_id and supp_h.cancelled = false
		where acc.home_id = p_home_id and acc.account = p_account and acc.zhek_code = p_zhek and supp.service_id = 1;
	END if;

	if (v_org_id is not null) then
		set v_org_id = get_main_org_id(v_org_id);
	END if;

RETURN v_org_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_zhek_org_id_by_home_and_account` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_zhek_org_id_by_home_and_account`(`p_zhek` int, `p_home_id` int, `p_account` int) RETURNS int(11)
BEGIN
	DECLARE v_org_id INT;
	DECLARE v_one_zhek_org boolean default false;

	select supp.supplier_org_id into v_org_id from main_new.m_supplier supp
		inner join main_new.m_account acc on acc.id = supp.account_id
		inner join main_new.m_history as supp_h on supp_h.id = supp.history_id and supp_h.cancelled = false
	where acc.home_id = p_home_id and acc.account = p_account and acc.zhek_code = p_zhek and supp.service_id = 1;

	if (v_org_id is null) then
		SELECT SQL_CALC_FOUND_ROWS org_id into v_org_id FROM main_new.m_org_cmsu where zhek = p_zhek limit 1;
		SELECT FOUND_ROWS() = 1 into v_one_zhek_org;

		if (v_org_id is not null and v_one_zhek_org = false) then
			set v_org_id = get_main_org_id(v_org_id);
		END if;
	END if;

RETURN v_org_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_parent_or_this` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `is_parent_or_this`(`p_sub_ogr_id` int, `p_check_org_id` int) RETURNS tinyint(1)
BEGIN
	DECLARE v_org_id INT;
	DECLARE v_is_parent boolean default false;
	DECLARE v_is_main boolean default false;

	set	v_org_id = p_sub_ogr_id;

	while (v_is_main = false and v_is_parent = false) do
		select (org.id = org.parent_id), (org.id = p_check_org_id),  org.parent_id into v_is_main, v_is_parent, v_org_id
			from main_new.m_organization org
		where org.id = v_org_id;
	END while;

RETURN v_is_parent;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `KOATUU_CLASS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `KOATUU_CLASS`(`p_id` char(10)) RETURNS char(1) CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Возвращает тип (С, Щ, Т, М)'
BEGIN
    DECLARE v_class CHAR(1) CHARACTER SET utf8;
    
    IF p_id = '' THEN
        RETURN '';
    END IF;

    SELECT
            m_koatuu.class INTO v_class
    FROM m_koatuu
    WHERE
            m_koatuu.id = p_id
    LIMIT 1;
    
    RETURN v_class;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `KOATUU_LOCAL_AUTH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `KOATUU_LOCAL_AUTH`(`p_id` char(10), `p_name` varchar(128)) RETURNS varchar(128) CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Возвращает название местной рады'
BEGIN
    DECLARE v_name VARCHAR(128) CHARACTER SET utf8;
    DECLARE v_count INT;
    
    IF p_id = '' THEN
        RETURN '';
    END IF;
    
    SET v_name = '';

    SELECT COUNT(*) INTO v_count FROM m_koatuu WHERE m_koatuu.id LIKE CONCAT(LEFT(p_id, 5), '_____') AND m_koatuu.name = p_name LIMIT 1;

    IF count > 1 THEN
        SELECT m_koatuu.name INTO v_name FROM m_koatuu WHERE m_koatuu.id = CONCAT(LEFT(id, 8), '00') LIMIT 1;
    END IF;

    RETURN name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `KOATUU_NAME` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `KOATUU_NAME`(`p_id` char(10), `level` tinyint(1)) RETURNS varchar(255) CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Возвращает название административной единицы согласно КОАТУУ'
BEGIN
    DECLARE v_name VARCHAR(255) CHARACTER SET utf8;
    DECLARE suffix VARCHAR(10);
    DECLARE strip TINYINT(1);
    
    IF p_id = '' THEN
        RETURN '';
    END IF;

    CASE level
        WHEN 1 THEN
            SET strip = 2;
            SET suffix = '00000000';
        WHEN 2 THEN
            SET strip = 5;
            SET suffix = '00000';
        WHEN 3 THEN
            SET strip = 8;
            SET suffix = '00';
        ELSE
            SET strip = 10;
            SET suffix = '';
    END CASE;

    SELECT
            m_koatuu.name INTO v_name
    FROM m_koatuu
    WHERE
            m_koatuu.id = CONCAT(LEFT(id, strip), suffix)
    LIMIT 1;
    
    RETURN v_name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `tariff` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `tariff`(`p_zhek` int, `p_account` int, `p_service_id` int, `p_dt` date) RETURNS decimal(10,4)
BEGIN
    DECLARE v_value DECIMAL(10, 4);
	DECLARE v_count INT;

	IF (p_service_id != 19 and p_service_id != 24) THEN
		select t.interest into v_value
		from m_zhek_interest t, m_history h 
		where  t.history_id = h.id and p_dt between h.in_date and h.out_date and h.cancelled = 0 and 
				addservice_id = p_service_id and t.zhek_code = p_zhek;
	END IF;

	if (p_service_id = 24) THEN

		SELECT count(tbl_tarkv_home.value) INTO v_count
		FROM m_tarkv_home tbl_tarkv_home
		INNER JOIN m_account ON
				m_account.zhek_code = p_zhek
			AND m_account.account = p_account
			AND m_account.home_id = tbl_tarkv_home.home_id
		WHERE
				tbl_tarkv_home.dt <= p_dt
			AND tbl_tarkv_home.addservice_id = p_service_id
			AND tbl_tarkv_home.zhek = p_zhek
		ORDER BY dt DESC
		LIMIT 1;

		IF (v_count > 0) THEN
			select t.interest into v_value
			from m_zhek_interest t, m_history h 
			where  t.history_id = h.id and p_dt between h.in_date and h.out_date and h.cancelled = 0 and 
					addservice_id = p_service_id and t.zhek_code = p_zhek;
		END IF;

	END IF;

	if ((v_value is null or v_value = 0) and (p_service_id != 24 or v_count > 0)) then
		SELECT tbl_tarkv_home.value INTO v_value
		FROM m_tarkv_home tbl_tarkv_home
		INNER JOIN m_account ON
				m_account.zhek_code = p_zhek
			AND m_account.account = p_account
			AND m_account.home_id = tbl_tarkv_home.home_id
		WHERE
				tbl_tarkv_home.dt <= p_dt
			AND tbl_tarkv_home.addservice_id = p_service_id
			AND tbl_tarkv_home.zhek = p_zhek
		ORDER BY dt DESC
		LIMIT 1;


		IF ((p_service_id != 19 and p_service_id != 24) and (v_value is null or v_value = 0)) THEN
			SELECT 	tbl_tarkv_house.value INTO v_value
			FROM m_tarkv_house tbl_tarkv_house
			INNER JOIN m_home ON
					m_home.house_id = tbl_tarkv_house.house_id
			INNER JOIN m_account ON
					m_account.zhek_code = p_zhek
				AND m_account.account = p_account
				AND m_account.home_id = m_home.id
			WHERE
					tbl_tarkv_house.dt <= p_dt
				AND tbl_tarkv_house.addservice_id = p_service_id
				AND tbl_tarkv_house.zhek = p_zhek
			ORDER BY dt DESC
			LIMIT 1;
			
		END IF;
	END IF;

    IF v_value < 0 THEN
        SET v_value = NULL;
    END IF;

    RETURN v_value;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `tariff_acceptor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `tariff_acceptor`(`p_zhek` int, `p_account` int, `p_addservice` int, `p_dt` date) RETURNS int(11) unsigned
BEGIN
    DECLARE v_acceptor INT DEFAULT 0;
    DECLARE v_house_id   INT;
    DECLARE v_acceptor_count_in_addservice_supplier INT;

	select
		house_id INTO v_house_id
	from m_account a, 
	     m_home    h
	where
		zhek_code = p_zhek
		and account = p_account and 
		a.home_id = h.id
	LIMIT 1;


    select 
		SQL_CALC_FOUND_ROWS 
		acceptor_org_id INTO v_acceptor
    from 
		m_addservice_supplier s, 
		m_history h 
    where 
		s.history_id = h.id and 
		h.cancelled = 0 and
		p_dt between h.in_date and h.out_date and
		s.house_id = v_house_id and
		s.addservice_id = p_addservice and 
		p_zhek = zhek_code
		limit 1;

  IF (v_acceptor <= 0) THEN
    select 
		acceptor_org_id INTO v_acceptor
	from m_zhek_interest t, 
        m_history h 
	where  
		t.history_id = h.id and  
		p_dt between h.in_date and h.out_date and h.cancelled = 0 and 
		addservice_id = p_addservice and 
		t.zhek_code = p_zhek 
		limit 1;
    END IF;


    RETURN v_acceptor;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `tariff_acceptor_adminer_52d8f532a9e35` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `tariff_acceptor_adminer_52d8f532a9e35`(`p_zhek` int, `p_account` int, `p_addservice` int, `p_dt` date) RETURNS int(11) unsigned
BEGIN
    DECLARE v_acceptor INT DEFAULT 0;
    DECLARE v_house_id   INT;
    DECLARE v_acceptor_count_in_addservice_supplier INT;

	select
		house_id INTO v_house_id
	from m_account a, 
	     m_home    h
	where
		zhek_code = p_zhek
		and account = p_account and 
		a.home_id = h.id
	LIMIT 1;


    select 
	SQL_CALC_FOUND_ROWS 
	acceptor_org_id INTO v_acceptor_count_in_addservice_supplier
    from 
	m_addservice_supplier s, 
	m_history ash 
    where 
	s.history_id = ash.id and 
	ash.cancelled = 0 and
	p_dt between ash.in_date and ash.out_date and
	s.house_id = v_house_id and
	s.addservice_id = p_addservice and 
	p_zhek = zhek_code
    limit 1;

   
 IF (v_acceptor_count_in_addservice_supplier IS NOT NULL) THEN 
	select 
		acceptor_org_id INTO v_acceptor
	from 
		m_addservice_supplier s, 
		m_history ash 
	where 
		s.history_id = ash.id and 
		ash.cancelled = 0 and
		p_dt between ash.in_date and ash.out_date and
		s.house_id = v_house_id and
		s.addservice_id = p_addservice and 
		p_zhek = zhek_code
	    limit 1;
	RETURN  v_acceptor;
	END IF;


    select 
	acceptor_org_id INTO v_acceptor
   from m_zhek_interest t, 
        m_history h 
   where  
	t.history_id = h.id and  
	p_dt between h.in_date and h.out_date and h.cancelled = 0 and 
	addservice_id = p_addservice and 
	t.zhek_code = p_zhek 
    limit 1;
    RETURN v_acceptor;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `tariff_total` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `tariff_total`(`p_zhek` int, `p_account` int, `p_dt` date) RETURNS decimal(10,4)
BEGIN
    DECLARE v_total DECIMAL(10, 4);

    SELECT SQL_CALC_FOUND_ROWS
            m_tarkv_home_total.total
        INTO
            v_total
    FROM m_tarkv_home_total
    INNER JOIN m_account ON
            m_account.zhek_code = p_zhek
        AND m_account.account = p_account
        AND m_account.home_id = m_tarkv_home_total.home_id
    WHERE
            m_tarkv_home_total.dt <= p_dt
        AND m_tarkv_home_total.zhek = p_zhek
    ORDER BY dt DESC
    LIMIT 1;

    
    
    

    IF v_total IS NULL THEN
        SELECT SQL_CALC_FOUND_ROWS
                m_tarkv_house_total.total
            INTO
                v_total
        FROM m_tarkv_house_total
        INNER JOIN m_home ON
                m_home.house_id = m_tarkv_house_total.house_id
        INNER JOIN m_account ON
                m_account.zhek_code = p_zhek
            AND m_account.account = p_account
            AND m_account.home_id = m_home.id
        WHERE
                m_tarkv_house_total.dt <= p_dt
            AND m_tarkv_house_total.zhek = p_zhek
        ORDER BY dt DESC
        LIMIT 1;
        
        
        
    END IF;

    IF v_total < 0 THEN
        SET v_total = NULL;
    ELSE
        SET @sumToAdd = (SELECT
                SUM(value) * -1
        FROM m_tarkv_home
        INNER JOIN m_account ON
                m_account.zhek_code = p_zhek
            AND m_account.account = p_account
            AND m_account.home_id = m_tarkv_home.home_id
        WHERE
                m_tarkv_home.value < 0
            AND m_tarkv_home.dt <= p_dt
        GROUP BY m_tarkv_home.home_id, m_tarkv_home.dt
        ORDER BY m_tarkv_home.dt DESC
        LIMIT 1);
        IF @sumToAdd IS NULL THEN
            SET @sumToAdd = (SELECT
                    SUM(value) * -1
            FROM m_tarkv_house
            INNER JOIN m_home ON
                    m_home.house_id = m_tarkv_house.house_id
            INNER JOIN m_account ON
                    m_account.zhek_code = p_zhek
                AND m_account.account = p_account
                AND m_account.home_id = m_home.id
            WHERE
                    m_tarkv_house.value < 0
                AND m_tarkv_house.dt <= p_dt
            GROUP BY m_tarkv_house.house_id, m_tarkv_house.dt
            ORDER BY m_tarkv_house.dt DESC
            LIMIT 1);
        END IF;

        If @sumToAdd IS NOT NULL THEN
            SET v_total = v_total + @sumToAdd;
        END IF;
    END IF;

    RETURN v_total;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `account_update_zhek` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `account_update_zhek`(
    IN p_house_id INT(11) UNSIGNED
  , IN p_zhek_code DECIMAL(2,0) UNSIGNED
  , IN p_org_id INT(11) UNSIGNED
  , IN p_zhek_code_old DECIMAL(2,0) UNSIGNED
  , IN p_org_id_old INT(11) UNSIGNED
  , IN p_with_duties TINYINT(1) UNSIGNED
  , IN p_in_date DATE
  , IN p_user_id INT(11) UNSIGNED
  , IN p_user_app VARCHAR(255) CHARACTER SET 'utf8'
  , IN p_user_ip VARCHAR(255) CHARACTER SET 'utf8'
)
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Обновляет (не устанавливает!) атрибуты за указанный период'
BEGIN 
    DECLARE v_zhek DECIMAL(2, 0) UNSIGNED;
    DECLARE v_account DECIMAL(10, 0) UNSIGNED;
    DECLARE v_home_id INT(11) UNSIGNED; 
    DECLARE v_service_id SMALLINT(5) UNSIGNED;
    DECLARE v_supplier_org_id INT(11) UNSIGNED;
    DECLARE v_acceptor_org_id INT(11) UNSIGNED;

    DECLARE v_done TINYINT(1) UNSIGNED;
    
    DECLARE v_supplier_cur CURSOR FOR SELECT
            m_account.zhek_code
        ,   m_account.account
        ,   m_account.home_id
        ,   m_supplier.service_id
        ,   m_supplier.supplier_org_id
        ,   m_supplier.acceptor_org_id
    FROM m_supplier
    INNER JOIN m_account ON
            m_account.id = m_supplier.account_id
        AND m_account.zhek_code = p_zhek_code_old
    INNER JOIN m_home ON
            m_home.id = m_account.home_id
        AND m_home.house_id = p_house_id;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
    SET @tx_id = LAST_INSERT_ID();

    OPEN v_supplier_cur;
    supplier_cur_loop: LOOP
        FETCH v_supplier_cur INTO v_zhek, v_account, v_home_id, v_service_id, v_supplier_org_id, v_acceptor_org_id;
        IF v_done = 1 THEN
            LEAVE supplier_cur_loop;
        END IF;

        IF p_with_duties = 1 THEN
            CALL supplier_unsafe_delete(v_zhek, v_account, v_service_id, DATE_SUB(p_in_date, INTERVAL 1 DAY), @tx_id);
        END IF;

        IF v_account < 10000 THEN
            SET v_account = (p_zhek_code * 10000) + v_account;
        END IF;

        SET @found = (SELECT id FROM m_account WHERE zhek_code = p_zhek_code AND account = v_account AND home_id = v_home_id LIMIT 1);
        IF @found IS NULL THEN
                INSERT INTO m_account(zhek_code, account, home_id) VALUE(p_zhek_code, v_account, v_home_id);
        END IF;
            
        SET @supplier_org_id = v_supplier_org_id;
        IF @supplier_org_id = p_org_id_old THEN
            SET @supplier_org_id = p_org_id;
        END IF;

        SET @acceptor_org_id = v_acceptor_org_id;
        IF @acceptor_org_id = p_org_id_old THEN
            SET @acceptor_org_id = p_org_id;
        END IF;

        SET foreign_key_checks = 0;
            CALL supplier_unsafe_add(p_zhek_code, v_account, v_service_id, @supplier_org_id, @acceptor_org_id, p_in_date, @tx_id);
        SET foreign_key_checks = 1;
    END LOOP supplier_cur_loop;
    CLOSE v_supplier_cur;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_ssbor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_ssbor`(IN `p_org_id` int(11) unsigned, IN `:bank_id` int(11) unsigned, IN `:acceptor_id` int(11) unsigned, IN `:dt` date, IN `:summa` decimal(10,2), IN `:letter` varchar(520), IN `:limit_percent` decimal(10,2))
BEGIN 
	DECLARE A INT; 
	DECLARE BORG DECIMAL(10,2) DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
		SELECT 'Невозможно выполнить запрос';			
	END;


	START TRANSACTION;
		insert INTO m_org_findoc (org_id,bank_id,acceptor_id,summa,Letter,dt) VALUES (p_org_id,`:bank_id`,`:acceptor_id`,`:summa`,`:letter`,`:dt`);
		select count(*) into A from m_org_balance where bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;
		IF (A=0) THEN 
			INSERT INTO m_org_balance (org_id, bank_id, acceptor_id, limit_percent, summa, dt) VALUES (p_org_id,`:bank_id`,`:acceptor_id`,`:limit_percent`,`:summa`,`:dt`);
		ELSE 
			select summa into BORG from m_org_balance where bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;
			IF (BORG=0) THEN 
				UPDATE m_org_balance SET summa = `:summa`, limit_percent = `:limit_percent`, dt = `:dt` WHERE bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;
			ELSE 
				UPDATE m_org_balance SET summa = summa + `:summa`, limit_percent = `:limit_percent` WHERE bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;
			END IF;
		END IF;
	COMMIT; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `api_key_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_key_create`(
    IN p_org_id INT(11) UNSIGNED
  , IN p_app_name VARCHAR(128) CHARACTER SET 'utf8'
  , IN p_description VARCHAR(512) CHARACTER SET 'utf8'
  , IN p_is_internal TINYINT(1) UNSIGNED
  , OUT p_key_id INT(11) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Создаёт новый ключ для указанной организации'
BEGIN
    SET @min_value = 1901502003;
    SET @max_value = 4290463291;

    generate_key: LOOP
        SET p_key_id = FLOOR(@min_value - RAND() * (@max_value - @min_value));

        SET @exists = (
            SELECT
                    m_api_key.id
            FROM m_api_key
            WHERE
                    m_api_key.id = p_key_id
            LIMIT 1
        );

        IF @exists IS NULL THEN
            LEAVE generate_key;
        END IF;
    END LOOP generate_key;

    INSERT INTO m_api_key(
        id
      , org_id
      , app_name
      , description
      , is_internal
      , created_on
    ) VALUES (
        p_key_id
      , p_org_id
      , p_app_name
      , p_description
      , p_is_internal
      , NOW()
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buildpart_meter_outdate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buildpart_meter_outdate`(
    IN  p_meter_id INT(11) UNSIGNED
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Задаёт out_date для активного отношения'
BEGIN
    DECLARE v_buildpart_no VARCHAR(255) CHARACTER SET 'utf8';
    DECLARE v_history_id INT(11) UNSIGNED;
    DECLARE v_in_date DATE;

    SELECT
            vmte_buildpart_meter.buildpart_no
          , m_history.id
          , m_history.in_date
        INTO
            v_buildpart_no
          , v_in_date
          , v_history_id
    FROM
            vmte_buildpart_meter
    INNER JOIN m_history ON
            m_history.id = vmte_buildpart_meter.history_id
        AND m_history.cancelled = 0
        AND m_history.out_date = '9999-12-31'
    WHERE
            vmte_buildpart_meter.meter_id = p_meter_id
    LIMIT 1;

    IF v_history_id IS NULL THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'buildpart_meter_outdate: no active meter were found';
    END IF;

    CALL history_cancel(v_history_id, p_tx_id);

    CALL history_create(v_in_date, p_out_date, p_tx_id, @history_id);

    INSERT INTO vmte_buildpart_meter(
        buildpart_no
      , meter_id
      , history_id
    ) VALUE (
        v_buildpart_no
      , p_meter_id
      , @history_id
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buildpart_meter_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buildpart_meter_set`(
    IN  p_buildpart_no VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_meter_id INT(11) UNSIGNED
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE v_buildpart_exists TINYINT(1) UNSIGNED;

    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_buildpart_meter
    INNER JOIN m_history ON
            m_history.id = m_buildpart_meter.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_buildpart_meter.buildpart_no = p_buildpart_no
        AND m_buildpart_meter.meter_id = p_meter_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SET v_buildpart_exists = (
        SELECT
                IF(m_history.id IS NULL, 0, 1)
        FROM vmte_buildpart_home
        INNER JOIN m_history ON
                m_history.id = vmte_buildpart_home.history_id
            AND m_history.cancelled = 0
        LIMIT 1
    );

    IF NOT v_buildpart_exists THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'buildpart_meter_set: no buildpart were found';
    END IF;
    

    
    SELECT
            m_history.id
          , m_history.in_date
        INTO
            bef_history_id
          , bef_in_date
    FROM m_buildpart_meter
    INNER JOIN m_history ON
            m_history.id = m_buildpart_meter.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            m_buildpart_meter.buildpart_no = p_buildpart_no
        AND m_buildpart_meter.meter_id = p_meter_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_buildpart_meter (
            buildpart_no
          , meter_id
          , history_id
        ) VALUE (
            p_buildpart_no
          , p_meter_id
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params (
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    INSERT INTO m_buildpart_meter (
        buildpart_no
      , meter_id
      , history_id
    ) VALUE (
        p_buildpart_no
      , p_meter_id
      , @history_id
    );
    

    
    SELECT
            m_history.id
          , m_history.out_date
        INTO
            aft_history_id
          , aft_out_date
    FROM m_buildpart_meter
    INNER JOIN m_history ON
            m_history.id = m_buildpart_meter.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            m_buildpart_meter.buildpart_no = p_buildpart_no
        AND m_buildpart_meter.meter_id = p_meter_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );
        INSERT INTO m_buildpart_meter (
            buildpart_no
          , meter_id
          , history_id
        ) VALUE (
            p_buildpart_no
          , p_meter_id
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `change_account` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `change_account`(IN `p_zhek` smallint(5) unsigned, IN `p_account` int(11) unsigned, IN `p_service_id` smallint(5), IN `p_supplier_id` smallint(5) unsigned, IN `p_acceptor_id` smallint(5) unsigned, IN `p_dt` date)
BEGIN
    DECLARE count_records tinyint(1);
	SELECT count(*) INTO count_records 
			FROM m_supplier_new WHERE zhek = p_zhek and account = p_account and service_id = p_service_id and out_date = '0000-00-00';
	START TRANSACTION;
     IF (count_records <> 0) THEN
        UPDATE m_supplier_new SET out_date = (p_dt - INTERVAL 1 day) WHERE zhek = p_zhek and account = p_account and service_id = p_service_id and out_date = '0000-00-00';				
    END IF;       
	INSERT INTO `m_supplier_new` (`home_id`, `zhek`, `account`, `service_id`, `supplier_id`, `acceptor_id`, `in_date`, `out_date`, `cancelled`) 
		VALUES (get_home_id(p_zhek,p_account,p_dt),p_zhek,p_account,p_service_id,p_supplier_id,p_acceptor_id, p_dt, '0000-00-00',0);
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `comission_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `comission_add`(
    IN p_bank_org_id INT,
    IN p_supplier_org_id INT,
    IN p_comission_type_id SMALLINT(5),
    IN p_service_id INT,
    IN p_acceptor_org_id INT,
    IN p_reason_id INT,
    IN p_minimum DECIMAL(10, 2),
    IN p_percent DECIMAL(10, 2),
    IN p_maximum DECIMAL(10, 2),
    IN p_is_month TINYINT(1),
    IN p_in_date DATE,
    IN p_out_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    COMMENT 'Устанавливает атрибуты поставщика для указанного периода'
BEGIN
    DECLARE v_found_id INT;

    START TRANSACTION;
        SELECT
                id
            INTO
                v_found_id
        FROM m_comission
        INNER JOIN m_history ON
                m_history.id = m_comission.history_id
            AND m_history.out_date > p_in_date
        WHERE
                m_comission.bank_org_id = p_bank_org_id
            AND m_comission.supplier_org_id = p_supplier_org_id
            AND m_comission.service_id = p_service_id
            
            
            
            AND m_comission.comission_type_id = p_comission_type_id
            AND m_comission.comission_acceptor_org_id = p_acceptor_org_id
        LIMIT 1;

        IF v_found_id IS NOT NULL THEN
            SET @errmsg = CONCAT("Record attributes {bank_org_id=", p_bank_org_id
                , ", supplier_org_id=", p_supplier_org_id
                , ", comission_type_id=", p_comission_type_id
                , ", comission_acceptor_org_id=", p_acceptor_org_id
                , "} already exists!");
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = @errmsg;
        END IF;

        SET p_out_date = IF(p_out_date IS NULL, '9999-12-31', p_out_date);

        SET foreign_key_checks = 0;

        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

        INSERT INTO m_comission_formulae(
            minimum
          , percent
          , maximum
          , is_month)
        VALUE (
            p_minimum
          , p_percent
          , p_maximum
          , p_is_month
        );
        SET @formula_id = LAST_INSERT_ID();

        INSERT INTO m_comission(
            bank_org_id
          , supplier_org_id
          , comission_type_id
          , service_id
          , comission_acceptor_org_id
          , comission_formula_id
          , comission_reason_id
          , history_id)
        VALUE (
            p_bank_org_id
          , p_supplier_org_id
          , p_comission_type_id
          , p_service_id
          , p_acceptor_org_id
          , @formula_id
          , p_reason_id
          , @history_id
        );
        SET @comission_id = LAST_INSERT_ID();

        SET foreign_key_checks = 1;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `comission_cancel_transaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `comission_cancel_transaction`(
    IN p_tx_id INT,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255),
    IN p_user_ip VARCHAR(50)
)
    MODIFIES SQL DATA
    COMMENT 'Производит отмену указанной транзакции, накатывая последующие изменения'
BEGIN
    DECLARE v_history_id INT;
    DECLARE v_transaction_id INT;

    DECLARE v_bank_org_id INT;
    DECLARE v_supplier_org_id INT;
    DECLARE v_zhek DECIMAL(2, 0);
    DECLARE v_service_id INT;
    DECLARE v_acceptor_org_id INT;
    DECLARE v_reason_id INT;
    DECLARE v_minimum DECIMAL(10, 0);
    DECLARE v_percent DECIMAL(10, 0);
    DECLARE v_maximum DECIMAL(10, 0);
    DECLARE v_is_month TINYINT(1);
    DECLARE v_in_date DATE;
    DECLARE v_out_date DATE;

    DECLARE v_done TINYINT(1);

    DECLARE valid_cur CURSOR FOR SELECT
            m_history.id
          , m_transaction_validate.transaction_id
    FROM m_history
    INNER JOIN m_comission ON
            m_comission.history_id = m_history.id 
    INNER JOIN m_transaction_validate ON
            m_transaction_validate.history_id = m_history.id
        AND m_transaction_validate.transaction_id >= p_tx_id
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_validate.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE rollforward_cur CURSOR FOR SELECT
            m_transaction_params.transaction_id
          , m_comission.bank_org_id
          , m_comission.supplier_org_id
          , m_comission.zhek_code
          , m_comission.service_id
          , m_comission.comission_acceptor_org_id
          , m_comission.comission_reason_id
          , m_comission_formulae.minimum
          , m_comission_formulae.percent
          , m_comission_formulae.maximum
          , m_comission_formulae.is_month
          , m_history.in_date
          , m_history.out_date
    FROM m_comission
    INNER JOIN m_history ON
            m_history.id = m_comission.history_id
    INNER JOIN m_transaction_params ON
            m_transaction_params.history_id = m_history.id
        AND m_transaction_params.transaction_id >= (p_tx_id - 1)
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_params.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    START TRANSACTION;
        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        OPEN valid_cur;
        valid_cur_loop: LOOP
            FETCH valid_cur INTO v_history_id, v_transaction_id;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE valid_cur_loop;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = v_history_id;
            UPDATE m_transaction SET cancelling_transaction = @tx_id WHERE id = v_transaction_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);
        END LOOP valid_cur_loop;
        CLOSE valid_cur;

        OPEN rollforward_cur;
        rollforward_cur_loop: LOOP
            FETCH rollforward_cur INTO v_transaction_id, v_bank_org_id, v_supplier_org_id, v_zhek, v_service_id, v_acceptor_org_id, v_reason_id, v_minimum, v_percent, v_maximum, v_is_month, v_in_date, v_out_date;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE rollforward_cur_loop;
            END IF;
            IF v_transaction_id <> p_tx_id THEN
                CALL comission_set_attributes(v_bank_org_id, v_supplier_org_id, v_zhek, v_service_id, v_acceptor_org_id, v_reason_id, v_minimum, v_percent, v_maximum, v_is_month, v_in_date, v_out_date, p_user_id, p_user_app, p_user_ip);
            END IF;
        END LOOP rollforward_cur_loop;
        CLOSE rollforward_cur;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `comission_close` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `comission_close`(IN `p_k` INT, IN `p_out_date` DATE, IN `p_user_id` INT, IN `p_user_app` VARCHAR(255), IN `p_user_ip` VARCHAR(50))
BEGIN
    DECLARE v_k INT;
    DECLARE v_done TINYINT(1);

    DECLARE c CURSOR FOR SELECT
            k
    FROM m_comission
    INNER JOIN m_history ON 
                	m_history.id = m_comission.history_id 
                        AND m_history.cancelled = 0
    WHERE
            comission_reason_id IN (
                SELECT
                        comission_reason_id
                FROM m_comission
                INNER JOIN m_history ON 
                	m_history.id = m_comission.history_id 
                        AND m_history.cancelled = 0
                WHERE
                        m_comission.k = p_k
            );

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    OPEN c;
    l: LOOP
        FETCH c INTO v_k;
        IF v_done THEN
            LEAVE l;
        END IF;

        CALL comission_delete(v_k, p_out_date, p_user_id, p_user_app, p_user_ip);
    END LOOP l;
    CLOSE c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `comission_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `comission_delete`(
    IN p_k INT,
    IN p_out_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255),
    IN p_user_ip VARCHAR(50)
)
BEGIN
    DECLARE v_history_id INT;
    DECLARE v_in_date DATE;

    START TRANSACTION;
        SELECT
                m_history.id
              , m_history.in_date
            INTO
                v_history_id
              , v_in_date
        FROM m_comission
        INNER JOIN m_history ON
                m_history.id = m_comission.history_id
        WHERE
                m_comission.k = p_k
        LIMIT 1;

        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        UPDATE m_history SET cancelled = 1 WHERE id = v_history_id;
        INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);

        INSERT INTO m_history(in_date, out_date) VALUE(v_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);

        INSERT INTO m_comission_formulae(
            minimum
          , percent
          , maximum
          , is_month)
            SELECT
                m_comission_formulae.minimum
              , m_comission_formulae.percent
              , m_comission_formulae.maximum
              , m_comission_formulae.is_month
            FROM m_comission_formulae
            INNER JOIN m_comission ON
                    m_comission.comission_formula_id = m_comission_formulae.id
                AND m_comission.k = p_k
            LIMIT 1;
        SET @formula_id = LAST_INSERT_ID();

        INSERT INTO m_comission(
            bank_org_id
          , supplier_org_id
          , service_id
          , comission_type_id
          , comission_acceptor_org_id
          , comission_formula_id
          , comission_reason_id
          , history_id)
            SELECT
                bank_org_id
              , supplier_org_id
              , service_id
              , comission_type_id
              , comission_acceptor_org_id
              , @formula_id
              , comission_reason_id
              , @history_id
            FROM m_comission
            WHERE
                    k = p_k
            LIMIT 1;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `comission_formulae_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `comission_formulae_add`(
	IN p_minimum DECIMAL(10, 2),
	IN p_percent DECIMAL(10, 2),
	IN p_maximum DECIMAL(10, 2),
	IN p_is_month TINYINT(1),
	OUT p_id INT
)
BEGIN
	INSERT INTO m_comission_formulae(minimum, percent, maximum, is_month) VALUE(p_minimum, p_percent, p_maximum, p_is_month);
	SET p_id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `comission_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `comission_insert`(
	IN p_bank_org_id INT,
	IN p_supplier_org_id INT,
	IN p_comission_type_id INT,
	IN p_service_id INT,
	IN p_comission_acceptor_org_id INT,
	IN p_comission_formulae_id INT,
	IN p_comission_reason_id INT,
	IN p_in_date DATE,
	IN p_out_date DATE,
	IN p_tx_id INT
)
BEGIN
	INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
	SET @history_id = LAST_INSERT_ID();
	INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(p_tx_id, @history_id);
	INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(p_tx_id, @history_id);

	INSERT INTO m_comission(bank_org_id, supplier_org_id, comission_type_id, service_id, comission_acceptor_org_id, comission_formula_id, history_id) VALUE(p_bank_org_id, p_supplier_org_id, p_comission_type_id, p_service_id, p_comission_acceptor_org_id, p_comission_formulae_id, p_comission_reason_id, @history_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_meter_by_normative` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_meter_by_normative`(
	IN p_meter_id INT(11),
    IN p_date_begin DATE,
	IN p_date_end DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    COMMENT 'Створює запис про переведення лічильника на норматив'
BEGIN

    START TRANSACTION;
		CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

		insert into main_new.m_history set cancelled = false;
		SET @history_id = LAST_INSERT_ID();
		INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
		INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);


		INSERT INTO main_new.m_meter_by_normative
			(meter_id,
			date_begin,
			date_end,
			history_id)
		VALUES
			(p_meter_id,
			p_date_begin,
			p_date_end,
			@history_id);

    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_ssbor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_ssbor`(
	IN `p_org_id` int(11) unsigned,
	IN `p_bank_id` int(11) unsigned,
	IN `p_acceptor_id` int(11) unsigned,
	IN `p_summa` decimal(10,2)
)
    READS SQL DATA
    DETERMINISTIC
BEGIN DECLARE NEWHOMEID INT DEFAULT 0;
START TRANSACTION;
	DELETE FROM m_org_findoc WHERE org_id = p_org_id AND bank_id = p_bank_id AND acceptor_id = p_acceptor_id AND summa = p_summa;
	DELETE FROM m_org_balance WHERE bank_id = p_bank_id and  acceptor_id = p_acceptor_id and p_org_id= org_id AND summa = p_summa;
	
COMMIT; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fill` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `fill`()
BEGIN  
    DECLARE variable1 INT;  
    SET variable1 = 1;  
  
    WHILE variable1 <= 3000000 DO  
        INSERT INTO m_home_id_2 SET id = variable1;   
        SET variable1 = variable1 + 1;  
    END WHILE;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `find_balance_main_org_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `find_balance_main_org_id`(
	IN p_org_id INT
)
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Рекурсивно ищет IDqq главной балансовой организации !'
BEGIN

	DECLARE balance INT DEFAULT 0;
    DECLARE balanceId INT DEFAULT 0;
    DECLARE mainOrgId INT DEFAULT 0;

	SELECT m_organization.id, m_organization.parent_id, m_organization.isBalance
    INTO balanceId, mainOrgId, balance
	FROM m_organization
	INNER JOIN m_org_attributes ON
			m_org_attributes.org_id = m_organization.id
	INNER JOIN m_history ON
			m_history.id = m_org_attributes.history_id
	WHERE
			m_organization.id = p_org_id
	ORDER BY m_history.out_date DESC
	LIMIT 1;    
   
	SET max_sp_recursion_depth = 10;
	IF p_org_id <> mainOrgId AND balance <> 1 THEN
		CALL find_balance_main_org_id(p_main_org_id);
        ELSE SET mainOrgId = balanceId;
	END IF;
    
    SELECT mainOrgId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `find_main_org_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_main_org_id`(
	IN p_org_id INT,
	OUT p_main_org_id INT
)
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Рекурсивно ищет ID главной организации'
BEGIN
	SELECT
			parent_id
		INTO
			p_main_org_id
	FROM m_organization
	INNER JOIN m_org_attributes ON
			m_org_attributes.org_id = m_organization.id
	INNER JOIN m_history ON
			m_history.id = m_org_attributes.history_id
	WHERE
			m_organization.id = p_org_id
	ORDER BY m_history.out_date DESC
	LIMIT 1;

	SET max_sp_recursion_depth = 255;
	IF p_org_id <> p_main_org_id THEN
		CALL find_main_org_id(p_main_org_id, p_main_org_id);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_accounts_in_home` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_accounts_in_home`(
    IN  p_home_id INT(11) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Возвращает лицевые счета по указанному коду помещения'
BEGIN
    SELECT
    		m_account.id
          , m_account.zhek_code
          , m_account.account
    FROM m_account
    WHERE
            m_account.home_id = p_home_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_accounts_in_house` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_accounts_in_house`(
    IN  p_house_id INT(11) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Возвращает лицевые счета по указанному коду дома'
BEGIN
    SELECT
            m_account.id
          , m_account.zhek_code
          , m_account.account
    FROM m_account
    INNER JOIN m_home ON
            m_home.id = m_account.home_id
    WHERE
        m_home.house_id = p_house_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_account_in_house` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `get_account_in_house`(IN `p_house_id` tinyint)
BEGIN
	SELECT * FROM m_supplier_new s, m_home h WHERE h.house_id = p_house_id and s.home_id = h.home_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_homes_in_house` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `get_homes_in_house`(IN `p_house` int(11) unsigned)
BEGIN
	SELECT id from m_home where house_id = p_house;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_home_by_street` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_home_by_street`(IN `p_street_id` smallint(5) unsigned)
BEGIN
	CALL _get_houses_by_street(p_street_id) ;
	SELECT * FROM m_home home, temp_houses house where home.house_id = house.id; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_houses_by_street` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `get_houses_by_street`(IN `p_street_id` smallint(5) unsigned)
BEGIN
	call _get_houses_by_street(p_street_id);
	select * from temp_houses;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_person_by_account` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_person_by_account`(
    IN p_zhek int unsigned
  , IN p_account int unsigned
  , IN p_dt date
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Получает всех человеков по указанному лицевому счёту'
BEGIN
    SELECT
            m_person_attributes.person_id
          , m_person_at_home.home_id
          , m_person_attributes.first_name
          , m_person_attributes.middle_name
          , m_person_attributes.last_name
          , m_person_attributes.birthday
          , m_privilege_document.privilege_id
          , m_privilege_document.doc_ser
          , m_privilege_document.doc_no
          , m_privilege_document.doc_date_begin
          , m_privilege_document.doc_date_end
          , m_privilege_document.doc_info
    FROM m_account
    LEFT OUTER JOIN m_person_at_home USING (home_id)
    INNER JOIN m_history AS person_home_history ON
            m_person_at_home.history_id = person_home_history.id
        AND person_home_history.cancelled = 0
        AND p_dt BETWEEN person_home_history.in_date AND person_home_history.out_date
    LEFT OUTER JOIN m_person_attributes USING (person_id)
    INNER JOIN m_history AS person_history ON
            m_person_attributes.history_id = person_history.id
        AND person_history.cancelled = 0
        AND p_dt BETWEEN person_history.in_date AND person_history.out_date
    LEFT OUTER JOIN m_person_privilege_doc AS person_privilege_doc USING (person_id)
    LEFT OUTER JOIN m_history AS person_priv_history ON
            person_privilege_doc.history_id = person_priv_history.id
        AND person_history.cancelled = 0
        AND p_dt BETWEEN person_priv_history.in_date AND person_priv_history.out_date
    LEFT OUTER JOIN m_privilege_document ON
            m_privilege_document.id = person_privilege_doc.privilege_doc_id
    WHERE
            m_account.zhek_code = p_zhek
        AND m_account.account = p_account;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `history_cancel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `history_cancel`(
    IN  p_history_id BIGINT(20) UNSIGNED
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Отменяет запись в истории'
BEGIN
    UPDATE m_history SET cancelled = 1 WHERE id = p_history_id;
    
    INSERT INTO m_transaction_invalidate(
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , p_history_id
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `history_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `history_create`(
    IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
  , OUT p_history_id BIGINT(20) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Создаёт новую запись в истории'
BEGIN
    INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
    SET p_history_id = LAST_INSERT_ID();

    INSERT INTO m_transaction_validate (
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , p_history_id
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `home_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `home_add`(
    IN p_home_id INT(11) UNSIGNED
  , IN p_house_id INT(11) UNSIGNED
  , IN p_flat_no VARCHAR(8) CHARACTER SET 'utf8'
  , IN p_gen_area DECIMAL(8, 2) UNSIGNED
  , IN p_heat_area DECIMAL(8, 2) UNSIGNED
  , IN p_owning_type_id SMALLINT(5) UNSIGNED
  , IN p_rooms TINYINT(4) UNSIGNED
  , IN p_in_date DATE
  , IN p_user_id INT(11) UNSIGNED
  , IN p_user_app VARCHAR(255) CHARACTER SET 'utf8'
  , IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Безопасно производит вставку новой записи'
BEGIN
    START TRANSACTION;
        INSERT INTO m_home(id, house_id, flat_no) VALUE(p_home_id, p_house_id, p_flat_no);
        SET @home_id = LAST_INSERT_ID();

        CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, '9999-12-31');
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

        SET foreign_key_checks = 0;
        INSERT INTO m_home_attributes(
            home_id
          , gen_area
          , heat_area
          , owning_type_id
          , rooms
          , history_id)
        VALUE(
            @home_id
          , p_gen_area
          , p_heat_area
          , p_owning_type_id
          , p_rooms
          , @history_id);
        SET foreign_key_checks = 1;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `home_attributes_enmass` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `home_attributes_enmass`()
BEGIN
	DECLARE v_home_id INT;
	DECLARE v_gen_area DECIMAL(10,2);
	DECLARE v_heat_area DECIMAL(10,2);
	DECLARE v_owning_type_id INT;
	DECLARE v_entrance INT;
	DECLARE v_floor INT;
	DECLARE v_rooms INT;
	DECLARE v_elevator_mark INT;
	DECLARE c  CURSOR FOR  select home_id, gen_area,heat_area, owning_type_id, entrance,floor,rooms,elevator_mark from mha;
	DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
	END;
	OPEN c;
	LOOP
	    FETCH c INTO v_home_id, v_gen_area, v_heat_area,  v_owning_type_id, v_entrance, v_floor,v_rooms, v_elevator_mark ;
	    CALL home_set_attributes(v_home_id, v_gen_area, v_heat_area,  v_owning_type_id, v_entrance, v_floor,v_rooms, v_elevator_mark, "0000-00-00", "9999-12-31", 0, "Ручная загрузка из предыдущей версии", '10.0.2.49');
	END LOOP;
	CLOSE c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `home_cancel_transaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `home_cancel_transaction`(
    IN p_tx_id INT,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255),
    IN p_user_ip VARCHAR(50)
)
    MODIFIES SQL DATA
    COMMENT 'Производит отмену указанной транзакции, накатывая последующие изменения'
BEGIN
    DECLARE v_history_id INT;
    DECLARE v_transaction_id INT;


    DECLARE v_home_id INT;
    DECLARE v_gen_area DECIMAL(8, 2);
    DECLARE v_heat_area DECIMAL(8, 2);
    DECLARE v_owning_type_id SMALLINT(5);
    DECLARE v_rooms TINYINT(4);
    DECLARE v_out_period INT;
    DECLARE v_in_date DATE;
    DECLARE v_out_date DATE;

    DECLARE v_done TINYINT(1);

    DECLARE valid_cur CURSOR FOR SELECT
            m_history.id
          , m_transaction_validate.transaction_id
    FROM m_history
    INNER JOIN m_home_attributes ON
            m_home_attributes.history_id = m_history.id 
    INNER JOIN m_transaction_validate ON
            m_transaction_validate.history_id = m_history.id
        AND m_transaction_validate.transaction_id >= p_tx_id
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_validate.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE rollforward_cur CURSOR FOR SELECT
            m_transaction_params.transaction_id
          , m_home_attributes.home_id
          , m_home_attributes.gen_area
          , m_home_attributes.heat_area
          , m_home_attributes.owning_type_id
          , m_home_attributes.rooms
          , m_home_attributes.out_period
          , m_history.in_date
          , m_history.out_date
    FROM m_home_attributes
    INNER JOIN m_history ON
            m_history.id = m_home_attributes.history_id
    INNER JOIN m_transaction_params ON
            m_transaction_params.history_id = m_history.id
        AND m_transaction_params.transaction_id >= (p_tx_id - 1)
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_params.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    START TRANSACTION;
        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        OPEN valid_cur;
        valid_cur_loop: LOOP
            FETCH valid_cur INTO v_history_id, v_transaction_id;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE valid_cur_loop;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = v_history_id;
            UPDATE m_transaction SET cancelling_transaction = @tx_id WHERE id = v_transaction_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);
        END LOOP valid_cur_loop;
        CLOSE valid_cur;

        OPEN rollforward_cur;
        rollforward_cur_loop: LOOP
            FETCH rollforward_cur INTO v_transaction_id, v_home_id, v_gen_area, v_heat_area, v_owning_type_id
                                     , v_rooms, v_out_period, v_in_date, v_out_date;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE rollforward_cur_loop;
            END IF;
            IF v_transaction_id <> p_tx_id THEN
                CALL home_set_attributes(v_home_id, v_gen_area, v_heat_area, v_owning_type_id, v_rooms, v_out_period
                                      , v_in_date, v_out_date, p_user_id, p_user_app, p_user_ip);
            END IF;
        END LOOP rollforward_cur_loop;
        CLOSE rollforward_cur;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `home_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `home_delete`(
    IN p_home_id INT(11) UNSIGNED
  , IN p_out_date DATE
  , IN p_user_id INT(11) UNSIGNED
  , IN p_user_app VARCHAR(255) CHARACTER SET 'utf8'
  , IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Задаёт out_date записи'
BEGIN
    DECLARE v_id INT(11) UNSIGNED;
    DECLARE v_history_id BIGINT(20) UNSIGNED;
    DECLARE v_in_date DATE;

    START TRANSACTION;
        SELECT
                m_home_attributes.home_id
              , m_home_attributes.history_id
              , m_history.in_date
            INTO
                v_id
              , v_history_id
              , v_in_date
        FROM m_home_attributes
        INNER JOIN m_history ON
                m_history.id = m_home_attributes.history_id
            AND m_history.cancelled = 0
        WHERE
                m_home_attributes.home_id = p_home_id
        ORDER BY m_history.out_date DESC
        LIMIT 1;
        IF v_id IS NULL THEN
            SET @errmsg = CONCAT('Record with m_org.id=', p_home_id, ' is not found our is not active!');
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = @errmsg;
        ELSE
            CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

            UPDATE m_history SET
                cancelled = 1
            WHERE
                    id = v_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);

            INSERT INTO m_history(in_date, out_date) VALUE(v_in_date, p_out_date);
            SET @history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
            INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

            INSERT INTO m_home_attributes(
                home_id
              , gen_area
              , heat_area
              , owning_type_id
              , entrance
              , floor
              , rooms
              , elevator_mark
              , history_id
            ) SELECT
                    home_id
                  , gen_area
                  , heat_area
                  , owning_type_id
                  , entrance
                  , floor
                  , rooms
                  , elevator_mark
                  , @history_id
            FROM m_home_attributes
            INNER JOIN m_history ON
                    m_history.id = m_home_attributes.history_id
            WHERE
                    m_home_attributes.home_id = p_home_id
                AND m_history.id = v_history_id
            LIMIT 1;
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `home_old_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `home_old_to_new`(
)
BEGIN
	DECLARE v_home_id INT;
	DECLARE v_house_id INT;
	DECLARE v_flat_no VARCHAR(8);

	DECLARE v_gen_area DECIMAL(8, 2);
	DECLARE v_heat_area DECIMAL(8, 2);
	DECLARE v_private_id INT;
	DECLARE v_rooms INT;
	DECLARE v_out_period INT;

	DECLARE v_done TINYINT(1);

	DECLARE c CURSOR FOR SELECT main.m_home.home_id, main.m_home.house_id, flat_no, gen_area, heat_area, private_id, rooms, out_period FROM main.m_home INNER JOIN main.m_home_attributes ON main.m_home_attributes.home_id = main.m_home.home_id;

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	END;

	DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
		SELECT "Done";
		SET v_done = 1;
	END;

	OPEN c;
	l: LOOP
		FETCH c INTO v_home_id, v_house_id, v_flat_no, v_gen_area, v_heat_area, v_private_id, v_rooms, v_out_period;
		IF v_done = 1 THEN
			LEAVE l;
		END IF;

		IF v_house_id <> 0 THEN
			CALL home_add(v_home_id, v_house_id, v_flat_no, v_gen_area, v_heat_area, v_private_id, v_rooms, v_out_period, '0000-00-00', 0, "stored routine home_old_to_new", 0);
		END IF;
	END LOOP l;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `home_owner_enmass` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `home_owner_enmass`()
BEGIN
	DECLARE v_org_id INT;
	DECLARE v_type_id INT;
	DECLARE v_name VARCHAR(64);
	DECLARE v_shortname VARCHAR(64);
	DECLARE v_enabled INT;

	DECLARE c CURSOR FOR SELECT org_id, type_id, name, short_name, enabled  FROM mat;	
DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
	END;
	OPEN c;
	LOOP
	    FETCH c INTO v_org_id, v_type_id, v_name, v_shortname, v_enabled;
	    CALL org_set_attributes(v_org_id, v_type_id, v_name, v_shortname, v_enabled, '0000-00-00', '9999-12-31', 0, "Ручная загрузка из предыдущей версии", '10.0.2.49');
	END LOOP;
	CLOSE c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `home_set_attributes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `home_set_attributes`(IN `p_home_id` int, IN `p_gen_area` decimal(8,2), IN `p_heat_area` decimal(8,2), IN `p_owning_type_id` smallint(5), IN `p_rooms` tinyint(4), IN `p_entrance` int, IN `p_floor` int, IN `p_elevator_mark` int, IN `p_in_date` date, IN `p_out_date` date, IN `p_user_id` int, IN `p_user_app` varchar(255) CHARACTER SET 'utf8', IN `p_user_ip` varchar(50) CHARACTER SET 'utf8')
    MODIFIES SQL DATA
    COMMENT 'Устанавливает атрибуты поставщика для указанного периода'
BEGIN
    
    
    
    
    DECLARE bef_gen_area DECIMAL(8, 2);
    DECLARE bef_heat_area DECIMAL(8, 2);
    DECLARE bef_owning_type_id SMALLINT(5);
    DECLARE bef_rooms TINYINT(4);
    DECLARE bef_entrance INT;
    DECLARE bef_floor INT;
    DECLARE bef_elevator_mark INT;
    DECLARE bef_history_id INT;
    DECLARE bef_in_date DATE;
    DECLARE bef_out_date DATE;

    DECLARE aft_gen_area DECIMAL(8, 2);
    DECLARE aft_heat_area DECIMAL(8, 2);
    DECLARE aft_owning_type_id SMALLINT(5);
    DECLARE aft_rooms TINYINT(4);
    DECLARE aft_entrance INT;
    DECLARE aft_floor INT;
    DECLARE aft_elevator_mark INT;
    DECLARE aft_history_id INT;
    DECLARE aft_in_date DATE;
    DECLARE aft_out_date DATE;

    
    DECLARE in_eq_p_in INT;
    DECLARE out_eq_p_out INT;

    DECLARE btwn_history_id INT;
    DECLARE btwn_tx_id INT;
    DECLARE bwtn_cur_done TINYINT(1);
    
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_home_attributes
    INNER JOIN m_history ON
            m_history.id = m_home_attributes.history_id
        AND m_history.cancelled = 0
    WHERE
            m_home_attributes.home_id = p_home_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET bwtn_cur_done = 1;
    END;

    START TRANSACTION;
        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();
        
        
        SELECT
                m_home_attributes.gen_area
              , m_home_attributes.heat_area
              , m_home_attributes.owning_type_id
              , m_home_attributes.rooms
              , m_home_attributes.entrance
              , m_home_attributes.floor
              , m_home_attributes.elevator_mark
              , m_home_attributes.history_id
              , m_history.in_date
              , m_history.out_date
            INTO
                bef_gen_area
              , bef_heat_area
              , bef_owning_type_id
              , bef_rooms
	      ,bef_entrance
	      , bef_floor
		, bef_elevator_mark 
              , bef_history_id
              , bef_in_date
              , bef_out_date
        FROM m_home_attributes
        INNER JOIN m_history ON
                m_history.id = m_home_attributes.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date < p_in_date
            AND m_history.out_date >= p_in_date
        WHERE
                m_home_attributes.home_id = p_home_id
        LIMIT 1;

        
        SELECT
                m_home_attributes.gen_area
              , m_home_attributes.heat_area
              , m_home_attributes.owning_type_id
              , m_home_attributes.rooms
              , m_home_attributes.entrance
              , m_home_attributes.floor
              , m_home_attributes.elevator_mark
              , m_home_attributes.history_id
              , m_history.in_date
              , m_history.out_date
            INTO
                aft_gen_area
              , aft_heat_area
              , aft_owning_type_id
              , aft_rooms
	      , aft_entrance
	      , aft_floor
		, aft_elevator_mark 
              , aft_history_id
              , aft_in_date
              , aft_out_date
        FROM m_home_attributes
        INNER JOIN m_history ON
                m_history.id = m_home_attributes.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date <= p_out_date
            AND m_history.out_date > p_out_date
        WHERE
                m_home_attributes.home_id = p_home_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                in_eq_p_in
        FROM m_home_attributes
        INNER JOIN m_history ON
                m_history.id = m_home_attributes.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date = p_in_date
        WHERE
                m_home_attributes.home_id = p_home_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                out_eq_p_out
        FROM m_home_attributes
        INNER JOIN m_history ON
                m_history.id = m_home_attributes.history_id
            AND m_history.cancelled = 0
            AND m_history.out_date = p_out_date
        WHERE
                m_home_attributes.home_id = p_home_id
        LIMIT 1;


        IF bef_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = bef_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, bef_history_id);
        END IF;

        
        SET bwtn_cur_done = 0;
        OPEN btwn_cur;
        btwn: LOOP
            FETCH btwn_cur INTO btwn_history_id;
            IF bwtn_cur_done = 1 THEN
                LEAVE btwn;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = btwn_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, btwn_history_id);
        END LOOP btwn;

        IF aft_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = aft_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, aft_history_id);
        END IF;



        IF
                bef_history_id IS NOT NULL
            AND bef_in_date <= DATE_SUB(p_in_date, INTERVAL 1 DAY)
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(bef_in_date, DATE_SUB(p_in_date, INTERVAL 1 DAY));
            SET @bef_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @bef_history_id);
        END IF;

        
        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

        IF
                aft_history_id IS NOT NULL
            AND DATE_ADD(p_out_date, INTERVAL 1 DAY) <= aft_out_date
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(DATE_ADD(p_out_date, INTERVAL 1 DAY), aft_out_date);
            SET @aft_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @aft_history_id);
        END IF;

        IF
                @bef_history_id IS NOT NULL
        THEN
            INSERT INTO m_home_attributes(
                home_id
              , gen_area
              , heat_area
              , owning_type_id
		, rooms
	      , entrance
	      , floor
		, elevator_mark 

              , history_id)
            VALUES (
                p_home_id
              , bef_gen_area
              , bef_heat_area
              , bef_owning_type_id
              , bef_rooms
	      ,bef_entrance
	      , bef_floor
		, bef_elevator_mark 
              , @bef_history_id
            );
        END IF;

INSERT INTO m_home_attributes(
                home_id
              , gen_area
              , heat_area
              , owning_type_id
		, rooms
	      , entrance
	      , floor
		, elevator_mark 

              , history_id)
            VALUES (
                p_home_id
              , p_gen_area
              , p_heat_area
              , p_owning_type_id
              , p_rooms
	      , p_entrance
	      , p_floor
	      , p_elevator_mark 
              , @history_id
            );


        IF
                @aft_history_id IS NOT NULL
        THEN
            INSERT INTO m_home_attributes(
                home_id
              , gen_area
              , heat_area
              , owning_type_id
		, rooms
	      , entrance
	      , floor
		, elevator_mark 
              , history_id)
            VALUES (
                p_home_id
              , aft_gen_area
              , aft_heat_area
              , aft_owning_type_id
              , aft_rooms
	      , aft_entrance
	      , aft_floor
		, aft_elevator_mark 
              , @aft_history_id
            );
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `log_m_owner` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `log_m_owner`(IN `p_person_id` INT, IN `p_fieldname` VARCHAR(255), IN `p_old_value` VARCHAR(255), IN `p_new_value` VARCHAR(255), IN `p_user_id` INT)
begin
 insert into m_owner_log set
 	  person_id = p_person_id,
          fieldname = p_fieldname,
          old_value = p_old_value,
          new_value = p_new_value,
	  user_id   = p_user_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `log_m_person` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `log_m_person`(IN `p_person_id` int, IN `p_fieldname` varchar(255), IN `p_old_value` varchar(255), IN `p_new_value` varchar(255), IN `p_user_id` int)
begin
 insert into m_person_log set
 	  person_id = p_person_id,
          fieldname = p_fieldname,
          old_value = p_old_value,
          new_value = p_new_value,
	  user_id   = p_user_id,
	  dt_creation = NOW();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `log_m_person_home` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `log_m_person_home`(IN `p_person_id` INT, IN `p_fieldname` VARCHAR(255), IN `p_old_value` VARCHAR(255), IN `p_new_value` VARCHAR(255), IN `p_user_id` INT)
begin
 insert into m_person_home_log set
 	  person_id = p_person_id,
          fieldname = p_fieldname,
          old_value = p_old_value,
          new_value = p_new_value,
	  user_id   = p_user_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `log_m_privilege` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `log_m_privilege`(IN `p_person_id` INT, IN `p_fieldname` VARCHAR(255), IN `p_old_value` VARCHAR(255), IN `p_new_value` VARCHAR(255), IN `p_user_id` INT)
begin
 insert into m_privilege_log set
 	  person_id = p_person_id,
          fieldname = p_fieldname,
          old_value = p_old_value,
          new_value = p_new_value,
	  user_id   = p_user_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `log_m_relation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `log_m_relation`(IN `p_person_id` INT, IN `p_fieldname` VARCHAR(255), IN `p_old_value` VARCHAR(255), IN `p_new_value` VARCHAR(255), IN `p_user_id` INT)
begin
 insert into m_relation_log set
 	  person_id = p_person_id,
          fieldname = p_fieldname,
          old_value = p_old_value,
          new_value = p_new_value,
	  user_id   = p_user_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `memo_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `memo_create`(
	IN p_external_id VARCHAR(22),
	IN p_payment_id INT(11),
	IN p_letter VARCHAR(513),
	IN p_wrong_account_id INT(11) UNSIGNED,
	IN p_wrong_service_id SMALLINT(5) UNSIGNED,
	IN p_wrong_acceptor_id INT(11) UNSIGNED,
	IN p_bank_id INT(11) UNSIGNED,
	IN p_right_service_id SMALLINT(5) UNSIGNED,
	IN p_right_acceptor_id INT(11) UNSIGNED,
	IN p_right_account_id INT(11) UNSIGNED,
	IN p_sum DECIMAL(10,2),
	IN p_user_id INT(11) UNSIGNED,
	IN p_user_app VARCHAR(255),
	IN p_user_ip VARCHAR(50)
)
    COMMENT 'Создаёт меморандум, чо.'
BEGIN
	INSERT INTO m_billing_memorandum(
		external_id
	  , payment_id
	  , letter
	  , wrong_account_id
	  , wrong_service_id
	  , wrong_acceptor_id
	  , bank_id
	  , user_id
	  , user_app
	  , user_ip)
	VALUE(
		p_external_id
	  , p_payment_id
	  , p_letter
	  , p_wrong_account_id
	  , p_wrong_service_id
	  , p_wrong_acceptor_id
	  , p_bank_id
	  , p_user_id
	  , p_user_app
	  , p_user_id
	);
	SET @memo_id = LAST_INSERT_ID();

	INSERT INTO m_billing_memorandum_redirect(
		memo_id
	  , service_id
	  , right_acceptor_id
	  , right_account_id)
	VALUE(
		@memo_id
	  , p_right_service_id
	  , p_right_acceptor_id
	  , p_right_account_id
	);
	SET @redirector_id = LAST_INSERT_ID();

	INSERT INTO m_billing_memorandum_document(
		redirector_id
	  , summa
	  , reason
	  , dt)
	VALUE(
		@redirector_id
	  , p_sum
	  , p_letter
	  , NOW()
	);

	INSERT INTO m_billing_memorandum_rest(
		redirector_id
	  , start_value
	  , current_value
	  , dt
	)
	VALUE(
		@redirector_id
	  , p_sum
	  , p_sum
	  , NOW()
	);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `meter_at_home_outdate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `meter_at_home_outdate`(
    IN  p_meter_id INT(11) UNSIGNED
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Задаёт out_date для активного счётчика'
BEGIN
    DECLARE v_meter_id INT(11) UNSIGNED;
    DECLARE v_home_id INT(11) UNSIGNED;
    DECLARE v_location_id SMALLINT(11) UNSIGNED;
    DECLARE v_history_id BIGINT(20) UNSIGNED;
    DECLARE v_in_date DATE;

    SELECT
            m_meter_at_home.meter_id
          , m_meter_at_home.home_id
          , m_meter_at_home.location_id
          , m_history.id
          , m_history.in_date
        INTO
            v_meter_id
          , v_home_id
          , v_location_id
          , v_history_id
          , v_in_date
    FROM m_meter_at_home
    INNER JOIN m_history ON
            m_history.id = m_meter_at_home.history_id
        AND m_history.cancelled = 0
        AND m_history.out_date = '9999-12-31'
    WHERE
            m_meter_at_home.meter_id = p_meter_id
    LIMIT 1;

    IF v_meter_id IS NULL THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'meter_at_home_outdate: this meter is not active';
    END IF;

    CALL history_cancel(v_history_id, p_tx_id);

    CALL meter_at_home_set(
        v_meter_id
      , v_home_id
      , v_location_id
      , v_in_date
      , p_out_date
      , p_tx_id
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `meter_at_home_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `meter_at_home_set`(
    IN  p_meter_id INT(11) UNSIGNED
  , IN  p_home_id INT(11) UNSIGNED
  , IN  p_location_id SMALLINT(5) UNSIGNED
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE bef_home_id INT(11) UNSIGNED;
    DECLARE bef_location_id SMALLINT(5) UNSIGNED;
    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_home_id INT(11) UNSIGNED;
    DECLARE aft_location_id SMALLINT(5) UNSIGNED;
    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_meter_at_home
    INNER JOIN m_history ON
            m_history.id = m_meter_at_home.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_meter_at_home.meter_id = p_meter_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SELECT
            m_meter_at_home.home_id
          , m_meter_at_home.location_id
          , m_history.id
          , m_history.in_date
        INTO
            bef_home_id
          , bef_location_id
          , bef_history_id
          , bef_in_date
    FROM m_meter_at_home
    INNER JOIN m_history ON
            m_history.id = m_meter_at_home.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            m_meter_at_home.meter_id = p_meter_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_meter_at_home(
            meter_id
          , home_id
          , location_id
          , history_id
        ) VALUE (
            p_meter_id
          , bef_home_id
          , bef_location_id
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params(
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    INSERT INTO m_meter_at_home(
        meter_id
      , home_id
      , location_id
      , history_id
    ) VALUE (
        p_meter_id
      , p_home_id
      , p_location_id
      , @history_id
    );
    

    
    SELECT
            m_meter_at_home.home_id
          , m_meter_at_home.location_id
          , m_history.id
          , m_history.out_date
        INTO
            aft_home_id
          , aft_location_id
          , aft_history_id
          , aft_out_date
    FROM m_meter_at_home
    INNER JOIN m_history ON
            m_history.id = m_meter_at_home.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            m_meter_at_home.meter_id = p_meter_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_meter_at_home(
            meter_id
          , home_id
          , location_id
          , history_id
        ) VALUE (
            p_meter_id
          , aft_home_id
          , aft_location_id
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `meter_correction_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `meter_correction_set`(
    IN  p_meter_id INT(11) UNSIGNED
  , IN  p_month_id SMALLINT(5) UNSIGNED
  , IN  p_correction DECIMAL(18, 8)
  , IN  p_measurement_unit_id SMALLINT(5) UNSIGNED
  , IN  p_time_without_meter SMALLINT(5) UNSIGNED
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE bef_correction DECIMAL(18, 8);
    DECLARE bef_measurement_unit_id SMALLINT(5);
    DECLARE bef_time_without_meter SMALLINT(5);
    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_correction DECIMAL(18, 8);
    DECLARE aft_measurement_unit_id SMALLINT(5);
    DECLARE aft_time_without_meter SMALLINT(5);
    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_meter_correction
    INNER JOIN m_history ON
            m_history.id = m_meter_correction.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_meter_correction.meter_id = p_meter_id
        AND m_meter_correction.month_id = p_month_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SELECT
            m_meter_correction.correction
          , m_meter_correction.measurement_unit_id
          , m_meter_correction.time_without_meter
          , m_history.id
          , m_history.in_date
        INTO
            bef_correction
          , bef_measurement_unit_id
          , bef_time_without_meter
          , bef_history_id
          , bef_in_date
    FROM m_meter_correction
    INNER JOIN m_history ON
            m_history.id = m_meter_correction.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            m_meter_correction.meter_id = p_meter_id
        AND m_meter_correction.month_id = p_month_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_meter_at_home(
            meter_id
          , month_id
          , correction
          , measurement_unit_id
          , time_without_meter
          , history_id
        ) VALUE (
            p_meter_id
          , p_month_id
          , bef_correction
          , bef_measurement_unit_id
          , bef_time_without_meter
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params(
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    IF p_time_without_meter IS NULL THEN
        SET p_time_without_meter = (
            SELECT p_measurement_unit_id - p_correction
        );
    END IF;

    INSERT INTO m_meter_correction(
        meter_id
      , month_id
      , correction
      , measurement_unit_id
      , time_without_meter
      , history_id
    ) VALUE (
        p_meter_id
      , p_month_id
      , p_correction
      , p_measurement_unit_id
      , p_time_without_meter
      , @history_id
    );
    

    
    SELECT
            m_meter_correction.correction
          , m_meter_correction.measurement_unit_id
          , m_meter_correction.time_without_meter
          , m_history.id
          , m_history.out_date
        INTO
            aft_correction
          , aft_measurement_unit_id
          , aft_time_without_meter
          , aft_history_id
          , aft_out_date
    FROM m_meter_correction
    INNER JOIN m_history ON
            m_history.id = m_meter_correction.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            m_meter_correction.meter_id = p_meter_id
        AND m_meter_correction.month_id = p_month_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_meter_correction(
            meter_id
          , month_id
          , correction
          , measurement_unit_id
          , time_without_meter
          , history_id
        ) VALUE (
            p_meter_id
          , p_month_id
          , aft_correction
          , aft_measurement_unit_id
          , aft_time_without_meter
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `meter_data_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `meter_data_set`(
    IN  p_meter_id INT(11) UNSIGNED
  , IN  p_month_id SMALLINT(5) UNSIGNED
  , IN  p_prior_data DECIMAL(18, 8) UNSIGNED
  , IN  p_current_data DECIMAL(18, 8) UNSIGNED
  , IN  p_delta DECIMAL(18, 8) UNSIGNED
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE bef_prior_data DECIMAL(18, 8) UNSIGNED;
    DECLARE bef_current_data DECIMAL(18, 8) UNSIGNED;
    DECLARE bef_delta DECIMAL(18, 8);
    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_prior_data DECIMAL(18, 8) UNSIGNED;
    DECLARE aft_current_data DECIMAL(18, 8) UNSIGNED;
    DECLARE aft_delta DECIMAL(18, 8);
    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_meter_data
    INNER JOIN m_history ON
            m_history.id = m_meter_data.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_meter_data.meter_id = p_meter_id
        AND m_meter_data.month_id = p_month_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SELECT
            m_meter_data.prior_data
          , m_meter_data.current_data
          , m_meter_data.delta
          , m_history.id
          , m_history.in_date
        INTO
            bef_prior_data
          , bef_current_data
          , bef_delta
          , bef_history_id
          , bef_in_date
    FROM m_meter_data
    INNER JOIN m_history ON
            m_history.id = m_meter_data.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            m_meter_data.meter_id = p_meter_id
        AND m_meter_data.month_id = p_month_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_meter_data(
            meter_id
          , month_id
          , prior_data
          , current_data
          , delta
          , history_id
        ) VALUE (
            p_meter_id
          , p_month_id
          , bef_prior_data
          , bef_current_data
          , bef_delta
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params(
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    IF p_delta IS NULL THEN
        SET p_delta = (
            SELECT p_current_data - p_prior_data
        );
    END IF;

    INSERT INTO m_meter_data(
        meter_id
      , month_id
      , prior_data
      , current_data
      , delta
      , history_id
    ) VALUE (
        p_meter_id
      , p_month_id
      , p_prior_data
      , p_current_data
      , p_delta
      , @history_id
    );
    

    
    SELECT
            m_meter_data.prior_data
          , m_meter_data.current_data
          , m_meter_data.delta
          , m_history.id
          , m_history.out_date
        INTO
            aft_prior_data
          , aft_current_data
          , aft_delta
          , aft_history_id
          , aft_out_date
    FROM m_meter_data
    INNER JOIN m_history ON
            m_history.id = m_meter_data.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            m_meter_data.meter_id = p_meter_id
        AND m_meter_data.month_id = p_month_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_meter_data(
            meter_id
          , month_id
          , prior_data
          , current_data
          , delta
          , history_id
        ) VALUE (
            p_meter_id
          , p_month_id
          , aft_prior_data
          , aft_current_data
          , aft_delta
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `meter_test_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `meter_test_set`(
    IN  p_meter_id INT(11) UNSIGNED
  , IN  p_lock_dt DATE
  , IN  p_unlock_dt DATE
  , IN  p_next_test_dt DATE
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE bef_lock_dt DATE;
    DECLARE bef_unlock_dt DATE;
    DECLARE bef_next_test_dt DATE;
    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_lock_dt DATE;
    DECLARE aft_unlock_dt DATE;
    DECLARE aft_next_test_dt DATE;
    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_meter_test
    INNER JOIN m_history ON
            m_history.id = m_meter_test.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_meter_test.meter_id = p_meter_id;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SELECT
            m_meter_test.lock_dt
          , m_meter_test.unlock_dt
          , m_meter_test.next_test_dt
          , m_history.id
          , m_history.in_date
        INTO
            bef_lock_dt
          , bef_unlock_dt
          , bef_next_test_dt
          , bef_history_id
          , bef_in_date
    FROM m_meter_test
    INNER JOIN m_history ON
            m_history.id = m_meter_test.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            m_meter_test.meter_id = p_meter_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_meter_test(
            meter_id
          , lock_dt
          , unlock_dt
          , next_test_dt
          , history_id
        ) VALUE (
            p_meter_id
          , bef_lock_dt
          , bef_unlock_dt
          , bef_next_test_dt
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params(
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    INSERT INTO m_meter_test(
        meter_id
      , lock_dt
      , unlock_dt
      , next_test_dt
      , history_id
    ) VALUE (
        p_meter_id
      , p_lock_dt
      , p_unlock_dt
      , p_next_test_dt
      , @history_id
    );
    

    
    SELECT
            m_meter_test.lock_dt
          , m_meter_test.unlock_dt
          , m_meter_test.next_test_dt
          , m_history.id
          , m_history.out_date
        INTO
            aft_lock_dt
          , aft_unlock_dt
          , aft_next_test_dt
          , aft_history_id
          , aft_out_date
    FROM m_meter_test
    INNER JOIN m_history ON
            m_history.id = m_meter_test.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            m_meter_test.meter_id = p_meter_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_meter_data(
            meter_id
          , prior_data
          , current_data
          , next_test_dt
          , history_id
        ) VALUE (
            p_meter_id
          , aft_prior_data
          , aft_current_data
          , aft_next_test_dt
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `minus_ssbor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `minus_ssbor`(IN `p_org_id` INT UNSIGNED, IN `:bank_id` INT UNSIGNED, IN `:acceptor_id` INT UNSIGNED, IN `:dt` DATE, IN `:summa` DECIMAL(10,2), IN `:letter` VARCHAR(520) CHARSET utf8)
BEGIN 
	DECLARE A INT; 
	DECLARE BORG DECIMAL(10,2); 
	DECLARE REST DECIMAL(10,2);
	DECLARE EGGOG VARCHAR(255);
	DECLARE K INT DEFAULT NULL;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
		SELECT 'Невозможно выполнить запрос';	


	END;

	set REST = 0;
	START TRANSACTION;


		select count(*) into A from m_org_balance where bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;
		IF (A=0) THEN 

	
			CALL RAISE_ERROR;
		END IF;

		
		select CAST(summa as DECIMAL(10,2)) into BORG from m_org_balance where bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id = org_id;
		
		if (BORG = 0) THEN 
			CALL RAISE_EXCEPTION;
		END IF;
		IF ( BORG >= `:summa`) THEN 
			SET REST = 0; 
			update m_org_balance SET summa = summa - `:summa` where bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;
		ELSE 
			SET REST = `:summa` - BORG;
			update m_org_balance SET summa = 0 where bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;			
		END IF;
	

		insert INTO m_org_findoc (org_id,bank_id,acceptor_id,summa,Letter,dt) VALUES (p_org_id,`:bank_id`,`:acceptor_id`,0 - (`:summa` - REST),`:letter`,`:dt`);
	COMMIT; 


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `m_homegc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `m_homegc`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_dt DATE
)
    READS SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Файл 4'
BEGIN
    SELECT
            house_id
          , flat_no
          , gen_area
          , heat_area
          , rooms
          , owning_type_id
          , owning_type_name
    FROM m_zhyto_home
    WHERE
            home_id = p_home_id
        AND home_attributes_in_date >= FIRST_DAY(p_dt)
        AND home_attributes_out_date <= LAST_DAY(p_dt);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `m_housegc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `m_housegc`(
    IN  p_house_id INT(11) UNSIGNED
)
    READS SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Файл 2'
BEGIN
    SELECT
    		house_no
    	  , house_street_id
    FROM m_zhyto_house
    WHERE
    		m_zhyto_house.house_id = p_house_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `m_lgogc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `m_lgogc`(
    IN  p_privilege_id SMALLINT(5) UNSIGNED
)
    READS SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Файл 3'
BEGIN
    SELECT
            privilege_name
          , privilege_area_id
          , privilege_area_name
          , privilege_in_date
          , privilege_rate
        
        
          , privilege_out_date
    FROM v_zhyto_privilege
    WHERE
            v_zhyto_privilege.privilege_id = p_privilege_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `m_persongc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `m_persongc`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_dt DATE
)
    READS SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Файл 5'
BEGIN
    SELECT
            person_id
          , person_full_name
          , person_birthday
          , person_doc_no
          , person_tax_id
          , person_at_home_in_date
          , person_at_home_out_date
          , relation_id
          , relation_name
    FROM v_zhyto_person
    WHERE
            home_id = p_home_id
        AND (
            (
                    person_at_home_in_date >= FIRST_DAY(p_dt)
                AND person_at_home_in_date <= LAST_DAY(p_dt)
            ) OR (
                    person_at_home_in_date <= FIRST_DAY(p_dt)
                AND person_at_home_out_date >= LAST_DAY(p_dt)
            ) OR (
                    person_at_home_out_date >= FIRST_DAY(p_dt)
                AND person_at_home_out_date <= LAST_DAY(p_dt)
            )
        )
        AND (
            (
                    person_attributes_in_date >= FIRST_DAY(p_dt)
                AND person_attributes_in_date <= LAST_DAY(p_dt)
            ) OR (
                    person_attributes_in_date <= FIRST_DAY(p_dt)
                AND person_attributes_out_date >= LAST_DAY(p_dt)
            ) OR (
                    person_attributes_out_date >= FIRST_DAY(p_dt)
                AND person_attributes_out_date <= LAST_DAY(p_dt)
            )
        );

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `m_personlg` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `m_personlg`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_dt DATE
)
    READS SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Файл 6'
BEGIN
    SELECT
            person_id
          , person_privilege_doc_in_date
          , privilege_id
          , privilege_doc_date_begin
          , privilege_doc_no
          , privilege_owner_full_name
          , person_privilege_doc_out_date
    FROM v_zhyto_person_privilege_doc
    WHERE
            home_id = p_home_id
        AND (
            (
                    person_privilege_doc_in_date >= FIRST_DAY(p_dt)
                AND person_privilege_doc_in_date <= LAST_DAY(p_dt)
            ) OR (
                    person_privilege_doc_in_date <= FIRST_DAY(p_dt)
                AND person_privilege_doc_out_date >= LAST_DAY(p_dt)
            ) OR (
                    person_privilege_doc_out_date >= FIRST_DAY(p_dt)
                AND person_privilege_doc_out_date <= LAST_DAY(p_dt)
            )
        )
        AND (
            (
                    privilege_owner_attributes_in_date >= FIRST_DAY(p_dt)
                AND privilege_owner_attributes_in_date <= LAST_DAY(p_dt)
            ) OR (
                    privilege_owner_attributes_in_date <= FIRST_DAY(p_dt)
                AND privilege_owner_attributes_out_date >= LAST_DAY(p_dt)
            ) OR (
                    privilege_owner_attributes_out_date >= FIRST_DAY(p_dt)
                AND privilege_owner_attributes_out_date <= LAST_DAY(p_dt)
            )
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `m_stritgc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `m_stritgc`(
    IN  p_street_id SMALLINT(5) UNSIGNED
)
    READS SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Файл 1'
BEGIN
    SELECT
            street_name
          , street_prefix_id
          , street_prefix
    FROM v_zhyto_street
    WHERE
            v_zhyto_street.street_id = p_street_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `new_max_social_volume_values` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_max_social_volume_values`(
    IN p_in_date DATE,
	IN p_max_social_cold_water DECIMAL(18,4),
	IN p_max_social_hot_water DECIMAL(18,4),
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    COMMENT 'Оновлює значення максимальних об"ємі соціального споживання води за місяць'
BEGIN
	DECLARE v_id INT;
    DECLARE v_history_id INT;
    DECLARE v_in_date DATE;
    DECLARE v_out_date DATE;

    START TRANSACTION;
		CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

		SELECT volume.k, hist.id, hist.in_date, hist.out_date
			INTO v_id, v_history_id, v_in_date, v_out_date
			FROM main_new.m_max_social_water_volume volume
			inner join main_new.m_history hist on volume.history_id = hist.id
		where hist.cancelled = false
			ORDER BY hist.out_date DESC
			LIMIT 1;

		IF (v_id IS NOT NULL and v_history_id IS NOT NULL)
        THEN
			IF (p_in_date <= v_in_date)
			THEN
					UPDATE m_history SET cancelled = true WHERE id = v_history_id;
					INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);
			END IF;

			IF (p_in_date > v_in_date and p_in_date <= v_out_date)
			THEN
					UPDATE m_history SET out_date = (p_in_date - INTERVAL 1 DAY) WHERE id = v_history_id;
			END IF;
		END IF;

			INSERT INTO m_history(in_date) VALUE(p_in_date);
            SET @history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
            INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);


		INSERT INTO main_new.m_max_social_water_volume
			(max_social_cold_water,
			max_social_hot_water,
			history_id)
		VALUES
			(p_max_social_cold_water,
			p_max_social_hot_water,
			@history_id);

    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `old_percent_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `old_percent_to_new`(
)
BEGIN
	DECLARE v_bank_id SMALLINT(5);
	DECLARE v_acceptor_id SMALLINT(5);
	DECLARE v_zhek SMALLINT(5);
	DECLARE v_service_id SMALLINT(5);
	DECLARE v_supplier_id SMALLINT(5);
	DECLARE v_per_bank DECIMAL(5, 2);
	DECLARE v_per_zhek DECIMAL(5, 2);
	DECLARE v_per_cc DECIMAL(4, 2);
	DECLARE v_per_aval DECIMAL(4, 2);
	DECLARE v_lmonth TINYINT(1);
	DECLARE v_lmonthcc TINYINT(1);
	DECLARE v_per_min DECIMAL(4, 2);
	DECLARE v_date DATE;

	DECLARE c CURSOR FOR SELECT bankid, acceptor_id, nomj
							  , service_id, supplier_id
							  , per_bank, per_jek, per_cc
							  , per_aval, lmonth, lmonthcc
							  , per_min, date
	FROM tbl_percent;

	OPEN c;
	l: LOOP
		FETCH c INTO v_bank_id, v_acceptor_id, v_zhek, v_service_id
				   , v_supplier_id, v_per_bank, v_per_zhek, v_per_cc
				   , v_per_aval, v_lmonth, v_lmonthcc, v_per_min, v_date;
		IF v_bank_id IS NULL THEN
			LEAVE l;
		END IF;

		CALL percent_row_to_comission(v_bank_id, v_acceptor_id, v_zhek
									, v_service_id, v_supplier_id
									, v_per_bank, v_per_zhek, v_per_cc
									, v_per_aval, v_lmonth, v_lmonthcc
									, v_per_min, v_date, 0, 'stored routine', 0);
	END LOOP l;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `old_proc_zhek_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `old_proc_zhek_to_new`()
BEGIN
	DECLARE v_zhek_code DECIMAL(2,0);
	DECLARE v_addservice_id SMALLINT(5);
	DECLARE v_supplier_org_id INT;
	DECLARE v_acceptor_org_id INT;
	DECLARE v_interest DECIMAL(10, 4);
	DECLARE v_in_date DATE;

	DECLARE v_done TINYINT(1);

	DECLARE c CURSOR FOR SELECT zhek_code, addservice_id
			, supplier_org_id, acceptor_org_id, percent
			, dt
	FROM m_proc_zhek
	INNER JOIN (
		SELECT k, MAX(dt) FROM m_proc_zhek GROUP BY zhek_code, addservice_id
	) AS actual ON
		actual.k = m_proc_zhek.k;

	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
		SET v_done = 1;
	END;

	OPEN c;
		loop_c: LOOP
			FETCH c INTO v_zhek_code, v_addservice_id, v_supplier_org_id, v_acceptor_org_id, v_interest, v_in_date;
				IF v_done = 1 THEN
					LEAVE loop_c;
				END IF;

				CALL zhek_interest_set_attributes(v_zhek_code, v_addservice_id, v_supplier_org_id, v_acceptor_org_id,
					v_interest, v_in_date, '9999-12-31', 1, 'stored routine old_proc_zhek_to_new', '127.0.0.1');
		END LOOP loop_c;
	CLOSE c;
	SELECT "Done";
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `online_home_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `online_home_to_new`(
)
BEGIN
    DECLARE v_online_record_id INT;
    DECLARE v_online_home_id INT;
    DECLARE v_online_district_id INT;
    DECLARE v_online_town_id INT;
    DECLARE v_online_street_id INT;
    DECLARE v_online_house_no VARCHAR(8);
    DECLARE v_online_flat_no VARCHAR(8);
    DECLARE v_online_gen_area DECIMAL(8, 2);
    DECLARE v_online_heat_area DECIMAL(8, 2);
    DECLARE v_online_private_id INT;
    DECLARE v_online_rooms INT;

    DECLARE v_new_home_id INT;
    DECLARE v_new_house_id INT;
    DECLARE v_new_flat_no VARCHAR(8);
    DECLARE v_new_gen_area DECIMAL(8, 2);
    DECLARE v_new_heat_area DECIMAL(8, 2);
    DECLARE v_new_owning_type_id INT;
    DECLARE v_new_rooms INT;
    DECLARE v_new_out_period INT;

    DECLARE v_street_id INT;
    DECLARE v_house_id INT;

    DECLARE v_done TINYINT(1);
    DECLARE v_error TINYINT(1);

    DECLARE c CURSOR FOR SELECT online.tbl_home.id, online.tbl_home.home_id, online.tbl_home.district_id, online.tbl_home.town_id, online.tbl_home.street_id, online.tbl_home.house_no
                        , online.tbl_home.flat_no, online.tbl_home.gen_area, online.tbl_home.heat_area, online.tbl_home.private_id, online.tbl_home.rooms
                  FROM online.tbl_home
                  LEFT OUTER JOIN main_new.m_home ON
                        main_new.m_home.id = online.tbl_home.home_id
                  WHERE
                        online.tbl_home.bdate_id = 13
                    AND main_new.m_home.id IS NULL;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    DROP TABLE IF EXISTS online_home_to_new_error;
    CREATE TABLE online_home_to_new_error(
        online_record_id INT,
        online_district_id INT,
        online_town_id INT,
        online_street_id INT,
        new_street_id INT,
        online_house_no VARCHAR(8),
        description VARCHAR(255)
    );

    OPEN c;
    l: LOOP
        FETCH c INTO v_online_record_id, v_online_home_id, v_online_district_id, v_online_town_id, v_online_street_id, v_online_house_no, v_online_flat_no, v_online_gen_area, v_online_heat_area, v_online_private_id, v_online_rooms;
        IF v_done = 1 THEN
            LEAVE l;
        END IF;

        SET v_street_id = (
            SELECT main_new.m_street.id
            FROM main_new.m_street_code
            INNER JOIN main_new.m_street ON
                    main_new.m_street.id = main_new.m_street_code.street_id
            INNER JOIN main_new.m_town ON
                    main_new.m_town.id = main_new.m_street.town_id
                AND main_new.m_town.id = v_online_town_id
            INNER JOIN main_new.m_town_district ON
                    main_new.m_town_district.town_id = main_new.m_town.id
                AND main_new.m_town_district.district_id = v_online_district_id
            INNER JOIN main_new.m_history AS town_district_history ON
                    town_district_history.id = main_new.m_town_district.history_id
            WHERE
                    main_new.m_street_code.online_street_id = v_online_street_id
            LIMIT 1
        );

        SET v_house_id = (
            SELECT main_new.m_house.id
            FROM main_new.m_house
            WHERE
                    main_new.m_house.house_no = v_online_house_no
                AND main_new.m_house.street_id = v_street_id
            LIMIT 1
        );

        IF v_street_id IS NULL THEN
            INSERT INTO online_home_to_new_error VALUE(v_online_record_id, v_online_district_id, v_online_town_id, v_online_street_id, v_street_id, v_online_house_no, "street_id is NULL");
            SET v_error = 1;
        END IF;

        IF v_house_id IS NULL THEN
            INSERT INTO online_home_to_new_error VALUE(v_online_record_id, v_online_district_id, v_online_town_id, v_online_street_id, v_street_id, v_online_house_no, "house_id is NULL");
            SET v_error = 1;
        END IF;

        IF v_error = 0 THEN
            CALL home_add(v_online_home_id, v_house_id, v_online_flat_no, v_online_gen_area, v_online_heat_area, v_online_private_id, v_online_rooms, 0, "0000-00-00", 0, "stored routine online_home_to_new", 0);
        END IF;

        SET v_done = 0;
        SET v_error = 0;
    END LOOP l;

    SELECT "Done";
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organization_enmass` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `organization_enmass`()
BEGIN

    
    DECLARE p_id INT;
    DECLARE p_type_id INT;
    DECLARE p_edrpou CHAR(32);
    DECLARE p_phone VARCHAR(255) CHARACTER SET 'utf8';
    DECLARE p_name VARCHAR(64) CHARACTER SET 'utf8';
    DECLARE p_short_name VARCHAR(16) CHARACTER SET 'utf8';
    DECLARE p_enabled TINYINT(1);
    DECLARE p_parent_id INT;
    DECLARE p_in_date DATE;

    DECLARE c CURSOR FOR SELECT org_id, type_id, name, short_name, enabled, '0000-00-00' FROM m_attributes;

    DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
    END;
    
    OPEN c;
    LOOP
	    FETCH c INTO p_id, p_type_id, p_name, p_short_name, p_enabled,  p_in_date;
	    CALL org_set_attributes(p_id, p_type_id, p_name, p_short_name, p_enabled, p_in_date, '9999-12-31', 0, "Ручная загрузка из предыдущей версии", '10.0.2.49');
	END LOOP;
    CLOSE c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `org_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `org_add`(INOUT `p_id` INT, IN `p_type_id` INT, IN `p_edrpou` CHAR(32), IN `p_phone` VARCHAR(255), IN `is_balance` TINYINT(1), IN `p_name` VARCHAR(64), IN `p_short_name` VARCHAR(16), IN `p_enabled` TINYINT(1), IN `p_parent_id` INT, IN `p_in_date` DATE, IN `p_user_id` INT, IN `p_user_app` VARCHAR(255), IN `p_user_ip` VARCHAR(50))
    COMMENT 'Добавляет новую организацию, поправлено '
BEGIN
    DECLARE v_org_id INT;

    START TRANSACTION;
        SELECT
                m_org.id
            INTO
                v_org_id
        FROM m_organization AS m_org
        INNER JOIN m_org_attributes AS m_org_attrs ON
                m_org_attrs.org_id = m_org.id
        INNER JOIN m_history ON
                m_history.id = m_org_attrs.history_id
            AND m_history.cancelled = 0
        WHERE
                m_org.id = p_id
        ORDER BY m_history.out_date DESC
        LIMIT 1;
        IF v_org_id IS NOT NULL THEN
            SET @errmsg = CONCAT("Organization with id=", p_id, " already exists!");
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = @errmsg;
        END IF;

        CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);
        SET foreign_key_checks = 0;
        INSERT INTO m_organization(
            id,
            parent_id,
            edrpou,
            phone,
            isBalance )
        VALUE (
            p_id,
            p_parent_id,
            p_edrpou,
            p_phone,
            is_balance
        );
        IF
                p_id IS NULL
            OR  p_id = 0
        THEN
            SET p_id = LAST_INSERT_ID();
        END IF;

        IF
                p_parent_id = 0
            OR  p_parent_id IS NULL
        THEN
            UPDATE m_organization SET parent_id = p_id WHERE id = p_id;
        END IF;

        INSERT INTO m_history(in_date) VALUE(p_in_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);


        INSERT INTO m_org_attributes(
            org_id,
            type_id,
            name,
            short_name,
            enabled,
            history_id)
        VALUE (
            p_id,
            p_type_id,
            p_name,
            p_short_name,
            p_enabled,
            @history_id
        );
        SET foreign_key_checks = 1;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `org_cancel_transaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `org_cancel_transaction`(
    IN p_tx_id INT,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255),
    IN p_user_ip VARCHAR(50)
)
    MODIFIES SQL DATA
    COMMENT 'Производит отмену указанной транзакции, накатывая последующие изменения'
BEGIN
    DECLARE v_history_id INT;
    DECLARE v_transaction_id INT;

    DECLARE v_privilege_id INT;
    DECLARE v_person_id INT;
    DECLARE v_in_date DATE;
    DECLARE v_out_date DATE;

    DECLARE v_done TINYINT(1);

    DECLARE valid_cur CURSOR FOR SELECT
            m_history.id
          , m_transaction_validate.transaction_id
    FROM m_history
    INNER JOIN m_privilege_person ON
            m_privilege_person.history_id = m_history.id 
    INNER JOIN m_transaction_validate ON
            m_transaction_validate.history_id = m_history.id
        AND m_transaction_validate.transaction_id >= p_tx_id
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_validate.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE rollforward_cur CURSOR FOR SELECT
            m_transaction_params.transaction_id
          , m_privilege_person.privilege_id
          , m_privilege_person.type_id
          , m_history.in_date
          , m_history.out_date
    FROM m_privilege_person
    INNER JOIN m_history ON
            m_history.id = m_privilege_person.history_id
    INNER JOIN m_transaction_params ON
            m_transaction_params.history_id = m_history.id
        AND m_transaction_params.transaction_id >= (p_tx_id - 1)
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_params.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    START TRANSACTION;
        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        OPEN valid_cur;
        valid_cur_loop: LOOP
            FETCH valid_cur INTO v_history_id, v_transaction_id;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE valid_cur_loop;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = v_history_id;
            UPDATE m_transaction SET cancelling_transaction = @tx_id WHERE id = v_transaction_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);
        END LOOP valid_cur_loop;
        CLOSE valid_cur;

        OPEN rollforward_cur;
        rollforward_cur_loop: LOOP
            FETCH rollforward_cur INTO v_transaction_id, v_privilege_id, v_person_id, v_in_date, v_out_date;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE rollforward_cur_loop;
            END IF;
            IF v_transaction_id <> p_tx_id THEN
                CALL org_set_attributes(v_privilege_id, v_person_id, v_in_date, v_out_date
                                      , p_user_id, p_user_app, p_user_ip);
            END IF;
        END LOOP rollforward_cur_loop;
        CLOSE rollforward_cur;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `org_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `org_create`(
    IN p_id INT,
    IN p_type_id INT,
    IN p_edrpou CHAR(32) CHARACTER SET 'utf8',
    IN p_phone VARCHAR(255) CHARACTER SET 'utf8',
    IN p_name VARCHAR(64) CHARACTER SET 'utf8',
    IN p_short_name VARCHAR(16) CHARACTER SET 'utf8',
    IN p_enabled TINYINT(1),
    IN p_parent_id INT,
    IN p_in_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    COMMENT 'Добавляет новую организацию (одноразовая, для первичного заполнения базы)'
BEGIN
    DECLARE v_id INT;
    DECLARE v_type_id INT;
    DECLARE v_name VARCHAR(64) CHARACTER SET 'utf8';
    DECLARE v_short_name VARCHAR(16) CHARACTER SET 'utf8';
    DECLARE v_enabled TINYINT(1);
    DECLARE v_in_date DATE;

    START TRANSACTION;
        SELECT
                m_org.id
              , m_org_attrs.type_id
              , m_org_attrs.name
              , m_org_attrs.short_name
              , m_org_attrs.enabled
              , m_history.in_date
            INTO
                v_id
              , v_type_id
              , v_name
              , v_short_name
              , v_enabled
              , v_in_date
        FROM m_organization AS m_org
        INNER JOIN m_org_attributes AS m_org_attrs ON
                m_org_attrs.org_id = m_org.id
        INNER JOIN m_history ON
                m_history.id = m_org_attrs.history_id
            AND m_history.cancelled = 0
        WHERE
                m_org.id = p_id
        ORDER BY m_history.out_date DESC
        LIMIT 1;
        IF v_id IS NULL THEN
            CALL org_add(p_id, p_type_id, p_edrpou, p_phone, p_name, p_short_name, p_enabled, p_parent_id, p_in_date, p_user_id, p_user_app, p_user_ip);
        ELSE
            IF
                    p_type_id <> v_type_id
                OR  p_name <> v_name
                OR  p_short_name <> v_short_name
                OR  p_enabled <> p_enabled
            THEN
                IF p_in_date > v_in_date THEN
                    CALL org_update(p_id, p_type_id, p_phone, p_name, p_short_name, p_enabled, p_in_date, p_user_id, p_user_app, p_user_ip);
                ELSE
                    SET @errmsg = CONCAT('p_in_date=', p_in_date, ' is less than MAX(history.in_date)=', v_in_date, ' for id=', p_id);
                    SIGNAL SQLSTATE '45000'
                        SET MESSAGE_TEXT = @errmsg;
                END IF;
            END IF;
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `org_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `org_delete`(
    IN p_id INT,
    IN p_out_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    COMMENT 'Производит обновление записи'
BEGIN
    DECLARE v_id INT;
    DECLARE v_history_id INT;
    DECLARE v_in_date DATE;

    START TRANSACTION;
        SELECT
                m_org_attributes.org_id
              , m_org_attributes.history_id
              , m_history.in_date
            INTO
                v_id
              , v_history_id
              , v_in_date
        FROM m_org_attributes
        INNER JOIN m_history ON
                m_history.id = m_org_attributes.history_id
            AND m_history.cancelled = 0
        WHERE
                m_org_attributes.org_id = p_id
        ORDER BY m_history.out_date DESC
        LIMIT 1;
        IF v_id IS NULL THEN
            SET @errmsg = CONCAT('Record with m_org.id=', p_id, ' is not found our is not active!');
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = @errmsg;
        ELSE
            CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

            UPDATE m_history SET
                cancelled = 1
            WHERE
                    id = v_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);

            INSERT INTO m_history(in_date, out_date) VALUE(v_in_date, p_out_date);
            SET @history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
            INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

            INSERT INTO m_org_attributes(
                org_id
              , type_id
              , name
              , short_name
              , enabled
              , history_id)
                SELECT
                        org_id
                      , type_id
                      , name
                      , short_name
                      , enabled
                      , @history_id
                FROM m_org_attributes
                INNER JOIN m_history ON
                        m_history.id = m_org_attributes.history_id
                    AND m_history.cancelled = 0
                WHERE m_org_attributes.org_id = p_id
                ORDER BY m_history.out_date DESC
                LIMIT 1;
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `org_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `org_id`()
BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE a CHAR(16);
  DECLARE b, c INT;
  DECLARE cur1 CURSOR FOR SELECT id,data FROM test.t1;
  DECLARE cur2 CURSOR FOR SELECT i FROM test.t2;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur1;
  OPEN cur2;

  read_loop: LOOP
    FETCH cur1 INTO a, b;
    FETCH cur2 INTO c;
    IF done THEN
      LEAVE read_loop;
    END IF;
    IF b < c THEN
      INSERT INTO test.t3 VALUES (a,b);
    ELSE
      INSERT INTO test.t3 VALUES (a,c);
    END IF;
  END LOOP;

  CLOSE cur1;
  CLOSE cur2;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `org_old_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `org_old_to_new`()
BEGIN

    
    DECLARE p_id INT;
    DECLARE p_type_id INT;
    DECLARE p_edrpou CHAR(32);
    DECLARE p_phone VARCHAR(255) CHARACTER SET 'utf8';
    DECLARE p_name VARCHAR(64) CHARACTER SET 'utf8';
    DECLARE p_short_name VARCHAR(16) CHARACTER SET 'utf8';
    DECLARE p_enabled TINYINT(1);
    DECLARE p_parent_id INT;
    DECLARE p_in_date DATE;

    DECLARE c CURSOR FOR SELECT id, type_id, edrpou, phone, name, short_name, enabled, parent_id, in_date FROM m_organization_old WHERE cancelled = 0;

    DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
    END;
    
    OPEN c;
    LOOP
	    FETCH c INTO p_id, p_type_id, p_edrpou, p_phone, p_name, p_short_name, p_enabled, p_parent_id, p_in_date;
	    CALL org_create(p_id, p_type_id, p_edrpou, p_phone, p_name, p_short_name, p_enabled, p_parent_id, p_in_date, 0, 0);
	END LOOP;
    CLOSE c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `org_set_attributes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `org_set_attributes`(
    IN p_org_id INT,
    IN p_type_id INT,
    IN p_name VARCHAR(64) CHARACTER SET 'utf8',
    IN p_short_name VARCHAR(16) CHARACTER SET 'utf8',
    IN p_enabled TINYINT(1),
    IN p_in_date DATE,
    IN p_out_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    COMMENT 'Устанавливает атрибуты организации для указанного периода'
BEGIN
    
    
    
    
    DECLARE bef_type_id INT;
    DECLARE bef_name VARCHAR(64) CHARACTER SET 'utf8';
    DECLARE bef_short_name VARCHAR(16) CHARACTER SET 'utf8';
    DECLARE bef_enabled TINYINT(1);
    DECLARE bef_history_id INT;
    DECLARE bef_in_date DATE;
    DECLARE bef_out_date DATE;
    DECLARE bef_tx_id INT;

    DECLARE aft_type_id INT;
    DECLARE aft_name VARCHAR(64) CHARACTER SET 'utf8';
    DECLARE aft_short_name VARCHAR(16) CHARACTER SET 'utf8';
    DECLARE aft_enabled TINYINT(1);
    DECLARE aft_history_id INT;
    DECLARE aft_in_date DATE;
    DECLARE aft_out_date DATE;
    DECLARE aft_tx_id INT;

    
    DECLARE in_eq_p_in INT;
    DECLARE out_eq_p_out INT;

    DECLARE btwn_history_id INT;
    DECLARE btwn_tx_id INT;
    DECLARE bwtn_cur_done TINYINT(1);
    
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_org_attributes
    INNER JOIN m_history ON
            m_history.id = m_org_attributes.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_org_attributes.org_id = p_org_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET bwtn_cur_done = 1;
    END;

    START TRANSACTION;
        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        
        SELECT
                type_id,
                name,
                short_name,
                enabled,
                history_id,
                in_date,
                out_date
            INTO
                bef_type_id,
                bef_name,
                bef_short_name,
                bef_enabled,
                bef_history_id,
                bef_in_date,
                bef_out_date
        FROM m_org_attributes
        INNER JOIN m_history ON
                m_history.id = m_org_attributes.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date < p_in_date
            AND m_history.out_date >= p_in_date
        WHERE
                m_org_attributes.org_id = p_org_id
        LIMIT 1;

        
        SELECT
                type_id,
                name,
                short_name,
                enabled,
                history_id,
                in_date,
                out_date
            INTO
                aft_type_id,
                aft_name,
                aft_short_name,
                aft_enabled,
                aft_history_id,
                aft_in_date,
                aft_out_date
        FROM m_org_attributes
        INNER JOIN m_history ON
                m_history.id = m_org_attributes.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date <= p_out_date
            AND m_history.out_date > p_out_date
        WHERE
                m_org_attributes.org_id = p_org_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                in_eq_p_in
        FROM m_org_attributes
        INNER JOIN m_history ON
                m_history.id = m_org_attributes.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date = p_in_date
        WHERE
                m_org_attributes.org_id = p_org_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                out_eq_p_out
        FROM m_org_attributes
        INNER JOIN m_history ON
                m_history.id = m_org_attributes.history_id
            AND m_history.cancelled = 0
            AND m_history.out_date = p_out_date
        WHERE
                m_org_attributes.org_id = p_org_id
        LIMIT 1;


        IF bef_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = bef_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, bef_history_id);
        END IF;

        
        SET bwtn_cur_done = 0;
        OPEN btwn_cur;
        btwn: LOOP
            FETCH btwn_cur INTO btwn_history_id;
            IF bwtn_cur_done = 1 THEN
                LEAVE btwn;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = btwn_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, btwn_history_id);
        END LOOP btwn;

        IF aft_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = aft_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, aft_history_id);
        END IF;



        IF
                bef_history_id IS NOT NULL
            AND bef_in_date <= DATE_SUB(p_in_date, INTERVAL 1 DAY)
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(bef_in_date, DATE_SUB(p_in_date, INTERVAL 1 DAY));
            SET @bef_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @bef_history_id);
        END IF;

        
        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

        IF
                aft_history_id IS NOT NULL
            AND DATE_ADD(p_out_date, INTERVAL 1 DAY) <= aft_out_date
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(DATE_ADD(p_out_date, INTERVAL 1 DAY), aft_out_date);
            SET @aft_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @aft_history_id);
        END IF;


        IF
                @bef_history_id IS NOT NULL
        THEN
            INSERT INTO m_org_attributes(
                org_id,
                type_id,
                name,
                short_name,
                enabled,
                history_id)
            VALUES (
                p_org_id,
                bef_type_id,
                bef_name,
                bef_short_name,
                bef_enabled,
                @bef_history_id
            );
        END IF;

        INSERT INTO m_org_attributes(
            org_id,
            type_id,
            name,
            short_name,
            enabled,
            history_id)
        VALUES (
            p_org_id,
            p_type_id,
            p_name,
            p_short_name,
            p_enabled,
            @history_id
        );

        IF
                @aft_history_id IS NOT NULL
        THEN
            INSERT INTO m_org_attributes(
                org_id,
                type_id,
                name,
                short_name,
                enabled,
                history_id)
            VALUES (
                p_org_id,
                aft_type_id,
                aft_name,
                aft_short_name,
                aft_enabled,
                @aft_history_id
            );
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `org_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `org_update`(
    IN p_id INT,
    IN p_type_id INT,
    IN p_phone VARCHAR(255) CHARACTER SET 'utf8',
    IN p_name VARCHAR(64) CHARACTER SET 'utf8',
    IN p_short_name VARCHAR(16) CHARACTER SET 'utf8',
    IN p_enabled TINYINT(1),
    IN p_in_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    COMMENT 'Производит обновление записи'
BEGIN
    DECLARE v_id INT;
    DECLARE v_type_id INT;
    DECLARE v_phone VARCHAR(255) CHARACTER SET 'utf8';
    DECLARE v_name VARCHAR(64) CHARACTER SET 'utf8';
    DECLARE v_short_name VARCHAR(16) CHARACTER SET 'utf8';
    DECLARE v_enabled TINYINT(1);
    DECLARE v_history_id INT;
    DECLARE v_in_date DATE;
    DECLARE v_out_date DATE;

    START TRANSACTION;
        SELECT
                m_org.id
              , m_org_attrs.type_id
              , m_org.phone
              , m_org_attrs.name
              , m_org_attrs.short_name
              , m_org_attrs.enabled
              , m_history.id
              , m_history.in_date
              , m_history.out_date
            INTO
                v_id
              , v_type_id
              , v_phone
              , v_name
              , v_short_name
              , v_enabled
              , v_history_id
              , v_in_date
              , v_out_date
        FROM m_organization AS m_org
        INNER JOIN m_org_attributes AS m_org_attrs ON
                m_org_attrs.org_id = m_org.id
        INNER JOIN m_history ON
                m_history.id = m_org_attrs.history_id
            AND m_history.cancelled = 0
        WHERE
                m_org.id = p_id
        ORDER BY m_history.out_date DESC
        LIMIT 1;
        IF
                v_id IS NULL
            OR  v_in_date >= p_in_date
        THEN
            SET @errmsg = CONCAT('Record with m_org.id=', p_id, ' is not found!');
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = @errmsg;
        ELSE
            CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

            IF v_phone <> p_phone THEN
                UPDATE m_organization SET phone = p_phone WHERE id = p_id;
            END IF;

            IF v_out_date >= p_in_date THEN
                UPDATE m_history SET
                    cancelled = 1
                WHERE
                        id = v_history_id;
                INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);
            END IF;

            INSERT INTO m_history(in_date) VALUE(p_in_date);
            SET @history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
            INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

            INSERT INTO m_org_attributes(
                org_id
              , type_id
              , name
              , short_name
              , enabled
              , history_id)
            VALUE(
                v_id
              , p_type_id
              , p_name
              , p_short_name
              , p_enabled
              , @history_id
            );
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `percent_old_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `percent_old_to_new`(
)
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Берёт строки из tbl_percent и преобразовывает их в m_comission'
BEGIN
	DECLARE v_bank_id SMALLINT(5);
	DECLARE v_acceptor_id SMALLINT(5);
	DECLARE v_zhek SMALLINT(5);
	DECLARE v_service_id SMALLINT(5);
	DECLARE v_supplier_id SMALLINT(5);
	DECLARE v_per_bank DECIMAL(5, 2);
	DECLARE v_per_zhek DECIMAL(5, 2);
	DECLARE v_per_cc DECIMAL(4, 2);
	DECLARE v_per_aval DECIMAL(4, 2);
	DECLARE v_lmonth TINYINT(1);
	DECLARE v_lmonthcc TINYINT(1);
	DECLARE v_per_min DECIMAL(4, 2);
	DECLARE v_date DATE;

	DECLARE v_k INTEGER UNSIGNED;

	DECLARE c CURSOR FOR SELECT tbl_percent.bankid, tbl_percent.acceptor_id
			, tbl_percent.nomj, tbl_percent.service_id, tbl_percent.supplier_id
			, tbl_percent.per_bank, tbl_percent.per_jek, tbl_percent.per_cc
			, tbl_percent.per_aval, tbl_percent.lmonth, tbl_percent.lmonthcc
			, tbl_percent.per_min, tbl_percent.date
	FROM process.tbl_percent
	INNER JOIN (
		SELECT
				tbl_percent.bankid
			  , tbl_percent.acceptor_id
			  , tbl_percent.nomj
			  , tbl_percent.service_id
			  , tbl_percent.supplier_id
			  , MAX(tbl_percent.date) AS date
		FROM process.tbl_percent
		GROUP BY tbl_percent.bankid, tbl_percent.acceptor_id, tbl_percent.nomj
			   , tbl_percent.service_id, tbl_percent.supplier_id
	) AS by_max_date ON
			by_max_date.bankid = tbl_percent.bankid
		AND by_max_date.acceptor_id = tbl_percent.acceptor_id
		AND by_max_date.nomj = tbl_percent.nomj
		AND by_max_date.service_id = tbl_percent.service_id
		AND by_max_date.supplier_id = tbl_percent.supplier_id
		AND by_max_date.date = tbl_percent.date
	;

	DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
		SELECT "Done";
	END;

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN

	END;

	DROP TABLE IF EXISTS percent_to_comission_errors;
	CREATE TABLE percent_to_comission_errors(
		k INTEGER UNSIGNED,
		bank_id SMALLINT(5),
		acceptor_id SMALLINT(5),
		supplier_id SMALLINT(5),
		service_id SMALLINT(5),
		zhek_code SMALLINT(5),
		per_bank DECIMAL(5, 2),
		per_zhek DECIMAL(5, 2),
		per_cc DECIMAL(5, 2),
		description VARCHAR(255)
	) ENGINE=InnoDb;

	SET v_k = 0;

	OPEN c;
	l: LOOP
		FETCH c INTO v_bank_id, v_acceptor_id, v_zhek, v_service_id
				   , v_supplier_id, v_per_bank, v_per_zhek, v_per_cc
				   , v_per_aval, v_lmonth, v_lmonthcc, v_per_min, v_date;
		IF v_bank_id IS NULL THEN
			LEAVE l;
		END IF;

		SET v_k = v_k + 1;
		CALL percent_row_to_comission(v_k, v_bank_id, v_acceptor_id, v_zhek
									, v_service_id, v_supplier_id
									, v_per_bank, v_per_zhek, v_per_cc
									, v_per_aval, v_lmonth, v_lmonthcc
									, v_per_min, v_date, 0, 'stored routine percent_old_to_new()', 0);
	END LOOP l;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `percent_row_to_comission` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `percent_row_to_comission`(
    IN  p_k INTEGER UNSIGNED
  , IN  p_bank_id SMALLINT UNSIGNED
  , IN  p_acceptor_id SMALLINT UNSIGNED
  , IN  p_zhek DECIMAL(2,0) UNSIGNED
  , IN  p_service_id SMALLINT UNSIGNED
  , IN  p_supplier_id SMALLINT UNSIGNED
  , IN  p_per_bank DECIMAL(5, 2) UNSIGNED
  , IN  p_per_zhek DECIMAL(5, 2) UNSIGNED
  , IN  p_per_cc DECIMAL(4, 2) UNSIGNED
  , IN  p_per_aval DECIMAL(4, 2) UNSIGNED
  , IN  p_lmonth TINYINT UNSIGNED
  , IN  p_lmonthcc TINYINT UNSIGNED
  , IN  p_per_min DECIMAL(4, 2) UNSIGNED
  , IN  p_date DATE
  , IN  p_user_id INT UNSIGNED
  , IN  p_user_app VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Берёт строку из tbl_percent и преобразовывает её в m_comission'
BEGIN
    DECLARE v_bank_org_id INT UNSIGNED;
    DECLARE v_bank_main_org_id INT UNSIGNED;

    DECLARE v_supplier_org_id INT UNSIGNED;
    DECLARE v_supplier_main_org_id INT UNSIGNED;

    DECLARE v_zhek_org_id INT UNSIGNED;
    DECLARE v_zhek_main_org_id INT UNSIGNED;
    DECLARE v_service_id SMALLINT UNSIGNED;

    DECLARE v_comission_cmsu TINYINT UNSIGNED;

    DECLARE v_comission_type_id_1 TINYINT UNSIGNED;
    DECLARE v_comission_type_id_2 TINYINT UNSIGNED;

    SELECT
            m_org_cmsu.org_id
        INTO
            v_bank_org_id
    FROM m_org_cmsu
    INNER JOIN m_organization ON
            m_organization.id = m_org_cmsu.org_id
    WHERE
            m_org_cmsu.bank_id = p_bank_id
    LIMIT 1;

    CALL find_main_org_id(v_bank_org_id, v_bank_main_org_id);

    SELECT
            m_org_cmsu.org_id
        INTO
            v_supplier_org_id
    FROM m_org_cmsu
    WHERE
            supplier_id = p_supplier_id
    LIMIT 1;
    
    CALL find_main_org_id(v_supplier_org_id, v_supplier_main_org_id);



        SELECT
                m_org_cmsu.org_id
            INTO
                v_zhek_org_id
        FROM m_org_cmsu
        WHERE
                zhek = p_zhek
        LIMIT 1;

        CALL find_main_org_id(v_zhek_org_id, v_zhek_main_org_id);


    SELECT
            m_service.id
        INTO
            v_service_id
    FROM m_service
    WHERE
            m_service.id = p_service_id
    LIMIT 1;

    SELECT
            m_comission.comission_type_id
        INTO
            v_comission_cmsu
    FROM m_comission
    WHERE
            m_comission.bank_org_id = v_bank_main_org_id
        AND m_comission.supplier_org_id = v_supplier_main_org_id
        AND m_comission.service_id = p_service_id
        AND m_comission.comission_type_id = 1
        AND m_comission.comission_acceptor_org_id = 9999
    LIMIT 1;

    SELECT
            m_comission.comission_type_id
        INTO
            v_comission_type_id_1
    FROM m_comission
    WHERE
            m_comission.bank_org_id = v_bank_main_org_id
        AND m_comission.supplier_org_id = v_supplier_main_org_id
        AND m_comission.service_id = p_service_id
        AND m_comission.comission_type_id = 1
        
        AND m_comission.comission_acceptor_org_id = v_bank_main_org_id
    LIMIT 1;

    SELECT
            m_comission.comission_type_id
        INTO
            v_comission_type_id_2
    FROM m_comission
    WHERE
            m_comission.bank_org_id = v_bank_main_org_id
        AND m_comission.supplier_org_id = v_supplier_main_org_id
        AND m_comission.service_id = p_service_id
        AND m_comission.comission_type_id = 2
        AND m_comission.comission_acceptor_org_id = v_zhek_main_org_id
    LIMIT 1;

    SET @rb = 0;

    IF v_bank_org_id IS NULL THEN
        INSERT INTO percent_to_comission_errors VALUE(p_k, p_bank_id, p_acceptor_id, p_supplier_id, p_service_id, p_zhek, p_per_bank, p_per_zhek, p_per_cc, "bank_org_id is NULL");
        SET @rb = 1;
    END IF;

    IF v_bank_main_org_id IS NULL THEN
        INSERT INTO percent_to_comission_errors VALUE(p_k, p_bank_id, p_acceptor_id, p_supplier_id, p_service_id, p_zhek, p_per_bank, p_per_zhek, p_per_cc, "bank_main_org_id is NULL");
        SET @rb = 1;
    END IF;

    IF v_supplier_org_id IS NULL THEN
        INSERT INTO percent_to_comission_errors VALUE(p_k, p_bank_id, p_acceptor_id, p_supplier_id, p_service_id, p_zhek, p_per_bank, p_per_zhek, p_per_cc, "supplier_org_id is NULL");
        SET @rb = 1;
    END IF;


    IF v_supplier_main_org_id IS NULL THEN
        INSERT INTO percent_to_comission_errors VALUE(p_k, p_bank_id, p_acceptor_id, p_supplier_id, p_service_id, p_zhek, p_per_bank, p_per_zhek, p_per_cc, "supplier_main_org_id is NULL");
        SET @rb = 1;
    END IF;

    IF p_per_zhek <> 0 AND v_zhek_org_id IS NULL THEN
        INSERT INTO percent_to_comission_errors VALUE(p_k, p_bank_id, p_acceptor_id, p_supplier_id, p_service_id, p_zhek, p_per_bank, p_per_zhek, p_per_cc, "acceptor_org_id is NULL");
        SET @rb = 1;
    END IF;

    IF p_per_zhek <> 0 AND v_zhek_main_org_id IS NULL THEN
        INSERT INTO percent_to_comission_errors VALUE(p_k, p_bank_id, p_acceptor_id, p_supplier_id, p_service_id, p_zhek, p_per_bank, p_per_zhek, p_per_cc, "acceptor_main_org_id is NULL");
        SET @rb = 1;
    END IF;

    IF v_service_id IS NULL THEN
        INSERT INTO percent_to_comission_errors VALUE(p_k, p_bank_id, p_acceptor_id, p_supplier_id, p_service_id, p_zhek, p_per_bank, p_per_zhek, p_per_cc, "m_service.id is NULL");
        SET @rb = 1;
    END IF;

    
    
    
    

    
    
    
    

    IF @rb = 0 THEN
        SET foreign_key_checks = 0;
        START TRANSACTION;
            INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
            SET @tx_id = LAST_INSERT_ID();

            INSERT INTO m_comission_reason(doc_reason) VALUE("import from tbl_percent");
            SET @reason_id = LAST_INSERT_ID();
            
        
            IF v_comission_cmsu IS NULL  THEN
                CALL comission_add(v_bank_main_org_id, v_supplier_main_org_id, 1, v_service_id, 9999, @reason_id, 0, p_per_cc, 0, p_lmonthcc, p_date, '9999-12-31', p_user_id, p_user_app, p_user_ip);
            END IF;

            IF v_comission_type_id_1 IS NULL THEN
        
                    CALL comission_add(v_bank_main_org_id, v_supplier_main_org_id, 1, v_service_id, v_bank_main_org_id, @reason_id, 0, p_per_bank, 0, p_lmonth, p_date, '9999-12-31', p_user_id, p_user_app, p_user_ip);
        
            END IF;

            IF v_comission_type_id_2 IS NULL THEN
                IF p_per_zhek <> 0 THEN
                    
                    CALL comission_add(v_zhek_main_org_id, v_supplier_main_org_id, 2, v_service_id, v_zhek_main_org_id, @reason_id, 0, p_per_zhek, 0, p_lmonth, p_date, '9999-12-31', p_user_id, p_user_app, p_user_ip);
                END IF;
            END IF;
        COMMIT;
        SET foreign_key_checks = 1;
    END IF;
    SET @rb = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `person_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `person_add`(
    INOUT p_person_id INT(11) UNSIGNED
  , IN p_tax_id VARCHAR(16) CHARACTER SET 'utf8'

  , IN p_status TINYINT(1) UNSIGNED
  , IN p_first_name VARCHAR(128) CHARACTER SET 'utf8'
  , IN p_last_name VARCHAR(128) CHARACTER SET 'utf8'
  , IN p_middle_name VARCHAR(128) CHARACTER SET 'utf8'
  , IN p_doc_type_id SMALLINT(5) UNSIGNED
  , IN p_doc_ser VARCHAR(8) CHARACTER SET 'utf8'
  , IN p_doc_no VARCHAR(128) CHARACTER SET 'utf8'
  , IN p_doc_gov_id SMALLINT(5) UNSIGNED

  , IN p_birthday DATE
  , IN p_birth_country_id SMALLINT(5) UNSIGNED
  , IN p_birth_region VARCHAR(128) CHARACTER SET 'utf8'
  , IN p_birth_town VARCHAR(128) CHARACTER SET 'utf8'
  , IN p_birth_town_koatuu_id INT(11) UNSIGNED
  , IN p_nationality_country_id SMALLINT(5)
  , IN p_sex TINYINT(1) UNSIGNED
  , IN p_p26 TINYINT(1) UNSIGNED
  , IN p_in_date DATE
  , IN p_out_date DATE
  , IN p_user_id INT(11) UNSIGNED
  , IN p_user_app VARCHAR(255) CHARACTER SET 'utf8'
  , IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Добавляет нового человека в базу'
BEGIN
    START TRANSACTION;
        INSERT INTO m_person(id, tax_id) VALUE(p_person_id, p_tax_id);
        IF p_person_id IS NULL THEN
            SET p_person_id = LAST_INSERT_ID();
        END IF;

        CALL person_set_attributes(
                p_person_id, NULL, p_first_name, p_last_name, p_middle_name
              , p_doc_type_id, p_doc_ser, p_doc_no, p_doc_gov_id 
              , p_birthday, p_birth_country_id, p_birth_region, p_birth_town
              , p_birth_town_koatuu_id, p_nationality_country_id, p_sex, p_p26, p_in_date, p_out_date
              , p_user_id, p_user_app, p_user_ip);
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `person_at_home_set_attributes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `person_at_home_set_attributes`(IN `p_person_id` int, IN `p_home_id` int, IN `p_status_id` smallint(5) unsigned, IN `p_in_date` date, IN `p_out_date` date, IN `p_user_id` int, IN `p_user_app` varchar(255) CHARACTER SET 'utf8', IN `p_user_ip` varchar(50) CHARACTER SET 'utf8')
BEGIN
    
    
    
    

    DECLARE bef_home_id INT;
    DECLARE bef_history_id INT;
    DECLARE bef_status_id INT;
    DECLARE bef_in_date DATE;
    DECLARE bef_out_date DATE;
    DECLARE bef_tx_id INT;

    DECLARE aft_home_id INT;
    DECLARE aft_status_id INT;
    DECLARE aft_history_id INT;
    DECLARE aft_in_date DATE;
    DECLARE aft_out_date DATE;
    DECLARE aft_tx_id INT;

    
    DECLARE in_eq_p_in INT;
    DECLARE out_eq_p_out INT;

    DECLARE btwn_history_id INT;
    DECLARE btwn_tx_id INT;
    DECLARE bwtn_cur_done TINYINT(1);
    
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_person_at_home
    INNER JOIN m_history ON
            m_history.id = m_person_at_home.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_person_at_home.person_id = p_person_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET bwtn_cur_done = 1;
    END;

    START TRANSACTION;
        IF p_out_date IS NULL THEN
            SET p_out_date = '9999-12-31';
        END IF;

        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        
        SELECT
                home_id
              , history_id
		, status_id
              , in_date
              , out_date
            INTO
                bef_home_id
              , bef_history_id
		, bef_status_id
              , bef_in_date
              , bef_out_date
        FROM m_person_at_home
        INNER JOIN m_history ON
                m_history.id = m_person_at_home.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date < p_in_date
            AND m_history.out_date >= p_in_date
        WHERE
                m_person_at_home.person_id = p_person_id
        LIMIT 1;

        
        SELECT
                home_id
              , history_id
		, status_id
              , in_date
              , out_date
            INTO
                aft_home_id
              , aft_history_id
		, aft_status_id
              , aft_in_date
              , aft_out_date
        FROM m_person_at_home
        INNER JOIN m_history ON
                m_history.id = m_person_at_home.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date <= p_out_date
            AND m_history.out_date > p_out_date
        WHERE
                m_person_at_home.person_id = p_person_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                in_eq_p_in
        FROM m_person_at_home
        INNER JOIN m_history ON
                m_history.id = m_person_at_home.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date = p_in_date
        WHERE
                m_person_at_home.person_id = p_person_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                out_eq_p_out
        FROM m_person_at_home
        INNER JOIN m_history ON
                m_history.id = m_person_at_home.history_id
            AND m_history.cancelled = 0
            AND m_history.out_date = p_out_date
        WHERE
                m_person_at_home.person_id = p_person_id
        LIMIT 1;


        IF bef_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = bef_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, bef_history_id);
        END IF;

        
        SET bwtn_cur_done = 0;
        OPEN btwn_cur;
        btwn: LOOP
            FETCH btwn_cur INTO btwn_history_id;
            IF bwtn_cur_done = 1 THEN
                LEAVE btwn;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = btwn_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, btwn_history_id);
        END LOOP btwn;

        IF aft_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = aft_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, aft_history_id);
        END IF;



        IF
                bef_history_id IS NOT NULL
            AND bef_in_date <= DATE_SUB(p_in_date, INTERVAL 1 DAY)
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(bef_in_date, DATE_SUB(p_in_date, INTERVAL 1 DAY));
            SET @bef_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @bef_history_id);
        END IF;

        
        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

        IF
                aft_history_id IS NOT NULL
            AND DATE_ADD(p_out_date, INTERVAL 1 DAY) <= aft_out_date
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(DATE_ADD(p_out_date, INTERVAL 1 DAY), aft_out_date);
            SET @aft_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @aft_history_id);
        END IF;


        IF
                @bef_history_id IS NOT NULL
        THEN
            INSERT INTO m_person_at_home(
                person_id
              , home_id
	      , status_id
              , history_id)
            VALUES (
                p_person_id
              , p_home_id
		, p_status_id
              , @bef_history_id
            );
        END IF;

        
        INSERT INTO m_person_at_home(
            person_id
          , home_id
	, status_id
          , history_id)
        VALUES (
            p_person_id
          , p_home_id
	  , p_status_id
          , @history_id
        );

        IF
                @aft_history_id IS NOT NULL
        THEN
            INSERT INTO m_person_at_home(
                person_id
              , home_id
		,status_id
              , history_id)
            VALUES (
                p_person_id
              , p_home_id
		,p_status_id
              , @aft_history_id
            );
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `person_at_home_set_owner_one_time` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `person_at_home_set_owner_one_time`(IN `p_person_id` int, IN `p_privilege_doc_id` int, IN `p_in_date` date, IN `p_out_date` date, IN `p_user_id` int, IN `p_user_app` varchar(255) CHARACTER SET 'utf8', IN `p_user_ip` varchar(50) CHARACTER SET 'utf8')
BEGIN

    START TRANSACTION;
        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();
        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);
            INSERT INTO m_person_privilege_doc(
                person_id
              , privilege_doc_id
              , history_id)
            VALUES (
                p_person_id
              , p_privilege_doc_id
              , @history_id);
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `person_cancel_transaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `person_cancel_transaction`(
    IN p_tx_id INT,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255),
    IN p_user_ip VARCHAR(50)
)
    MODIFIES SQL DATA
    COMMENT 'Производит отмену указанной транзакции, накатывая последующие изменения'
BEGIN
    DECLARE v_history_id INT;
    DECLARE v_transaction_id INT;

    DECLARE v_person_id INT;
    DECLARE v_status TINYINT(1);
    DECLARE v_first_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_last_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_middle_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_doc_type_id INT;
    DECLARE v_doc_ser VARCHAR(8) CHARACTER SET 'utf8';
    DECLARE v_doc_no VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_doc_gov_id INT;
    DECLARE v_doc_date_begin DATE;
    DECLARE v_doc_date_end DATE;
    DECLARE v_birthday DATE;
    DECLARE v_birth_country_id INT;
    DECLARE v_birth_region VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_birth_town VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_birth_town_koatuu_id INT;
    DECLARE v_nationality_country_id INT;
    DECLARE v_sex TINYINT(1);
    DECLARE v_p26 TINYINT(1);
    DECLARE v_in_date DATE;
    DECLARE v_out_date DATE;

    DECLARE v_done TINYINT(1);

    DECLARE valid_cur CURSOR FOR SELECT
            m_history.id
          , m_transaction_validate.transaction_id
    FROM m_history
    INNER JOIN m_person_doc ON
            m_person_doc.history_id = m_history.id 
    INNER JOIN m_transaction_validate ON
            m_transaction_validate.history_id = m_history.id
        AND m_transaction_validate.transaction_id >= p_tx_id
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_validate.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE rollforward_cur CURSOR FOR SELECT
            m_transaction_params.transaction_id
          , m_person_doc.person_id
          , m_person_doc.status
          , m_person_doc.first_name
          , m_person_doc.last_name
          , m_person_doc.middle_name
          , m_person_doc.doc_type_id
          , m_person_doc.doc_ser
          , m_person_doc.doc_no
          , m_person_doc.doc_gov_id
          , m_person_doc.doc_date_begin
          , m_person_doc.doc_date_end
          , m_person_doc.birthday
          , m_person_doc.birth_country_id
          , m_person_doc.birth_region
          , m_person_doc.birth_town
          , m_person_doc.birth_town_koatuu_id
          , m_person_doc.nationality_country_id
          , m_person_doc.sex
          , m_person_doc.p26
          , m_history.in_date
          , m_history.out_date
    FROM m_person_doc
    INNER JOIN m_history ON
            m_history.id = m_person_doc.history_id
    INNER JOIN m_transaction_params ON
            m_transaction_params.history_id = m_history.id
        AND m_transaction_params.transaction_id >= (p_tx_id - 1)
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_params.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    START TRANSACTION;
        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        OPEN valid_cur;
        valid_cur_loop: LOOP
            FETCH valid_cur INTO v_history_id, v_transaction_id;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE valid_cur_loop;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = v_history_id;
            UPDATE m_transaction SET cancelling_transaction = @tx_id WHERE id = v_transaction_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);
        END LOOP valid_cur_loop;
        CLOSE valid_cur;

        OPEN rollforward_cur;
        rollforward_cur_loop: LOOP
            FETCH rollforward_cur INTO v_transaction_id, v_person_id, v_status, v_first_name, v_last_name
                                     , v_middle_name, v_doc_type_id, v_doc_ser, v_doc_no, v_doc_gov_id
                                     , v_doc_date_begin, v_doc_date_end, v_birthday, v_birth_country_id
                                     , v_birth_region, v_birth_town, v_birth_town_koatuu_id, v_nationality_country_id
                                     , v_sex, v_p26, v_in_date, v_out_date;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE rollforward_cur_loop;
            END IF;
            IF v_transaction_id <> p_tx_id THEN
                CALL person_set_attributes(v_person_id, v_status, v_first_name, v_last_name, v_middle_name
                                      , v_doc_type_id, v_doc_ser, v_doc_no, v_doc_gov_id, v_doc_date_begin
                                      , v_doc_date_end, v_birthday, v_birth_country_id, v_birth_region
                                      , v_birth_town, v_birth_town_koatuu_id, v_nationality_country_id
                                      , v_sex, v_p26, v_in_date, v_out_date, p_user_id, p_user_app, p_user_ip);
            END IF;
        END LOOP rollforward_cur_loop;
        CLOSE rollforward_cur;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `person_home_to_person_at_home` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `person_home_to_person_at_home`()
BEGIN
	DECLARE v_person_id INT;
	DECLARE v_home_id INT;
	DECLARE v_action_dt DATE;

	DECLARE v_done TINYINT(1);

	DECLARE c CURSOR FOR SELECT
		main_.m_person_home.person_id, main_.m_person_home.home_id, main_.m_person_home.action_dt
	FROM main_.m_person_home
	INNER JOIN (
		SELECT person_id, MAX(action_dt) AS dt, action_id FROM main_.m_person_home GROUP BY person_id
	) AS actual ON
			actual.person_id = m_person_home.person_id
		AND	actual.dt = m_person_home.action_dt
		AND	actual.action_id = m_person_home.action_id;

	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
		SET v_done = 1;
	END;

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	END;

	OPEN c;
	loop_c: LOOP
		FETCH c INTO v_person_id, v_home_id, v_action_dt;
		IF v_done = 1 THEN
			LEAVE loop_c;
		END IF;

		CALL person_at_home_set_attributes(v_person_id, v_home_id, v_action_dt, NULL, 1, 'routine person_home_to_person_at_home', '127.0.0.1');
	END LOOP loop_c;
	CLOSE c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `person_old_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `person_old_to_new`(
)
BEGIN
	
    DECLARE v_person_id INT;
    DECLARE v_tax_id VARCHAR(16) CHARACTER SET 'utf8';
    DECLARE v_status TINYINT(1);
    DECLARE v_first_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_last_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_middle_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_doc_type_id INT;
    DECLARE v_doc_ser VARCHAR(8) CHARACTER SET 'utf8';
    DECLARE v_doc_no VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_doc_gov_id INT;
    DECLARE v_doc_in_date DATE;
    DECLARE v_doc_out_date DATE;
    DECLARE v_birthday DATE;
    DECLARE v_birth_country_id INT;
    DECLARE v_birth_region VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_birth_town VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_birth_town_koatuu_id INT;
    DECLARE v_nationality_country_id INT;
    DECLARE v_sex TINYINT(1);
    DECLARE v_p26 TINYINT(1);
    DECLARE v_in_date DATE;
    DECLARE v_out_date DATE;
    DECLARE v_user_id INT;
    DECLARE v_user_app VARCHAR(255) CHARACTER SET 'utf8';
    DECLARE v_user_ip VARCHAR(50) CHARACTER SET 'utf8';

    DECLARE v_done TINYINT(1);

    DECLARE c CURSOR FOR SELECT person_id, status, name1, name2, name3, doc_type, doc_ser, doc_no, doc_info, doc_dt, doc_period, id_code, birthday, birthday_country, birthday_place, birthday_code, in_date, out_date, nationality, sex, p26 FROM main.m_person WHERE main.m_person.out_date = 0;

    DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
    	SELECT "Done";
    	SET v_done = 1;
    END;

    OPEN c;
    l: LOOP
    	FETCH c INTO v_person_id, v_status, v_last_name, v_first_name, v_middle_name, v_doc_type_id, v_doc_ser, v_doc_no, v_doc_gov_id, v_doc_in_date, v_doc_out_date, v_tax_id, v_birthday, v_birth_country_id, v_birth_town, v_birth_town_koatuu_id, v_in_date, v_out_date, v_nationality_country_id, v_sex, v_p26;
    	IF v_done = 1 THEN
    		LEAVE l;
    	END IF;

    	IF v_doc_out_date = 0 THEN
    		SET v_doc_out_date = '9999-12-31';
    	END IF;

    	IF
    			v_doc_ser = ''
    		OR  v_doc_no = ''
    	THEN
    		SET v_doc_type_id = NULL;
    	END IF;

    	IF v_doc_gov_id = '' THEN
    		SET v_doc_gov_id = NULL;
    	END IF;

    	IF v_birth_country_id = '' THEN
    		SET v_birth_country_id = NULL;
    	END IF;

    	IF v_birth_town = '' THEN
    		SET v_birth_town = NULL;
    	END IF;

    	
    		SET v_birth_town_koatuu_id = NULL;
    	

    	IF v_nationality_country_id = '' THEN
    		SET v_nationality_country_id = NULL;
    	END IF;

    	CALL person_add(v_person_id, v_tax_id, v_status, v_first_name, v_last_name, v_middle_name, v_doc_type_id, v_doc_ser, v_doc_no, v_doc_gov_id, v_doc_in_date, v_doc_out_date, v_birthday, v_birth_country_id, v_birth_region, v_birth_town, v_birth_town_koatuu_id, v_nationality_country_id, v_sex, v_p26, v_in_date, 0, 'stored routine person_old_to_new', 0);
    END LOOP l;
    CLOSE c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `person_old_to_new_v2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `person_old_to_new_v2`()
BEGIN
	
    DECLARE v_person_id INT;
    DECLARE v_tax_id VARCHAR(16) CHARACTER SET 'utf8';
    DECLARE v_status TINYINT(1);
    DECLARE v_first_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_last_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_middle_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_doc_type_id INT;
    DECLARE v_doc_ser VARCHAR(8) CHARACTER SET 'utf8';
    DECLARE v_doc_no VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_doc_gov_id INT;
    DECLARE v_doc_in_date DATE;
    DECLARE v_doc_out_date DATE;
    DECLARE v_birthday DATE;
    DECLARE v_birth_country_id INT;
    DECLARE v_birth_region VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_birth_town VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE v_birth_town_koatuu_id INT;
    DECLARE v_nationality_country_id INT;
    DECLARE v_sex TINYINT(1);
    DECLARE v_p26 TINYINT(1);
    DECLARE v_in_date DATE;
    DECLARE v_out_date DATE;
    DECLARE v_user_id INT;
    DECLARE v_user_app VARCHAR(255) CHARACTER SET 'utf8';
    DECLARE v_user_ip VARCHAR(50) CHARACTER SET 'utf8';

    DECLARE v_done TINYINT(1);

    DECLARE c CURSOR FOR SELECT  p.id as person_id
              , a.first_name
              , a.last_name
              , a.middle_name
              , a.doc_type_id
              , a.doc_ser
              , a.doc_no
	      , a.doc_date_begin
	      , a.doc_date_end
              , a.doc_gov_id                            
              , a.birthday
              , a.birth_country_id
              , a.birth_region
              , a.birth_town
              , a.birth_town_authority_id
              , a.citizenship_country_id
              , a.sex
              , a.p26
              
	 FROM m_person p, 
	      m_p_at a 
	WHERE 
		p.id = a.person_id;

    DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
    	SELECT "Done";
    	SET v_done = 1;
    END;

    OPEN c;
    l: LOOP
    	FETCH c INTO v_person_id,  v_last_name, v_first_name, v_middle_name, v_doc_type_id, v_doc_ser, v_doc_no, v_doc_gov_id, v_doc_in_date, v_doc_out_date, v_tax_id, v_birthday, v_birth_country_id, v_birth_town, v_birth_town_koatuu_id, v_nationality_country_id, v_sex, v_p26;
    	IF v_done = 1 THEN
    		LEAVE l;
    	END IF;
	SET v_in_date = '0000-00-00';
    	IF v_doc_out_date = 0 THEN
    		SET v_doc_out_date = '9999-12-31';
    	END IF;

    	IF
    			v_doc_ser = ''
    		OR  v_doc_no = ''
    	THEN
    		SET v_doc_type_id = NULL;
    	END IF;

    	IF v_doc_gov_id = '' THEN
    		SET v_doc_gov_id = NULL;
    	END IF;

    	IF v_birth_country_id = '' THEN
    		SET v_birth_country_id = NULL;
    	END IF;

    	IF v_birth_town = '' THEN
    		SET v_birth_town = NULL;
    	END IF;

    	
    		SET v_birth_town_koatuu_id = NULL;
    	

    	IF v_nationality_country_id = '' THEN
    		SET v_nationality_country_id = NULL;
    	END IF;

    	CALL person_set_attributes(
        	v_person_id,
            	v_first_name, 
                v_last_name, 
                v_middle_name, 
                v_doc_type_id, 
                v_doc_ser, 
                v_doc_no, 
                v_doc_gov_id, 
 
 
                v_birthday, 
                v_birth_country_id, 
                v_birth_region, 
                v_birth_town, 
                v_birth_town_koatuu_id, 
                v_nationality_country_id, 
                v_sex, 
                v_p26, 
                v_in_date, 
                v_out_date,
                0,                 
                'stored routine person_old_to_new', 
                0);
    END LOOP l;
    CLOSE c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `person_set_attributes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `person_set_attributes`(IN `p_person_id` int, IN `p_first_name` varchar(128) CHARACTER SET 'utf8', IN `p_last_name` varchar(128) CHARACTER SET 'utf8', IN `p_middle_name` varchar(128) CHARACTER SET 'utf8', IN `p_doc_type_id` int, IN `p_doc_ser` varchar(8) CHARACTER SET 'utf8', IN `p_doc_no` varchar(128) CHARACTER SET 'utf8', IN `p_doc_gov_id` int, IN `p_birthday` date, IN `p_birth_country_id` int, IN `p_birth_region` varchar(128) CHARACTER SET 'utf8', IN `p_birth_town` varchar(128) CHARACTER SET 'utf8', IN `p_birth_town_koatuu_id` int, IN `p_nationality_country_id` int, IN `p_sex` tinyint(1), IN `p_p26` tinyint(1), IN `p_in_date` date, IN `p_out_date` date, IN `p_user_id` int, IN `p_user_app` varchar(255) CHARACTER SET 'utf8', IN `p_user_ip` varchar(50) CHARACTER SET 'utf8')
BEGIN
    
    
    
    
    DECLARE bef_status TINYINT(1);
    DECLARE bef_first_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE bef_last_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE bef_middle_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE bef_doc_type_id INT;
    DECLARE bef_doc_ser VARCHAR(8) CHARACTER SET 'utf8';
    DECLARE bef_doc_no VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE bef_doc_gov_id INT;
    
    
    DECLARE bef_birthday DATE;
    DECLARE bef_birth_country_id INT;
    DECLARE bef_birth_region VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE bef_birth_town VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE bef_birth_town_koatuu_id INT;
    DECLARE bef_nationality_country_id INT;
    DECLARE bef_sex TINYINT(1);
    DECLARE bef_p26 TINYINT(1);
    DECLARE bef_history_id INT;
    DECLARE bef_in_date DATE;
    DECLARE bef_out_date DATE;
    DECLARE bef_tx_id INT;

    DECLARE aft_status TINYINT(1);
    DECLARE aft_first_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE aft_last_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE aft_middle_name VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE aft_doc_type_id INT;
    DECLARE aft_doc_ser VARCHAR(8) CHARACTER SET 'utf8';
    DECLARE aft_doc_no VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE aft_doc_gov_id INT;
    
    
    DECLARE aft_birthday DATE;
    DECLARE aft_birth_country_id INT;
    DECLARE aft_birth_region VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE aft_birth_town VARCHAR(128) CHARACTER SET 'utf8';
    DECLARE aft_birth_town_koatuu_id INT;
    DECLARE aft_nationality_country_id INT;
    DECLARE aft_sex TINYINT(1);
    DECLARE aft_p26 TINYINT(1);
    DECLARE aft_history_id INT;
    DECLARE aft_in_date DATE;
    DECLARE aft_out_date DATE;
    DECLARE aft_tx_id INT;

    
    DECLARE in_eq_p_in INT;
    DECLARE out_eq_p_out INT;

    DECLARE btwn_history_id INT;
    DECLARE btwn_tx_id INT;
    DECLARE bwtn_cur_done TINYINT(1);
    
    DECLARE btwn_cur CURSOR FOR SELECT
            h.id
    FROM m_person_attributes a
    INNER JOIN m_history h ON
            h.id = a.history_id
        AND h.cancelled = 0
        AND h.in_date >= p_in_date
        AND h.out_date <= p_out_date
    WHERE
            a.person_id = p_person_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET bwtn_cur_done = 1;
    END;

    START TRANSACTION;
        IF p_out_date IS NULL THEN
            SEt p_out_date = '9999-12-31';
        END IF;

        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        
        SELECT
              first_name
              , last_name
              , middle_name
              , doc_type_id
              , doc_ser
              , doc_no
              , doc_gov_id
              
              
              , birthday
              , birth_country_id
              , birth_region
              , birth_town
              , birth_town_authority_id
              , citizenship_country_id
              , sex
              , p26
              , history_id
              , in_date
              , out_date
            INTO
		bef_first_name
              , bef_last_name
              , bef_middle_name
              , bef_doc_type_id
              , bef_doc_ser
              , bef_doc_no
              , bef_doc_gov_id
              
              
              , bef_birthday
              , bef_birth_country_id
              , bef_birth_region
              , bef_birth_town
              , bef_birth_town_koatuu_id
              , bef_nationality_country_id
              , bef_sex
              , bef_p26
              , bef_history_id
              , bef_in_date
              , bef_out_date
        FROM m_person_attributes a

        INNER JOIN m_history h ON
                h.id = a.history_id
            AND h.cancelled = 0
            AND h.in_date < p_in_date
            AND h.out_date >= p_in_date
        WHERE
               a.person_id = p_person_id
        LIMIT 1;
        
        SELECT
		 first_name
              , last_name
              , middle_name
              , doc_type_id
              , doc_ser
              , doc_no
              , doc_gov_id
              
              
              , birthday
              , birth_country_id
              , birth_region
              , birth_town
              , birth_town_authority_id
              , citizenship_country_id
              , sex
              , p26
              , history_id
              , in_date
              , out_date
            INTO
		 aft_first_name
              , aft_last_name
              , aft_middle_name
              , aft_doc_type_id
              , aft_doc_ser
              , aft_doc_no
              , aft_doc_gov_id
              
              
              , aft_birthday
              , aft_birth_country_id
              , aft_birth_region
              , aft_birth_town
              , aft_birth_town_koatuu_id
              , aft_nationality_country_id
              , aft_sex
              , aft_p26
              , aft_history_id
              , aft_in_date
              , aft_out_date

         FROM m_person_attributes a

        INNER JOIN m_history h ON
                h.id = a.history_id
            AND h.cancelled = 0
            AND h.in_date < p_out_date
            AND h.out_date >= p_out_date
        WHERE
               a.person_id = p_person_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                in_eq_p_in
        FROM m_person_attributes a
        INNER JOIN m_history h ON
                h.id = a.history_id
            AND h.cancelled = 0
            AND h.in_date = p_in_date
        WHERE
                a.person_id = p_person_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                out_eq_p_out
        FROM m_person_attributes a
        INNER JOIN m_history h ON
                h.id = a.history_id
            AND h.cancelled = 0
            AND h.out_date = p_out_date
        WHERE
                a.person_id = p_person_id
        LIMIT 1;


        IF bef_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = bef_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, bef_history_id);
        END IF;

        
        SET bwtn_cur_done = 0;
        OPEN btwn_cur;
        btwn: LOOP
            FETCH btwn_cur INTO btwn_history_id;
            IF bwtn_cur_done = 1 THEN
                LEAVE btwn;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = btwn_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, btwn_history_id);
        END LOOP btwn;

        IF aft_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = aft_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, aft_history_id);
        END IF;



        IF
                bef_history_id IS NOT NULL
            AND bef_in_date <= DATE_SUB(p_in_date, INTERVAL 1 DAY)
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(bef_in_date, DATE_SUB(p_in_date, INTERVAL 1 DAY));
            SET @bef_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @bef_history_id);
        END IF;

        
        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

        IF
                aft_history_id IS NOT NULL
            AND DATE_ADD(p_out_date, INTERVAL 1 DAY) <= aft_out_date
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(DATE_ADD(p_out_date, INTERVAL 1 DAY), aft_out_date);
            SET @aft_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @aft_history_id);
        END IF;


        IF
                @bef_history_id IS NOT NULL
        THEN
            INSERT INTO m_person_attributes(
                person_id              
              , first_name
              , last_name
              , middle_name
              , doc_type_id
              , doc_ser
              , doc_no
              , doc_gov_id
              
              
              , birthday
              , birth_country_id
              , birth_region
              , birth_town
              , birth_town_authority_id
              , citizenship_country_id
              , sex
              , p26
              , history_id)
            VALUES (
                p_person_id
              , bef_first_name
              , bef_last_name
              , bef_middle_name
              , bef_doc_type_id
              , bef_doc_ser
              , bef_doc_no
              , bef_doc_gov_id
              , bef_birthday
              , bef_birth_country_id
              , bef_birth_region
              , bef_birth_town
              , bef_birth_town_koatuu_id
              , bef_nationality_country_id
              , bef_sex
              , bef_p26
              , @bef_history_id
            );
        END IF;

        
        INSERT INTO m_person_attributes(
            person_id
          , first_name
          , last_name
          , middle_name
          , doc_type_id
          , doc_ser
          , doc_no
          , doc_gov_id
          
          
          , birthday
          , birth_country_id
          , birth_region
          , birth_town
          , birth_town_authority_id
          , citizenship_country_id
          , sex
          , p26
          , history_id)
        VALUES (
            p_person_id
          , p_first_name
          , p_last_name
          , p_middle_name
          , p_doc_type_id
          , p_doc_ser
          , p_doc_no
          , p_doc_gov_id
          
          
          , p_birthday
          , p_birth_country_id
          , p_birth_region
          , p_birth_town
          , p_birth_town_koatuu_id
          , p_nationality_country_id
          , p_sex
          , p_p26
          , @history_id
        );

        IF
                @aft_history_id IS NOT NULL
        THEN
            INSERT INTO m_person_doc(
                person_id
              , first_name
              , last_name
              , middle_name
              , doc_type_id
              , doc_ser
              , doc_no
              , doc_gov_id
              
              
              , birthday
              , birth_country_id
              , birth_region
              , birth_town
              , birth_town_authority_id
              , citizenship_country_id
              , sex
              , p26
              , history_id)
            VALUES (
                p_person_id
              , bef_first_name
              , bef_last_name
              , bef_middle_name
              , bef_doc_type_id
              , bef_doc_ser
              , bef_doc_no
              , bef_doc_gov_id
              
              
              , bef_birthday
              , bef_birth_country_id
              , bef_birth_region
              , bef_birth_town
              , bef_birth_town_koatuu_id
              , bef_nationality_country_id
              , bef_sex
              , bef_p26
              , @aft_history_id
            );
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `privilege_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `privilege_add`(
    INOUT p_privilege_doc_id INT(11) UNSIGNED
  , IN p_owner_person_id INT(11) UNSIGNED
  , IN p_privilege_id SMALLINT(5) UNSIGNED
  , IN p_doc_ser VARCHAR(10) CHARACTER SET 'utf8'
  , IN p_doc_no VARCHAR(10) CHARACTER SET 'utf8'
  , IN p_doc_date_begin DATE
  , IN p_doc_date_end DATE
  , IN p_doc_info TEXT
  , IN p_user_id INT(11) UNSIGNED
  , IN p_user_app VARCHAR(255) CHARACTER SET 'utf8'
  , IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Добавляет нового льготчика с документом'
BEGIN
    DECLARE owner_person_id INT(11) UNSIGNED;
    DECLARE privilege_id SMALLINT(5) UNSIGNED;
    DECLARE doc_ser VARCHAR(10) CHARACTER SET 'utf8';
    DECLARE doc_no VARCHAR(10) CHARACTER SET 'utf8';
    DECLARE doc_date_begin DATE;
    DECLARE doc_date_end DATE;
    DECLARE doc_info TEXT;

    START TRANSACTION;
        INSERT INTO m_privilege_document(
            id
          , owner_person_id
          , privilege_id
          , doc_ser
          , doc_no
          , doc_date_begin
          , doc_date_end
          , doc_info)
        VALUE (
            p_privilege_doc_id
          , p_owner_person_id
          , p_privilege_id
          , p_doc_ser
          , p_doc_no
          , p_doc_date_begin
          , p_doc_date_end
          , p_doc_info
        );
        IF p_privilege_doc_id IS NULL THEN
            SET p_privilege_doc_id = LAST_INSERT_ID();
        END IF;

        CALL privilege_person_set_attributes(p_privilege_doc_id, p_owner_person_id, p_doc_date_begin, p_doc_date_end, p_user_id, p_user_app, p_user_ip);
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `privilege_old_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `privilege_old_to_new`(
)
BEGIN
	DECLARE v_privilege_id INT;
	DECLARE v_person_id INT;
    DECLARE v_owner_person_id INT;
    DECLARE v_privilege_type_id SMALLINT(5);
    DECLARE v_doc_ser VARCHAR(10);
    DECLARE v_doc_no VARCHAR(10);
    DECLARE v_date_begin DATE;
    DECLARE v_date_end DATE;
    DECLARE v_doc_info TEXT;

    DECLARE v_done TINYINT(1);

    DECLARE v_user_app VARCHAR(255);

    DECLARE c CURSOR FOR SELECT main.m_privilege.id, main.m_privilege.person_id, main.m_privilege.main_person_id, main.m_privilege.privilege_id, main.m_privilege.date_begin, main.m_privilege.date_end, main.m_privilege.doc_ser, main.m_privilege.doc_no, main.m_privilege.doc_info FROM main.m_privilege INNER JOIN m_person ON m_person.id = main.m_privilege.person_id INNER JOIN m_person AS p2 ON p2.id = main.m_privilege.main_person_id WHERE main.m_privilege.cancelled = 0;

    DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
    	SELECT "Done";
    	SET v_done = 1;
    END;

    SET v_user_app = "stored routine privilege_old_to_new";

    SET foreign_key_checks = 0;

    OPEN c;
    l: LOOP
    	FETCH c INTO v_privilege_id, v_person_id, v_owner_person_id, v_privilege_type_id, v_date_begin, v_date_end, v_doc_ser, v_doc_no, v_doc_info;
    	IF v_done = 1 THEN
    		LEAVE l;
    	END IF;

    	IF v_person_id = v_owner_person_id THEN
    		CALL privilege_add(v_privilege_id, v_owner_person_id, v_privilege_type_id, v_doc_ser, v_doc_no, v_date_begin, v_date_end, v_doc_info, 0, v_user_app, 0);
    	ELSE
    		CALL privilege_person_set_attributes(v_privilege_id, v_person_id, v_date_begin, v_date_end, 0, v_user_app, 0);
    	END IF;
    END LOOP l;
    CLOSE c;

    SET foreign_key_checks = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `privilege_person_set_attributes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `privilege_person_set_attributes`(
    IN p_privilege_doc_id INT(11) UNSIGNED
  , IN p_person_id INT(11) UNSIGNED
  , IN p_in_date DATE
  , IN p_out_date DATE
  , IN p_user_id INT(11) UNSIGNED
  , IN p_user_app VARCHAR(255) CHARACTER SET 'utf8'
  , IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;
    DECLARE bef_out_date DATE;

    DECLARE aft_history_id BIGINT(11) UNSIGNED;
    DECLARE aft_in_date DATE;
    DECLARE aft_out_date DATE;

    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE bwtn_cur_done TINYINT(1) UNSIGNED;
    
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_person_privilege_doc
    INNER JOIN m_history ON
            m_history.id = m_person_privilege_doc.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_person_privilege_doc.privilege_doc_id = p_privilege_doc_id
        AND m_person_privilege_doc.person_id = p_person_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET bwtn_cur_done = 1;
    END;

    START TRANSACTION;
        SET p_out_date = IF(p_out_date IS NULL, '9999-12-31', p_out_date);

        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();
        
        SELECT
                m_person_privilege_doc.history_id
              , m_history.in_date
              , m_history.out_date
            INTO
                bef_history_id
              , bef_in_date
              , bef_out_date
        FROM m_person_privilege_doc
        INNER JOIN m_history ON
                m_history.id = m_person_privilege_doc.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date < p_in_date
            AND m_history.out_date >= p_in_date
        WHERE
                m_person_privilege_doc.privilege_doc_id = p_privilege_doc_id
            AND m_person_privilege_doc.person_id = p_person_id
        LIMIT 1;
        
        SELECT
                m_person_privilege_doc.history_id
              , m_history.in_date
              , m_history.out_date
            INTO
                aft_history_id
              , aft_in_date
              , aft_out_date
        FROM m_person_privilege_doc
        INNER JOIN m_history ON
                m_history.id = m_person_privilege_doc.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date <= p_out_date
            AND m_history.out_date > p_out_date
        WHERE
                m_person_privilege_doc.privilege_doc_id = p_privilege_doc_id
            AND m_person_privilege_doc.person_id = p_person_id
        LIMIT 1;

        IF bef_history_id IS NOT NULL THEN
            UPDATE m_history SET cancelled = 1 WHERE id = bef_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, bef_history_id);
        END IF;

        
        SET bwtn_cur_done = 0;
        OPEN btwn_cur;
        btwn: LOOP
            FETCH btwn_cur INTO btwn_history_id;
            IF bwtn_cur_done = 1 THEN
                LEAVE btwn;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = btwn_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, btwn_history_id);
        END LOOP btwn;

        IF aft_history_id IS NOT NULL THEN
            UPDATE m_history SET cancelled = 1 WHERE id = aft_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, aft_history_id);
        END IF;

        IF
                bef_history_id IS NOT NULL
            AND bef_in_date <= DATE_SUB(p_in_date, INTERVAL 1 DAY)
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(bef_in_date, DATE_SUB(p_in_date, INTERVAL 1 DAY));
            SET @bef_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @bef_history_id);
        END IF;
        
        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

        IF
                aft_history_id IS NOT NULL
            AND DATE_ADD(p_out_date, INTERVAL 1 DAY) <= aft_out_date
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(DATE_ADD(p_out_date, INTERVAL 1 DAY), aft_out_date);
            SET @aft_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @aft_history_id);
        END IF;

        IF
                @bef_history_id IS NOT NULL
        THEN
            INSERT INTO m_person_privilege_doc(
                privilege_doc_id
              , person_id
              , history_id)
            VALUE (
                p_privilege_doc_id
              , p_person_id
              , @bef_history_id
            );
        END IF;

        INSERT INTO m_person_privilege_doc(
            privilege_doc_id
          , person_id
          , history_id)
        VALUE (
            p_privilege_doc_id
          , p_person_id
          , @history_id
        );

        IF
                @aft_history_id IS NOT NULL
        THEN
            INSERT INTO m_person_privilege_doc(
                privilege_doc_id
              , person_id
              , history_id)
            VALUE (
                p_privilege_doc_id
              , p_person_id
              , @aft_history_id
            );
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `relocate_ssbor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`10.0.2.57` PROCEDURE `relocate_ssbor`(IN `p_org_id` int unsigned, IN `:bank_id` int unsigned, IN `:acceptor_id` int unsigned, IN `:dt` date, IN `:letter` varchar(520))
BEGIN 
	DECLARE A INT; 
	DECLARE BORG DECIMAL(10,2) DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
		SELECT 'Невозможно выполнить запрос';			
	END;


	START TRANSACTION;
		insert INTO m_org_findoc (org_id,bank_id,acceptor_id,summa,Letter,dt) VALUES (p_org_id,`:bank_id`,`:acceptor_id`,0,`:letter`,`:dt`);
		select count(*) into A from m_org_balance where bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;
		IF (A=0) THEN 
			CALL RAISE_EXCEPTION;
		ELSE 
			select summa into BORG from m_org_balance where bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;
			IF (BORG=0) THEN 
				CALL RAISE_EXCEPTION;
			ELSE 
				UPDATE m_org_balance SET dt = `:dt` WHERE bank_id = `:bank_id` and  `:acceptor_id` = acceptor_id and p_org_id= org_id;
			END IF;
		END IF;
	COMMIT; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `revert_balance` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `revert_balance`( IN `:dt` date)
BEGIN

	DECLARE _ORG_ID INT; 
	DECLARE _BANK_ID INT;
	DECLARE _ACCEPTOR_ID INT; 
	DECLARE _REMAINDER DECIMAL(10,2) DEFAULT 0;
	DECLARE _DIFF DECIMAL(10,2) DEFAULT 0;
	DECLARE done boolean default false;
	
	DECLARE finDocCursor Cursor for 
									SELECT sum(summa) as diff, org_id, bank_id, acceptor_id 
									FROM main_new.m_org_findoc
									where dt = `:dt` and summa < 0
									group by org_id, bank_id, acceptor_id;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = true;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
		SELECT 'Неможливо виконати запит';			
	END;

	START TRANSACTION;
		OPEN finDocCursor;

		doc_loop: LOOP
			FETCH finDocCursor INTO _DIFF, _ORG_ID, _BANK_ID, _ACCEPTOR_ID;
			IF done THEN
			  LEAVE doc_loop;
			END IF;
			
			select summa into _REMAINDER from main_new.m_org_balance bal where bal.org_id = _ORG_ID and bal.bank_id = _BANK_ID and bal.acceptor_id = _ACCEPTOR_ID;
			update main_new.m_org_balance bal SET summa = (_REMAINDER - _DIFF) where bal.org_id = _ORG_ID and bal.bank_id = _BANK_ID and bal.acceptor_id = _ACCEPTOR_ID;	

		END LOOP doc_loop;

		CLOSE finDocCursor;

		delete from main_new.m_org_findoc where dt = `:dt` and summa < 0;

	COMMIT; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `routine1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `routine1`()
BEGIN

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `service_exists_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `service_exists_set`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_service_id SMALLINT(5) UNSIGNED
  , IN  p_supplier_id INT(11) UNSIGNED
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_service_exists
    INNER JOIN m_history ON
            m_history.id = m_service_exists.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_service_exists.home_id = p_home_id
        AND m_service_exists.service_id = p_service_id
        AND m_service_exists.supplier_id = p_supplier_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SELECT
            m_history.id
          , m_history.in_date
        INTO
            bef_history_id
          , bef_in_date
    FROM m_service_exists
    INNER JOIN m_history ON
            m_history.id = m_service_exists.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            m_service_exists.home_id = p_home_id
        AND m_service_exists.service_id = p_service_id
        AND m_service_exists.supplier_id = p_supplier_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_service_exists (
            home_id
          , service_id
          , supplier_id
          , history_id
        ) VALUE (
            p_home_id
          , p_service_id
          , p_supplier_id
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params (
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    INSERT INTO m_service_exists (
        home_id
      , service_id
      , supplier_id
      , history_id
    ) VALUE (
        p_home_id
      , p_service_id
      , p_supplier_id
      , @history_id
    );
    

    
    SELECT
            m_history.id
          , m_history.out_date
        INTO
            aft_history_id
          , aft_out_date
    FROM m_service_exists
    INNER JOIN m_history ON
            m_history.id = m_service_exists.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            m_service_exists.home_id = p_home_id
        AND m_service_exists.service_id = p_service_id
        AND m_service_exists.supplier_id = p_supplier_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );
        INSERT INTO m_service_exists (
            home_id
          , service_id
          , supplier_id
          , history_id
        ) VALUE (
            p_home_id
          , p_service_id
          , p_supplier_id
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `service_out_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `service_out_set`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_service_id SMALLINT(5) UNSIGNED
  , IN  p_supplier_id INT(11) UNSIGNED
  , IN  p_date_begin DATE
  , IN  p_date_end DATE
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE bef_date_begin DATE;
    DECLARE bef_date_end DATE;
    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_date_begin DATE;
    DECLARE aft_date_end DATE;
    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_service_out
    INNER JOIN m_history ON
            m_history.id = m_service_out.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_service_out.home_id = p_home_id
        AND m_service_out.service_id = p_service_id
        AND m_service_out.supplier_id = p_supplier_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SELECT
            m_service_out.date_begin
          , m_service_out.date_end
          , m_history.id
          , m_history.in_date
        INTO
            bef_date_begin
          , bef_date_end
          , bef_history_id
          , bef_in_date
    FROM m_service_out
    INNER JOIN m_history ON
            m_history.id = m_service_out.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            m_service_out.home_id = p_home_id
        AND m_service_out.service_id = p_service_id
        AND m_service_out.supplier_id = p_supplier_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_service_out(
            home_id
          , service_id
          , supplier_id
          , date_begin
          , date_end
          , history_id
        ) VALUE (
            p_home_id
          , p_service_id
          , p_supplier_id
          , bef_date_begin
          , bef_date_end
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params(
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    INSERT INTO m_service_out(
        home_id
      , service_id
      , supplier_id
      , date_begin
      , date_end
      , history_id
    ) VALUE (
        p_home_id
      , p_service_id
      , p_supplier_id
      , p_date_begin
      , p_date_end
      , @history_id
    );
    

    
    SELECT
            m_service_out.date_begin
          , m_service_out.date_end
          , m_history.id
          , m_history.out_date
        INTO
            aft_date_begin
          , aft_date_end
          , aft_history_id
          , aft_out_date
    FROM m_service_out
    INNER JOIN m_history ON
            m_history.id = m_service_out.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            m_service_out.home_id = p_home_id
        AND m_service_out.service_id = p_service_id
        AND m_service_out.supplier_id = p_supplier_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_service_out(
            home_id
          , service_id
          , supplier_id
          , date_begin
          , date_end
          , history_id
        ) VALUE (
            p_home_id
          , p_service_id
          , p_supplier_id
          , aft_date_begin
          , aft_date_end
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `service_quality_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `service_quality_set`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_service_id SMALLINT(5) UNSIGNED
  , IN  p_supplier_id INT(11) UNSIGNED
  , IN  p_quality_parameter DECIMAL(18, 8)
  , IN  p_measurement_unit_id SMALLINT(5) UNSIGNED
  , IN  p_date_begin DATE
  , IN  p_date_end DATE
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE bef_quality_parameter DECIMAL(18, 2);
    DECLARE bef_measurement_unit_id SMALLINT(5) UNSIGNED;
    DECLARE bef_date_begin DATE;
    DECLARE bef_date_end DATE;
    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_quality_parameter DECIMAL(18, 2);
    DECLARE aft_measurement_unit_id SMALLINT(5) UNSIGNED;
    DECLARE aft_date_begin DATE;
    DECLARE aft_date_end DATE;
    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_service_quality
    INNER JOIN m_history ON
            m_history.id = m_service_quality.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_service_quality.home_id = p_home_id
        AND m_service_quality.service_id = p_service_id
        AND m_service_quality.supplier_id = p_supplier_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SELECT
            m_service_quality.quality_parameter
          , m_service_quality.measurement_unit_id
          , m_service_quality.date_begin
          , m_service_quality.date_end
          , m_history.id
          , m_history.in_date
        INTO
            bef_quality_parameter
          , bef_measurement_unit_id
          , bef_date_begin
          , bef_date_end
          , bef_history_id
          , bef_in_date
    FROM m_service_quality
    INNER JOIN m_history ON
            m_history.id = m_service_quality.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            m_service_quality.home_id = p_home_id
        AND m_service_quality.service_id = p_service_id
        AND m_service_quality.supplier_id = p_supplier_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_service_quality(
            home_id
          , service_id
          , supplier_id
          , quality_parameter
          , measurement_unit_id
          , date_begin
          , date_end
          , history_id
        ) VALUE (
            p_home_id
          , p_service_id
          , p_supplier_id
          , bef_quality_parameter
          , bef_measurement_unit_id
          , bef_date_begin
          , bef_date_end
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params(
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    INSERT INTO m_service_quality(
        home_id
      , service_id
      , supplier_id
      , quality_parameter
      , measurement_unit_id
      , date_begin
      , date_end
      , history_id
    ) VALUE (
        p_home_id
      , p_service_id
      , p_supplier_id
      , p_quality_parameter
      , p_measurement_unit_id
      , p_date_begin
      , p_date_end
      , @history_id
    );
    

    
    SELECT
            m_service_quality.quality_parameter
          , m_service_quality.measurement_unit_id
          , m_service_quality.date_begin
          , m_service_quality.date_end
          , m_history.id
          , m_history.out_date
        INTO
            aft_quality_parameter
          , aft_measurement_unit_id
          , aft_date_begin
          , aft_date_end
          , aft_history_id
          , aft_out_date
    FROM m_service_quality
    INNER JOIN m_history ON
            m_history.id = m_service_quality.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            m_service_quality.home_id = p_home_id
        AND m_service_quality.service_id = p_service_id
        AND m_service_quality.supplier_id = p_supplier_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_service_quality(
            home_id
          , service_id
          , supplier_id
          , quality_parameter
          , measurement_unit_id
          , date_begin
          , date_end
          , history_id
        ) VALUE (
            p_home_id
          , p_service_id
          , p_supplier_id
          , aft_quality_parameter
          , aft_measurement_unit_id
          , aft_date_begin
          , aft_date_end
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `se_optional_parts_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `se_optional_parts_set`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_service_part_id SMALLINT(5) UNSIGNED
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE v_buildpart_exists TINYINT(1) UNSIGNED;

    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_se_optional_parts
    INNER JOIN m_history ON
            m_history.id = m_se_optional_parts.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_se_optional_parts.home_id = p_home_id
        AND m_se_optional_parts.optional_parts = p_service_part_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SET v_buildpart_exists = (
        SELECT
                IF(m_history.id IS NULL, 0, 1)
        FROM vmte_buildpart_home
        INNER JOIN m_history ON
                m_history.id = vmte_buildpart_home.history_id
            AND m_history.cancelled = 0
        LIMIT 1
    );

    IF NOT v_buildpart_exists THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'buildpart_meter_set: no buildpart were found';
    END IF;
    

    
    SELECT
            m_history.id
          , m_history.in_date
        INTO
            bef_history_id
          , bef_in_date
    FROM m_se_optional_parts
    INNER JOIN m_history ON
            m_history.id = m_se_optional_parts.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            m_se_optional_parts.home_id = p_home_id
        AND m_se_optional_parts.optional_parts = p_service_part_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO m_se_optional_parts (
            home_id
          , optional_parts
          , history_id
        ) VALUE (
            p_home_id
          , p_service_part_id
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params (
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    INSERT INTO m_se_optional_parts (
        home_id
      , optional_parts
      , history_id
    ) VALUE (
        p_home_id
      , p_service_part_id
      , @history_id
    );
    

    
    SELECT
            m_history.id
          , m_history.out_date
        INTO
            aft_history_id
          , aft_out_date
    FROM m_se_optional_parts
    INNER JOIN m_history ON
            m_history.id = m_se_optional_parts.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            m_se_optional_parts.home_id = p_home_id
        AND m_se_optional_parts.optional_parts = p_service_part_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );
        INSERT INTO m_se_optional_parts (
            home_id
          , optional_parts
          , history_id
        ) VALUE (
            p_home_id
          , p_service_part_id
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ssbor_old_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssbor_old_to_new`(
)
BEGIN
    DECLARE v_org_id INT;
    DECLARE v_bank_main_org_id INT;
    DECLARE v_acceptor_org_id INT;
    DECLARE v_acceptor_main_org_id INT;
    DECLARE v_date DATE;
    DECLARE v_sum DECIMAL(10, 2);
    DECLARE v_bank_sum DECIMAL(10, 2);
    DECLARE v_vc_sum DECIMAL(10, 2);
    DECLARE v_zhek_sum DECIMAL(10, 2);
    DECLARE v_letter VARCHAR(520);
    DECLARE v_limit_percent DECIMAL(5, 2);
    DECLARE v_rr VARCHAR(255);

    DECLARE v_old_org_id INT;
    DECLARE v_old_bank_org_id INT;

    DECLARE v_done TINYINT(1);
    DECLARE v_error INT;

    DECLARE ssbor_cur CURSOR FOR SELECT bankid, destorgid, summbank, summccc, summakt, dateb, leter FROM tmp_ssbor;

    DECLARE rr_ctrl_cur CURSOR FOR SELECT bankid, rr_new, summa_s, destorgid, leter, `date`, percent_s FROM tmp_rr_ctrl;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    DROP TABLE IF EXISTS ssbor_errors;
    CREATE TABLE ssbor_errors(
        bankid INT,
        destorgid INT,
        description VARCHAR(255)
    );

    OPEN ssbor_cur;
    ssbor_loop: LOOP
        FETCH ssbor_cur INTO v_old_bank_org_id, v_old_org_id, v_bank_sum, v_vc_sum, v_zhek_sum, v_date, v_letter;
        IF v_done = 1 THEN
            LEAVE ssbor_loop;
        END IF;

        SET @bank_org_id = NULL;
        SELECT org_id INTO @bank_org_id FROM m_org_cmsu WHERE bank_id = v_old_bank_org_id LIMIT 1;

        IF @bank_org_id IS NULL THEN
            INSERT INTO ssbor_errors(bankid, destorgid, description) VALUE(v_old_bank_org_id, v_old_org_id, "@bank_org_id is NULL");
            SET v_error = 1;
        END IF;

        CALL find_main_org_id(@bank_org_id, v_bank_main_org_id);
        IF v_bank_main_org_id IS NULL THEN
            INSERT INTO ssbor_errors(bankid, destorgid, description) VALUE(v_old_bank_org_id, v_old_org_id, "bank_main_org_id is NULL");
            SET v_error = 1;
        END IF;

        SET v_org_id = (SELECT org_id FROM m_org_cmsu WHERE supplier_id = v_old_org_id LIMIT 1);
        IF v_org_id IS NULL THEN
            INSERT INTO ssbor_errors(bankid, destorgid, description) VALUE(v_old_bank_org_id, v_old_org_id, "org_id is NULL");
            SET v_error = 1;
        END IF;

        IF v_error = 0 THEN
            CALL add_ssbor(v_org_id, v_bank_main_org_id, v_bank_main_org_id, v_date, v_bank_sum, v_letter, 50);
            CALL add_ssbor(v_org_id, v_bank_main_org_id, 9999, v_date, v_bank_sum, v_letter, 50);
        END IF;

        SET v_done = 0;
        SET v_error = 0;
    END LOOP ssbor_loop;
    CLOSE ssbor_cur;

    SET v_done = 0;
    SET v_error = 0;

    DROP TABLE IF EXISTS rr_ctrl_errors;
    CREATE TABLE rr_ctrl_errors(
        bankid INT,
        rr_new VARCHAR(255),
        destorgid VARCHAR(255),
        description VARCHAR(255)
    );

    OPEN rr_ctrl_cur;
    rr_ctrl_loop: LOOP
        FETCH rr_ctrl_cur INTO v_old_bank_org_id, v_rr, v_sum, v_old_org_id, v_letter, v_date, v_limit_percent;
        IF v_done = 1 THEN
            LEAVE rr_ctrl_loop;
        END IF;

        SET @bank_org_id = NULL;
        SET @bank_org_id = (SELECT org_id FROM m_org_cmsu WHERE bank_id = v_old_bank_org_id LIMIT 1);
        IF @bank_org_id IS NULL THEN
            INSERT INTO rr_ctrl_errors(bankid, rr_new, destorgid, description) VALUE(v_old_bank_org_id, v_rr, v_old_org_id, "bank_org_id NULL");
            SET v_error = 1;
        END IF;

        CALL find_main_org_id(@bank_org_id, v_bank_main_org_id);
        IF v_bank_main_org_id IS NULL THEN
            INSERT INTO rr_ctrl_errors(bankid, rr_new, destorgid, description) VALUE(v_old_bank_org_id, v_rr, v_old_org_id, "bank_main_org_id is NULL");
            SET v_error = 1;
        END IF;

        SET v_org_id = (SELECT org_id FROM m_org_cmsu WHERE supplier_id = v_old_org_id LIMIT 1);
        IF v_org_id IS NULL THEN
            INSERT INTO rr_ctrl_errors(bankid, rr_new, destorgid, description) VALUE(v_old_bank_org_id, v_rr, v_old_org_id, "org_id is NULL");
            SET v_error = 1;
        END IF;

        SET v_acceptor_org_id = (SELECT org_id FROM m_org_accounts WHERE account = v_rr LIMIT 1);
        IF v_acceptor_org_id IS NULL THEN
            INSERT INTO rr_ctrl_errors(bankid, rr_new, destorgid, description) VALUE(v_old_bank_org_id, v_rr, v_old_org_id, "acceptor_org_id is NULL");
            SET v_error = 1;
        END IF;

        CALL find_main_org_id(v_acceptor_org_id, v_acceptor_main_org_id);
        IF v_acceptor_main_org_id IS NULL THEN
            INSERT INTO rr_ctrl_errors(bankid, rr_new, destorgid, description) VALUE(v_old_bank_org_id, v_rr, v_old_org_id, "acceptor_main_org_id is NULL");
            SET v_error = 1;
        END IF;

        IF v_error = 0 THEN
            CALL add_ssbor(v_org_id, v_bank_main_org_id, v_acceptor_main_org_id, v_date, v_sum, v_letter, v_limit_percent);
        END IF;

        SET v_done = 0;
        SET v_error = 0;
    END LOOP rr_ctrl_loop;
    CLOSE rr_ctrl_cur;
    
    SELECT "Done";
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `street_old_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `street_old_to_new`(
)
BEGIN
    DECLARE v_id SMALLINT(5);
    DECLARE v_town_id SMALLINT(5);
    DECLARE v_online_street_id SMALLINT(5);
    DECLARE v_full_name VARCHAR(40);
    DECLARE v_str_code INT(5);
    DECLARE v_name VARCHAR(31);
    DECLARE v_pref_code INT(4);
    DECLARE v_old_code VARCHAR(3);

    DECLARE v_done TINYINT(1);

    DECLARE c CURSOR FOR SELECT
            id
          , town_id
          , online_street_id
          , full_name
          , str_code
          , name
          , pref_code
          , old_code
    FROM main.m_street;

    DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
        SELECT "Done";
        SET v_done = 1;
    END;

    OPEN c;
    l: LOOP
        FETCH c INTO v_id, v_town_id, v_online_street_id, v_full_name, v_str_code, v_name, v_pref_code, v_old_code;

        IF v_done = 1 THEN
            LEAVE l;
        END IF;

        SELECT v_id;

        INSERT INTO m_street(
            id
          , town_id
          , full_name
          , name
          , street_type_id)
        VALUE(
            v_id
          , v_town_id
          , v_full_name
          , v_name
          , v_pref_code);
        SET @street_id = LAST_INSERT_ID();

        INSERT INTO m_street_code(
            street_id
          , online_street_id
          , str_code
          , old_code)
        VALUE(
            @street_id
          , v_online_street_id
          , v_str_code
          , v_old_code);
    END LOOP l;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_add`(
    IN p_zhek DECIMAL(2, 0),
    IN p_account DECIMAL(10, 0),
    IN p_service_id INT,
    IN p_supplier_org_id INT,
    IN p_acceptor_org_id INT,
    IN p_in_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    COMMENT 'Безопасно производит вставку новой записи'
BEGIN
    DECLARE v_account_id INT;

    START TRANSACTION;
        SELECT
            m_supplier.account_id INTO v_account_id
        FROM m_supplier
        INNER JOIN m_history ON
                m_history.id = m_supplier.history_id
            AND m_history.out_date = '9999-12-31'
            AND m_history.cancelled = 0
        INNER JOIN m_account ON
                m_account.id = m_supplier.account_id
            AND m_account.zhek_code = p_zhek
            AND m_account.account = p_account
        WHERE
                m_supplier.service_id = p_service_id
        LIMIT 1;

        IF v_account_id IS NULL THEN
            CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

            CALL supplier_unsafe_add(p_zhek, p_account, p_service_id, p_supplier_org_id, p_acceptor_org_id, p_in_date, @tx_id);
        ELSE
            SET @errmsg = CONCAT("Record with zhek=", p_zhek, ", account=", p_account, ", service_id=", p_service_id, " already exists!");
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = @errmsg;
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_add_by_house` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_add_by_house`(
    IN p_house_id INT,
    IN p_serivce_id INT,
    IN p_supplier_org_id INT,
    IN p_acceptor_org_id INT,
    IN p_in_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    COMMENT 'Добавляет записи по house_id, возвращает ResultSet, в случае ошибки'
BEGIN
    DECLARE v_zhek DECIMAL(2, 0);
    DECLARE v_account DECIMAL(10, 0);
    DECLARE v_history_id INT;

    DECLARE v_error TINYINT(1);

    DECLARE v_done TINYINT(1);
    DECLARE v_homes_cursor CURSOR FOR SELECT
            m_account.zhek
          , m_account.account
          , m_history.id
    FROM m_home
    INNER JOIN m_account ON
            m_account.home_id = m_home.home_id
    LEFT OUTER JOIN m_supplier ON
            m_supplier.account_id = m_account.id
        AND m_supplier.service_id = p_serivce_id
    LEFT OUTER JOIN m_history ON
            m_history.id = m_supplier.history_id
        AND m_history.cancelled = 0
        AND m_history.out_date = '9999-12-31'
    WHERE
            m_home.house_id = p_house_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    DECLARE CONTINUE HANDLER FOR SQLSTATE VALUE '45000' BEGIN
        SET v_error = 1;
        INSERT INTO tmp_supplier_op_error VALUE(v_zhek, v_account);
    END;

    CALL supplier_op_prepare();

    START TRANSACTION;
        OPEN v_homes_cursor;

        CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

        l: LOOP
            FETCH v_homes_cursor INTO v_zhek, v_account, v_history_id;
            IF v_done = 1 THEN LEAVE l; END IF;

            IF v_history_id IS NOT NULL THEN
                SIGNAL SQLSTATE '45000';
            ELSEIF v_error IS NULL THEN
                CALL supplier_unsafe_add(v_zhek, v_account, p_serivce_id, p_supplier_org_id, p_acceptor_org_id, p_in_date, @tx_id);
            END IF;
        END LOOP l;

        CLOSE v_homes_cursor;
    IF v_error = 1 THEN
        SELECT * FROM tmp_supplier_op_error;
        ROLLBACK;
    ELSE
        COMMIT;
    END IF;
    
    CALL supplier_op_cleanup();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_cancel_transaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_cancel_transaction`(
    IN p_tx_id INT,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255),
    IN p_user_ip VARCHAR(50)
)
    MODIFIES SQL DATA
    COMMENT 'Производит отмену указанной транзакции, накатывая последующие изменения'
BEGIN
    DECLARE v_history_id INT;
    DECLARE v_transaction_id INT;

    DECLARE v_zhek DECIMAL(2, 0);
    DECLARE v_account DECIMAL(10, 0);
    DECLARE v_service_id INT;
    DECLARE v_supplier_org_id INT;
    DECLARE v_acceptor_org_id INT;
    DECLARE v_in_date DATE;
    DECLARE v_out_date DATE;

    DECLARE v_done TINYINT(1);

    DECLARE valid_cur CURSOR FOR SELECT
            m_history.id
          , m_transaction_validate.transaction_id
    FROM m_history
    INNER JOIN m_supplier ON
            m_supplier.history_id = m_history.id 
    INNER JOIN m_transaction_validate ON
            m_transaction_validate.history_id = m_history.id
        AND m_transaction_validate.transaction_id >= p_tx_id
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_validate.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE rollforward_cur CURSOR FOR SELECT
            m_transaction_params.transaction_id
          , m_account.zhek_code
          , m_account.account
          , m_supplier.service_id
          , m_supplier.supplier_org_id
          , m_supplier.acceptor_org_id
          , m_history.in_date
          , m_history.out_date
    FROM m_supplier
    INNER JOIN m_account ON
            m_account.id = m_supplier.account_id
    INNER JOIN m_history ON
            m_history.id = m_supplier.history_id
    INNER JOIN m_transaction_params ON
            m_transaction_params.history_id = m_history.id
        AND m_transaction_params.transaction_id >= (p_tx_id - 1)
    INNER JOIN m_transaction ON
            m_transaction.id = m_transaction_params.transaction_id
        AND m_transaction.cancelling_transaction IS NULL;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    START TRANSACTION;
        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        OPEN valid_cur;
        valid_cur_loop: LOOP
            FETCH valid_cur INTO v_history_id, v_transaction_id;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE valid_cur_loop;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = v_history_id;
            UPDATE m_transaction SET cancelling_transaction = @tx_id WHERE id = v_transaction_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);
        END LOOP valid_cur_loop;
        CLOSE valid_cur;

        OPEN rollforward_cur;
        rollforward_cur_loop: LOOP
            FETCH rollforward_cur INTO v_transaction_id, v_zhek, v_account, v_service_id, v_supplier_org_id
                                     , v_acceptor_org_id, v_in_date, v_out_date;
            IF v_done = 1 THEN
                SET v_done = 0;
                LEAVE rollforward_cur_loop;
            END IF;
            IF v_transaction_id <> p_tx_id THEN
                CALL supplier_set_attributes(v_zhek, v_account, v_service_id, v_supplier_org_id, v_acceptor_org_id
                                           , v_in_date, v_out_date, p_user_id, p_user_app, p_user_ip);
            END IF;
        END LOOP rollforward_cur_loop;
        CLOSE rollforward_cur;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_delete`(
    IN p_zhek DECIMAL(2, 0),
    IN p_account DECIMAL(10, 0),
    IN p_service_id INT,
    IN p_out_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    COMMENT 'Производит удаление записи'
BEGIN
    DECLARE v_found_id INT;

    START TRANSACTION;
        SELECT
                m_supplier.id INTO v_found_id
        FROM m_supplier
        INNER JOIN m_account ON
                m_account.id = m_supplier.account_id
            AND m_account.zhek_code = p_zhek
            AND m_account.account = p_account
        INNER JOIN m_history ON
                m_history.id = m_supplier.history_id
            AND m_history.cancelled = 0
            AND m_history.out_date = '9999-12-31'
        WHERE
                m_supplier.service_id = p_service_id;

        IF v_found_id IS NULL THEN
            SET @errmsg = CONCAT("No active record with zhek=", p_zhek, ", account=", p_account, ", service_id=", p_service_id, " were found!");
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = @errmsg;
        ELSE
            CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

            CALL supplier_unsafe_delete(p_zhek, p_account, p_service_id, p_out_date, @tx_id);
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_delete_by_house` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_delete_by_house`(
	IN p_house_id INT,
	IN p_serivce_id INT,
	IN p_out_date DATE,
	IN p_user_id INT,
	IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
	IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    COMMENT 'Удаляет записи по house_id'
BEGIN
	DECLARE v_zhek DECIMAL(2, 0);
	DECLARE v_account DECIMAL(10, 0);
	DECLARE v_history_id INT;

	DECLARE v_error TINYINT(1);

	DECLARE v_done TINYINT(1);
	DECLARE v_homes_cursor CURSOR FOR SELECT
			m_account.zhek
		  , m_account.account
		  , m_history.id
	FROM m_home
	INNER JOIN m_account ON
			m_account.home_id = m_home.home_id
	LEFT OUTER JOIN m_supplier ON
			m_supplier.account_id = m_account.id
		AND m_supplier.service_id = p_serivce_id
	LEFT OUTER JOIN m_history ON
			m_history.id = m_supplier.history_id
		AND m_history.cancelled = 0
		AND m_history.out_date = '9999-12-31'
	WHERE
			m_home.house_id = p_house_id;

	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
		SET v_done = 1;
	END;

	DECLARE CONTINUE HANDLER FOR SQLSTATE VALUE '45000' BEGIN
		SET v_error = 1;
		INSERT INTO tmp_supplier_op_error VALUE(v_zhek, v_account);
	END;

	CALL supplier_op_prepare();

	START TRANSACTION;
		OPEN v_homes_cursor;

		CALL transaction_add(p_user_id, p_user_ip, @tx_id);

		l: LOOP
			FETCH v_homes_cursor INTO v_zhek, v_account, v_history_id;
			IF v_done = 1 THEN LEAVE l; END IF;

			IF v_history_id IS NULL THEN
				SIGNAL SQLSTATE '45000';
			ELSEIF v_error IS NULL THEN
				CALL supplier_unsafe_delete(v_zhek, v_account, p_serivce_id, p_out_date, @tx_id);
			END IF;
		END LOOP l;

		CLOSE v_homes_cursor;
	IF v_error = 1 THEN
		SELECT * FROM tmp_supplier_op_error;
		ROLLBACK;
	ELSE
		COMMIT;
	END IF;

	CALL supplier_op_cleanup();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_old_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_old_to_new`()
BEGIN
	DECLARE v_zhek DECIMAL(2, 0);
	DECLARE v_account DECIMAL(10, 0);
	DECLARE v_service_id SMALLINT(5);
	DECLARE v_supplier_org_id INT;
	DECLARE v_acceptor_org_id INT;

	DECLARE v_done TINYINT(1);

	DECLARE c CURSOR FOR SELECT zhek_code, m_account.account, service_id, supplier_org_id, supplier_org_id FROM main_new.for_supplier fs INNER JOIN m_account ON m_account.id = fs.account_id;

	DECLARE EXIT HANDLER FOR NOT FOUND BEGIN
		SELECT "Done";
		SET v_done = 1;
	END;

	OPEN c;
	l: LOOP
		FETCH c INTO v_zhek, v_account, v_service_id, v_supplier_org_id, v_acceptor_org_id;
		IF v_done = 1 THEN
			LEAVE l;
		END IF;

		CALL supplier_add(v_zhek, v_account, v_service_id, v_supplier_org_id, v_acceptor_org_id, '0000-00-00', 0, "stored routine supplier_old_to_new", 0);
	END LOOP l;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_op_cleanup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_op_cleanup`()
    MODIFIES SQL DATA
    COMMENT 'Очищает окружение после supplier_(add/update/delete)_by_house'
BEGIN
    DROP TABLE IF EXISTS tmp_supplier_op_error;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_op_prepare` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_op_prepare`()
    MODIFIES SQL DATA
    COMMENT 'Подготавливает окружение для supplier_(add/update/delete)_by_house'
BEGIN
    CREATE TEMPORARY TABLE tmp_supplier_op_error(
        zhek DECIMAL(2, 0),
        account DECIMAL(10, 0)
    ) ENGINE = memory;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_outdate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_outdate`(
    IN  i_home_id INT UNSIGNED
  , IN  i_supplier_org_id INT UNSIGNED
  , IN  i_service_id SMALLINT UNSIGNED
  , IN  i_out_date DATE
  , IN  i_tx_id BIGINT UNSIGNED
)
    MODIFIES SQL DATA
    COMMENT 'Закрывает запись о поставщике по помещению'
BEGIN
    DECLARE v_account_id INT UNSIGNED;
    DECLARE v_acceptor_org_id INT UNSIGNED;
    DECLARE v_history_id BIGINT UNSIGNED;
    DECLARE v_in_date DATE;

    SELECT
            m_supplier.account_id
          , m_supplier.acceptor_org_id
          , m_history.id AS history_id
          , m_history.in_date
        INTO
            v_account_id
          , v_acceptor_org_id
          , v_history_id
          , v_in_date
    FROM m_supplier
    INNER JOIN m_account ON
            m_account.id = m_supplier.account_id
        AND m_account.home_id = i_home_id
    INNER JOIN m_history ON
            m_history.id = m_supplier.history_id
        AND m_history.cancelled = 0
        AND m_history.out_date = '9999-12-31'
    WHERE
            m_supplier.service_id = i_service_id
        AND m_supplier.supplier_org_id = i_supplier_org_id
    GROUP BY m_account.home_id
    ;

    CALL history_cancel(v_history_id, i_tx_id);

    CALL history_create(v_in_date, i_out_date, i_tx_id, @history_id);
    INSERT INTO m_transaction_params(history_id, transaction_id) VALUE (@history_id, i_tx_id);

    INSERT INTO m_supplier(
        account_id
      , service_id
      , supplier_org_id
      , acceptor_org_id
      , history_id
    ) VALUES (
        v_account_id
      , i_service_id
      , i_supplier_org_id
      , v_acceptor_org_id
      , @history_id
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_outdate_by_house` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_outdate_by_house`(
    IN  i_house_id INT UNSIGNED
  , IN  i_supplier_org_id INT UNSIGNED
  , IN  i_service_id SMALLINT UNSIGNED
  , IN  i_out_date DATE
  , IN  i_tx_id BIGINT UNSIGNED
)
    MODIFIES SQL DATA
    COMMENT 'Закрывает запись о поставщике по дому'
BEGIN
    DECLARE v_home_id INT UNSIGNED;

    DECLARE v_done TINYINT UNSIGNED;
    DECLARE v_home_cur CURSOR FOR SELECT
            m_account.home_id
    FROM m_supplier
    INNER JOIN m_account ON
            m_account.id = m_supplier.account_id
    INNER JOIN m_home ON
            m_home.id = m_account.home_id
        AND m_home.house_id = i_house_id
    INNER JOIN m_history ON
            m_history.id = m_supplier.history_id
        AND m_history.cancelled = 0
        AND m_history.out_date = '9999-12-31'
    WHERE
            m_supplier.supplier_org_id = i_supplier_org_id
        AND m_supplier.service_id = i_service_id
    GROUP BY m_account.home_id
    ;

    OPEN v_home_cur;
    home_cur_loop: LOOP
        FETCH v_home_cur INTO v_home_id;
        IF v_home_id IS NULL THEN
            LEAVE home_cur_loop;
        END IF;

        CALL supplier_outdate(v_home_id, i_supplier_org_id, i_service_id, i_out_date, i_tx_id);
    END LOOP home_cur_loop;
    CLOSE v_home_cur;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_unsafe_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_unsafe_add`(IN `p_zhek` decimal(2,0), IN `p_account` decimal(10,0), IN `p_service_id` int, IN `p_supplier_org_id` int, IN `p_acceptor_org_id` int, IN `p_in_date` date, IN `p_tx_id` int)
    MODIFIES SQL DATA
    COMMENT 'Небезопасно производит вставку новой записи'
BEGIN
    DECLARE v_account_id INT;

    INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, '9999-12-31');
    SET @history_id = LAST_INSERT_ID();
    INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(p_tx_id, @history_id);
    INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(p_tx_id, @history_id);

    SELECT id INTO v_account_id FROM m_account WHERE zhek_code = p_zhek AND account = p_account LIMIT 1;
    INSERT INTO m_supplier(
        account_id,
        service_id,
        supplier_org_id,
        acceptor_org_id,
        history_id)
    VALUE(
        v_account_id,
        p_service_id,
        p_supplier_org_id,
        IF(p_acceptor_org_id IS NULL, p_supplier_org_id, p_acceptor_org_id),
        @history_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_unsafe_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_unsafe_delete`(
    IN p_zhek DECIMAL(2,0)
  , IN p_account DECIMAL(10,0)
  , IN p_service_id INT
  , IN p_out_date DATE
  , IN p_tx_id INT
)
    MODIFIES SQL DATA
    DETERMINISTIC
    COMMENT 'Производит удаление записи'
BEGIN
    DECLARE v_found_id INT;
    DECLARE v_in_date DATE;

    SELECT
            m_history.id
          , m_history.in_date
        INTO
            v_found_id
          , v_in_date
    FROM m_supplier
    INNER JOIN m_account ON
            m_account.id = m_supplier.account_id
        AND m_account.zhek_code = p_zhek
        AND m_account.account = p_account
    INNER JOIN m_history ON
            m_history.id = m_supplier.history_id
        AND m_history.cancelled = 0
        AND m_history.out_date >= p_out_date
    WHERE
            m_supplier.service_id = p_service_id
    LIMIT 1;

    UPDATE m_history
    SET
        cancelled = 1
    WHERE
            id = v_found_id;
    INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(p_tx_id, v_found_id);

    INSERT INTO m_history(in_date, out_date) VALUE(v_in_date, p_out_date);
    SET @history_id = LAST_INSERT_ID();
    INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(p_tx_id, @history_id);
    INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(p_tx_id, @history_id);

    INSERT INTO m_supplier(
        account_id
      , service_id
      , supplier_org_id
      , acceptor_org_id
      , history_id)
        SELECT
            account_id
          , service_id
          , supplier_org_id
          , acceptor_org_id
          , @history_id
        FROM m_supplier
        WHERE
                m_supplier.history_id = v_found_id
        LIMIT 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_unsafe_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_unsafe_update`(
    IN p_zhek DECIMAL(2,0) UNSIGNED
  , IN p_account DECIMAL(10,0) UNSIGNED
  , IN p_service_id SMALLINT(5) UNSIGNED
  , IN p_supplier_org_id INT(11) UNSIGNED
  , IN p_acceptor_org_id INT(11) UNSIGNED
  , IN p_in_date DATE
  , IN p_tx_id INT(11) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Обновляет (не устанавливает!) атрибуты за указанный период'
BEGIN
    DECLARE v_record_id BIGINT(20) UNSIGNED;
    DECLARE v_account_id INT(11) UNSIGNED;
    DECLARE v_history_id BIGINT(20) UNSIGNED;
    DECLARE v_supplier_org_id INT(11) UNSIGNED;
    DECLARE v_acceptor_org_id INT(11) UNSIGNED;
    DECLARE v_in_date DATE;

    SELECT
            m_supplier.k
          , m_account.id
          , m_supplier.history_id
          , m_history.in_date
          , m_supplier.supplier_org_id
          , m_supplier.acceptor_org_id
        INTO
            v_record_id
          , v_account_id
          , v_history_id
          , v_in_date
          , v_supplier_org_id
          , v_acceptor_org_id
    FROM m_supplier
    INNER JOIN m_account ON
            m_account.id = m_supplier.account_id
        AND m_account.zhek_code = p_zhek
        AND m_account.account = p_account
    INNER JOIN m_history ON
            m_history.id = m_supplier.history_id
        AND m_history.cancelled = 0
        AND m_history.out_date = '9999-12-31'
    WHERE
            m_supplier.service_id = p_service_id
    LIMIT 1;

    UPDATE m_history
    SET
        cancelled = 1
    WHERE
            id = v_history_id;

    INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(p_tx_id, v_history_id);

    IF v_in_date <> p_in_date THEN
        INSERT INTO m_history(in_date, out_date) VALUE(v_in_date, DATE_SUB(p_in_date, INTERVAL 1 DAY));
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(p_tx_id, @history_id);
        INSERT INTO m_supplier(
            account_id
        ,   service_id
        ,   supplier_org_id
        ,   acceptor_org_id
        ,   history_id
        ) VALUE(
            v_account_id
        ,   p_service_id
        ,   v_supplier_org_id
        ,   v_acceptor_org_id
        ,   @history_id
        );
    END IF;
    
    INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, '9999-12-31');
    SET @history_id = LAST_INSERT_ID();
    INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(p_tx_id, @history_id);
    INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(p_tx_id, @history_id);

    IF p_supplier_org_id IS NULL THEN
        SET p_supplier_org_id = (SELECT supplier_org_id FROM m_supplier WHERE account_id = v_account_id ORDER BY history_id DESC LIMIT 1);
    END IF;

    IF p_acceptor_org_id IS NULL THEN
        SET p_acceptor_org_id = (SELECT acceptor_org_id FROM m_supplier WHERE account_id = v_account_id ORDER BY history_id DESC LIMIT 1);
    END IF;

    INSERT INTO m_supplier(
        account_id
      , service_id
      , supplier_org_id
      , acceptor_org_id
      , history_id)
    VALUE(
        v_account_id
      , p_service_id
      , p_supplier_org_id
      , p_acceptor_org_id
      , @history_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_update`(
    IN p_zhek DECIMAL(2,0) UNSIGNED
  , IN p_account DECIMAL(10,0) UNSIGNED
  , IN p_service_id SMALLINT(5) UNSIGNED
  , IN p_supplier_org_id INT(11) UNSIGNED
  , IN p_acceptor_org_id INT(11) UNSIGNED
  , IN p_in_date DATE
  , IN p_user_id INT(11) UNSIGNED
  , IN p_user_app VARCHAR(255) CHARACTER SET 'utf8'
  , IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Обновляет (не устанавливает!) атрибуты за указанный период'
BEGIN
    DECLARE v_found_id BIGINT(20) UNSIGNED;
    DECLARE v_supplier_org_id INT(11) UNSIGNED;
    DECLARE v_acceptor_org_id INT(11) UNSIGNED;

    IF p_supplier_org_id IS NULL AND p_acceptor_org_id IS NULL THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = "One of supplier_org_id and acceptor_org_id MUST be not null!";
    END IF;

    START TRANSACTION;
        SELECT
                m_supplier.k
              , m_supplier.supplier_org_id
              , m_supplier.acceptor_org_id
            INTO
                v_found_id
              , v_supplier_org_id
              , v_acceptor_org_id
        FROM m_supplier
        INNER JOIN m_history ON
                m_history.id = m_supplier.history_id
            AND m_history.out_date = '9999-12-31'
        INNER JOIN m_account ON
                m_account.id = m_supplier.account_id
            AND m_account.zhek_code = p_zhek
            AND m_account.account = p_account
        WHERE
                m_supplier.service_id = p_service_id
        ORDER BY m_history.id DESC
        LIMIT 1;

        IF v_found_id IS NULL THEN
            SET @errmsg = CONCAT("No active record with zhek=", p_zhek, ", account=", p_account, ", service_id=", p_service_id, " were found!");
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = @errmsg;
        ELSE
            CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

            CALL supplier_unsafe_update(
                p_zhek,
                p_account,
                p_service_id,
                IF(p_supplier_org_id IS NULL, v_supplier_org_id, p_supplier_org_id),
                IF(p_acceptor_org_id IS NULL, v_acceptor_org_id, p_acceptor_org_id),
                p_in_date,
                @tx_id);
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_update_attributes_by_house` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_update_attributes_by_house`(
    IN p_house_id INT,
    IN p_serivce_id INT,
    IN p_supplier_org_id INT,
    IN p_acceptor_org_id INT,
    IN p_in_date DATE,
    IN p_out_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255),
    IN p_user_ip VARCHAR(50)
)
    MODIFIES SQL DATA
    COMMENT 'Обновляет записи по house_id'
BEGIN
    DECLARE v_zhek DECIMAL(2, 0);
    DECLARE v_account DECIMAL(10, 0);

    DECLARE v_done TINYINT(1);
    DECLARE v_homes_cursor CURSOR FOR SELECT
            m_account.zhek_code
          , m_account.account
          , m_supplier.id
    FROM m_home
    INNER JOIN m_account ON
            m_account.home_id = m_home.home_id
    INNER JOIN m_supplier ON
            m_supplier.account_id = m_account.id
        AND m_supplier.service_id = p_serivce_id
    INNER JOIN m_history ON
            m_history.id = m_supplier.history_id
        AND m_history.cancelled = 0
    WHERE
            m_home.house_id = p_house_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    START TRANSACTION;
        OPEN v_homes_cursor;

        CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

        l: LOOP
            FETCH v_homes_cursor INTO v_zhek, v_account;
            IF v_done = 1 THEN
                LEAVE l;
            END IF;

            CALL supplier_unsafe_update_attributes(v_zhek, v_account, p_serivce_id, p_supplier_org_id, p_acceptor_org_id, p_in_date, @tx_id);
        END LOOP l;

        CLOSE v_homes_cursor;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `supplier_update_by_house` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `supplier_update_by_house`(
	IN `p_house_id` INT(11) UNSIGNED
  , IN `p_service_id` INT(11) UNSIGNED
  , IN `p_supplier_org_id` INT(11) UNSIGNED
  , IN `p_acceptor_org_id` INT(11) UNSIGNED
  , IN `p_in_date` DATE
  , IN `p_user_id` INT(11) UNSIGNED
  , IN `p_user_app` VARCHAR(255) CHARACTER SET 'utf8'
  , IN `p_user_ip` VARCHAR(50) CHARACTER SET 'utf8'
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Обновляет записи по house_id, возвращает ResultSet, в случае ошибки'
BEGIN
	DECLARE v_zhek DECIMAL(2, 0) UNSIGNED;
	DECLARE v_account DECIMAL(10, 0) UNSIGNED;
	DECLARE v_supplier_id BIGINT(20) UNSIGNED;

	DECLARE v_error TINYINT(1);

	DECLARE v_done TINYINT(1);
	DECLARE v_homes_cursor CURSOR FOR SELECT
			m_account.zhek_code
		  , m_account.account
		  , m_supplier.k
	FROM m_home
	INNER JOIN m_account ON
			m_account.home_id = m_home.id
	INNER JOIN m_supplier ON
			m_supplier.account_id = m_account.id
		AND m_supplier.service_id = p_service_id
	INNER JOIN m_history ON
			m_history.id = m_supplier.history_id
		AND m_history.cancelled = 0
		AND m_history.out_date = '9999-12-31'
	WHERE
			m_home.house_id = p_house_id;

	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
		SET v_done = 1;
	END;

	CALL supplier_op_prepare();

	START TRANSACTION;
		OPEN v_homes_cursor;

		INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
		SET @tx_id = LAST_INSERT_ID();

		l: LOOP
			FETCH v_homes_cursor INTO v_zhek, v_account, v_supplier_id;
			IF v_done = 1 THEN LEAVE l; END IF;

			IF v_supplier_id IS NULL THEN
				SIGNAL SQLSTATE '45000';
			ELSEIF v_error IS NULL THEN
				CALL supplier_unsafe_update(v_zhek, v_account, p_service_id, p_supplier_org_id, p_acceptor_org_id, p_in_date, @tx_id);
			END IF;
		END LOOP l;

		CLOSE v_homes_cursor;
	IF v_error = 1 THEN
		SELECT * FROM tmp_supplier_op_error;
		ROLLBACK;
	ELSE
		COMMIT;
	END IF;

	CALL supplier_op_cleanup();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `throw_error` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `throw_error`(IN message VARCHAR(256))
BEGIN
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = message;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `town_district_old_to_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `town_district_old_to_new`(
)
BEGIN
	DECLARE v_town_id INT;
	DECLARE v_district_id INT;

	DECLARE c CURSOR FOR SELECT town_id, district_id FROM main.m_town_district;

	OPEN c;
	LOOP
		FETCH c INTO v_town_id, v_district_id;

		CALL town_district_set_attributes(v_town_id, v_district_id, '0000-00-00', '9999-12-31', 0, 'stored routing town_district_old_to_new', 0);
	END LOOP;
	CLOSE c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `town_district_set_attributes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `town_district_set_attributes`(
	IN p_town_id SMALLINT(5),
	IN p_district_id SMALLINT(5),
    IN p_in_date DATE,
    IN p_out_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    MODIFIES SQL DATA
    COMMENT 'Устанавливает атрибуты поставщика для указанного периода'
BEGIN
    
    
    
    
    DECLARE bef_district_id INT;
    DECLARE bef_history_id INT;
    DECLARE bef_in_date DATE;
    DECLARE bef_out_date DATE;
    DECLARE bef_tx_id INT;

    DECLARE aft_district_id INT;
    DECLARE aft_history_id INT;
    DECLARE aft_in_date DATE;
    DECLARE aft_out_date DATE;
    DECLARE aft_tx_id INT;

    
    DECLARE in_eq_p_in INT;
    DECLARE out_eq_p_out INT;

    DECLARE btwn_history_id INT;
    DECLARE btwn_tx_id INT;
    DECLARE bwtn_cur_done TINYINT(1);
    
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_town_district
    INNER JOIN m_history ON
            m_history.id = m_town_district.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_town_district.town_id = p_town_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET bwtn_cur_done = 1;
    END;

    START TRANSACTION;
        SET p_out_date = IF(p_out_date IS NULL, '9999-12-31', p_out_date);

        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();
        
        SELECT
                m_town_district.district_id
              , m_town_district.history_id
              , m_history.in_date
              , m_history.out_date
            INTO
            	bef_district_id
              , bef_history_id
              , bef_in_date
              , bef_out_date
        FROM m_town_district
        INNER JOIN m_history ON
                m_history.id = m_town_district.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date < p_in_date
            AND m_history.out_date >= p_in_date
        WHERE
                m_town_district.town_id = p_town_id
        LIMIT 1;

        
        SELECT
                m_town_district.district_id
              , m_town_district.history_id
              , m_history.in_date
              , m_history.out_date
            INTO
            	bef_district_id
              , bef_history_id
              , bef_in_date
              , bef_out_date
        FROM m_town_district
        INNER JOIN m_history ON
                m_history.id = m_town_district.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date <= p_out_date
            AND m_history.out_date > p_out_date
        WHERE
                m_town_district.town_id = p_town_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                in_eq_p_in
        FROM m_town_district
        INNER JOIN m_history ON
                m_history.id = m_town_district.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date = p_in_date
        WHERE
                m_town_district.town_id = p_town_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                out_eq_p_out
        FROM m_town_district
        INNER JOIN m_history ON
                m_history.id = m_town_district.history_id
            AND m_history.cancelled = 0
            AND m_history.out_date = p_out_date
        WHERE
                m_town_district.town_id = p_town_id
        LIMIT 1;


        IF bef_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = bef_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, bef_history_id);
        END IF;

        
        SET bwtn_cur_done = 0;
        OPEN btwn_cur;
        btwn: LOOP
            FETCH btwn_cur INTO btwn_history_id;
            IF bwtn_cur_done = 1 THEN
                LEAVE btwn;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = btwn_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, btwn_history_id);
        END LOOP btwn;

        IF aft_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = aft_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, aft_history_id);
        END IF;



        IF
                bef_history_id IS NOT NULL
            AND bef_in_date <= DATE_SUB(p_in_date, INTERVAL 1 DAY)
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(bef_in_date, DATE_SUB(p_in_date, INTERVAL 1 DAY));
            SET @bef_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @bef_history_id);
        END IF;

        
        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

        IF
                aft_history_id IS NOT NULL
            AND DATE_ADD(p_out_date, INTERVAL 1 DAY) <= aft_out_date
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(DATE_ADD(p_out_date, INTERVAL 1 DAY), aft_out_date);
            SET @aft_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @aft_history_id);
        END IF;

        IF
                @bef_history_id IS NOT NULL
        THEN
            INSERT INTO m_town_district(
                town_id
              , district_id
              , history_id)
            VALUE (
            	p_town_id
              , bef_district_id
              , @bef_history_id
            );
        END IF;

        INSERT INTO m_town_district(
            town_id
          , district_id
          , history_id)
        VALUE (
        	p_town_id
          , p_district_id
          , @history_id
        );

        IF
                @aft_history_id IS NOT NULL
        THEN
            INSERT INTO m_town_district(
                town_id
              , district_id
              , history_id)
            VALUE (
            	p_town_id
              , aft_district_id
              , @aft_history_id
            );
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `transaction_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `transaction_add`(
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50),
    OUT r_tx_id INT
)
    MODIFIES SQL DATA
    COMMENT 'Производит вставку новой транзакции, возвращая её id'
BEGIN
    INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
    SET r_tx_id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `transaction_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `transaction_create`(
    IN  p_user_id VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_api_key_id INT(11) UNSIGNED
  , OUT p_tx_id BIGINT(20) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Создаёт новую транзакцию по API-ключу, правильно устанавливая id пользователей'
BEGIN
    DECLARE v_api_key_is_internal TINYINT(1) UNSIGNED;

    IF NOT api_key_is_valid(p_api_key_id) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'transaction_create: invalid api key';
    END IF;

    SELECT
            m_api_key.is_internal
        INTO
            v_api_key_is_internal
    FROM m_api_key
    WHERE
            m_api_key.id = p_api_key_id
    LIMIT 1;

    IF v_api_key_is_internal THEN
        SET @user_id = (
            SELECT CAST(p_user_id AS UNSIGNED INTEGER)
            FROM DUAL
        );

        INSERT INTO m_transaction(
            user_id
          , api_key_id
        ) VALUE (
            @user_id
          , p_api_key_id
        );
    ELSE
        INSERT INTO m_transaction(
            foreign_user_id
          , api_key_id
        ) VALUE (
            p_user_id
          , p_api_key_id
        );
    END IF;

    SET p_tx_id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_meter_by_normative` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_meter_by_normative`(
	IN p_record_k INT(11),
    IN p_date_begin DATE,
	IN p_date_end DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255) CHARACTER SET 'utf8',
    IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
    COMMENT 'Змінює запис про переведення лічильника на норматив (відміняє дію попереднього та створює новий)'
BEGIN
	DECLARE v_meter_id INT(11);
	DECLARE v_old_history_id BIGINT(20);


    START TRANSACTION;
		CALL transaction_add(p_user_id, p_user_app, p_user_ip, @tx_id);

		select meter_id, history_id into v_meter_id, v_old_history_id
			from main_new.m_meter_by_normative
		where k = p_record_k;

		CALL main_new.history_cancel(v_old_history_id, @tx_id);

		insert into main_new.m_history set cancelled = false;
		SET @history_id = LAST_INSERT_ID();
		INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
		INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);


		INSERT INTO main_new.m_meter_by_normative
			(meter_id,
			date_begin,
			date_end,
			history_id)
		VALUES
			(p_meter_id,
			p_date_begin,
			p_date_end,
			@history_id);

    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vmte_buildpart_meter_service` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vmte_buildpart_meter_service`(
    IN  p_buildpart_no VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_meter_no VARCHAR(32) CHARACTER SET 'utf8'
  , IN  p_service_id SMALLINT(5) UNSIGNED
  , IN  p_measurement_unit_id SMALLINT(5) UNSIGNED
  , IN  p_prior_data DECIMAL(18, 8) UNSIGNED
  , IN  p_current_data DECIMAL(18, 8) UNSIGNED
  , IN  p_delta DECIMAL(18, 8)
  , IN  p_correction DECIMAL(18, 8)
  , IN  p_time_without_meter SMALLINT(5) UNSIGNED
  , IN  p_dt DATE
  , IN  p_user_id VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_api_key_id INT(11) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Управление домовыми счётчиками'
BEGIN
    DECLARE v_month_id SMALLINT(5) UNSIGNED;

    DECLARE v_meter_id INT(11) UNSIGNED;

    DECLARE v_tx_id BIGINT(20) UNSIGNED;

    IF NOT api_key_is_valid(p_api_key_id) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'vmte_buildpart_meter_service: invalid api key';
    END IF;

    SET v_month_id = (
        SELECT
                m_meter_calendar.id
        FROM m_meter_calendar
        WHERE
                m_meter_calendar.first_day <= p_dt
            AND m_meter_calendar.last_day >= p_dt
        LIMIT 1
    );

    IF v_month_id IS NULL THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'alter_personal_meter: unknown month';
    END IF;

    SET v_meter_id = (
        SELECT
                m_meter.id
        FROM m_meter
        WHERE
                m_meter.meter_no = p_meter_no
            AND m_meter.service_id = p_service_id
            AND m_meter.measurement_unit_id = p_measurement_unit_id
        LIMIT 1
    );

    IF v_meter_id IS NULL THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'vmte_buildpart_meter_service: no meter were found';
    END IF;

    IF      p_prior_data IS NOT NULL
        OR  p_current_data IS NOT NULL
        OR  p_delta IS NOT NULL
    THEN
        CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);

        CALL history_create('0000-00-00', '9999-12-31', v_tx_id, @history_id);
        INSERT INTO m_transaction_params(
            transaction_id
          , history_id
        ) VALUE (
            v_tx_id
          , @history_id
        );

        INSERT INTO m_meter_data(
            meter_id
          , month_id
          , prior_data
          , current_data
          , delta
          , history_id
        ) VALUE (
            v_meter_id
          , v_month_id
          , p_prior_data
          , p_current_data
          , p_delta
          , @history_id
        );
    END IF;

    IF      p_correction IS NOT NULL
        OR  p_time_without_meter IS NOT NULL
    THEN
        IF v_tx_id IS NULL THEN
            CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        END IF;

        CALL history_create('0000-00-00', '9999-12-31', v_tx_id, @history_id);
        INSERT INTO m_transaction_params(
            transaction_id
          , history_id
        ) VALUE (
            v_tx_id
          , @history_id
        );

        INSERT INTO m_meter_correction (
            meter_id
          , month_id
          , correction
          , measurement_unit_id
          , time_without_meter
          , history_id
        ) VALUE (
            v_meter_id
          , v_month_id
          , p_correction
          , 1 
          , p_time_without_meter
          , @history_id
        );
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vmte_home_attributes_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vmte_home_attributes_set`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_square DECIMAL(10, 2) UNSIGNED
  , IN  p_qkv DECIMAL(18, 8) UNSIGNED
  , IN  p_qmzk DECIMAL(18, 8) UNSIGNED
  , IN  p_norma_water SMALLINT(5) UNSIGNED
  , IN  p_in_date DATE
  , IN  p_out_date DATE
  , IN  p_tx_id BIGINT(20) UNSIGNED
)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT 'Устанавливает (не обновляет!) атрибуты за указанный период'
BEGIN
    DECLARE bef_square DECIMAL(10, 2) UNSIGNED;
    DECLARE bef_qkv DECIMAL(18, 8) UNSIGNED;
    DECLARE bef_qmzk DECIMAL(18, 8) UNSIGNED;
    DECLARE bef_norma_water SMALLINT(5) UNSIGNED;
    DECLARE bef_history_id BIGINT(20) UNSIGNED;
    DECLARE bef_in_date DATE;

    DECLARE aft_square DECIMAL(10, 2) UNSIGNED;
    DECLARE aft_qkv DECIMAL(18, 8) UNSIGNED;
    DECLARE aft_qmzk DECIMAL(18, 8) UNSIGNED;
    DECLARE aft_norma_water SMALLINT(5) UNSIGNED;
    DECLARE aft_history_id BIGINT(20) UNSIGNED;
    DECLARE aft_out_date DATE;

    DECLARE v_done TINYINT(1) UNSIGNED;
    DECLARE btwn_history_id BIGINT(20) UNSIGNED;
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM vmte_attributes
    INNER JOIN m_history ON
            m_history.id = vmte_attributes.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            vmte_attributes.home_id = p_home_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET v_done = 1;
    END;

    IF p_in_date IS NULL THEN
        SET p_in_date = '0000-00-00';
    END IF;

    IF p_out_date IS NULL THEN
        SET p_out_date = '9999-12-31';
    END IF;

    
    SELECT
            vmte_attributes.square
          , vmte_attributes.qkv
          , vmte_attributes.qmzk
          , vmte_attributes.norma_water
          , m_history.id
          , m_history.in_date
        INTO
            bef_square
          , bef_qkv
          , bef_qmzk
          , bef_norma_water
          , bef_history_id
          , bef_in_date
    FROM vmte_attributes
    INNER JOIN m_history ON
            m_history.id = vmte_attributes.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date < p_in_date
        AND m_history.out_date >= p_in_date
    WHERE
            vmte_attributes.home_id = p_home_id
    LIMIT 1;

    IF bef_history_id IS NOT NULL THEN
        CALL history_cancel(bef_history_id, p_tx_id);

        CALL history_create(
            bef_in_date
          , DATE_SUB(p_in_date, INTERVAL 1 DAY)
          , p_tx_id
          , @history_id
        );

        INSERT INTO vmte_attributes(
            home_id
          , square
          , qkv
          , qmzk
          , norma_water
          , history_id
        ) VALUE (
            p_home_id
          , bef_square
          , bef_qkv
          , bef_qmzk
          , bef_norma_water
          , @history_id
        );
    END IF;
    

    
    OPEN btwn_cur;
    btwn_loop: LOOP
        FETCH btwn_cur INTO btwn_history_id;
        IF v_done THEN
            SET v_done = 0;
            LEAVE btwn_loop;
        END IF;

        CALL history_cancel(btwn_history_id, p_tx_id);
    END LOOP btwn_loop;
    CLOSE btwn_cur;
    

    
    CALL history_create(p_in_date, p_out_date, p_tx_id, @history_id);
    INSERT INTO m_transaction_params(
        transaction_id
      , history_id
    ) VALUE (
        p_tx_id
      , @history_id
    );

    INSERT INTO vmte_attributes(
        home_id
      , square
      , qkv
      , qmzk
      , norma_water
      , history_id
    ) VALUE (
        p_home_id
      , p_square
      , p_qkv
      , p_qmzk
      , p_norma_water
      , @history_id
    );
    

    
    SELECT
            vmte_attributes.square
          , vmte_attributes.qkv
          , vmte_attributes.qmzk
          , vmte_attributes.norma_water
          , m_history.id
          , m_history.out_date
        INTO
            aft_square
          , aft_qkv
          , aft_qmzk
          , aft_norma_water
          , aft_history_id
          , aft_out_date
    FROM vmte_attributes
    INNER JOIN m_history ON
            m_history.id = vmte_attributes.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date <= p_out_date
        AND m_history.out_date > p_out_date
    WHERE
            vmte_attributes.home_id = p_home_id
    LIMIT 1;

    IF aft_history_id IS NOT NULL THEN
        IF bef_history_id <> aft_history_id THEN
            CALL history_cancel(aft_history_id, p_tx_id);
        END IF;

        CALL history_create(
            DATE_ADD(p_out_date, INTERVAL 1 DAY)
          , aft_out_date
          , p_tx_id
          , @history_id
        );

        INSERT INTO vmte_attributes(
            home_id
          , square
          , qkv
          , qmzk
          , norma_water
          , history_id
        ) VALUE (
            p_home_id
          , aft_square
          , aft_qkv
          , aft_qmzk
          , aft_norma_water
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vmte_home_meter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vmte_home_meter`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_buildpart_no VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_location_id SMALLINT(5) UNSIGNED

  , IN  p_meter_no VARCHAR(32) CHARACTER SET 'utf8'
  , IN  p_service_id SMALLINT(5) UNSIGNED
  , IN  p_measurement_unit_id SMALLINT(5) UNSIGNED
  

  , IN  p_action TINYINT(1) UNSIGNED
  , IN  p_dt DATE
  , IN  p_user_id VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_api_key_id INT(11) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Управление установкой/снятием счётчиков'
BEGIN
    DECLARE v_meter_id INT(11) UNSIGNED;
    DECLARE v_meter_org_id INT(11) UNSIGNED;

    DECLARE v_tx_id BIGINT(20) UNSIGNED;

    IF p_action NOT IN(0, 1) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'vmte_home_meter: unknown action';
    END IF;

    IF NOT api_key_is_valid(p_api_key_id) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'vmte_home_meter: invalid api key';
    END IF;

    SET v_meter_id = (
        SELECT
                m_meter.id
        FROM m_meter
        WHERE
                m_meter.meter_no = p_meter_no
            AND m_meter.service_id = p_service_id
            AND m_meter.measurement_unit_id = p_measurement_unit_id
        LIMIT 1
    );

    SET v_meter_org_id = (
        SELECT
                m_api_key.org_id
        FROM m_api_key
        WHERE
                m_api_key.id = p_api_key_id
        LIMIT 1
    );

    IF      p_action = 1
        AND v_meter_id IS NULL
    THEN
        INSERT INTO m_meter(
            meter_no
          , service_id
          , measurement_unit_id
          , meter_org_id
        ) VALUE (
            p_meter_no
          , p_service_id
          , p_measurement_unit_id
          , v_meter_org_id
        );
        SET v_meter_id = LAST_INSERT_ID();
    END IF;

    IF p_home_id IS NULL THEN
        IF p_buildpart_no IS NULL THEN
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = 'vmte_home_meter: buildpart&home are null';
        END IF;

        CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        
        IF p_action = 1 THEN
            CALL buildpart_meter_set(
                p_buildpart_no
              , v_meter_id
              , p_dt
              , NULL
              , v_tx_id
            );
        ELSE
            CALL buildpart_meter_outdate(v_meter_id, p_dt, v_tx_id);
        END IF;
    ELSE
        IF v_tx_id IS NULL THEN
            CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        END IF;

        IF p_action = 1 THEN
            CALL meter_at_home_set(
                v_meter_id
              , p_home_id
              , p_location_id
              , p_dt
              , NULL
              , v_tx_id
            );
        ELSE
            CALL meter_at_home_outdate(v_meter_id, p_dt, v_tx_id);
        END IF;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vmte_home_meter_service` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vmte_home_meter_service`(
    IN  p_meter_no VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_service_id SMALLINT(5) UNSIGNED
  , IN  p_measurement_unit_id SMALLINT(5) UNSIGNED

  , IN  p_lock_dt DATE
  , IN  p_unlock_dt DATE

  , IN  p_prior_data DECIMAL(18, 8) UNSIGNED
  , IN  p_current_data DECIMAL(18, 8) UNSIGNED

  , IN  p_home_id INT(11) UNSIGNED
  , IN  p_location_id SMALLINT(5) UNSIGNED

  , IN  p_dt DATE

  , IN  p_user_id VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_api_key_id INT(11) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Управление персональными счётчиками'
BEGIN
    DECLARE v_meter_id INT(11) UNSIGNED;
    DECLARE v_tx_id BIGINT(20) UNSIGNED;

    IF NOT api_key_is_valid(p_api_key_id) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'vmte_home_meter_service: api key is not valid';
    END IF;

    SET v_meter_id = (
        SELECT
                m_meter.id
        FROM m_meter
        WHERE
                m_meter.meter_no = p_meter_no
            AND m_meter.service_id = p_service_id
            AND m_meter.measurement_unit_id = p_measurement_unit_id
        LIMIT 1
    );

    IF v_meter_id IS NULL THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'vmte_home_meter_service: no meter were found';
    END IF;

    
    IF      p_home_id IS NOT NULL
        AND p_location_id IS NOT NULL
    THEN
        CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        
    END IF;
    

    
    IF      p_prior_data IS NOT NULL
        OR  p_current_data IS NOT NULL
    THEN
        SET @month_id = (
            SELECT
                    m_meter_calendar.id
            FROM m_meter_calendar
            WHERE
                    m_meter_calendar.first_day <= p_dt
                AND m_meter_calendar.last_day >= p_dt
            LIMIT 1
        );

        IF @month_id IS NULL THEN
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = 'vmte_home_meter_service: unknown month';
        END IF;

        IF v_tx_id IS NULL THEN
            CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        END IF;

        CALL history_create('0000-00-00', '9999-12-31', v_tx_id, @history_id);
        INSERT INTO m_transaction_params(
            transaction_id
          , history_id
        ) VALUE (
            v_tx_id
          , @history_id
        );

        INSERT INTO m_meter_data(
            meter_id
          , month_id
          , prior_data
          , current_data
          , delta
          , history_id
        ) VALUE (
            v_meter_id
          , @month_id
          , p_prior_data
          , p_current_data
          , (p_current_data - p_prior_data)
          , @history_id
        );
    END IF;
    

    
    IF      p_lock_dt IS NOT NULL
        OR  p_unlock_dt IS NOT NULL
    THEN
        IF v_tx_id IS NULL THEN
            CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        END IF;

        CALL history_create('0000-00-00', '9999-12-31', v_tx_id, @history_id);
        INSERT INTO m_transaction_params(
            transaction_id
          , history_id
        ) VALUE (
            v_tx_id
          , @history_id
        );

        INSERT INTO m_meter_test(
            meter_id
          , lock_dt
          , unlock_dt
          , history_id
        ) VALUE (
            v_meter_id
          , p_lock_dt
          , p_unlock_dt
          , @history_id
        );
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vmte_home_service` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vmte_home_service`(
    IN  p_home_id INT(11) UNSIGNED

  , IN  p_square DECIMAL(10, 2) UNSIGNED
  , IN  p_qkv DECIMAL(18, 8) UNSIGNED
  , IN  p_qmzk DECIMAL(18, 8) UNSIGNED
  , IN  p_norma_water SMALLINT(5) UNSIGNED
  , IN  p_heat_dt DATE

  , IN  p_heat_status TINYINT(1) UNSIGNED
  , IN  p_water_status TINYINT(1) UNSIGNED

  , IN  p_transit_status TINYINT(1) UNSIGNED
  , IN  p_mzk_status TINYINT(1) UNSIGNED

  , IN  p_lock_dt DATE
  , IN  p_unlock_dt DATE

  , IN  p_water_dt DATE

  , IN  p_dt DATE
  , IN  p_user_id VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_api_key_id INT(11) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Управление услугами по персональным счётчиками'
BEGIN
    DECLARE v_tx_id BIGINT(20) UNSIGNED;

    IF NOT api_key_is_valid(p_api_key_id) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'vmte_home_service: invalid api key';
    END IF;

    IF      p_square IS NOT NULL
        OR  p_qkv IS NOT NULL
        OR  p_qmzk IS NOT NULL
        OR  p_norma_water IS NOT NULL
    THEN
        SELECT
                vmte_attributes.square
              , vmte_attributes.qkv
              , vmte_attributes.qmzk
              , vmte_attributes.norma_water
            INTO
                @square
              , @qkv
              , @qmzk
              , @norma_water
        FROM vmte_attributes
        INNER JOIN m_history ON
                m_history.id = vmte_attributes.history_id
            AND m_history.cancelled = 0
            AND m_history.out_date = '9999-12-31'
        LIMIT 1;

        IF      @square IS NULL
            AND p_square IS NULL
            AND p_qkv IS NULL
            AND p_qmzk IS NULL
            AND p_norma_water IS NULL
        THEN
            SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT = 'vmte_home_service: no home were found';
        END IF;

        IF p_square      IS NULL THEN SET p_square = @square; END IF;
        IF p_qkv         IS NULL THEN SET p_qkv = @qkv; END IF;
        IF p_qmzk        IS NULL THEN SET p_qmzk = @qmzk; END IF;
        IF p_norma_water IS NULL THEN SET p_norma_water = @norma_water; END IF;

        CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);

        CALL vmte_home_attributes_set(
            p_home_id
          , p_square
          , p_qkv
          , p_qmzk
          , p_norma_water
          , p_heat_dt 
          , NULL 
          , v_tx_id
        );
    END IF;

    IF p_heat_status IS NOT NULL THEN
        IF v_tx_id IS NULL THEN
            CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        END IF;

        SET @in_date = NULL;
        SET @out_date = NULL;

        IF p_heat_status = 1 THEN
            SET @in_date = p_heat_dt;
            SET @out_date = '9999-12-31';
        ELSE
            SET @in_date = (
                SELECT
                        m_history.out_date
                FROM m_service_exists
                INNER JOIN m_history ON
                        m_history.id = m_service_exists.history_id
                    AND m_history.cancelled = 0
                    AND m_history.out_date = '9999-12-31'
                WHERE
                        m_service_exists.home_id = p_home_id
                    AND m_service_exists.service_id = 5
                    AND m_service_exists.supplier_id = 4269
                LIMIT 1
            );
            SET @out_date = p_heat_dt;
        END IF;

        CALL service_exists_set(
            p_home_id
          , 5 
          , 4269 
          , @in_date
          , @out_date
          , v_tx_id
        );
    END IF;

    IF p_water_status IS NOT NULL THEN
        IF v_tx_id IS NULL THEN
            CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        END IF;

        SET @in_date = NULL;
        SET @out_date = NULL;

        IF p_water_status = 1 THEN
            SET @in_date = p_water_dt;
            SET @out_date = '9999-12-31';
        ELSE
            SET @in_date = (
                SELECT
                        m_history.out_date
                FROM m_service_exists
                INNER JOIN m_history ON
                        m_history.id = m_service_exists.history_id
                    AND m_history.cancelled = 0
                    AND m_history.out_date = '9999-12-31'
                WHERE
                        m_service_exists.home_id = p_home_id
                    AND m_service_exists.service_id = 4
                    AND m_service_exists.supplier_id = 4269
                LIMIT 1
            );
            SET @out_date = p_water_dt;
        END IF;

        CALL service_exists_set(
            p_home_id
          , 4 
          , 4269 
          , @in_date
          , @out_date
          , v_tx_id
        );
    END IF;

    IF p_transit_status IS NOT NULL THEN
        IF v_tx_id IS NULL THEN
            CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        END IF;

        SET @in_date = NULL;
        SET @out_date = NULL;

        IF p_transit_status = 1 THEN
            SET @in_date = p_heat_dt;
            SET @out_date = '9999-12-31';
        ELSE
            SET @in_date = (
                SELECT
                        m_history.in_date
                FROM m_se_optional_parts
                INNER JOIN m_history ON
                        m_history.id = m_se_optional_parts.history_id
                    AND m_history.cancelled = 0
                    AND m_history.out_date = '9999-12-31'
                WHERE
                        m_se_optional_parts.home_id = p_home_id
                    AND m_se_optional_parts.optional_parts = 2
                LIMIT 1
            );
            SET @out_date = p_heat_dt;
        END IF;

        CALL se_optional_parts_set(
            p_home_id
          , 2 
          , @in_date
          , @out_date
          , v_tx_id
        );
    END IF;

    IF p_mzk_status IS NOT NULL THEN
        IF v_tx_id IS NULL THEN
            CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);
        END IF;

        SET @in_date = NULL;
        SET @out_date = NULL;

        IF p_mzk_status = 1 THEN
            SET @in_date = p_heat_dt;
            SET @out_date = '9999-12-31';
        ELSE
            SET @in_date = (
                SELECT
                        m_history.in_date
                FROM m_se_optional_parts
                INNER JOIN m_history ON
                        m_history.id = m_se_optional_parts.history_id
                    AND m_history.cancelled = 0
                    AND m_history.out_date = '9999-12-31'
                WHERE
                        m_se_optional_parts.home_id = p_home_id
                    AND m_se_optional_parts.optional_parts = 3
                LIMIT 1
            );
            SET @out_date = p_heat_dt;
        END IF;

        CALL se_optional_parts_set(
            p_home_id
          , 3 
          , @in_date
          , @out_date
          , v_tx_id
        );
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vmte_home_service_out` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vmte_home_service_out`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_service_id SMALLINT(5) UNSIGNED
  , IN  p_date_begin DATE
  , IN  p_date_end DATE
  , IN  p_dt DATE
  , IN  p_user_id VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_api_key_id INT(11) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Управление временным отсутствием услуг'
BEGIN
    DECLARE v_tx_id BIGINT(20) UNSIGNED;

    CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);

    CALL history_create('0000-00-00', '9999-12-31', v_tx_id, @history_id);
    INSERT INTO m_transaction_params(
        transaction_id
      , history_id
    ) VALUE (
        v_tx_id
      , @history_id
    );

    INSERT INTO m_service_out(
        home_id
      , service_id
      , supplier_id
      , date_begin
      , date_end
      , history_id
    ) VALUE (
        p_home_id
      , p_service_id
      , 4269 
      , p_date_begin
      , p_date_end
      , @history_id
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `vmte_home_service_quality` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `vmte_home_service_quality`(
    IN  p_home_id INT(11) UNSIGNED
  , IN  p_service_id SMALLINT(5) UNSIGNED
  , IN  p_quality_parameter DECIMAL(18, 8)
  , IN  p_date_begin DATE
  , IN  p_date_end DATE
  , IN  p_dt DATE
  , IN  p_user_id VARCHAR(255) CHARACTER SET 'utf8'
  , IN  p_api_key_id INT(11) UNSIGNED
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Управление качеством предоставляемых услуг'
BEGIN
    DECLARE v_tx_id BIGINT(20) UNSIGNED;

    CALL transaction_create(p_user_id, p_api_key_id, v_tx_id);

    INSERT INTO m_history(in_date, out_date) VALUE ('0000-00-00', '9999-12-31');

    INSERT INTO m_service_quality(
        home_id
      , service_id
      , supplier_id
      , quality_parameter
      , measurement_unit_id
      , date_begin
      , date_end
      , history_id
    ) VALUE (
        p_home_id
      , p_service_id
      , 4269 
      , p_quality_parameter
      , 11 
      , p_date_begin
      , p_date_end
      , LAST_INSERT_ID()
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `zhek_interest_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `zhek_interest_delete`(
    IN p_k INT,
    IN p_out_date DATE,
    IN p_user_id INT,
    IN p_user_app VARCHAR(255),
    IN p_user_ip VARCHAR(50)
)
BEGIN
    DECLARE v_history_id INT;
    DECLARE v_in_date DATE;

    START TRANSACTION;
        SELECT
                m_history.id
              , m_history.in_date
            INTO
                v_history_id
              , v_in_date
        FROM m_zhek_interest
        INNER JOIN m_history ON
                m_history.id = m_zhek_interest.history_id
        WHERE
                m_zhek_interest.k = p_k
        LIMIT 1;

        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        UPDATE m_history SET cancelled = 1 WHERE id = v_history_id;
        INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, v_history_id);

        INSERT INTO m_history(in_date, out_date) VALUE(v_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);

        INSERT INTO m_zhek_interest(
            zhek_code
          , addservice_id
          , supplier_org_id
          , acceptor_org_id
          , interest
          , history_id)
            SELECT
                zhek_code
              , addservice_id
              , supplier_org_id
              , acceptor_org_id
              , interest
              , @history_id
            FROM m_zhek_interest
            WHERE
                    k = p_k
            LIMIT 1;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `zhek_interest_set_attributes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `zhek_interest_set_attributes`(
    IN p_zhek_code DECIMAL(2,0)
  , IN p_addservice_id INT
  , IN p_supplier_org_id INT
  , IN p_acceptor_org_id INT
  , IN p_interest DECIMAL(5, 2)
  , IN p_in_date DATE
  , IN p_out_date DATE
  , IN p_user_id INT
  , IN p_user_app VARCHAR(255) CHARACTER SET 'utf8'
  , IN p_user_ip VARCHAR(50) CHARACTER SET 'utf8'
)
BEGIN
    
    
    
    

    DECLARE bef_supplier_org_id INT;
    DECLARE bef_acceptor_org_id INT;
    DECLARE bef_interest DECIMAL(5, 2);
    DECLARE bef_history_id INT;
    DECLARE bef_in_date DATE;
    DECLARE bef_out_date DATE;
    DECLARE bef_tx_id INT;

    DECLARE aft_supplier_org_id INT;
    DECLARE aft_acceptor_org_id INT;
    DECLARE aft_interest DECIMAL(5, 2);
    DECLARE aft_history_id INT;
    DECLARE aft_in_date DATE;
    DECLARE aft_out_date DATE;
    DECLARE aft_tx_id INT;

    
    DECLARE in_eq_p_in INT;
    DECLARE out_eq_p_out INT;

    DECLARE btwn_history_id INT;
    DECLARE btwn_tx_id INT;
    DECLARE bwtn_cur_done TINYINT(1);
    
    DECLARE btwn_cur CURSOR FOR SELECT
            m_history.id
    FROM m_zhek_interest
    INNER JOIN m_history ON
            m_history.id = m_zhek_interest.history_id
        AND m_history.cancelled = 0
        AND m_history.in_date >= p_in_date
        AND m_history.out_date <= p_out_date
    WHERE
            m_zhek_interest.zhek_code = p_zhek_code
        AND m_zhek_interest.addservice_id = p_addservice_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN
        SET bwtn_cur_done = 1;
    END;

    START TRANSACTION;
        IF p_in_date IS NULL THEN
            SET p_in_date =(SELECT MAX(m_history.in_date)
                            FROM m_zhek_interest
                            INNER JOIN m_history ON
                                    m_history.id = m_zhek_interest.history_id
                                AND m_history.cancelled = 0
                            WHERE
                                    m_zhek_interest.zhek_code = p_zhek_code
                                AND m_zhek_interest.addservice_id = p_addservice_id
                            LIMIT 1);
        END IF;

        IF p_out_date IS NULL THEN
            SET p_out_date = '9999-12-31';
        END IF;

        INSERT INTO m_transaction(user_id, user_app, user_ip) VALUE(p_user_id, p_user_app, p_user_ip);
        SET @tx_id = LAST_INSERT_ID();

        
        SELECT
                supplier_org_id
              , acceptor_org_id
              , interest
              , history_id
              , in_date
              , out_date
            INTO
                bef_supplier_org_id
              , bef_acceptor_org_id
              , bef_interest
              , bef_history_id
              , bef_in_date
              , bef_out_date
        FROM m_zhek_interest
        INNER JOIN m_history ON
                m_history.id = m_zhek_interest.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date < p_in_date
            AND m_history.out_date >= p_in_date
        WHERE
                m_zhek_interest.zhek_code = p_zhek_code
            AND m_zhek_interest.addservice_id = p_addservice_id
        LIMIT 1;

        
        SELECT
                supplier_org_id
              , acceptor_org_id
              , interest
              , history_id
              , in_date
              , out_date
            INTO
                aft_supplier_org_id
              , aft_acceptor_org_id
              , aft_interest
              , aft_history_id
              , aft_in_date
              , aft_out_date
        FROM m_zhek_interest
        INNER JOIN m_history ON
                m_history.id = m_zhek_interest.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date <= p_out_date
            AND m_history.out_date > p_out_date
        WHERE
                m_zhek_interest.zhek_code = p_zhek_code
            AND m_zhek_interest.addservice_id = p_addservice_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                in_eq_p_in
        FROM m_zhek_interest
        INNER JOIN m_history ON
                m_history.id = m_zhek_interest.history_id
            AND m_history.cancelled = 0
            AND m_history.in_date = p_in_date
        WHERE
                m_zhek_interest.zhek_code = p_zhek_code
            AND m_zhek_interest.addservice_id = p_addservice_id
        LIMIT 1;

        SELECT
                history_id
            INTO
                out_eq_p_out
        FROM m_zhek_interest
        INNER JOIN m_history ON
                m_history.id = m_zhek_interest.history_id
            AND m_history.cancelled = 0
            AND m_history.out_date = p_out_date
        WHERE
                m_zhek_interest.zhek_code = p_zhek_code
            AND m_zhek_interest.addservice_id = p_addservice_id
        LIMIT 1;


        IF bef_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = bef_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, bef_history_id);
        END IF;

        
        SET bwtn_cur_done = 0;
        OPEN btwn_cur;
        btwn: LOOP
            FETCH btwn_cur INTO btwn_history_id;
            IF bwtn_cur_done = 1 THEN
                LEAVE btwn;
            END IF;
            UPDATE m_history SET cancelled = 1 WHERE id = btwn_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, btwn_history_id);
        END LOOP btwn;

        IF aft_history_id IS NOT NULL THEN
            
            UPDATE m_history SET cancelled = 1 WHERE id = aft_history_id;
            INSERT INTO m_transaction_invalidate(transaction_id, history_id) VALUE(@tx_id, aft_history_id);
        END IF;



        IF
                bef_history_id IS NOT NULL
            AND bef_in_date <= DATE_SUB(p_in_date, INTERVAL 1 DAY)
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(bef_in_date, DATE_SUB(p_in_date, INTERVAL 1 DAY));
            SET @bef_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @bef_history_id);
        END IF;

        
        INSERT INTO m_history(in_date, out_date) VALUE(p_in_date, p_out_date);
        SET @history_id = LAST_INSERT_ID();
        INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @history_id);
        INSERT INTO m_transaction_params(transaction_id, history_id) VALUE(@tx_id, @history_id);

        IF
                aft_history_id IS NOT NULL
            AND DATE_ADD(p_out_date, INTERVAL 1 DAY) <= aft_out_date
        THEN
            
            INSERT INTO m_history(in_date, out_date) VALUE(DATE_ADD(p_out_date, INTERVAL 1 DAY), aft_out_date);
            SET @aft_history_id = LAST_INSERT_ID();
            INSERT INTO m_transaction_validate(transaction_id, history_id) VALUE(@tx_id, @aft_history_id);
        END IF;


        IF
                @bef_history_id IS NOT NULL
        THEN
            INSERT INTO m_zhek_interest(
                zhek_code
              , addservice_id
              , supplier_org_id
              , acceptor_org_id
              , interest
              , history_id)
            VALUES (
                p_zhek_code
              , p_addservice_id
              , bef_supplier_org_id
              , bef_acceptor_org_id
              , bef_interest
              , @bef_history_id
            );
        END IF;

        
        INSERT INTO m_zhek_interest(
            zhek_code
          , addservice_id
          , supplier_org_id
          , acceptor_org_id
          , interest
          , history_id)
        VALUES (
            p_zhek_code
          , p_addservice_id
          , p_supplier_org_id
          , p_acceptor_org_id
          , p_interest
          , @history_id
        );

        IF
                @aft_history_id IS NOT NULL
        THEN
            INSERT INTO m_zhek_interest(
                zhek_code
              , addservice_id
              , supplier_org_id
              , acceptor_org_id
              , interest
              , history_id)
            VALUES (
                p_zhek_code
              , p_addservice_id
              , aft_supplier_org_id
              , aft_acceptor_org_id
              , aft_interest
              , @aft_history_id
            );
        END IF;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `_get_houses_by_street` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `_get_houses_by_street`(IN `p_street_id` smallint(5) unsigned)
BEGIN
create temporary table temp_houses select id from m_house where street_id = p_street_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-05 16:46:01
