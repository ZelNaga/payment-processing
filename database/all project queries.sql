#1)--------------------------------------------------- формування списків даних для подальшого опрацювання -------------
#                                                     приймальна установа -> касир -> транзакції -> оплати

#1.1) вибирає усіх касирів і організації до яких вони належать, а також ргафіки організацій
SELECT DISTINCT
  user.organization_id AS ORGANIZATION_ID,
  transaction.user_id  AS USER_ID,
  timetable.begin_time,
  timetable.end_time
FROM online.tbl_user AS user
  RIGHT OUTER JOIN online.tbl_transaction AS transaction
    ON user.id = transaction.user_id
  LEFT OUTER JOIN process.tbl_tranzaction_mode AS timetable
    ON user.organization_id = timetable.organization_id
WHERE
  transaction.dt_our BETWEEN :beginDate AND :endDate # beginDate і endDate часовий проміжок (календарний день) за який вибираються транзакції
ORDER BY user.organization_id;

#1.2) отримуємо усі транзакції по конкретному касиру за певний розрахунковий період
SELECT
  id,
  home_id,
  user_id,
  dt_our,
  type_cash
FROM online.tbl_transaction AS t
WHERE t.user_id = :user_id
      AND t.dt_our BETWEEN :start AND :end # start і end часовий проміжок за який вибираються транзакції з урахуванням графіків
      AND t.cancelled = FALSE
      AND t.home_id != 0;

#1.3) повертає список операцій по конкретній транзакції
SELECT
  p.ID,
  p.no,
  p.zhek,
  p.account,
  a.home_id,
  p.service_id,
  p.value,
  s.supplier_org_id,
  s.acceptor_org_id
FROM online.tbl_payment AS p
  LEFT JOIN main_new.m_account AS a
    ON p.account = a.account AND p.zhek = a.zhek_code
  LEFT JOIN main_new.m_supplier AS s
    ON a.id = s.account_id AND p.service_id = s.service_id
WHERE p.transaction_id = :id; # id певної транзакції
########################################################################################################################




#2)--------------------------------------------------- перенаправлення коштів на додаткові рахунки ---------------------

#2.1) повертає правило перенаправлення коштів по конкретній організації
SELECT
  r.account,
  a.rule_attribute_type,
  a.rule_attribute_value
FROM process.tbl_rules AS r
  LEFT JOIN process.tbl_rules_attributes AS a
    ON r.id = a.rule_id
WHERE r.organization_id = :orgId;
# orgId id конкретної організації

#2.2) вибирає основіні реквізити по id організації
SELECT
  account,
  mfo
FROM main_new.m_org_accounts
WHERE main_new.m_org_accounts.org_id = :p1 # p1 id організації
      AND main_new.m_org_accounts.priority = 0;

#2.3) вибирає додаткові реквізити по id запису в main_new.m_org_accounts
SELECT
  account,
  mfo
FROM main_new.m_org_accounts
WHERE main_new.m_org_accounts.id = :p1; # p1 id запису
########################################################################################################################




#3)--------------------------------------------------- утримання комісій з платежу -------------------------------------

#3.1) вибирає усі формули по утриманню комісії з конкретної оплати по конкретному постачальнику послуги
SELECT
  c.*,
  f.*,
  o.zhek,
  t.type_id
FROM main_new.m_comission AS c
  LEFT JOIN main_new.m_comission_formulae AS f
    ON c.comission_formula_id = f.id
  LEFT JOIN main_new.m_org_cmsu AS o
    ON c.comission_acceptor_org_id = o.org_id
  LEFT JOIN `online`.`tbl_organization` AS t
    ON c.comission_acceptor_org_id = t.id
WHERE c.service_id = :serviceId # serviceId - id виду послуги
      AND c.supplier_org_id = :supplierOrgId # supplierOrgId - id постачальника
      AND c.bank_org_id = :bankOrgId # bankOrgId - це mainOrgId
      AND f.is_month = FALSE
      AND f.percent != 0.00;
########################################################################################################################




#4)--------------------------------------------------- розщеплення квартплати ------------------------------------------

#4.1) отримуємо складові кварплати по конкретному жекові
SELECT *
FROM main_new.m_zhek_interest
WHERE main_new.m_zhek_interest.history_id IN
      (SELECT MAX(main_new.m_zhek_interest.history_id)
       FROM main_new.m_zhek_interest
       WHERE main_new.m_zhek_interest.zhek_code = :zhek # id жеку
       GROUP BY main_new.m_zhek_interest.addservice_id)
      AND main_new.m_zhek_interest.zhek_code = :zhek; # id жеку

#4.2) повертає конкретного постачальника послуги по конкретній квартирі при наявності у будинку декілько
SELECT *
FROM main_new.m_addservice_supplier
WHERE main_new.m_addservice_supplier.zhek_code = :zhek # id жеку
      AND main_new.m_addservice_supplier.house_id = (SELECT main_new.m_home.house_id
                                                     FROM main_new.m_home
                                                     WHERE main_new.m_home.id = :homeId) # id квартири
      AND main_new.m_addservice_supplier.addservice_id = :addServiceId; # id сервісу

#4.3) визначає частину загального коефіцієнта по квартплаті  конкретної оселі
SELECT main_new.m_tarkv_home.value
FROM main_new.m_tarkv_home
WHERE main_new.m_tarkv_home.home_id = :homeId # homeId - id квартири чи прирватного будинку
      AND main_new.m_tarkv_home.addservice_id = :addServiceId # serviceId - id виду послуги
ORDER BY main_new.m_tarkv_home.k DESC
LIMIT 1;

#11) визначає загальний коефіцієнт по квартплаті конкретної оселі
SELECT main_new.m_tarkv_home_total.total
FROM main_new.m_tarkv_home_total
WHERE main_new.m_tarkv_home_total.home_id = :homeId # homeId - id квартири чи прирватного будинку
ORDER BY k DESC
LIMIT 1;
########################################################################################################################




#5)--------------------------------------------------- опрацювання боргів одних організації перед іншими організаціями -

#5.1) визначає борг однієї організації перед іншою
SELECT
  id,
  org_id,
  bank_id,
  acceptor_id,
  summa,
  limit_percent
FROM main_new.m_org_balance
WHERE summa != 0.00;
########################################################################################################################