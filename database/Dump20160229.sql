-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: 192.168.0.52    Database: process
-- ------------------------------------------------------
-- Server version	5.5.47

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `organizations_debts`
--

DROP TABLE IF EXISTS `organizations_debts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizations_debts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `acceptor_id` int(11) NOT NULL,
  `organization_debt` decimal(10,2) NOT NULL,
  `limit_percent` decimal(10,2) NOT NULL,
  `account_id` bigint(32) unsigned NOT NULL,
  `reason` varchar(520) NOT NULL,
  `processing_date` date NOT NULL,
  `creation_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizations_debts`
--

LOCK TABLES `organizations_debts` WRITE;
/*!40000 ALTER TABLE `organizations_debts` DISABLE KEYS */;
INSERT INTO `organizations_debts` VALUES (1,5143,5940,8253,7333.39,50.00,123456789,'TEST','2015-11-15','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `organizations_debts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizations_debts_balance`
--

DROP TABLE IF EXISTS `organizations_debts_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizations_debts_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_debt_id` int(11) NOT NULL,
  `organization_saldo` decimal(10,2) NOT NULL,
  `organization_credit` decimal(10,2) NOT NULL,
  `processed_date` date NOT NULL,
  `creation_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizations_debts_balance`
--

LOCK TABLES `organizations_debts_balance` WRITE;
/*!40000 ALTER TABLE `organizations_debts_balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizations_debts_balance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments_commissions`
--

DROP TABLE IF EXISTS `payments_commissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments_commissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `comission_acceptor_org_id` int(11) NOT NULL,
  `formula_id` int(11) NOT NULL,
  `comission` double(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payments_commissions_idx` (`payment_id`),
  CONSTRAINT `fk_payments_commissions` FOREIGN KEY (`payment_id`) REFERENCES `processed_payments` (`payment_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments_commissions`
--

LOCK TABLES `payments_commissions` WRITE;
/*!40000 ALTER TABLE `payments_commissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments_commissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processed_days`
--

DROP TABLE IF EXISTS `processed_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processed_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `processed_date` date NOT NULL,
  `operator_id` int(11) NOT NULL,
  `day_status_id` int(11) NOT NULL,
  `comment` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processed_days`
--

LOCK TABLES `processed_days` WRITE;
/*!40000 ALTER TABLE `processed_days` DISABLE KEYS */;
/*!40000 ALTER TABLE `processed_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processed_payments`
--

DROP TABLE IF EXISTS `processed_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processed_payments` (
  `payment_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `start_value` double(10,2) NOT NULL,
  `transfer_value` double(10,2) NOT NULL,
  `main_account` bigint(32) NOT NULL,
  `mfo` int(11) NOT NULL,
  `supplier_org_id` int(11) NOT NULL,
  `acceptor_org_id` int(11) NOT NULL,
  `zhek` smallint(5) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `home_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `quittance_number` int(11) NOT NULL,
  `cash_type_id` tinyint(2) NOT NULL,
  `processed_date_timestamp` datetime NOT NULL,
  `processed_day_id` int(11) NOT NULL,
  `main_org_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `fk_processed_payments_idx` (`processed_day_id`),
  CONSTRAINT `fk_processed_payments` FOREIGN KEY (`processed_day_id`) REFERENCES `processed_days` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processed_payments`
--

LOCK TABLES `processed_payments` WRITE;
/*!40000 ALTER TABLE `processed_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `processed_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'process'
--

--
-- Dumping routines for database 'process'
--
/*!50003 DROP FUNCTION IF EXISTS `get_account_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_account_id`(`p_org_id` int, `p_bank_id` int, `p_zhek` int, `p_service` int, `p_account` int, `p_home_id` int) RETURNS int(11)
BEGIN
	DECLARE v_house_id INT;
	DECLARE v_account_id INT;
	
	select main_new.get_house_Id(p_zhek, p_account) into v_house_id;

	if (v_house_id is not null) then
		select 
			rule.account into v_account_id
		from process.tbl_rules = rule
			left outer join process.tbl_rules_attributes bank on bank.rule_attribute_type = 1 and rule.id = bank.rule_id
			left outer join process.tbl_rules_attributes zhek on zhek.rule_attribute_type = 2 and rule.id = zhek.rule_id
			left outer join process.tbl_rules_attributes service on service.rule_attribute_type = 4 and rule.id = service.rule_id
			left outer join process.tbl_rules_attributes house on house.rule_attribute_type = 5 and rule.id = house.rule_id
		where
			rule.organization_id = p_org_id
			and bank.rule_attribute_value = p_bank_id
			and zhek.rule_attribute_value = p_zhek
			and service.rule_attribute_value = p_service
			and house.rule_attribute_value = v_house_id
			and rule.stopped = 0;
	end if;

	if (v_account_id is null) then
		select 
			m_org_account.id into v_account_id
		from main_new.m_org_accounts as m_org_account
		where m_org_account.org_id = p_org_id
		limit 1;
	end if;

RETURN v_account_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `other_pay_sum` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `other_pay_sum`(`p_org_id` int, `p_bank_id` int, `p_date_from` date, `p_date_to` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_summa DECIMAL(10, 2) default 0;

	select sum(ot_pay.summa) into v_summa
		from process.pp_other_payment ot_pay 
	where ot_pay.organization_id = p_bank_id 
			and ot_pay.dt between p_date_from and p_date_to
			and ot_pay.account in (select account from main_new.m_org_accounts where org_id = p_org_id);

RETURN v_summa;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `zhek_comission` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `zhek_comission`(`p_acceptor_id` int, `p_bank_id` int, `p_date` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_comission DECIMAL(10, 2) default 0;
	DECLARE v_zhek int default 0;

	select zhek into v_zhek 
		from main_new.m_org_cmsu m_org_cmsu
		where m_org_cmsu.org_id = p_acceptor_id;

	if (v_zhek > 0) then
		select SUM(cmsu_tr.zhek_akt + cmsu_tr.zhek_percent) into v_comission
			from process.cmsu_transactions as cmsu_tr
			INNER JOIN process.pp_rawtransactions as raw_tr on raw_tr.payment_id = cmsu_tr.payment_id and raw_tr.service = cmsu_tr.service_id
		WHERE raw_tr.zhek = v_zhek 
			and raw_tr.main_bank = p_bank_id
			and cmsu_tr.dt = p_date;
	END IF;

RETURN v_comission;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `zhek_comission_by_balance_bank` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `zhek_comission_by_balance_bank`(`p_acceptor_id` int, `p_bank_id` int, `p_date` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_comission DECIMAL(10, 2) default 0.0;
	DECLARE v_zhek int default 0;

	select zhek into v_zhek 
		from main_new.m_org_cmsu m_org_cmsu
		where m_org_cmsu.org_id = p_acceptor_id;

	if (v_zhek > 0) then
		select SUM(cmsu_tr.zhek_akt + cmsu_tr.zhek_percent) into v_comission
			from process.cmsu_transactions as cmsu_tr
			INNER JOIN process.pp_rawtransactions as raw_tr on raw_tr.payment_id = cmsu_tr.payment_id and raw_tr.service = cmsu_tr.service_id
		WHERE raw_tr.zhek = v_zhek 
			and raw_tr.balance_bank = p_bank_id
			and cmsu_tr.dt = p_date;
	END IF;

	if (isnull(v_comission)) then
		set v_comission = 0.0;
	END IF;

RETURN v_comission;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `zhek_comission_by_balance_bank_and_period` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `zhek_comission_by_balance_bank_and_period`(`p_acceptor_id` int, `p_bank_id` int, `p_date_from` date, `p_date_to` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_comission DECIMAL(10, 2) default 0;
	DECLARE v_zhek int default 0;

	select zhek into v_zhek 
		from main_new.m_org_cmsu m_org_cmsu
		where m_org_cmsu.org_id = p_acceptor_id;

	if (v_zhek > 0) then
		select SUM(cmsu_tr.zhek_akt + cmsu_tr.zhek_percent) into v_comission
			from process.cmsu_transactions as cmsu_tr
			INNER JOIN process.pp_rawtransactions as raw_tr on raw_tr.payment_id = cmsu_tr.payment_id and raw_tr.service = cmsu_tr.service_id
		WHERE raw_tr.zhek = v_zhek 
			and raw_tr.balance_bank = p_bank_id
			and cmsu_tr.dt between p_date_from and p_date_to;
	END IF;

RETURN v_comission;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `zhek_comission_by_period` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `zhek_comission_by_period`(`p_acceptor_id` int, `p_bank_id` int, `p_date_from` date, `p_date_to` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_comission DECIMAL(10, 2) default 0;
	DECLARE v_zhek int default 0;

	select zhek into v_zhek 
		from main_new.m_org_cmsu m_org_cmsu
		where m_org_cmsu.org_id = p_acceptor_id;

	if (v_zhek > 0) then
		select SUM(cmsu_tr.zhek_akt + cmsu_tr.zhek_percent) into v_comission
			from process.cmsu_transactions as cmsu_tr
			INNER JOIN process.pp_rawtransactions as raw_tr on raw_tr.payment_id = cmsu_tr.payment_id and raw_tr.service = cmsu_tr.service_id
		WHERE raw_tr.zhek = v_zhek 
			and raw_tr.main_bank = p_bank_id
			and cmsu_tr.dt between p_date_from and p_date_to;
	END IF;

RETURN v_comission;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `summary_report_by_bank_for_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `summary_report_by_bank_for_day`(
	IN parent_id INT
)
    COMMENT 'Build summary report by bank for day !'
BEGIN
	
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS commissions, payments;
		SELECT NULL LIMIT 0;
    END;

	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
    SELECT p.acceptor_org_id, o.type_id, SUM(c.comission ) AS sum
	FROM process.payments_commissions AS c
	LEFT JOIN `online`.`tbl_organization` AS o
	ON c.comission_acceptor_org_id = o.id
	LEFT JOIN process.processed_payments AS p
	ON c.payment_id = p.payment_id
	LEFT JOIN main_new.m_organization AS m_o
	ON p.organization_id = m_o.id    
	WHERE c.formula_id != 0 AND m_o.parent_id = parent_id
	GROUP BY p.acceptor_org_id, o.type_id);

	CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
	SELECT organization_name.name AS acceptor_name,
			payments.acceptor_org_id AS acceptor_id,
			payments.mfo AS mfo, 
			payments.main_account AS account, 
			SUM(payments.start_value) AS start_value, 
			SUM(payments.transfer_value) AS transfer_value,
			COUNT(payments.payment_id) AS total_payments
	FROM process.processed_payments AS payments
	LEFT JOIN main_new.m_organization AS organization
	ON payments.organization_id = organization.id
	LEFT JOIN `online`.`tbl_organization` AS organization_name
	ON payments.acceptor_org_id = organization_name.id
	WHERE organization.parent_id = parent_id
	GROUP BY payments.acceptor_org_id);

	SET @sql = (SELECT CONCAT(
		'SELECT p.acceptor_name AS `name`, ',
        'p.mfo AS `mfo`, ',
        'p.account AS `account`, ',
        'p.start_value AS `start_value`, ',
        'p.transfer_value AS `transfer_value`, ',
		GROUP_CONCAT(DISTINCT 'SUM(CASE c.type_id WHEN ', type_id, ' THEN c.sum ELSE 0 END) AS `', type_id SEPARATOR '`, '),
        '`, p.total_payments AS `total_payments` ',
		'FROM payments AS p ',
		'LEFT JOIN commissions AS c ',
		'ON p.acceptor_id = c.acceptor_org_id ',
		'GROUP BY p.acceptor_id;')
	FROM commissions);	
    
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    DROP TABLE IF EXISTS commissions, payments;  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-29 14:17:28
