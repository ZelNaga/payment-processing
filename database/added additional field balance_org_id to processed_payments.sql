-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: 192.168.0.52    Database: process
-- ------------------------------------------------------
-- Server version	5.5.47

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `processed_payments`
--

DROP TABLE IF EXISTS `processed_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processed_payments` (
  `payment_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `start_value` double(10,2) NOT NULL,
  `transfer_value` double(10,2) NOT NULL,
  `main_account` bigint(32) NOT NULL,
  `mfo` int(11) NOT NULL,
  `supplier_org_id` int(11) NOT NULL,
  `acceptor_org_id` int(11) NOT NULL,
  `zhek` smallint(5) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `home_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `quittance_number` int(11) NOT NULL,
  `cash_type_id` tinyint(2) NOT NULL,
  `processed_date_timestamp` datetime NOT NULL,
  `processed_day_id` int(11) NOT NULL,
  `main_org_id` int(11) NOT NULL,
  `balance_org_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `fk_processed_payments_idx` (`processed_day_id`),
  CONSTRAINT `fk_processed_payments` FOREIGN KEY (`processed_day_id`) REFERENCES `processed_days` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processed_payments`
--

LOCK TABLES `processed_payments` WRITE;
/*!40000 ALTER TABLE `processed_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `processed_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'process'
--

--
-- Dumping routines for database 'process'
--
/*!50003 DROP FUNCTION IF EXISTS `get_account_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_account_id`(`p_org_id` int, `p_bank_id` int, `p_zhek` int, `p_service` int, `p_account` int, `p_home_id` int) RETURNS int(11)
BEGIN
	DECLARE v_house_id INT;
	DECLARE v_account_id INT;
	
	select main_new.get_house_Id(p_zhek, p_account) into v_house_id;

	if (v_house_id is not null) then
		select 
			rule.account into v_account_id
		from process.tbl_rules = rule
			left outer join process.tbl_rules_attributes bank on bank.rule_attribute_type = 1 and rule.id = bank.rule_id
			left outer join process.tbl_rules_attributes zhek on zhek.rule_attribute_type = 2 and rule.id = zhek.rule_id
			left outer join process.tbl_rules_attributes service on service.rule_attribute_type = 4 and rule.id = service.rule_id
			left outer join process.tbl_rules_attributes house on house.rule_attribute_type = 5 and rule.id = house.rule_id
		where
			rule.organization_id = p_org_id
			and bank.rule_attribute_value = p_bank_id
			and zhek.rule_attribute_value = p_zhek
			and service.rule_attribute_value = p_service
			and house.rule_attribute_value = v_house_id
			and rule.stopped = 0;
	end if;

	if (v_account_id is null) then
		select 
			m_org_account.id into v_account_id
		from main_new.m_org_accounts as m_org_account
		where m_org_account.org_id = p_org_id
		limit 1;
	end if;

RETURN v_account_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `other_pay_sum` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `other_pay_sum`(`p_org_id` int, `p_bank_id` int, `p_date_from` date, `p_date_to` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_summa DECIMAL(10, 2) default 0;

	select sum(ot_pay.summa) into v_summa
		from process.pp_other_payment ot_pay 
	where ot_pay.organization_id = p_bank_id 
			and ot_pay.dt between p_date_from and p_date_to
			and ot_pay.account in (select account from main_new.m_org_accounts where org_id = p_org_id);

RETURN v_summa;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `zhek_comission` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `zhek_comission`(`p_acceptor_id` int, `p_bank_id` int, `p_date` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_comission DECIMAL(10, 2) default 0;
	DECLARE v_zhek int default 0;

	select zhek into v_zhek 
		from main_new.m_org_cmsu m_org_cmsu
		where m_org_cmsu.org_id = p_acceptor_id;

	if (v_zhek > 0) then
		select SUM(cmsu_tr.zhek_akt + cmsu_tr.zhek_percent) into v_comission
			from process.cmsu_transactions as cmsu_tr
			INNER JOIN process.pp_rawtransactions as raw_tr on raw_tr.payment_id = cmsu_tr.payment_id and raw_tr.service = cmsu_tr.service_id
		WHERE raw_tr.zhek = v_zhek 
			and raw_tr.main_bank = p_bank_id
			and cmsu_tr.dt = p_date;
	END IF;

RETURN v_comission;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `zhek_comission_by_balance_bank` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `zhek_comission_by_balance_bank`(`p_acceptor_id` int, `p_bank_id` int, `p_date` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_comission DECIMAL(10, 2) default 0.0;
	DECLARE v_zhek int default 0;

	select zhek into v_zhek 
		from main_new.m_org_cmsu m_org_cmsu
		where m_org_cmsu.org_id = p_acceptor_id;

	if (v_zhek > 0) then
		select SUM(cmsu_tr.zhek_akt + cmsu_tr.zhek_percent) into v_comission
			from process.cmsu_transactions as cmsu_tr
			INNER JOIN process.pp_rawtransactions as raw_tr on raw_tr.payment_id = cmsu_tr.payment_id and raw_tr.service = cmsu_tr.service_id
		WHERE raw_tr.zhek = v_zhek 
			and raw_tr.balance_bank = p_bank_id
			and cmsu_tr.dt = p_date;
	END IF;

	if (isnull(v_comission)) then
		set v_comission = 0.0;
	END IF;

RETURN v_comission;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `zhek_comission_by_balance_bank_and_period` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `zhek_comission_by_balance_bank_and_period`(`p_acceptor_id` int, `p_bank_id` int, `p_date_from` date, `p_date_to` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_comission DECIMAL(10, 2) default 0;
	DECLARE v_zhek int default 0;

	select zhek into v_zhek 
		from main_new.m_org_cmsu m_org_cmsu
		where m_org_cmsu.org_id = p_acceptor_id;

	if (v_zhek > 0) then
		select SUM(cmsu_tr.zhek_akt + cmsu_tr.zhek_percent) into v_comission
			from process.cmsu_transactions as cmsu_tr
			INNER JOIN process.pp_rawtransactions as raw_tr on raw_tr.payment_id = cmsu_tr.payment_id and raw_tr.service = cmsu_tr.service_id
		WHERE raw_tr.zhek = v_zhek 
			and raw_tr.balance_bank = p_bank_id
			and cmsu_tr.dt between p_date_from and p_date_to;
	END IF;

RETURN v_comission;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `zhek_comission_by_period` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `zhek_comission_by_period`(`p_acceptor_id` int, `p_bank_id` int, `p_date_from` date, `p_date_to` date) RETURNS decimal(10,2)
BEGIN
	DECLARE v_comission DECIMAL(10, 2) default 0;
	DECLARE v_zhek int default 0;

	select zhek into v_zhek 
		from main_new.m_org_cmsu m_org_cmsu
		where m_org_cmsu.org_id = p_acceptor_id;

	if (v_zhek > 0) then
		select SUM(cmsu_tr.zhek_akt + cmsu_tr.zhek_percent) into v_comission
			from process.cmsu_transactions as cmsu_tr
			INNER JOIN process.pp_rawtransactions as raw_tr on raw_tr.payment_id = cmsu_tr.payment_id and raw_tr.service = cmsu_tr.service_id
		WHERE raw_tr.zhek = v_zhek 
			and raw_tr.main_bank = p_bank_id
			and cmsu_tr.dt between p_date_from and p_date_to;
	END IF;

RETURN v_comission;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `report_for_supplier_by_bank_and_zhek_for_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `report_for_supplier_by_bank_and_zhek_for_day`(
	IN supplierOrgId INT, processedDate DATE
)
BEGIN
	/*DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS payments, commissions, total_commissions;
		SELECT NULL LIMIT 0;
    END;*/
    
    CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
		SELECT  p.balance_org_id,
				p.zhek, 
				(SELECT name FROM main_new.m_org_attributes WHERE org_id = p.balance_org_id AND enabled = true) AS bank_name,
				(SELECT name FROM main_new.m_org_attributes WHERE org_id = c.org_id AND enabled = true) AS zhek_name,
				SUM(p.start_value) AS start_value,
				SUM(p.transfer_value) AS transfer_value,
				COUNT(p.payment_id) AS total_payments 
		FROM process.processed_payments AS p
		LEFT JOIN main_new.m_org_cmsu AS c
		ON p.zhek = c.zhek
		JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		WHERE p.supplier_org_id = supplierOrgId
		AND d.processed_date = processedDate
		GROUP BY p.balance_org_id, p.zhek
	);
    
    CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
		SELECT p.balance_org_id, p.zhek, c.comission_acceptor_type AS type_id, SUM(c.comission) AS sum
		FROM process.payments_commissions AS c		
		RIGHT JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id
        JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		WHERE c.formula_id != 0
        AND p.supplier_org_id = supplierOrgId
        AND d.processed_date = processedDate
		GROUP BY p.balance_org_id, p.zhek, c.comission_acceptor_type 
	);
    
    CREATE TEMPORARY TABLE IF NOT EXISTS total_commissions AS (
		SELECT c.balance_org_id, c.zhek, SUM(c.sum) AS sum
		FROM commissions AS c
		GROUP BY c.balance_org_id, c.zhek 
	);
    
    SET @sql = (SELECT CONCAT('SELECT p.bank_name, ',
                                '	  p.zhek_name, ',                                
								'	  p.start_value, ',
								'	  p.transfer_value, ',
                                '     (CASE WHEN tc.sum THEN tc.sum ELSE 0.00 END) AS total_commission, ',
									  GROUP_CONCAT(DISTINCT 'ROUND((CASE c.type_id WHEN ', commission_type_id, ' THEN c.sum ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'`, p.total_payments '
                                'FROM payments AS p ',
								'LEFT JOIN commissions AS c ',
								'ON p.balance_org_id = c.balance_org_id AND p.zhek = c.zhek ',
                                'LEFT JOIN total_commissions AS tc ',
                                'ON p.balance_org_id = tc.balance_org_id AND p.zhek = tc.zhek;'
							)
					FROM main_new.report_commissions_types);	
		
	PREPARE stmt FROM @sql;    
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    DROP TABLE IF EXISTS payments, commissions, total_commissions;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `report_for_zhek_by_banks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `report_for_zhek_by_banks`(
	IN acceptorOrgId INT, processedDate DATE
)
    COMMENT 'Build report for the zhek in the context of banks !'
BEGIN

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS payments, commissions, zhek_commissions;
		SELECT NULL LIMIT 0;
    END;

	CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
		SELECT o_p.service_id, 
				p_p.acceptor_org_id, 
				p_p.payment_id,
				p_p.start_value, 
				p_p.transfer_value, 
				(SELECT main_new.get_balance_org_id(p_p.organization_id) ) AS balance_org_id
		FROM `process`.`processed_payments` AS p_p
		LEFT JOIN `process`.`processed_days` AS p_d
		ON p_p.processed_day_id = p_d.id
		LEFT JOIN `online`.`tbl_payment` AS o_p
		ON p_p.payment_id = o_p.ID
		WHERE p_p.acceptor_org_id = acceptorOrgId
		AND p_d.processed_date = processedDate
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS commissions AS (
		SELECT p.balance_org_id, c.comission_acceptor_type AS type_id, SUM(c.comission) AS sum
		FROM `process`.`payments_commissions` AS c		
		RIGHT JOIN payments AS p
		ON c.payment_id = p.payment_id
		WHERE c.formula_id != 0
		GROUP BY c.comission_acceptor_type, p.balance_org_id
	);
      
    CREATE TEMPORARY TABLE IF NOT EXISTS zhek_commissions AS (
		SELECT (SELECT main_new.get_balance_org_id(p.organization_id)) AS balance_org_id,
				a.short_name AS balance_org_name,
				c.service_id,
				s.short_name AS service_name,
				p.acceptor_org_id,
				(SELECT short_name FROM main_new.m_org_attributes WHERE org_id = p.acceptor_org_id) AS acceptor_org_name,
				0.00 AS start_value,
				SUM(c.comission) AS transfer_value,
                c.comission_acceptor_type AS type_id
		FROM process.payments_commissions AS c
		JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id
		JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		JOIN main_new.m_org_attributes AS a
		ON p.main_org_id = a.org_id 
		JOIN main_new.m_service AS s
		ON c.service_id = s.id        
		WHERE c.comission_acceptor_org_id = acceptorOrgId
		AND d.processed_date = processedDate
		GROUP BY balance_org_id, c.service_id, p.acceptor_org_id
	);
	
	SET @sql = (SELECT CONCAT('SELECT a.short_name AS balance_org_name, ',
                                '	  s.short_name AS service_name, ',
                                '	  (SELECT short_name FROM main_new.m_org_attributes WHERE org_id = p.acceptor_org_id) AS acceptor_org_name, ',
                                '     ROUND((CASE WHEN c1.sum THEN c1.sum ELSE 0.00 END), 2) AS total_commission, ',
								'	  ROUND(SUM(p.start_value), 2) AS start_value, ',
								'	  ROUND(SUM(p.transfer_value), 2) AS transfer_value, ',
									  GROUP_CONCAT(DISTINCT 'ROUND((CASE c1.type_id WHEN ', commission_type_id, ' THEN c1.sum ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'` FROM payments AS p ',
								'LEFT JOIN commissions AS c1 ',
								'ON p.balance_org_id = c1.balance_org_id ',
                                'LEFT JOIN main_new.m_org_attributes AS a ',
                                'ON p.balance_org_id = a.org_id ',
                                'LEFT JOIN main_new.m_service AS s ',
                                'ON p.service_id = s.id ',
								'GROUP BY p.balance_org_id, p.service_id ',
                                'UNION ALL ',
								'SELECT c.balance_org_name, ',
                                '		c.service_name, ',
                                '		c.acceptor_org_name, ',
                                '		ROUND(c.transfer_value, 2) AS total_commission, ',
                                '		c.start_value, ',
                                '		c.transfer_value, ',
										GROUP_CONCAT(DISTINCT 'ROUND((CASE c.type_id WHEN ', commission_type_id, ' THEN c.transfer_value ELSE 0.00 END), 2) AS `', commission_type_id SEPARATOR '`, '),
								'` FROM zhek_commissions AS c ',
                                'ORDER BY balance_org_name, service_name;'
							)
					FROM main_new.report_commissions_types);	
		
	PREPARE stmt FROM @sql;    
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    #SELECT @sql;

	DROP TABLE IF EXISTS payments, commissions, zhek_commissions;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `summary_report_by_bank_for_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `summary_report_by_bank_for_day`(
	IN parentId INT, processedDate DATE
)
    COMMENT 'Build summary report by bank for day !!!'
BEGIN
	
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		DROP TABLE IF EXISTS commissions1, commissions2, commissions_cache, payments, acceptor_org_id_cache;
		SELECT NULL LIMIT 0;
    END;

	CREATE TEMPORARY TABLE IF NOT EXISTS commissions1 AS (
		SELECT p.acceptor_org_id, o.type_id, SUM(c.comission ) AS sum
		FROM process.payments_commissions AS c
		LEFT JOIN `online`.`tbl_organization` AS o
		ON c.comission_acceptor_org_id = o.id
		LEFT JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		LEFT JOIN main_new.m_organization AS m_o
		ON p.organization_id = m_o.id    
		WHERE c.formula_id != 0 AND m_o.parent_id = parentId 
		AND d.processed_date = processedDate
		GROUP BY p.acceptor_org_id, o.type_id
    );
    
    CREATE TEMPORARY TABLE IF NOT EXISTS commissions2 AS (
		SELECT p.acceptor_org_id, o.type_id, SUM(c.comission ) AS sum
		FROM process.payments_commissions AS c
		LEFT JOIN `online`.`tbl_organization` AS o
		ON c.comission_acceptor_org_id = o.id
		LEFT JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		LEFT JOIN main_new.m_organization AS m_o
		ON p.organization_id = m_o.id    
		WHERE c.formula_id != 0 AND m_o.parent_id = parentId 
		AND d.processed_date = processedDate
		GROUP BY p.acceptor_org_id, o.type_id
    );
    
    CREATE TEMPORARY TABLE IF NOT EXISTS commissions_cache AS (
		SELECT c.comission_acceptor_org_id AS acceptor_org_id, c.comission_acceptor_type AS type_id, c.comission AS comission
		FROM process.payments_commissions AS c
		LEFT JOIN process.processed_payments AS p
		ON c.payment_id = p.payment_id 
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		LEFT JOIN main_new.m_organization AS m_o
		ON p.organization_id = m_o.id    
		WHERE c.formula_id = 0 AND m_o.parent_id = parentId
		AND d.processed_date = processedDate
    ); 

	CREATE TEMPORARY TABLE IF NOT EXISTS payments AS (
		SELECT  org_cmsu.supplier_id AS supplier_id,
				organization_name.name AS acceptor_name,
				payments.acceptor_org_id AS acceptor_id,
				payments.mfo AS mfo, 
				payments.main_account AS account, 
				ROUND(SUM(payments.start_value), 2) AS start_value,
                ROUND(SUM(payments.transfer_value)
				+ CASE WHEN (@debt := (SELECT SUM(b.organization_credit) 
										FROM process.organizations_debts AS d
										LEFT JOIN process.organizations_debts_balance AS b
										ON d.id = b.organization_debt_id
										WHERE payments.acceptor_org_id = d.acceptor_id 
										AND b.processed_date = processedDate)) 
					THEN @debt
					ELSE 0 END
				+ CASE WHEN (@commission := (SELECT SUM(c_com.comission ) AS sum
											FROM commissions_cache AS c_com
											WHERE c_com.acceptor_org_id = payments.acceptor_org_id))
					THEN @commission
					ELSE 0 END, 2) 
				AS transfer_value,			
				COUNT(payments.payment_id) AS total_payments
		FROM process.processed_payments AS payments
		LEFT JOIN main_new.m_organization AS organization
		ON payments.organization_id = organization.id
		LEFT JOIN process.processed_days AS p_d
		ON payments.processed_day_id = p_d.id
		LEFT JOIN `online`.`tbl_organization` AS organization_name
		ON payments.acceptor_org_id = organization_name.id
		LEFT JOIN main_new.m_org_cmsu AS org_cmsu
		ON payments.acceptor_org_id = org_cmsu.org_id
		WHERE organization.parent_id = parentId
		AND p_d.processed_date = processedDate
		GROUP BY payments.main_account
    );
    
    CREATE TEMPORARY TABLE IF NOT EXISTS acceptor_org_id_cache AS (
		SELECT p.acceptor_org_id
		FROM process.processed_payments AS p
		LEFT JOIN process.processed_days AS d
		ON p.processed_day_id = d.id
		LEFT JOIN main_new.m_organization AS m_o
		ON p.organization_id = m_o.id
		WHERE m_o.parent_id = parentId
		AND d.processed_date = processedDate
    );

	SET @sql = (SELECT CONCAT(
				'SELECT p.supplier_id AS supplier_id, p.acceptor_name AS `name`, ',
				'p.mfo AS `mfo`, ',
				'p.account AS `account`, ',
				'p.start_value AS `start_value`, ',
				'p.transfer_value AS `transfer_value`, ',
				GROUP_CONCAT(DISTINCT 'SUM(CASE c1.type_id WHEN ', commission_type_id, ' THEN c1.sum ELSE 0 END) AS `', commission_type_id SEPARATOR '`, '),
				'`, p.total_payments AS `total_payments` ',
				'FROM payments AS p ',
				'LEFT JOIN commissions1 AS c1 ',
				'ON p.acceptor_id = c1.acceptor_org_id ',
				'GROUP BY p.account ',
                'UNION '
                'SELECT org_cmsu.supplier_id AS supplier_id, ',
					'organization_name.name AS acceptor_name, ',
					'a.mfo AS mfo, ',
					'a.account AS account, ',
					'0 AS start_value, ',
					'SUM(c_cache.comission) AS transfer_value, ',
                    GROUP_CONCAT(DISTINCT 'SUM(CASE c2.type_id WHEN ', commission_type_id, ' THEN c2.sum ELSE 0 END) AS `', commission_type_id SEPARATOR '`, '),
					'`, 0 AS total_payments ',
				'FROM commissions_cache AS c_cache ',
                'LEFT JOIN commissions2 AS c2 ',
				'ON c_cache.acceptor_org_id = c2.acceptor_org_id ',
                'LEFT JOIN main_new.m_org_accounts AS a ',
                'ON c_cache.acceptor_org_id = a.org_id '
				'LEFT JOIN `online`.`tbl_organization` AS organization_name ',
				'ON c_cache.acceptor_org_id = organization_name.id ',
				'LEFT JOIN main_new.m_org_cmsu AS org_cmsu ',
				'ON c_cache.acceptor_org_id = org_cmsu.org_id	',			  
				'WHERE c_cache.acceptor_org_id NOT IN (SELECT * FROM acceptor_org_id_cache) ',
                'AND a.priority = 0 '
				'GROUP BY c_cache.acceptor_org_id;')
                FROM main_new.report_commissions_types);	
    
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    DROP TABLE IF EXISTS commissions1, commissions2, commissions_cache, payments, acceptor_org_id_cache;  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-22 15:55:15
