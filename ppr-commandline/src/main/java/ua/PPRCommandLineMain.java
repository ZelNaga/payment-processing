package ua;

/**
 * Created by zelnaga on 29.11.16.
 */
public class PPRCommandLineMain {

    private static PPRCommandLineProgressListener progressListener = new PPRCommandLineProgressListener();

    public static void main(String[] args) {
        PPRCoreMain.getInstance().registerProgressListener(progressListener);
        PPRCoreMain.getInstance().proceed(true);
    }
}
