package ua;

import com.google.common.eventbus.Subscribe;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

/**
 * Created by asv on 29.11.16.
 */
class PPRCommandLineProgressListener {

    static org.slf4j.Logger LOGGER = LoggerFactory.getLogger("Progress");

    @Subscribe
    public void onProgress(Double percent) throws ExecutionException, InterruptedException {
        LOGGER.info("Progress - {}%", percent.intValue() );
    }
}
