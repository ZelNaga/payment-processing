package tasks;

import ua.PPCoreMain;
import ua.events.PaymentProcessingListener;

/**
 * Created by zelnaga on 24.11.16.
 */
public class PPTask {
    public static void execute(PaymentProcessingListener progressListener, Integer operatorId, String startDate, String endDate, String comment) {
        new Thread(() -> {
            PPCoreMain.getInstance().setPPListener(progressListener);
            PPCoreMain.getInstance().proceed(operatorId, startDate,endDate, comment);
        }).start();
    }
}
