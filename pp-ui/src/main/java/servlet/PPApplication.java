package servlet;


import eu.webtoolkit.jwt.*;
import tasks.PPTask;
import ua.events.PaymentProcessingListener;

/**
 * Created by asv on 14.11.16.
 */
public class PPApplication extends WApplication {
    private static final String START_OF_DAY = " 00:00:00";
    private static final String END_OF_DAY = " 23:59:59";

    //TODO: make this hardcoded constants dynamically generated
    private static final String OPERATOR_ID = "007";
    private static final String COMMENT = "Enter some comment";

    private PPServletEventListener progressListener = new PPServletEventListener();
    private final WProgressBar progressBar = new WProgressBar();
    private final WText infoBox = new WText("", TextFormat.XHTMLUnsafeText);

    public PPApplication(WEnvironment env) {
        super(env);
        this.setTheme(new WBootstrapTheme());
        this.useStyleSheet(new WLink("css/style.css"));
        infoBox.setWordWrap(false);
        addWidgets();
    }

    private void addWidgets() {
        setTitle("PPServlet");

        final WDateEdit startDate = new WDateEdit();
        startDate.setDate(WDate.getCurrentServerDate().addDays(-1));
        startDate.setFormat("yyyy-MM-dd");

        final WDateEdit endDate = new WDateEdit();
        endDate.setDate(WDate.getCurrentServerDate().addDays(-1));
        startDate.setTop(endDate.getDate());
        endDate.setFormat("yyyy-MM-dd");
        endDate.setBottom(startDate.getDate());

        final WPushButton startButton = new WPushButton("Start");
        startButton.setStyleClass("btn btn-primary");

        WGroupBox appArgsBox = new WGroupBox(getRoot());
        appArgsBox.setTitle("Application arguments.");
        appArgsBox.addWidget(new WText("Start date "));
        appArgsBox.addWidget(startDate);
        appArgsBox.addWidget(new WText("End date "));
        appArgsBox.addWidget(endDate);
        appArgsBox.addWidget(startButton);

        getRoot().addWidget(new WBreak());

        progressBar.setRange(0, 100);

        WPanel progressBarPanel = new WPanel(getRoot());
        progressBarPanel.setTitle("Application total progress.");
        progressBarPanel.hide();
        progressBarPanel.setCentralWidget(progressBar);

        getRoot().addWidget(new WBreak());

        WPanel infoBoxPanel = new WPanel(getRoot());
        infoBoxPanel.setTitle("Application logout.");
        infoBoxPanel.hide();
        infoBoxPanel.setCentralWidget(infoBox);

        infoBox.addStyleClass("info-box");

        final WTimer intervalTimer = new WTimer(getRoot()); //TODO: should be replaced by WApplication#enableUpdates()
        intervalTimer.setInterval(500);

        startDate.changed().addListener(this, () -> {
            if (startDate.validate() == WValidator.State.Valid) {
                endDate.setBottom(startDate.getDate());
            }
        });

        endDate.changed().addListener(this, () -> {
            if (startDate.validate() == WValidator.State.Valid) {
                startDate.setTop(endDate.getDate());
            }
        });

        startButton.clicked().addListener(this, () -> {
            appArgsBox.hide();
            intervalTimer.start();
            progressBar.show();
            startButton.disable();
            PPTask.execute(progressListener, Integer.valueOf(OPERATOR_ID), startDate.getText() + START_OF_DAY,
                            endDate.getText() + END_OF_DAY, COMMENT);
            progressBarPanel.show();
            infoBoxPanel.show();
        });

        intervalTimer.timeout().addListener(this, () -> {
            if (!progressListener.getAppStatus()) {
                progressBarPanel.hide();
                intervalTimer.stop();
                startButton.enable();
                appArgsBox.show();
            }
        });
    }

    private class PPServletEventListener implements PaymentProcessingListener {

        private volatile boolean appRun = false;

        public synchronized boolean getAppStatus() {
            return this.appRun;
        }

        @Override
        public synchronized void onStart() {
            attachThread();
            this.appRun = true;
            progressBar.setValue(0.0);
            infoBox.setText("");
        }

        @Override
        public synchronized void onFinish() {
            this.appRun = false;
        }

        @Override
        public synchronized void onProgress(double progress) {
            attachThread();
            progressBar.setValue(progress);
        }

        @Override
        public synchronized void onMessage(String msg) {
            attachThread();
            infoBox.setText(new WString("{1}<p>{2}</p>").arg(infoBox.getText()).arg(msg));
        }

        @Override
        public synchronized void onError(String msg) {
            attachThread();
            infoBox.setText(new WString("{1}<p style=\"color : red\">{2}</p>").arg(infoBox.getText()).arg(msg));
        }
    }
}
