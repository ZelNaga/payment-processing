package ua;

public class PPCommandLineMain {

    private static PPCommandLineProgressListener progressListener = new PPCommandLineProgressListener() ;

    public static void main(String[] args) {
        PPCoreMain.getInstance().registerProgressListener( progressListener );
        PPCoreMain.getInstance().proceed( Runtime.getRuntime().availableProcessors(), true );
    }
}