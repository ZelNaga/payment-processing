package ua;

import com.google.common.eventbus.Subscribe;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

/**
 * Created by ekh on 18.11.15.
 */
class PPCommandLineProgressListener {

    static org.slf4j.Logger LOGGER = LoggerFactory.getLogger("Progress");

    @Subscribe
    public void onProgress(Double percent) throws ExecutionException, InterruptedException {
        LOGGER.info("Progress - {}%", percent.intValue() );
    }

}
