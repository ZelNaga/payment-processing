#!/usr/bin/env bash
PASSWORD=465364
USER=root
ADDR=192.168.5.7

sshpass -p "$PASSWORD" scp -r "pp-core/target/lib" "$USER"@"$ADDR":/home/ekh/paymentProcessing
sshpass -p "$PASSWORD" scp "pp-core/target/pp-core-1.0-SNAPSHOT.jar" "$USER"@"$ADDR":/home/ekh/paymentProcessing/lib/
sshpass -p "$PASSWORD" scp "pp-commandline/target/pp-commandline-1.0-SNAPSHOT.jar" "$USER"@"$ADDR":/home/ekh/paymentProcessing/