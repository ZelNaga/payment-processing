package ua;

import org.slf4j.LoggerFactory;
import ua.utils.DbfFile;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by asv on 29.11.16.
 */
//TODO: Develop architecture for DBF files building
public class DBFBuilder {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DBFBuilder.class);

    public static void launchDBFsBuilding(Connection c) {
        try (ResultSet rs = ReportsDAO.getAllBanksId(c)) {
            while (rs.next()) {
                String orgId = rs.getString("id");
                generate(c, orgId);
            }
        } catch (Exception e) {
            LOGGER.error("Exception accuse in dbf generation", e);
        }
    }

    private static void generate(Connection c, String orgId) throws Exception {
        String sql = "call process.dbf_bank(?, ?, ?);";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setString(1, orgId);
        pstmt.setString(2, ReportsConfig.getStartDate());
        pstmt.setString(3, ReportsConfig.getEndDate());

        String dirName = "dbf/";

        File theDir = new File(dirName);

        if (!theDir.exists()) {
            try {
                theDir.mkdirs();
            } catch(SecurityException e) {
                LOGGER.error("Cant create dir {} ! ",  dirName, e);
            }
        }

        try (ResultSet rs = pstmt.executeQuery()) {
            if (rs.isBeforeFirst()) {
                DbfFile.resultSetToDbf(rs, dirName + orgId + ".dbf");
            }
        } catch (Exception e) {
            LOGGER.error("Exception accuse in resultSetToDbf", e);
        }
    }
}
