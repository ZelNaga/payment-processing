package ua;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalAdjusters.*;
import static java.time.temporal.ChronoUnit.DAYS;
import static ua.StringHelper.stringConcat;



/**
 * Created by zelnaga on 14.06.16.
 */
public class DateHelper {
    private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final String FIRST_DAY_OF_MONTH = calculateFirstDayOfMonth();
    public static final String REPORT_DATE = getReportDate();

    private static String calculateFirstDayOfMonth() {
        LocalDate initial = LocalDate.parse(ReportsConfig.getStartDate(), DATETIME_FORMATTER);
        LocalDate firstDayOfMonth = initial.with(firstDayOfMonth());
        return firstDayOfMonth.toString();
    }

    private static boolean isDaysBetween() {
        LocalDate startDate = LocalDate.parse(ReportsConfig.getStartDate(), DATETIME_FORMATTER);
        LocalDate endDate = LocalDate.parse(ReportsConfig.getEndDate(), DATETIME_FORMATTER);
        return DAYS.between(startDate, endDate) > 0;
    }

    private static String getReportDate() {
        return isDaysBetween() ? stringConcat(ReportsConfig.getStartDate(), " - ", ReportsConfig.getEndDate())
                                          : ReportsConfig.getStartDate();
    }
}
