package ua;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;
import java.awt.Color;
import net.sf.dynamicreports.report.builder.ReportTemplateBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.VerticalTextAlignment;

/**
 * Created by zelnaga on 28.03.16.
 */
public class Templates {
    public static final StyleBuilder rootStyle;
    public static final StyleBuilder boldStyle;
    public static final StyleBuilder italicStyle;
    public static final StyleBuilder titleStyle;
    public static final StyleBuilder columnStyle;
    public static final StyleBuilder columnTitleStyle;
    public static final StyleBuilder groupStyle;
    public static final StyleBuilder subtotalStyle;
    public static final ReportTemplateBuilder reportTemplate;

    static {
        rootStyle           = stl.style().setPadding(2);
        boldStyle           = stl.style(rootStyle).bold();
        italicStyle         = stl.style(rootStyle).italic();
        titleStyle          = stl.style(boldStyle).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        columnStyle         = stl.style(rootStyle)
                                 .setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)
                                 .setBorder(stl.penThin());
        columnTitleStyle    = stl.style(columnStyle)
                                 .setBorder(stl.pen1Point())
                                 .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                                 .setBackgroundColor(Color.LIGHT_GRAY)
                                 .bold();
        groupStyle          = stl.style(boldStyle)
                                 .setHorizontalTextAlignment(HorizontalTextAlignment.LEFT);
        subtotalStyle       = stl.style(boldStyle)
                                 .setTopBorder(stl.pen1Point())
                                 .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        reportTemplate      = template().setTitleStyle(titleStyle)
                                        .setColumnStyle(columnStyle)
                                        .setColumnTitleStyle(columnTitleStyle)
                                        .setGroupStyle(groupStyle)
                                        .setGroupTitleStyle(groupStyle)
                                        .setSubtotalStyle(subtotalStyle)
                                        .ignorePagination();
    }
}
