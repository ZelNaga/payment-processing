package ua;

import com.google.common.eventbus.EventBus;
import ua.events.ReportProgressEvent;

/**
 * Created by zelnaga on 30.11.16.
 */
public class ReportProgressHandler {

    private EventBus reportEventBus;
    private int totalReportProgress;
    private Double lastReportPercent = .0;
    private int currentReportProgress;
    private ReportProgressEvent reportProgressEvent = new ReportProgressEvent();

    public ReportProgressHandler(EventBus reportEventBus) {
        this.reportEventBus = reportEventBus;
    }

    public void processProgress() {
        currentReportProgress++;
        int percent = (int)((double) currentReportProgress /(double)(totalReportProgress) * 100);
        if( lastReportPercent != percent ) {
            lastReportPercent = (double) percent;
            reportProgressEvent.setReportProgress(percent);
            reportEventBus.post(reportProgressEvent);
        }
    }

    public void resetInitialState(int reportsNumber, String reportName) {
        currentReportProgress = 0;
        this.totalReportProgress = reportsNumber;
        reportProgressEvent.setReportName(reportName);
        reportProgressEvent.setReportProgress(0);
        reportEventBus.post(reportProgressEvent);
    }
}
