package ua.reports;

import static ua.StringHelper.stringConcat;

import com.google.common.collect.ImmutableMap;
import ua.DateHelper;
import ua.ReportBuilder;
import ua.ReportsConfig;

import java.sql.*;
import java.util.Map;

import ua.ReportsDAO;

public class ReportZhekByBank extends ManyReportsGenerator implements RowGrouper {
    private static final String REPORT_NAME = "zhek_by_banks";
    private static final String REPORT_TITLE = stringConcat("Реэстр платежів за " ,
                                                            DateHelper.REPORT_DATE,
                                                            ". Отримувач платежу ");
    private static final int FIRST_REPORT_COLUMN = 1;
    private static final Map<String, String> COLUMNS_ALIASES = ImmutableMap.<String, String>builder()
            .put("balance_org_name", "Назва балансової організації")
            .put("service_name", "Послуга")
            .put("acceptor_org_name", "Одержувач платежу")
            .put("start_value", "Сума платежу грн.")
            .put("transfer_value", "Сума перерах. грн.")
            .put("total_commission", "Сума збору грн.")
            .put("1", "ЦМСУ")
            .put("2", "Приймальник")
            .put("4", "ЖЕК")
            .build();
    public static final String ROWS_GROUP_COLUMN_NAME = "balance_org_name";
    public static final int START_SUMMING_COLUMN = 2;
    public static final String ROWS_GROUP_TITLE = "Пункт прийому";

    protected  ResultSet getDataSource(Connection c, ResultSet params) throws SQLException {
        String sql =  "call process.report_zhek_by_banks(?, ?, ?);";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setInt(1, params.getInt("id"));
        pstmt.setString(2, ReportsConfig.getStartDate());
        pstmt.setString(3, ReportsConfig.getEndDate());
        return pstmt.executeQuery();
    }

    protected String getReportName() {
        return REPORT_NAME;
    }

    protected String getReportTitle() {
        return REPORT_TITLE;
    }

    protected int getFirstReportColumn() {
        return FIRST_REPORT_COLUMN;
    }

    protected Map<String, String> getColumnsAliases() {
        return COLUMNS_ALIASES;
    }

    protected ResultSet getOrganizationsList(Connection c) throws SQLException {
        return ReportsDAO.getAllZheksId(c);
    }

    protected int getStartSummingColumn() {
        return START_SUMMING_COLUMN;
    }

    public String getRowsGroupColumnName() {
        return ROWS_GROUP_COLUMN_NAME;
    }

    public String getRowsGroupTitle() {
        return ROWS_GROUP_TITLE;
    }


}
