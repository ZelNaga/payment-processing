package ua.reports;

import com.google.common.collect.ImmutableMap;
import ua.DateHelper;
import ua.ReportBuilder;
import ua.ReportsConfig;
import ua.ReportsDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static ua.StringHelper.stringConcat;

/**
 * Created by zelnaga on 22.06.16.
 */
public class ReportCommissionsForCmsu extends ManyReportsGenerator implements RowGrouper {
    private static final String REPORT_NAME = "commissions_for_cmsu";
    private static final String REPORT_TITLE = stringConcat("Зведена вiдомiсть за ",
            DateHelper.REPORT_DATE, " для ");
    private static final int FIRST_REPORT_COLUMN = 1;
    private static final Map<String, String> COLUMNS_ALIASES = ImmutableMap.<String, String>builder()
            .put("supplier_org_name", "Одержувач платежу (постачальник послуг)")
            .put("start_value", "Сума платежу")
            .put("transfer_value", "Сума перерах.")
            .put("commission", "Сума збору ТОВ ЦМСУ")
            .build();
    public static final String ROWS_GROUP_COLUMN_NAME = "main_org_name";
    private static final int START_SUMMING_COLUMN = 1;
    public static final String ROWS_GROUP_TITLE = "Пункт прийому";


    protected ResultSet getDataSource(java.sql.Connection c, ResultSet params) throws SQLException {
        String sql =    "SELECT (SELECT name FROM main_new.m_org_attributes WHERE org_id = p.main_org_id) AS main_org_name, " +
                        "       (SELECT name FROM main_new.m_org_attributes WHERE org_id = p.supplier_org_id) AS supplier_org_name, " +
                        "       SUM(p.start_value) AS start_value, " +
                        "       SUM(p.transfer_value) AS transfer_value, " +
                        "       SUM(c.comission) AS commission " +
                        "FROM process.processed_payments AS p " +
                        "JOIN process.processed_days AS d " +
                        "ON p.processed_day_id = d.id " +
                        "JOIN process.payments_commissions AS c " +
                        "ON p.payment_id = c.payment_id " +
                        "WHERE d.processed_date BETWEEN ? AND ? " +
                        "AND c.comission_acceptor_org_id = ? " +
                        "GROUP BY p.main_org_id;";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setString(1, ReportsConfig.getStartDate());
        pstmt.setString(2, ReportsConfig.getEndDate());
        pstmt.setInt(3, params.getInt("id"));
        return pstmt.executeQuery();
    }

    protected String getReportName() {
        return REPORT_NAME;
    }

    protected String getReportTitle() {
        return REPORT_TITLE;
    }

    protected int getFirstReportColumn() {
        return FIRST_REPORT_COLUMN;
    }

    protected Map<String, String> getColumnsAliases()    {
        return COLUMNS_ALIASES;
    }

    protected ResultSet getOrganizationsList(Connection c) throws SQLException {
        return ReportsDAO.getCmsuInfo(c);
    }

    protected int getStartSummingColumn() {
        return START_SUMMING_COLUMN;
    }

    public String getRowsGroupColumnName() {
        return ROWS_GROUP_COLUMN_NAME;
    }

    public String getRowsGroupTitle() {
        return ROWS_GROUP_TITLE;
    }

}
