package ua.reports;

import com.google.common.base.Stopwatch;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import ua.ReportBuilder;
import ua.events.*;

import java.sql.Connection;
import java.sql.ResultSet;

import static ua.StringHelper.stringConcat;

/**
 * Created by zelnaga on 24.06.16.
 */
public abstract class SingleReportGenerator extends ReportBuilder {
    public void generate(Connection c) {
        makeReportDirectories();
        Stopwatch sw = Stopwatch.createStarted();
        try (ResultSet dataSource = getDataSource(c, null)) {
            dataSource.last();
            progressHandler.resetInitialState(dataSource.getRow(), getReportName());
            dataSource.beforeFirst();
            if (dataSource.isBeforeFirst()) {
                JasperReportBuilder builder = buildReport(dataSource, getReportTitle());
                saveReport(builder, getReportName());
                progressHandler.processProgress();
            }
        } catch (Exception e) {
            LOGGER.error("Error during making {} report", getReportName(), e);
            eventBus.post(new ReportsBuildingErrorEvent(stringConcat("Error during making ", getReportName(), " report")));
        }
        eventBus.post(new TotalProgressEvent());
        LOGGER.info("Reports {} building finished ! Elapsed time: [{}]", getReportName(), sw.stop());
        eventBus.post(new ReportsBuildingMsgEvent(stringConcat("Reports ", getReportName(),
                                                                " building finished ! Elapsed time: ",
                                                                sw.toString())));
    }
}
