package ua.reports;

/**
 * Created by zelnaga on 02.06.16.
 */
public interface TitleGrouper {
    String getGroupTitle();
    int getStartGroupColumn();
}
