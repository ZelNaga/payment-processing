package ua.reports;

import static ua.StringHelper.stringConcat;

import com.google.common.collect.ImmutableMap;
import ua.DateHelper;
import ua.ReportBuilder;
import ua.ReportsConfig;
import ua.ReportsDAO;

import java.sql.*;
import java.util.Map;

/**
 * Created by zelnaga on 26.04.16.
 */
public class ReportSupplierByBanksAndZheks extends ManyReportsGenerator implements RowGrouper {
    private static final String REPORT_NAME = "supplier_by_banks_and_zheks";
    private static final String REPORT_TITLE = stringConcat("Реэстр платежів за " ,
                                                            DateHelper.REPORT_DATE,
                                                            ". Отримувач платежу ");
    private static final int FIRST_REPORT_COLUMN = 1;
    private static final Map<String, String> COLUMNS_ALIASES = ImmutableMap.<String, String>builder()
            .put("zhek_name", "Установа")
            .put("service_name", "Послуга")
            .put("start_value", "Сума платежу")
            .put("transfer_value", "Сума перерах.")
            .put("total_commission", "Сума збору")
            .put("1", "Сума збору ЦМСУ")
            .put("2", "Сума збору Приймальника")
            .put("4", "Сума збору ЖЕК")
            .put("total_payments", "Кільк")
            .build();
    public static final String ROWS_GROUP_COLUMN_NAME = "bank_name";
    public static final int START_SUMMING_COLUMN = 2;
    public static final String ROWS_GROUP_TITLE = "Пункт прийому";

    protected ResultSet getDataSource(java.sql.Connection c, ResultSet params) throws SQLException {
        String sql =  "call process.report_supplier_by_banks_and_zheks(?, ?, ?);";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setInt(1, params.getInt("id"));
        pstmt.setString(2, ReportsConfig.getStartDate());
        pstmt.setString(3, ReportsConfig.getEndDate());
        return pstmt.executeQuery();
    }

    protected String getReportName() {
        return REPORT_NAME;
    }

    protected String getReportTitle() {
        return REPORT_TITLE;
    }

    protected int getFirstReportColumn() {
        return FIRST_REPORT_COLUMN;
    }

    protected Map<String, String> getColumnsAliases()    {
        return COLUMNS_ALIASES;
    }

    protected ResultSet getOrganizationsList(Connection c) throws SQLException {
        return ReportsDAO.getAllSuppliersId(c);
    }

    protected int getStartSummingColumn() {
        return START_SUMMING_COLUMN;
    }

    public String getRowsGroupColumnName() {
        return ROWS_GROUP_COLUMN_NAME;
    }

    public String getRowsGroupTitle() {
        return ROWS_GROUP_TITLE;
    }
}
