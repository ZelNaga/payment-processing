package ua.reports;

import com.google.common.collect.ImmutableMap;
import ua.DateHelper;
import ua.ReportBuilder;
import ua.ReportsConfig;
import ua.ReportsDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static ua.StringHelper.stringConcat;

/**
 * Created by zelnaga on 15.06.16.
 */
public class ReportBankForBookkeeping extends ManyReportsGenerator {
    private static final String REPORT_NAME = "bank_for_bookkeeping";
    private static final String REPORT_TITLE = stringConcat("Зведена відомість по сплаті за комунальні послуги (для ЦМСУ) за ",
                                                            DateHelper.FIRST_DAY_OF_MONTH,
                                                            " - ",
                                                            ReportsConfig.getEndDate(),
                                                            ". Приймальник ");
    private static final int FIRST_REPORT_COLUMN = 0;
    private static final Map<String, String> COLUMNS_ALIASES = ImmutableMap.<String, String>builder()
            .put("name", "Одержувач платежу")
            .put("mfo", "МФО")
            .put("account", "Р/Р")
            .put("start_value", "Сума платежу")
            .put("transfer_value", "Сума перерах.")
            .put("1", "ЦМСУ")
            .put("2", "Приймальник")
            .put("4", "ЖЕК")
            .put("total_payments", "Кільк. плат.")
            .build();
    public static final int START_SUMMING_COLUMN = 4;

    protected ResultSet getDataSource(java.sql.Connection c, ResultSet params) throws SQLException {
        String sql =  "call process.report_bank(?, ?, ?);";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setInt(1, params.getInt("id"));
        pstmt.setString(2, DateHelper.FIRST_DAY_OF_MONTH);
        pstmt.setString(3, ReportsConfig.getEndDate());
        return pstmt.executeQuery();
    }

    protected String getReportName() {
        return REPORT_NAME;
    }

    protected String getReportTitle() {
        return REPORT_TITLE;
    }

    protected int getFirstReportColumn() {
        return FIRST_REPORT_COLUMN;
    }

    protected Map<String, String> getColumnsAliases()    {
        return COLUMNS_ALIASES;
    }

    protected ResultSet getOrganizationsList(Connection c) throws SQLException {
        return ReportsDAO.getAllBanksId(c);
    }

    protected int getStartSummingColumn() {
        return START_SUMMING_COLUMN;
    }

}
