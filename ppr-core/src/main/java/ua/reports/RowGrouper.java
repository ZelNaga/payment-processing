package ua.reports;

/**
 * Created by zelnaga on 16.05.16.
 */
public interface RowGrouper {
    String getRowsGroupColumnName();
    String getRowsGroupTitle();
}
