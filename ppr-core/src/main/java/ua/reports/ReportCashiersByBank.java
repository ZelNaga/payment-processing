package ua.reports;

import com.google.common.collect.ImmutableMap;
import ua.DateHelper;
import ua.ReportBuilder;
import ua.ReportsConfig;
import ua.ReportsDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static ua.StringHelper.stringConcat;

/**
 * Created by zelnaga on 22.06.16.
 */
public class ReportCashiersByBank extends ManyReportsGenerator {
    private static final String REPORT_NAME = "cashiers_by_bank";
    private static final String REPORT_TITLE = stringConcat("ЗВIТ про надходження коштiв за ",
            DateHelper.REPORT_DATE,
            ". Приймальник ");
    private static final int FIRST_REPORT_COLUMN = 0;
    private static final Map<String, String> COLUMNS_ALIASES = ImmutableMap.<String, String>builder()
            .put("lfm", "ПІБ Касира")
            .put("sum", "Сума платежу")
            .put("total_payments", "Кiльк. платежів")
            .build();
    public static final int START_SUMMING_COLUMN = 1;

    protected ResultSet getDataSource(java.sql.Connection c, ResultSet params) throws SQLException {
        String sql = "SELECT  CONCAT(u.last_name, ' '," +
                     "               SUBSTRING(u.first_name, 1, 1), '.'," +
                     "               SUBSTRING(u.middle_name, 1, 1), '.'" +
                     "              ) AS lfm, " +
                     "SUM(p.start_value) AS sum, " +
                     "COUNT(p.user_id) AS total_payments " +
                     "FROM process.processed_payments AS p " +
                     "JOIN process.processed_days AS d " +
                     "ON p.processed_day_id = d.id " +
                     "JOIN online.tbl_user AS u " +
                     "ON p.user_id = u.id " +
                     "WHERE p.main_org_id = ? " +
                     "AND d.processed_date BETWEEN ? AND ? " +
                     "GROUP BY p.user_id;";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setInt(1, params.getInt("id"));
        pstmt.setString(2, ReportsConfig.getStartDate());
        pstmt.setString(3, ReportsConfig.getEndDate());
        return pstmt.executeQuery();
    }

    protected String getReportName() {
        return REPORT_NAME;
    }

    protected String getReportTitle() {
        return REPORT_TITLE;
    }

    protected int getFirstReportColumn() {
        return FIRST_REPORT_COLUMN;
    }

    protected Map<String, String> getColumnsAliases()    {
        return COLUMNS_ALIASES;
    }

    protected ResultSet getOrganizationsList(Connection c) throws SQLException {
        return ReportsDAO.getAllBanksId(c);
    }

    protected int getStartSummingColumn() {
        return START_SUMMING_COLUMN;
    }
}
