package ua.reports;

import com.google.common.collect.ImmutableMap;
import ua.DateHelper;
import ua.ReportsConfig;

import java.sql.*;
import java.util.Map;

import static ua.StringHelper.stringConcat;

/**
 * Created by zelnaga on 21.06.16.
 */
public class ReportPaymentsBySuppliers extends SingleReportGenerator {
    private static final String REPORT_NAME = "payments_by_suppliers";
    private static final String REPORT_TITLE = stringConcat("Зведений звіт по обробці платежів за ",
            DateHelper.REPORT_DATE);
    private static final int FIRST_REPORT_COLUMN = 0;
    private static final Map<String, String> COLUMNS_ALIASES = ImmutableMap.<String, String>builder()
            .put("name", "Назва організації")
            .put("rank", "№ з/п")
            .put("sum", "Сума, грн")
            .build();
    public static final int START_SUMMING_COLUMN = 2;

    protected ResultSet getDataSource(java.sql.Connection c, ResultSet params) throws SQLException {
        try(Statement stm = c.createStatement()) {
            stm.execute("SET @rank = 0;");
        } catch (SQLException e) {
            LOGGER.error("Error accrue when try too set session variable in Mysql");
        }

        String sql =    "SELECT @rank := @rank+1 AS rank, " +
                        "       a.name, " +
                        "       SUM(p.start_value) AS sum " +
                        "FROM process.processed_payments AS p " +
                        "JOIN process.processed_days AS d " +
                        "ON p.processed_day_id = d.id " +
                        "LEFT JOIN main_new.m_org_attributes AS a " +
                        "ON p.supplier_org_id = a.org_id " +
                        "WHERE p.supplier_org_id IN ( " +
                        "                               SELECT org_id " +
                        "                               FROM main_new.m_org_cmsu " +
                        "                               WHERE supplier_id != 0 " +
                        "                            ) " +
                        "AND d.processed_date BETWEEN ? AND ? " +
                        "AND a.enabled = TRUE " +
                        "GROUP BY p.supplier_org_id " +
                        "ORDER BY rank;";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setString(1, ReportsConfig.getStartDate());
        pstmt.setString(2, ReportsConfig.getEndDate());
        return pstmt.executeQuery();
    }

    protected String getReportName() {
        return REPORT_NAME;
    }

    protected String getReportTitle() {
        return REPORT_TITLE;
    }

    protected int getFirstReportColumn() {
        return FIRST_REPORT_COLUMN;
    }

    protected Map<String, String> getColumnsAliases()    {
        return COLUMNS_ALIASES;
    }

    protected int getStartSummingColumn() {
        return START_SUMMING_COLUMN;
    }
}
