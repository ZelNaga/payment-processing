package ua.reports;

import com.google.common.collect.ImmutableMap;
import ua.DateHelper;
import ua.ReportBuilder;
import ua.ReportsConfig;
import ua.ReportsDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static ua.StringHelper.stringConcat;

/**
 * Created by zelnaga on 24.05.16.
 */
public class ReportOsbbByBanksAndAccounts extends ManyReportsGenerator implements RowGrouper {
    private static final String REPORT_NAME = "osbb_by_banks_and_accounts";
    private static final String REPORT_TITLE = stringConcat("Реэстр платежів за " ,
            DateHelper.REPORT_DATE,
            " в розрізі квартир. Установа ");
    private static final int FIRST_REPORT_COLUMN = 1;
    private static final Map<String, String> COLUMNS_ALIASES = ImmutableMap.<String, String>builder()
            .put("balance_org_name", "Назва балансової організації")
            .put("account", "Особовий рахунок")
            .put("address", "Ареса")
            .put("owner", "П І Б")
            .put("service_name", "Послуга")
            .put("start_value", "Сума платежу грн.")
            .put("transfer_value", "Сума перерах. грн.")
            .put("total_commission", "Сума збору грн.")
            .put("1", "ЦМСУ")
            .put("2", "Приймальник")
            .put("4", "ЖЕК")
            .build();
    public static final String ROWS_GROUP_COLUMN_NAME = "balance_org_name";
    public static final int START_SUMMING_COLUMN = 4;
    public static final String ROWS_GROUP_TITLE = "Пункт прийому";

    protected ResultSet getDataSource(Connection c, ResultSet params) throws SQLException {
        String sql =  "call process.report_osbb_by_banks_and_accounts(?, ?, ?, ?);";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setInt(1, params.getInt("id"));
        pstmt.setInt(2, params.getInt("zhek"));
        pstmt.setString(3, ReportsConfig.getStartDate());
        pstmt.setString(4, ReportsConfig.getEndDate());
        return pstmt.executeQuery();
    }

    protected String getReportName() {
        return REPORT_NAME;
    }

    protected String getReportTitle() {
        return REPORT_TITLE;
    }

    protected int getFirstReportColumn() {
        return FIRST_REPORT_COLUMN;
    }

    protected Map<String, String> getColumnsAliases() {
        return COLUMNS_ALIASES;
    }

    protected ResultSet getOrganizationsList(Connection c) throws SQLException {
        return ReportsDAO.getAllOsbbId(c);
    }

    protected int getStartSummingColumn() {
        return START_SUMMING_COLUMN;
    }

    public String getRowsGroupColumnName() {
        return ROWS_GROUP_COLUMN_NAME;
    }

    public String getRowsGroupTitle() {
        return ROWS_GROUP_TITLE;
    }
}
