package ua.reports;

import com.google.common.base.Stopwatch;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import ua.ReportBuilder;
import ua.events.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static ua.StringHelper.stringConcat;

/**
 * Created by zelnaga on 24.06.16.
 */
public abstract class ManyReportsGenerator extends ReportBuilder {
    protected abstract ResultSet getOrganizationsList(Connection c) throws SQLException;

    public void generate(Connection c) {
        makeReportDirectories();
        try (ResultSet organizationsList =  getOrganizationsList(c)) {
            Stopwatch sw = Stopwatch.createStarted();
            organizationsList.last();
            progressHandler.resetInitialState(organizationsList.getRow(), getReportName());
            organizationsList.beforeFirst();
            while (organizationsList.next()) {
                try (ResultSet dataSource = getDataSource(c, organizationsList)) {
                    if (dataSource.isBeforeFirst()) {
                        JasperReportBuilder builder = buildReport(dataSource, stringConcat(getReportTitle(), organizationsList.getString("org_name")));
                        saveReport(builder, organizationsList.getString("id"));
                    }
                } catch (Exception e) {
                    LOGGER.error("Error during making {} report for organization id {}",
                            getReportName(), organizationsList.getString("id"), e);
                    eventBus.post(new ReportsBuildingErrorEvent(stringConcat("Error during making ", getReportName(),
                                                                                " report for organization id ",
                                                                                organizationsList.getString("id"))));
                }
                progressHandler.processProgress();
            }
            eventBus.post(new TotalProgressEvent());
            LOGGER.info("Reports {} building finished ! Elapsed time: [{}]", getReportName(), sw.stop());
            eventBus.post(new ReportsBuildingMsgEvent(stringConcat("Reports ", getReportName(),
                                                                    " building finished ! Elapsed time: ",
                                                                    sw.toString())));
        } catch (Exception e) {
            LOGGER.error("Error during getting organization list for {} report", getReportName(), e);
            eventBus.post(new ReportsBuildingErrorEvent(stringConcat("Error during getting organization list for ",
                                                                        getReportName(),
                                                                        " report")));
        }
    }

}
