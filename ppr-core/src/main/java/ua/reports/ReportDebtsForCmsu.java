package ua.reports;

import com.google.common.collect.ImmutableMap;
import ua.DateHelper;
import ua.ReportsConfig;
import ua.ReportsDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static ua.StringHelper.stringConcat;

/**
 * Created by zelnaga on 02.06.16.
 */
public class ReportDebtsForCmsu extends ManyReportsGenerator implements TitleGrouper {
    private static final String REPORT_NAME = "debts_for_cmsu";
    private static final String REPORT_TITLE = stringConcat("Звіт  про  утримання коштів за ",
            DateHelper.REPORT_DATE,
            " на користь ");
    private static final int FIRST_REPORT_COLUMN = 0;
    private static final Map<String, String> COLUMNS_ALIASES = ImmutableMap.<String, String>builder()
            .put("name", "Назва контрагента")
            .put("reason", "Підстава (розпорядження, номер, дата)")
            .put("sub_title", "Суми утримання за числами місяця (грн.)")
            .put("sum", "Всього")
            .build();
    private static final int START_SUMMING_COLUMN = 2;
    private static final int START_GROUP_COLUMN = 2;

    private static final String COLUMNS_GROUP_TITLE = "Суми утримання за числами місяця (грн.)";

    protected ResultSet getDataSource(java.sql.Connection c, ResultSet params) throws SQLException {
        String sql =  "call process.report_money_for_cmsu(?, ?, ?);";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setInt(1, params.getInt("id"));
        pstmt.setString(2, ReportsConfig.getStartDate());
        pstmt.setString(3, ReportsConfig.getEndDate());
        return pstmt.executeQuery();
    }

    protected String getReportName() {
        return REPORT_NAME;
    }

    protected String getReportTitle() {
        return REPORT_TITLE;
    }

    protected int getFirstReportColumn() {
        return FIRST_REPORT_COLUMN;
    }

    protected Map<String, String> getColumnsAliases()    {
        return COLUMNS_ALIASES;
    }

    protected ResultSet getOrganizationsList(Connection c) throws SQLException {
        return ReportsDAO.getCmsuInfo(c);
    }

    protected int getStartSummingColumn() {
        return START_SUMMING_COLUMN;
    }

    public String getGroupTitle() {
        return COLUMNS_GROUP_TITLE;
    }

    public int getStartGroupColumn() {
        return START_GROUP_COLUMN;
    }
}
