package ua.reports;

import com.google.common.collect.ImmutableMap;
import ua.DateHelper;
import ua.ReportsConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static ua.StringHelper.stringConcat;

/**
 * Created by zelnaga on 23.06.16.
 */
public class ReportAllDebts extends SingleReportGenerator implements TitleGrouper, RowGrouper {
    private static final String REPORT_NAME = "all_debts";
    private static final String REPORT_TITLE = stringConcat("Звіт про відображення сум розрахунків (за надані послуги між постачальниками послуг,фінансовими установами міста та іншими юридичними особами(за календарний місяць)) за ",
            DateHelper.REPORT_DATE);
    private static final int FIRST_REPORT_COLUMN = 1;
    private static final Map<String, String> COLUMNS_ALIASES = ImmutableMap.<String, String>builder()
            .put("reason", "Підстава (розпорядження, номер, дата)")
            .put("acceptor_name", "Суми утримання за числами місяця (грн.)")
            .put("sum", "Всього")
            .build();
    private static final int START_SUMMING_COLUMN = 2;
    private static final int START_GROUP_COLUMN = 2;

    private static final String COLUMNS_GROUP_TITLE = "Суми утримання за числами місяця (грн.)";
    private static final String ROWS_GROUP_COLUMN_NAME = "supplier_name";
    private static final String ROWS_GROUP_TITLE = "Назва постачальника комунальних послуг";

    protected ResultSet getDataSource(java.sql.Connection c, ResultSet params) throws SQLException {
        String sql =  "call process.report_all_debts(?, ?);";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setString(1, ReportsConfig.getStartDate());
        pstmt.setString(2, ReportsConfig.getEndDate());
        return pstmt.executeQuery();
    }

    protected String getReportName() {
        return REPORT_NAME;
    }

    protected String getReportTitle() {
        return REPORT_TITLE;
    }

    protected int getFirstReportColumn() {
        return FIRST_REPORT_COLUMN;
    }

    protected Map<String, String> getColumnsAliases()    {
        return COLUMNS_ALIASES;
    }

    protected ResultSet getOrganizationsList(Connection c) throws SQLException {
        return null;
    }

    protected int getStartSummingColumn() {
        return START_SUMMING_COLUMN;
    }

    public String getGroupTitle() {
        return COLUMNS_GROUP_TITLE;
    }

    public int getStartGroupColumn() {
        return START_GROUP_COLUMN;
    }

    public String getRowsGroupColumnName() {
        return ROWS_GROUP_COLUMN_NAME;
    }

    public String getRowsGroupTitle() {
        return ROWS_GROUP_TITLE;
    }
}
