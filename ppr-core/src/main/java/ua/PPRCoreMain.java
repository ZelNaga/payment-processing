package ua;

import static ua.StringHelper.stringConcat;


import com.google.common.base.Stopwatch;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableList;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.slf4j.LoggerFactory;
import ua.events.*;
import ua.reports.*;
import java.sql.Connection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class PPRCoreMain {

    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PPRCoreMain.class);
    private static Supplier<PPRCoreMain> instance = Suppliers.memoize(PPRCoreMain::new);

    private EventBus PPREventBus = new EventBus();
    private Optional<ReportsBuildingListener> listener = Optional.empty();
    private List<ReportBuilder> reportsList;

    private PPRCoreMain() {}

    public static PPRCoreMain getInstance() {
        return instance.get();
    }

    public void registerProgressListener(Object listener) {
        PPREventBus.register(listener);
    }

    public void setPPRListener(ReportsBuildingListener listener ) {
        this.listener = Optional.of(listener);
    }

    public void removePPRListener() {
        listener = null;
    }

    @Subscribe
    @AllowConcurrentEvents
    public synchronized void onTotalPPRProgress(Double percents) {
        this.fireOnTotalProgress(percents);
    }

    @Subscribe
    @AllowConcurrentEvents
    public synchronized void onPPRProgress(ReportProgressEvent reportProgressEvent) {
        this.fireOnReportProgress(reportProgressEvent);
    }


    @Subscribe
    @AllowConcurrentEvents
    public synchronized void onPPRMessage(ReportsBuildingMsgEvent msg) {
        this.fireOnMessage(msg.getMsg());
    }


    @Subscribe
    @AllowConcurrentEvents
    public synchronized void onPPRError(ReportsBuildingErrorEvent msg) {
        this.fireOnError(msg.getErrorMsg());
    }

    private void fireStartReportsBuilding() {
        PPREventBus.post(new ReportsBuildingBegin());
        listener.ifPresent( (l) -> l.onStart() );
    }

    private void fireEndReportsBuilding() {
        PPREventBus.post( new ReportsBuildingEnd() );
        listener.ifPresent( (l) -> l.onFinish() );
    }

    private void fireOnTotalProgress(double progress) {
        listener.ifPresent((l) -> l.onTotalProgress(progress));
    }

    private void fireOnReportProgress(ReportProgressEvent event) {
        listener.ifPresent((l) -> l.onReportProgress(event));
    }

    private void fireOnMessage(String msg) {
        listener.ifPresent((l) -> l.onMessage(msg));
    }

    private void fireOnError(String msg) {
        listener.ifPresent((l) -> l.onError(msg));
    }

    public void proceed(boolean loadConfiguration) {
        Stopwatch sw = Stopwatch.createStarted();
        PPREventBus.register(this);
        fireStartReportsBuilding();

        try {
            Locale.setDefault(new Locale("ua", "UA"));
            ReportsConfig.loadConfig(loadConfiguration);
            LOGGER.info("Start building reports for : [{}]", DateHelper.REPORT_DATE);
            fireOnMessage(stringConcat("Start building reports for : ",  DateHelper.REPORT_DATE));
            buildReportsList();
            launchReportsBuilding(new ReportsBuildingListenerInternal(0));
        } catch (Exception e) {
            LOGGER.error("PPRCoreMain FAILED: ", e);
            fireOnError("PPRCoreMain FAILED");
        } finally {
            LOGGER.info("Reports building finished: Total elapsed time: [{}]", sw.stop());
            fireOnMessage(stringConcat("Reports building finished: Total elapsed time: ", sw.toString()));
            fireEndReportsBuilding();
            PPREventBus.unregister(this);
            listener.ifPresent((l) -> this.removePPRListener());
        }
    }

    private void buildReportsList() {
        this.reportsList = ImmutableList.of(new ReportDebtsForCmsu(),
                                             new ReportOsbbByBanksAndAccounts(),
                                             new ReportBank(),
                                             new ReportBankForBookkeeping(),
                                             new ReportZhekByBank(),
                                             new ReportZhekByBanksAndServices(),
                                             new ReportSupplierByBanksAndZheks(),
                                             new ReportSupplierByZheks(),
                                             new ReportPaymentsBySuppliers(),
                                             new ReportCashiersByBank(),
                                             new ReportCommissionsForCmsu(),
                                             new ReportAllDebts());
    }

    private void launchReportsBuilding(ReportsBuildingListenerInternal internalListener) {
        try(Connection c = ReportsDAO.getConnection()) {

            internalListener.setTotal(reportsList.size());
            internalListener.setAnotherEventBus(PPREventBus);

            reportsList.forEach(r -> {
                r.registerReportsBuildingListener(internalListener);
                r.generate(c);
            });
        } catch (Exception e) {
            LOGGER.error("Reports building FAILED: ", e);
            fireOnError("Reports building FAILED");
        }
    }

    public void proceed(String startDate, String endDate) {
        try {
            ReportsConfig.loadConfig(true);
            ReportsConfig.setStartEndProcessing(startDate, endDate);
        } catch ( Exception e ) {
            LOGGER.error("LoadConfig FAILED: {}", e);
            fireOnError("LoadConfig FAILED");
        }
        proceed(false);
    }
}
