package ua;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import ua.events.*;

/**
 * Created by ekh on 18.11.15.
 */
class ReportsBuildingListenerInternal {

    private int total;
    private EventBus anotherEventBus;
    private Double lastPercent = .0;
    private volatile int current;

    public ReportsBuildingListenerInternal(int totalProgressValue) {
        this.total = totalProgressValue;
    }

    @Subscribe
    public synchronized void onTotalProgress(TotalProgressEvent progressEvent) {
        current++;
        int percent = (int)((double)current/(double)(total) * 100);
        if( lastPercent != percent ) {
            lastPercent = (double) percent;
            anotherEventBus.post(lastPercent);
        }
    }

    @Subscribe
    public synchronized void onReportProgress(ReportProgressEvent event) {
        anotherEventBus.post(event);
    }

    @Subscribe
    public synchronized void onMessage( ReportsBuildingMsgEvent msg ) {
        anotherEventBus.post(msg);
    }

    @Subscribe
    public synchronized void onError( ReportsBuildingErrorEvent msg ) {
        anotherEventBus.post(msg);
    }

    public void setAnotherEventBus(EventBus eventBus) {
        this.anotherEventBus = eventBus;
    }
    public void setTotal(int total) {
        this.total = total;
    }
}
