package ua.utils;

import org.xBaseJ.DBF;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

/**
 * Created by ekh on 4/26/16.
 */
public class DbfFile {

    static public void resultSetToDbf(ResultSet rs, String fileName) throws Exception {

        ArrayList<DbfField> fields = new ArrayList<>();
        ResultSetMetaData md = rs.getMetaData();
        DBF dbf = new DBF(fileName, true);
        for (int i = 1; i <= md.getColumnCount(); i++) {
            DbfField fld = new DbfField(md.getColumnName(i), md.getColumnType(i), md.getPrecision(i), md.getScale(i));
            fields.add(fld);
            dbf.addField(fld.getField());
        }

        rs.beforeFirst();
        while (rs.next()) {
            String name;
            for (int i = 1; i <= md.getColumnCount(); i++) {
                Object o = rs.getObject(i);
                DbfField fld = fields.get(i - 1);
                fld.put(o);
            }
            dbf.write();
        }
    }
}