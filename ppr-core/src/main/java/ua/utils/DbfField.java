package ua.utils;

import org.xBaseJ.fields.*;
import org.xBaseJ.xBaseJException;

import java.io.IOException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by ekh on 4/25/16.
 */
public class DbfField {

    private int sqlType;
    private int precision;
    private int scale;
    private Field field;
    private String name;

    public Field getField() {
        return field;
    }

    private char padChar = ' ';
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("ddmmyyyy");

    DecimalFormatSymbols dcs;

    private String charSet = "CP1125";

    public DbfField(String name, int sqlType, int precision, int scale) throws IOException, xBaseJException {
        this.sqlType = sqlType;
        this.precision = precision;
        this.scale = scale;
        this.name = name;

        field = createField();

        dcs = new DecimalFormatSymbols();
        dcs.setDecimalSeparator('.');
    }

    private Field createField() throws xBaseJException, IOException {
        Field field = null;
        switch (sqlType) {
            case Types.INTEGER: {
                int n = precision;
                if (precision == -1) n = 6;
                else n = ((precision > 9) ? 9 : precision);
                field = new NumField(name, n, 0);
                break;
            }
            case Types.BIGINT: {
                int n;
                if (precision == -1) n = 12;
                else n = ((precision <= 9) ? 12 : precision);
                field = new NumField(name, n, 0);
                break;
            }
            case Types.FLOAT:
            case Types.DECIMAL:
            case Types.DOUBLE:
            case Types.NUMERIC: {
                int n = precision;
                int d = scale;
                if (precision == -1) n = 10;
                if (scale == -1) d = 2;
                field = new NumField(name, n, d);
                break;
            }
            case Types.TIMESTAMP:
            case Types.TIME:
            case Types.DATE: {
                field = new DateField(name);
                break;
            }
            case Types.VARCHAR: {
                int n = precision;
                if (n == -1) n = 20;
                field = new CharField(name, n);
                break;
            }
            case Types.BOOLEAN: {
                field = new LogicalField(name);
                break;
            }
            case Types.LONGVARCHAR: {
                field = new MemoField(name);
                break;
            }
            default: {
                int n = precision;
                if (n == -1) n = 20;
                field = new CharField(name, n);
                break;
            }
        }
        return field;
    }

    private String padString(final String s, final int n, final char c) {
        if (n <= 0) {
            return s;
        }
        final StringBuilder sb = new StringBuilder(s);
        if (s != null && s.length() > n) {
            return s.substring(0, n);
        }
        while (sb.length() < n) {
            sb.append(c);
        }
        return sb.toString();
    }


    public void put(final Object o) throws xBaseJException, Exception {
        if (sqlType == Types.INTEGER) {
            ((NumField)field).put( (int)o );
        } else if (sqlType == Types.BIGINT) {
            field.put(String.valueOf(o));
        } else if (sqlType == Types.TIMESTAMP) {
            field.put(this.dateFormatter.format((Date) o));
        } else if (sqlType == Types.BOOLEAN) {
            ((LogicalField) field).put((boolean) o);
        } else if (sqlType == Types.FLOAT || sqlType == Types.DOUBLE || sqlType == Types.DECIMAL) {
            field.put(doubleFormatting( ((Number)o).doubleValue(), charSet,  field.getLength(), field.getDecimalPositionCount()) );
        } else if (sqlType == Types.NUMERIC) {
            field.put(o.toString().getBytes());
        } else {
            // TODO: to check len
            field.put( o.toString().getBytes(charSet) );
            //field.put(padString(o.toString(), field.getLength(), padChar));
        }

    }

    public byte[] doubleFormatting(final double n, final String s, final int n2, final int n3) throws Exception {
        final int n4 = n2 - ((n3 > 0) ? (n3 + 1) : 0);
        final StringBuffer sb = new StringBuffer(n2);
        for (int i = 0; i < n4; ++i) {
            sb.append("#");
        }
        if (n3 > 0) {
            sb.append(".");
            for (int j = 0; j < n3; ++j) {
                sb.append("0");
            }
        }
        return textPadding(new DecimalFormat(sb.toString(), dcs).format(n).toString(), s, n2, 12);
    }

    public byte[] textPadding(final String s, final String s2, final int n) throws Exception {
        return textPadding(s, s2, n, 10);
    }

    public byte[] textPadding(final String s, final String s2, final int n, final int n2) throws Exception {
        return textPaddingSupportingUTF8(s, s2, n, n2, (byte) 32);
    }

    public byte[] textPaddingSupportingUTF8(final String s, final String s2, final int n, final int n2, final byte b) throws Exception {
        final byte[] bytes = s.getBytes(s2);
        if (bytes.length > n) {
            return Arrays.copyOf(bytes, n);
        }
        final byte[] array = new byte[n];
        Arrays.fill(array, b);
        switch (n2) {
            case 10: {
                System.arraycopy(bytes, 0, array, 0, bytes.length);
                break;
            }
            case 12: {
                System.arraycopy(bytes, 0, array, n - s.length(), bytes.length);
                break;
            }
        }
        return array;
    }

}
