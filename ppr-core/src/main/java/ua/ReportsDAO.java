package ua;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class ReportsDAO {

    public static final int OSBB_TYPE = 7; // type_id = 7 means that all organizations that belongs to this type is OSBB
    public static final int CMSU_ID = 9999;
    static Logger LOGGER = LoggerFactory.getLogger(ReportsDAO.class);

    /**
     * Параметри для підключення до бази даних
     */
    public static final String USER = ReportsConfig.getDatabasedUser();
    public static final String PASSWORD = ReportsConfig.getDatabasePassword();
    public static final String URL = String.format( "jdbc:mysql://%s:%d",
            ReportsConfig.getDatabaseServer(), ReportsConfig.getDatabsePort() );

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection( URL, USER, PASSWORD );
    }

    public static ResultSet getAllBanksId(Connection c) throws SQLException {
            String sql = "SELECT c.org_id AS id, " +
                                "a.short_name AS org_name " +
                         "FROM main_new.m_org_cmsu AS c " +
                         "JOIN main_new.m_organization AS o " +
                         "ON c.org_id = o.id " +
                         "JOIN main_new.m_org_attributes AS a " +
                         "ON c.org_id = a.org_id " +
                         "WHERE c.bank_id != 0 " + // all organizations where value in column bank_id != 0 is bank
                         "AND (o.isBalance = TRUE OR o.id = parent_id) " +
                         "AND a.enabled = TRUE;";
        return c.prepareStatement(sql).executeQuery();
    }

    public static ResultSet getAllZheksId(Connection c) throws SQLException {
        String sql = "SELECT o.org_id AS id, " +
                            "a.short_name AS org_name " +
                     "FROM main_new.m_org_cmsu AS o " +
                     "JOIN main_new.m_org_attributes AS a " +
                     "ON o.org_id = a.org_id " +
                     "WHERE o.zhek != 0 " + // all organizations where value in column zhek != 0 is zhek
                     "AND a.enabled = TRUE;";
        return c.prepareStatement(sql).executeQuery();
    }

    public static ResultSet getAllSuppliersId(Connection c) throws SQLException {
        String sql = "SELECT o.org_id AS id, " +
                            "a.short_name AS org_name " +
                     "FROM main_new.m_org_cmsu AS o " +
                     "JOIN main_new.m_org_attributes AS a " +
                     "ON o.org_id = a.org_id " +
                     "WHERE o.supplier_id != 0 " + // all organizations where value in column supplier_id != 0 is service supplier
                     "AND a.enabled = TRUE;";
        return c.prepareStatement(sql).executeQuery();
    }

    public static ResultSet getAllZhekCodes(Connection c) throws SQLException {
        String sql = "SELECT DISTINCT zhek AS id, " +
                     "zhek AS org_name " +
                     "FROM main_new.m_org_cmsu " +
                     "WHERE zhek NOT IN (0, 90);"; // 0 - is none from organization types, 90 - is OSBB
        return c.prepareStatement(sql).executeQuery();
    }

    public static ResultSet getAllOsbbId(Connection c) throws SQLException {
        String sql = "SELECT DISTINCT a.org_id AS id, a.name AS org_name, o.zhek " +
                     "FROM main_new.m_org_attributes AS a " +
                     "LEFT JOIN main_new.m_org_cmsu AS o " +
                     "ON a.org_id = o.org_id " +
                     "WHERE a.type_id = ? " +
                     "AND a.enabled = true " +
                     "AND o.zhek != 0;"; // OSBB is some special case of ZHEK
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setInt(1, OSBB_TYPE);
        return pstmt.executeQuery();
    }

    public static ResultSet getCmsuInfo(Connection c) throws SQLException {
        String sql = "SELECT org_id AS id, name AS org_name " +
                     "FROM main_new.m_org_attributes " +
                     "WHERE org_id = ? " +
                     "AND enabled = true;";
        PreparedStatement pstmt = c.prepareStatement(sql);
        pstmt.setInt(1, CMSU_ID);
        return pstmt.executeQuery();
    }
}
