package ua.events;

/**
 * Created by ekh on 02.03.16.
 */
public interface ReportsBuildingListener {
    void onStart();
    void onFinish();
    void onTotalProgress(double progress);
    void onReportProgress(ReportProgressEvent event);
    void onMessage(String msg);
    void onError(String msg);
}
