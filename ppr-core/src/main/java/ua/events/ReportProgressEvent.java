package ua.events;

/**
 * Created by zelnaga on 30.11.16.
 */
public class ReportProgressEvent {
    private String reportName;
    private int reportProgress;

    public int getReportProgress() {
        return reportProgress;
    }

    public void setReportProgress(int reportProgress) {
        this.reportProgress = reportProgress;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }
}
