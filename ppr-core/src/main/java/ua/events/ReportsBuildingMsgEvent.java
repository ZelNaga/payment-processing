package ua.events;

/**
 * Created by ekh on 14.03.16.
 */
public class ReportsBuildingMsgEvent {
    private final String msg;
    public ReportsBuildingMsgEvent(String msg) {
        this.msg = msg;
    }
    public String getMsg() {return msg;}
}
