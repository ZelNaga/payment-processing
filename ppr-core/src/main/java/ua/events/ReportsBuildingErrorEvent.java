package ua.events;

/**
 * Created by ekh on 14.03.16.
 */
public class ReportsBuildingErrorEvent {
    private final String errorMsg;
    public ReportsBuildingErrorEvent(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    public String getErrorMsg() {
        return errorMsg;
    }
}
