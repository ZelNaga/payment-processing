package ua;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;
import static ua.StringHelper.stringConcat;

import com.google.common.collect.ImmutableList;
import com.google.common.eventbus.EventBus;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.jasper.builder.export.Exporters;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.grid.ColumnGridComponentBuilder;
import net.sf.dynamicreports.report.builder.grid.ColumnTitleGroupBuilder;
import net.sf.dynamicreports.report.builder.group.CustomGroupBuilder;
import net.sf.dynamicreports.report.builder.subtotal.SubtotalBuilder;
import net.sf.dynamicreports.report.constant.GroupHeaderLayout;
import net.sf.dynamicreports.report.exception.DRException;
import org.slf4j.LoggerFactory;
import ua.events.ReportsBuildingErrorEvent;
import ua.reports.RowGrouper;
import ua.reports.TitleGrouper;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

public abstract class ReportBuilder {
    protected static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ReportBuilder.class);

    private static final String DIR_CSV = "/csv/";
    private static final String DIR_HTML = "/html/";
    private static final String DIR_XLSX = "/xslx/";
    protected static final String DIR_DBF = "/DBF/";

    protected static final String REPORTS_ROOT_FOLDER = ReportsConfig.getReportsStoreDirectory();
    private static final String EXTENSION_CSV = ".csv";
    private static final String EXTENSION_HTML = ".html";
    private static final String EXTENSION_XLSX = ".xlsx";

    private static final int TITLE_WIDTH = 150;
    private static final String SUMMARY_TITLE = "Всього";

    protected EventBus eventBus = new EventBus();
    protected ReportProgressHandler progressHandler;

    public ReportBuilder() {
        progressHandler = new ReportProgressHandler(this.eventBus);
    }

    protected abstract ResultSet getDataSource(Connection c, ResultSet params) throws SQLException;
    public abstract void generate(Connection c);
    protected abstract String getReportName();
    protected abstract String getReportTitle();
    protected abstract int getFirstReportColumn();
    protected abstract Map<String, String> getColumnsAliases();
    protected abstract int getStartSummingColumn();

    public void registerReportsBuildingListener(Object listener ) {
        eventBus.register(listener);
    }

    protected JasperReportBuilder buildReport(ResultSet dataSource, String reportTitle) throws SQLException, DRException, ClassNotFoundException {
        TextColumnBuilder[] columns = buildColumns(dataSource.getMetaData());

        JasperReportBuilder reportBuilder = report().setTemplate(Templates.reportTemplate)
                                                    .highlightDetailEvenRows();

        titleGrouping(columns, reportBuilder);

        reportBuilder.columns(columns)
                     .title(cmp.text(reportTitle))
                     //.pageFooter(cmp.pa1geXofY()) //TODO: to uncomment when will be need in pagination
                     .setDataSource(dataSource);

        rowsGrouping(columns, reportBuilder);
        return reportBuilder.subtotalsAtSummary(reportSummary(columns));
    }

    private   TextColumnBuilder[] buildColumns(ResultSetMetaData rsmd) throws SQLException, DRException, ClassNotFoundException {
        TextColumnBuilder[] columnsArr = new TextColumnBuilder[rsmd.getColumnCount() - getFirstReportColumn()];
        for (int i = 0; i < columnsArr.length; i++) {
            int columnIndex = i + getFirstReportColumn() + 1;
            columnsArr[i] = col.column(getColumnsAliases().getOrDefault(rsmd.getColumnLabel(columnIndex), rsmd.getColumnLabel(columnIndex)),
                                        rsmd.getColumnLabel(columnIndex),
                                        Class.forName(rsmd.getColumnClassName(columnIndex)));
        }
        return columnsArr;
    }

    protected void saveReport(JasperReportBuilder builder, String fileName) throws FileNotFoundException, DRException {
        builder.toCsv(Exporters.csvExporter(stringConcat(REPORTS_ROOT_FOLDER, getReportName(), DIR_CSV, fileName, EXTENSION_CSV)))
                .toHtml(Exporters.htmlExporter(stringConcat(REPORTS_ROOT_FOLDER, getReportName(), DIR_HTML, fileName, EXTENSION_HTML)))
                .toXlsx(Exporters.xlsxExporter(stringConcat(REPORTS_ROOT_FOLDER, getReportName(), DIR_XLSX, fileName, EXTENSION_XLSX)));
    }

    private void titleGrouping (TextColumnBuilder[] columns, JasperReportBuilder reportBuilder) {
        if (this instanceof TitleGrouper) {
            int startGroupColumn = ((TitleGrouper)this).getStartGroupColumn();
            int endGroupColumn = columns.length - 1;

            ColumnTitleGroupBuilder titleGroup = grid.titleGroup(((TitleGrouper)this).getGroupTitle(),
                                                                  Arrays.copyOfRange(columns, startGroupColumn, endGroupColumn))
                                                     .setTitleMinWidth(TITLE_WIDTH);
            List<ColumnGridComponentBuilder> grid = new LinkedList<>();
            for (int i = 0; i < startGroupColumn; i++) {
                grid.add(columns[i]);
            }

            grid.add(titleGroup);

            for (int i = endGroupColumn; i < columns.length; i++) {
                grid.add(columns[i]);
            }

            reportBuilder.columnGrid(grid.toArray(new ColumnGridComponentBuilder[grid.size()-1]));
        }
    }

    private void rowsGrouping(TextColumnBuilder[] columns, JasperReportBuilder reportBuilder) {
        if (this instanceof RowGrouper) {
            CustomGroupBuilder itemGroup = grp.group(((RowGrouper)this).getRowsGroupColumnName(), String.class)
                                                .setTitle(((RowGrouper)this).getRowsGroupTitle())
                                                .setTitleWidth(TITLE_WIDTH)
                                                .setHeaderLayout(GroupHeaderLayout.TITLE_AND_VALUE);
            reportBuilder.groupBy(itemGroup);

            for (int i = getStartSummingColumn(); i < columns.length; i++) {
                reportBuilder.subtotalsAtGroupFooter(itemGroup, sbt.sum(columns[i]));
            }
        }
    }

    private SubtotalBuilder[] reportSummary(TextColumnBuilder[] columns) {
        List<SubtotalBuilder> subtotalsColumns = new LinkedList<>();
        subtotalsColumns.add(sbt.text(SUMMARY_TITLE, columns[0]));
        for (int i = getStartSummingColumn(); i < columns.length; i++) {
            subtotalsColumns.add(sbt.sum(columns[i]));
        }
        return subtotalsColumns.stream().toArray(SubtotalBuilder[]::new);
    }

    protected void makeReportDirectories() {
        ImmutableList.of(stringConcat(REPORTS_ROOT_FOLDER, getReportName(), DIR_CSV),
                         stringConcat(REPORTS_ROOT_FOLDER, getReportName(), DIR_HTML),
                         stringConcat(REPORTS_ROOT_FOLDER, getReportName(), DIR_XLSX),
                         stringConcat(REPORTS_ROOT_FOLDER, getReportName(), DIR_DBF))
                     .forEach(this::mkDir);
    }

    private void mkDir(String dirName) {
        File theDir = new File(dirName);

        if (!theDir.exists()) {
            try{
                theDir.mkdirs();
            } catch(SecurityException e) {
                LOGGER.error("Cant create dir {} ! ",  dirName, e);
                eventBus.post(new ReportsBuildingErrorEvent(stringConcat("Cant create dir ", dirName)));
            }
        }
    }
}
