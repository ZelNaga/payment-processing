package ua;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;

public class ReportsConfig {

    private static final String DATABASE_SERVER = "database.server";
    private static final String DATABSE_PORT = "database.port";
    private static final String DATABASE_USER = "database.user";
    private static final String DATABASE_PWD = "database.pwd";
    private static final String START_DATE = "start.date";
    private static final String END_DATE = "end.date";
    private static final String REPORTS_STORE_DIRECTORY = "reports.store.directory";


    private static Configuration cfg = null;

    public static synchronized Configuration loadConfig( boolean reload ) throws ConfigurationException {
        if ( cfg == null || reload ) {
            FileConfiguration fc = new PropertiesConfiguration();
            fc.load("conf/paymentprocessingreports.properties");
            cfg = fc;
        }
        return cfg;
    }

    public static String getDatabasedUser() {
        return cfg.getString(DATABASE_USER);
    }

    public static String getDatabaseServer()  {
        return cfg.getString(DATABASE_SERVER);
    }

    public static Integer getDatabsePort() {
        return cfg.getInteger(DATABSE_PORT, 3306);
    }

    public static String getDatabasePassword() {
        return cfg.getString(DATABASE_PWD);
    }

    public static String getStartDate()  {
        return cfg.getString(START_DATE);
    }

    public static String getEndDate()  {
        return cfg.getString(END_DATE);
    }

    public static String getReportsStoreDirectory()  {
        return cfg.getString(REPORTS_STORE_DIRECTORY);
    }

    public static void setStartEndProcessing( String start, String end ) {

        cfg.setProperty( START_DATE, start );
        cfg.setProperty( END_DATE, end );
    }

}