package ua;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sql2o.Query;
import org.sql2o.Sql2oException;
import org.sql2o.data.Row;
import org.sql2o.data.Table;
import ua.data.*;
import ua.utils.FluentPreparedStatement;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Created by ekh on 04.12.15.
 */
public class PaymentsDAOTest {

    @BeforeClass
    public static void preapere() {
        try {
            PaymentConfig.loadConfig(true);
        } catch ( Exception e ) {
            Assert.fail( e.getMessage() );
        }
    }
    @Test
    public void testGetSql2o() throws Exception {
        Assert.assertTrue( "Sql2o obtaining failed", PaymentsDAO.getSql2o() != null );
    }

    @Test
    public void testGetCashiersAndSchedules() throws Exception {

        PaymentsContext ctx = PaymentsContext.create();
        ctx.setSql2o( PaymentsDAO.getSql2o() );

        FetchResult<Cashier> cashiers = PaymentsDAO.getCashiersAndSchedules( ctx );
        Assert.assertTrue( cashiers != null );
    }

    @Test
    public void testGetMainOrgId() throws Exception {
        PaymentsContext ctx = PaymentsContext.create();
        ctx.setSql2o( PaymentsDAO.getSql2o() );
        try {
            PaymentsDAO.getMainOrgId( 0 );
        } catch ( Exception e ) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testCreateRetentionStoreStatement() throws Exception {

        try( org.sql2o.Connection c = PaymentsDAO.getSql2o().open() ) {
            PreparedStatement ps = PaymentsDAO.createRetentionStoreStatement(c.getJdbcConnection());
            Assert.assertTrue("Failed to create Retention statement", ps != null);
        } catch ( Sql2oException | SQLException e ){
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testCreatePaymentStoreStatement() throws Exception {
        try( org.sql2o.Connection c = PaymentsDAO.getSql2o().open() ) {
            PreparedStatement ps = PaymentsDAO.createPaymentStoreStatement(c.getJdbcConnection());
            Assert.assertTrue("Failed to create Payment store statement", ps != null);
        } catch ( Sql2oException | SQLException e ) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testGetTransactionByCashierAndPeriod() throws Exception {

        PaymentsContext ctx = PaymentsContext.create();
        ctx.setSql2o( PaymentsDAO.getSql2o() );

        try {
            FetchResult<Cashier> result = PaymentsDAO.getCashiersAndSchedules(ctx);
            Assert.assertTrue( result != null );
            Assert.assertTrue( result.getResultList() != null );
            Assert.assertTrue( result.getResultList().size() != 0 );

            FetchResult<PaymentTransaction> transactions =
                    PaymentsDAO.getTransactionByCashierAndPeriod(ctx, result.getResultList().get(0));

            Assert.assertTrue( transactions != null );

        } catch ( Sql2oException e  ) {
            Assert.fail( e.getMessage() );
        }

    }

    @Test
    public void testGetOperationsByTransactionId() throws Exception {

        try( org.sql2o.Connection c = PaymentsDAO.getSql2o().open() ) {

            PaymentsContext ctx = PaymentsContext.create();
            ctx.setSql2o( PaymentsDAO.getSql2o() );


            FetchResult<Cashier> result = PaymentsDAO.getCashiersAndSchedules(ctx);
            Assert.assertTrue( result != null );
            Assert.assertTrue( result.getResultList() != null );
            Assert.assertTrue( result.getResultList().size() != 0 );

            FetchResult<PaymentTransaction> transactions =
                    PaymentsDAO.getTransactionByCashierAndPeriod(ctx, result.getResultList().get(0));

            Assert.assertTrue( transactions != null );

            FetchResult<PaymentOperation> ops =
                    PaymentsDAO.getOperationsByTransactionId(ctx, transactions.getResultList().get(0).id );

            Assert.assertTrue( ops != null );
            Assert.assertTrue( ops.getResultList() != null );
            Assert.assertTrue( ops.getResultList().size() != 0 );


        } catch ( Sql2oException e  ) {
            Assert.fail( e.getMessage() );
        }
    }

    @Test
    public void testGetFormules() throws Exception {
        try( org.sql2o.Connection c = PaymentsDAO.getSql2o().open() ) {

            PaymentsContext ctx = PaymentsContext.create();
            ctx.setSql2o( PaymentsDAO.getSql2o() );


            FetchResult<Cashier> result = PaymentsDAO.getCashiersAndSchedules(ctx);
            Assert.assertTrue( result != null );
            Assert.assertTrue( result.getResultList() != null );
            Assert.assertTrue( result.getResultList().size() != 0 );

            Cashier cashier = result.getResultList().get(0);
            FetchResult<PaymentTransaction> transactions =
                    PaymentsDAO.getTransactionByCashierAndPeriod(ctx, cashier);

            ctx.setMainOrgId(PaymentsDAO.getMainOrgId(cashier.organization_id));
            Assert.assertTrue( transactions != null );

            FetchResult<PaymentOperation> ops =
                    PaymentsDAO.getOperationsByTransactionId(ctx, transactions.getResultList().get(0).id );

            Assert.assertTrue( ops != null );
            Assert.assertTrue( ops.getResultList() != null );
            Assert.assertTrue( ops.getResultList().size() != 0 );


            FetchResult<Formula> formules = PaymentsDAO.getFormulasInt( ctx.getServiceId(), ctx.getSupplierOrgId(), ctx.getMainOrgId() );

            Assert.assertTrue( formules  != null );
            Assert.assertTrue( formules .getResultList() != null );
            // TODO: to investigate
            //Assert.assertTrue( formules.getResultList().size() != 0 );

        } catch ( Sql2oException e  ) {
            Assert.fail( e.getMessage() );
        }

    }

    @Test
    public void testGetMoneyRedirectRule() {
        try (org.sql2o.Connection c = PaymentsDAO.getSql2o().open()) {
            ScalarResult<Table> mrr = PaymentsDAO.getMoneyRedirectRulesInternal(c, Integer.MAX_VALUE);
            Assert.assertTrue( mrr.get() != null );
        } catch (Exception e) {
            Assert.fail( e.getMessage() );
        }

    }

    @Test
    public void testGetZhekInterests() {
        try {
            PaymentsContext ctx = PaymentsContext.create();
            ctx.setSql2o( PaymentsDAO.getSql2o() );
            PaymentsDAO.getZhekInterests(ctx);
        } catch (Exception e) {
            Assert.fail( e.getMessage() );
        }

    }

    @Test
    public void testIsServiceLocationsExists() {
        try {
            PaymentsContext ctx = PaymentsContext.create();
            ctx.setSql2o( PaymentsDAO.getSql2o() );
            PaymentsDAO.isServiceLocationsExists(ctx, 0);
        } catch (Exception e) {
            Assert.fail( e.getMessage() );
        }

    }

    @Test
    public void testIsServiceExists() {
        try {
            PaymentsContext ctx = PaymentsContext.create();
            ctx.setSql2o( PaymentsDAO.getSql2o() );
            ctx.setZhek(15);
            ScalarResult<Table> t = PaymentsDAO.getZhekInterests(ctx);
            Optional<Row> r = t.get().rows().stream().findFirst();
            PaymentsDAO.isServiceExists(ctx, r.get());
        } catch (Exception e) {
            Assert.fail( e.getMessage() );
        }

    }

    @Test
    public void testGetTotalHomeRentCoefficient() {
        try {
            PaymentsContext ctx = PaymentsContext.create();
            ctx.setSql2o( PaymentsDAO.getSql2o() );
            PaymentsDAO.getTotalHomeRentCoefficient(ctx);
        } catch (Exception e) {
            Assert.fail( e.getMessage() );
        }

    }

    @Test
    public void testGetPartHomeRentCoefficient() {
        try {
            PaymentsContext ctx = PaymentsContext.create();
            ctx.setSql2o( PaymentsDAO.getSql2o() );
            PaymentsDAO.getService(ctx, 0);
        } catch (Exception e) {
            Assert.fail( e.getMessage() );
        }

    }

    @Test
    public void testCreatePaymentUpdateQuery() throws Exception {
        try( org.sql2o.Connection c = PaymentsDAO.getSql2o().open() ) {
            Query q = PaymentsDAO.createPaymentUpdateQuery(c);
            Assert.assertTrue("Failed to create Payment store statement", q != null);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testCreateBalanceInsertQuery() throws Exception {
        try( org.sql2o.Connection c = PaymentsDAO.getSql2o().open() ) {
            Query q = PaymentsDAO.createBalanceInsertQuery(c);
            Assert.assertTrue("Failed to create balance store statement", q != null);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testGetOrganizationPayments() {
        try(org.sql2o.Connection c = PaymentsDAO.getSql2o().open()) {
            ScalarResult<Table> result = PaymentsDAO.getOrganizationPayments(c, 0, 0);
        } catch (Exception e) {
            Assert.fail( e.getMessage() );
        }
    }

    @Test
    public void testGetOrganizationsDebts() throws Exception {
        try( org.sql2o.Connection c = PaymentsDAO.getSql2o().open() ) {
            PaymentsDAO.getOrganizationsDebts(c, PaymentConfig.getProcessingDate());
        } catch ( Exception e ) {
            Assert.fail( e.getMessage() );
        }
    }

    @Test
    public void testCreateSql2oSimple() throws Exception {

    }

    @Test
    public void testGetAcceptorRequisites() throws Exception {
        try (org.sql2o.Connection c = PaymentsDAO.getSql2o().open()) {
            Optional<AcceptorRequisites> result = Optional.ofNullable(PaymentsDAO.getAcceptorRequisitesInternal(Integer.MAX_VALUE, Integer.MAX_VALUE, false, c));
            Assert.assertTrue( !result.isPresent()  );
        } catch ( Exception e ) {
            Assert.fail( e.getMessage() );
        }

    }

    @Test
    public void testGetTableRows() {

        String sql = "SELECT NULL as FirstName, NULL as SecondName\n" +
                "UNION\n" +
                "SELECT \"EKh2\" as FirstName, \"EKh\" as SecondName\n" +
                "UNION\n" +
                "SELECT \"EKh3\" as FirstName, \"EKh4\" as SecondName\n" +
                "UNION\n" +
                "SELECT \"EKh3\" as FirstName, NULL as SecondName\n" +
                "\n";

        org.sql2o.Sql2o sql2o = PaymentsDAO.getSql2o();
        try (org.sql2o.Connection c = sql2o.open()) {
            Query q = c.createQuery(sql);
            Table t = q.executeAndFetchTable();
            String errors = PaymentsDAO.getTableErrors(t);
            Assert.assertTrue( !errors.isEmpty() );
        }
    }
}