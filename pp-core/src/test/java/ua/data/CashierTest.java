package ua.data;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ekh on 26.11.15.
 */
public class CashierTest {

    @Test
    public void testValidate() throws Exception {
        Cashier c = new Cashier();
        Assert.assertFalse( c.validate() );
    }
}