package ua.data;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by zelnaga on 25.12.15.
 */
public class RentapartCalculatorTest extends TestCase {
    @Test
    public void testCaclulateRentPart() {
        RentapartCalculator rentapartCalculator = new RentapartCalculator();
        // hardcoded values below for test calculation
        double rentStartValue = 100.00; // whole summa for rent
        double totalHomeRentCoefficient = 2.35; // this coefficient represent whole summa for rent of certain home
        double interest = 0.75; // part of the rent that takes a certain organization
        double transferValue = 54.77; // rent balance after previous rent splittings
        double expectedRentPart = 31.91; // expected rent part after calculations
        double expectedTransferValue = 22.86; // expected rent balance after calculations

        // test case 1 - test calculation
        double rentPart = rentapartCalculator.calculateRentPart(rentStartValue, totalHomeRentCoefficient, interest, transferValue);
        double newTransferValue = rentapartCalculator.getNewTransferValue();
        assertTrue("Incorrect calculation template !!!", newTransferValue == expectedTransferValue
                                                            && rentPart == expectedRentPart);

        // test case 2 - situation when rentPart greater than the balance of rent
        double TOTALLY_SPLITED_RENTA = 0.00; // completely splitted rent
        double lowTransferValue = 0.33; // this value could lead to the situation that the rentPart will be greater than the balance of rent
        rentPart = rentapartCalculator.calculateRentPart(rentStartValue, totalHomeRentCoefficient, interest, lowTransferValue);
        assertTrue("Transfer value cant be negative value !!!", rentapartCalculator.getNewTransferValue() == TOTALLY_SPLITED_RENTA
                                                            && rentPart == lowTransferValue);

        // test case 3 - situation when one or all of incoming arguments is negative
        try {
            rentapartCalculator.calculateRentPart(-1.01, -1.01, 10.01, 10.00);
            fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException illArgEx) {
            //ignore, this exception is expected.
        }

        // test case 4 - situation when percentage more than 100%
        try {
            rentapartCalculator.calculateRentPart(100, 7, 7.7, 30);
            fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException illArgEx) {
            //ignore, this exception is expected.
        }


    }
}
