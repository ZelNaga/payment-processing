package ua.data;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by zelnaga on 25.12.15.
 */
public class RetentionCalculatorTest {
    @Test
    public void testCalculateCommission() {
        RetentionCalculator retentionCalculator = new RetentionCalculator();
        // hardcoded values below for test calculation
        double startValue = 100.00; // payment summa value
        double percent = 10.57; // retention percent
        double transferValue = 11.5; // payment balance after the previous retentions
        double expectedCommission = 10.57; // expected retention commission after calculations
        double expectedNewTransferValue = 0.93; // expected payment balance after calculations

        // test case 1 - test calculation
        double commission = retentionCalculator.calculateCommission(startValue, percent, transferValue); // calculated retention commission
        double newTransferValue = retentionCalculator.getNewTransferValue(); // calculated payment balance
        assertTrue("Incorrect calculation template !!!", newTransferValue == expectedNewTransferValue
                                                            && commission == expectedCommission);

        // test case 2 - situation when commission greater than the balance of payment
        double TOTALLY_RETAINED_PAYMENT = 0.00; // completely deducted money from payment
        double lowTransferValue = 0.97; // this value could lead to the situation that the commission will be greater than the balance of payment
        commission = retentionCalculator.calculateCommission(startValue, percent, lowTransferValue);
        assertTrue("Transfer value cant be negative value !!!", retentionCalculator.getNewTransferValue() == TOTALLY_RETAINED_PAYMENT
                                                                                            && commission == lowTransferValue);

        // test case 3 - situation when one or all of incoming arguments is negative
        try {
            retentionCalculator.calculateCommission(-1.01, -1.01, 1.01);
            fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException illArgEx) {
            //ignore, this exception is expected.
        }

        // test case 4 - situation when percentage more than 100%
        try {
            retentionCalculator.calculateCommission(100, 101, 30);
            fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException illArgEx) {
            //ignore, this exception is expected.
        }
    }
}
