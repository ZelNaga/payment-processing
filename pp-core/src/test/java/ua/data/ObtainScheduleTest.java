package ua.data;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.PaymentConfig;
import ua.PaymentsContext;
import ua.PaymentsDAO;

import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;


/**
 * Created by zelnaga on 14.12.15.
 */
public class ObtainScheduleTest {

    @BeforeClass
    public static void loadConfig() throws Exception {
        try {
            PaymentConfig.loadConfig(true);
        } catch (Exception e) {
            e.getMessage();
            throw e;
        }
    }

    @Test
    public void testObtainSchedule() throws InterruptedException {
        int HOUR = 20, MINUTE = 0, SECOND = 0; //Time for test, it could be any time
        long ONE_DAY_IN_SECONDS = 60 * 60 * 24; //Etalon value of one day in seconds

        ZoneId zoneId = ZoneId.systemDefault();
        LocalTime time = LocalTime.of(HOUR, MINUTE, SECOND);

        PaymentsContext ctx = PaymentsContext.create();
        ctx.setSql2o(PaymentsDAO.getSql2o());

        // Simple case
        long beginDateSimple = PaymentsDAO.obtainSchedule(ctx.getPeriodBegin(), ctx.getPeriodBegin(), time, 1)
                .atZone(zoneId).toEpochSecond();
        long endDateSimple = PaymentsDAO.obtainSchedule(ctx.getPeriodEnd(), ctx.getPeriodBegin(), time, 0)
                .atZone(zoneId).toEpochSecond();
        long deltaDateSimple = endDateSimple - beginDateSimple;
        Assert.assertTrue(deltaDateSimple == ONE_DAY_IN_SECONDS);

        // Case with database
        FetchResult<Cashier> result = PaymentsDAO.getCashiersAndSchedules(ctx);
        result.getResultList().stream()
                .filter((c) -> c.begin_time != null && c.end_time != null)
                .forEach((c) -> {
                    long beginDateDatabase = PaymentsDAO.obtainSchedule(ctx.getPeriodBegin(), ctx.getPeriodBegin(), c.begin_time.toLocalTime(), 1)
                            .atZone(zoneId).toEpochSecond();
                    long endDateDatabase = PaymentsDAO.obtainSchedule(ctx.getPeriodEnd(), ctx.getPeriodBegin(), c.end_time.toLocalTime(), 0)
                            .atZone(zoneId).toEpochSecond();
                    long deltaBeginDate = ctx.getPeriodBegin().atZone(zoneId).toEpochSecond() - beginDateDatabase;
                    long deltaEndDate = ctx.getPeriodEnd().atZone(zoneId).toEpochSecond() - endDateDatabase;
                    Assert.assertTrue(deltaBeginDate >= 0 && deltaBeginDate <= ONE_DAY_IN_SECONDS);
                    Assert.assertTrue(deltaEndDate >= 0 && deltaEndDate <= ONE_DAY_IN_SECONDS);
                });

        // Multithreaded case
        int cores = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(cores);

        Callable<Boolean> task = () -> {
            long beginDateMultithreaded = PaymentsDAO.obtainSchedule(ctx.getPeriodBegin(), ctx.getPeriodBegin(), time, 1)
                    .atZone(zoneId).toEpochSecond();
            long endDateMultithreaded = PaymentsDAO.obtainSchedule(ctx.getPeriodEnd(), ctx.getPeriodBegin(), time, 0)
                    .atZone(zoneId).toEpochSecond();
            long deltaDateMultithreaded = endDateMultithreaded - beginDateMultithreaded;

            return deltaDateMultithreaded == ONE_DAY_IN_SECONDS;
        };

        List<Callable<Boolean>> tasks = Collections.nCopies(cores, task);
        List<Future<Boolean>> futures = executorService.invokeAll(tasks);

        Assert.assertTrue(cores == futures.size());

        futures.forEach((f) -> {
            try {
                Assert.assertTrue(f.get());
            } catch (Exception e) {
                e.getMessage();
                Assert.fail();
            }
        });
    }
}
