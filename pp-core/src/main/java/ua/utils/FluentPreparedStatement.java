package ua.utils;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by ekh on 05.02.16.
 */
public class FluentPreparedStatement implements AutoCloseable {
    private PreparedStatement statement;
    public FluentPreparedStatement(PreparedStatement statement) {
        this.statement = statement;
    }

    public FluentPreparedStatement setInt(int idx, int v) throws SQLException {
        statement.setInt(idx, v );
        return this;
    }

    public FluentPreparedStatement setDouble(int idx, double v) throws SQLException {
        statement.setDouble(idx, v );
        return this;
    }

    public FluentPreparedStatement setLong(int idx, long v) throws SQLException {
        statement.setLong(idx, v );
        return this;
    }

    public FluentPreparedStatement setString(int idx, String v) throws SQLException {
        statement.setString(idx, v );
        return this;
    }

    public FluentPreparedStatement executeBatch() throws SQLException {
        statement.executeBatch();
        return this;
    }

    public void addBatch() throws SQLException {
        statement.addBatch();
    }

    public PreparedStatement getPS() { return statement; }

    @Override
    public void close() throws Exception {
        statement.close();
    }
}
