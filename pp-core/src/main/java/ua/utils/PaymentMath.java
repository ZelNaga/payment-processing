package ua.utils;

import org.apache.commons.math3.util.Precision;

import java.math.BigDecimal;

/**
 * Created by ekh on 22.12.15.
 */
public class PaymentMath {
    static public Double round(Double d) {
        return Precision.round(d, 2, BigDecimal.ROUND_HALF_UP);
    }

}
