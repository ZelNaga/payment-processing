package ua;

import com.google.common.eventbus.EventBus;
import org.apache.commons.lang.mutable.MutableDouble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.data.Row;
import org.sql2o.data.Table;
import ua.data.*;
import ua.events.ProcessingErrorEvent;
import ua.events.ProcessingMsgEvent;
import ua.events.ProgressEvent;
import ua.utils.FluentPreparedStatement;
import ua.utils.OptionalConsumer;

import java.sql.SQLException;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Payment processor for  Organizations  List
 * Created by ekh on 19.11.15.
 */
public class PaymentProcessor implements Runnable {

    static Logger LOGGER = LoggerFactory.getLogger(PaymentProcessor.class);
    static int SERVICE_ID_RENTA = 1;

    private PaymentsContext ctx = null;
    private FetchResult<Cashier> cashiers = null;
    private FluentPreparedStatement storePaymentStatement,
            storeRetentionStatement;
    private int isErrors = 0;
    private String lastErrorMsg = "";
    private Connection transactedConnection;
    private RetentionCalculator retentionCalculator = new RetentionCalculator();
    private RentapartCalculator rentapartCalculator = new RentapartCalculator();

    private EventBus eventBus = new EventBus();

    private PaymentProcessor(FetchResult<Cashier> list, int processingDayId) {
        this.ctx = PaymentsContext.create();
        this.ctx.setSql2o(PaymentsDAO.getSql2o())
                .setProcessingDayId(processingDayId);
        this.cashiers = list;
    }

    public static PaymentProcessor Of(FetchResult<Cashier> fr, int processingDayId) {
        return new PaymentProcessor(fr, processingDayId);
    }

    @Override
    public void run() {
        proceed();
    }

    private void proceed() {
        try{
            initializePreparedStatements();
            processCashiers();
        } catch (Exception e) {
            logError("Thread FAILURE: ", e);
        }
    }

    private void processCashiers() throws SQLException {
        final int[] current = {0};
        for (Cashier cashier : cashiers.getResultList()) {
            validateEntity(cashier, () -> "Cashier Error, Cashier ID=" + cashier.user_id).ifPresent( c -> {
                ctx.buildCashierParameters(cashier);
                processPaymentTransactions(PaymentsDAO.getTransactionByCashierAndPeriod(ctx, cashier));
                eventBus.post(new ProgressEvent(Thread.currentThread().getId(), current[0]++, cashiers.getResultList().size()));
            });
        }
    }

    private void initializePreparedStatements() throws SQLException {
        this.storePaymentStatement = new FluentPreparedStatement(
                PaymentsDAO.createPaymentStoreStatement(transactedConnection.getJdbcConnection()));
        this.storeRetentionStatement = new FluentPreparedStatement(
                PaymentsDAO.createRetentionStoreStatement(transactedConnection.getJdbcConnection()));
    }

    private void processPaymentTransactions(FetchResult<PaymentTransaction> transactions) {
        for (PaymentTransaction transaction : transactions.getResultList()) {
            validateEntity(transaction, () -> "Transaction Error, Transaction Id= " + transaction.id)
                    .ifPresent( (t) -> {
                        try {
                            ctx.buildTransactionParameters(transaction);
                            processPayments(ctx, PaymentsDAO.getOperationsByTransactionId(ctx, transaction.id));
                        } catch (SQLException e) {
                            logError( "processPaymentTransactions", e );
                        }
                    });
        }
    }

    private void flushPaymentsAndRetentions() throws SQLException {
        log("Flushing payments and retentions...");
        storePaymentStatement.executeBatch();
        storeRetentionStatement.executeBatch();
    }

    private void processPayments(PaymentsContext ctx, FetchResult<PaymentOperation> operations) throws SQLException {
        for (PaymentOperation o : operations.getResultList()) {
            validateEntity(o, () -> "Error Payment Operation, OperationId=" +  o.ID).ifPresent( (op) -> {
                OptionalConsumer.of(PaymentsDAO.getAcceptorRequisites(ctx, o))
                        .ifPresent( (r) ->  applyRequisitesAndProceed(ctx, o, Optional.of(r) ) )
                        .ifNotPresent( () -> logError("There no payment requisites for operation Id=" + o.ID ));
            } );
        }
    }

    private void applyRequisitesAndProceed(PaymentsContext ctx, PaymentOperation o, Optional<AcceptorRequisites> requisites) {
        validateEntity( requisites.get(), () -> "Invalid requisites for Operation Id = " + o.ID  )
                .ifPresent( (r) -> {
                            try {
                                ctx.buildPaymentsProcessParameters(o, requisites.get());
                                processRetentions(ctx, o);
                                processRentaSplitting(ctx);
                                addBatchPayment(ctx);
                            } catch (SQLException e) {
                                logError("applyRequisitesAndProceed FAILED", e);
                            }
                        }
                );
    }

    private void processRentaSplitting(PaymentsContext ctx) throws SQLException {
        // only renta should be applied
        if( ctx.getServiceId() == SERVICE_ID_RENTA ) {
            Optional<Table> validated = validateTable( PaymentsDAO.getZhekInterests(ctx).get(),
                    () -> "Error:rentaSplitting:getZhekInterests" );
            // internal crash in VM on next commented line
            //validated.ifPresent( (t) -> applyZhekInterests(ctx, t) );
            if( validated.isPresent() ) {
                applyZhekInterests(ctx, validated.get());
            }

        }
    }


    private void applyZhekInterests(PaymentsContext ctx, final Table zhekInterests) {
        double rentStartValue = ctx.getTransferValue();
        final MutableDouble transferValue = new MutableDouble(ctx.getTransferValue());
        for (Row row : zhekInterests.rows()) {
            MutableDouble interest = new MutableDouble();
            if (checkServiceAndGetInterest(ctx, row, interest) && checkServiceLocation(ctx, row)) {
                ScalarResult<Double> rentCoefficient = PaymentsDAO.getTotalHomeRentCoefficient(ctx);
                OptionalConsumer.of( Optional.of(rentCoefficient.get()) )
                        .ifNotPresent( () -> logError("rentaSplitting:getTotalHomeRentCoefficient: returned NULL:" + rentCoefficient.getSql()) )
                        .ifPresent( (coefficient) -> applyRetention(ctx, rentStartValue, transferValue, row, interest, coefficient) );
            }
        }
    }

    private void applyRetention(PaymentsContext ctx, double rentStartValue, final MutableDouble transferValue, Row row,
                                MutableDouble interest, double coefficient ) {
        try{
            double rentPart = rentapartCalculator.calculateRentPart( rentStartValue, coefficient,
                    interest.doubleValue(), transferValue.doubleValue() );
            transferValue.setValue( rentapartCalculator.getNewTransferValue() );
            ctx.buildRentaSplittingParams(rentPart, transferValue.doubleValue(), row.getInteger("acceptor_org_id"));
            addBatchRetention(ctx, row.getInteger("addservice_id"));
        } catch (IllegalArgumentException e) {
            logError( "Error:applyRetention:", e);
        }
    }

    boolean checkServiceAndGetInterest(PaymentsContext ctx, Row row, MutableDouble interest ) {
        Optional<Double> service = Optional.ofNullable(PaymentsDAO.getService(ctx, row.getInteger("addservice_id")).get());
        service.ifPresent( (s) -> interest.setValue(row.getDouble("interest") == 0 ? service.get() : row.getDouble("interest")) );
        return service.isPresent();
    }

    boolean checkServiceLocation(PaymentsContext ctx, Row row) {
        return (PaymentsDAO.isServiceLocations(ctx, row.getInteger("addservice_id")).get()) ?
                                                PaymentsDAO.isServiceExists(ctx, row).get() :
                                                true;
    }


    private void processRetentions(PaymentsContext ctx,PaymentOperation o) throws SQLException {
        // розрахунок необхідних даних по кожній з формул
        final MutableDouble transferValue = new MutableDouble(ctx.getValue());
        o.formules.get().stream().filter( (f) -> shouldFormulaApplied(ctx, f ) ).forEachOrdered( f -> {
                    transferValue.setValue( getNewTransferValue(ctx, transferValue.doubleValue(), f) );
                    addBatchRetention(ctx, ctx.getServiceId());
                });
        ctx.setTransferValue(transferValue.doubleValue());
    }

    private double getNewTransferValue(PaymentsContext ctx, double oldTransferValue, Formula f) {
        Double commission = retentionCalculator.calculateCommission(ctx.getValue(), f.percent, oldTransferValue);
        ctx.buildProcessretentionsParameters(f, commission );
        return retentionCalculator.getNewTransferValue();
    }

    /**
     * handles special cases (should formula be applied or not)
     * @param ctx (  PaymentsContext(getZhek) )
     * @param f ( Formula(type_id|zhek) )
     * @return
     */
    private boolean shouldFormulaApplied(PaymentsContext ctx, Formula f) {
        return (f.type_id == PaymentsDAO.ORGANIZATION_TYPE_ZHEK && f.zhek.equals(ctx.getZhek()) ) ||
                (f.type_id != PaymentsDAO.ORGANIZATION_TYPE_ZHEK);
    }

    private void addBatchPayment(PaymentsContext ctx) throws SQLException {
        storePaymentStatement.setInt(1, ctx.getPaymentId())
                .setInt(2, ctx.getTransactionId())
                .setDouble(3, ctx.getValue())
                .setDouble(4, ctx.getTransferValue())
                .setLong(5, ctx.getMainAccount())
                .setInt(6, ctx.getMfo())
                .setInt(7, ctx.getServiceId())
                .setInt(8, ctx.getSupplierOrgId())
                .setInt(9, ctx.getAcceptorOrgId())
                .setInt(10, ctx.getZhek())
                .setInt(11, ctx.getOrganizationId())
                .setInt(12, ctx.getHomeId())
                .setInt(13, ctx.getUserId())
                .setInt(14, ctx.getAccount())
                .setInt(15, ctx.getQuittanceNumber())
                .setInt(16, ctx.getTypeCash())
                .setString(17, DateHelper.timestamp())
                .setInt(18, ctx.getProcessingDayId())
                .setInt(19, ctx.getMainOrgId()) //TODO: added additional field by request of Goncharenko. In the future, it must be removed !!!!
                .setInt(20, ctx.getBalanceOrgId())
                .addBatch();
    }

    private void addBatchRetention(PaymentsContext ctx, int serviceId) {
        try {
            storeRetentionStatement.setInt(1, ctx.getPaymentId())
                    .setInt(2, ctx.getComissionAcceptorOrgId())
                    .setInt(3, PaymentsDAO.getAcceptorType(ctx))
                    .setInt(4, serviceId)
                    .setDouble(5, ctx.getFormulaId())
                    .setDouble(6, ctx.getComission())
                    .addBatch();
        } catch (SQLException e) {
            logError("addBatchRetention FAILED", e);
        }
    }

    private void log(final String s) {
        LOGGER.info(s);
        eventBus.post( new ProcessingMsgEvent(s));
    }

    public Boolean isErrors() {
        return isErrors != 0;
    }

    void logError(String msg) {
        isErrors++;
        LOGGER.error(msg);
        lastErrorMsg = msg;
        eventBus.post( new ProcessingErrorEvent(msg) );
    }

    void logError(String msg, Exception e) {
        isErrors++;
        LOGGER.error(msg,e);
        lastErrorMsg = msg;
        eventBus.post( new ProcessingErrorEvent(msg) );
    }

    public void commit() throws SQLException {
        flushPaymentsAndRetentions();
    }

    public void setTransactedConnection(Connection c) {
        transactedConnection = c;
    }

    public void registerPaymentProcessingListener(Object listener ) {
        eventBus.register(listener);
    }

    private Optional<? extends BaseEntity> validateEntity( BaseEntity e, Supplier<String> msg ) {
        if( PaymentConfig.getCheckDatabaseIntegrity() ) {
            if ( !e.validate() ) {
                logError(msg.get() + " " + e.getErrors() );
                return Optional.empty();
            }
        }
        return  Optional.of( e );
    }

    private Optional<Table> validateTable( Table table, Supplier<String> msg ) {
        if( PaymentConfig.getCheckDatabaseIntegrity() ) {
            if( !PaymentsDAO.validateTable(table) ) {
                logError( msg.get() + " " + PaymentsDAO.getTableErrors(table) );
                return Optional.empty();
            }
        }
        return Optional.of( table );
    }
}
