package ua;

import gnu.trove.set.hash.TIntHashSet;
import org.apache.commons.lang.mutable.MutableDouble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.data.Row;
import org.sql2o.data.Table;
import ua.utils.PaymentMath;
import ua.data.ScalarResult;

import java.sql.SQLException;

/**
 * Created by zelnaga on 23.12.15.
 *
 */
public class DebtProcessor {
    private static Query insertBalanceQuery, updatePaymentQuery;

    private static Logger LOGGER = LoggerFactory.getLogger(DebtProcessor.class);

    private DebtProcessor() {
    }

    private static void createBalance(org.sql2o.Connection c, String processingDate) throws SQLException{
        Table t = PaymentsDAO.getNewDebts(c, processingDate);
        if (!t.rows().isEmpty()) {
            for (Row r : t.rows()) {
                addBatchInsertBalance(r, 0.00, processingDate); // hardcoded value means that balance created at first time and credit = 0.00
            }
            insertBalanceQuery.executeBatch();
            c.commit(false);
        }
    }

    static void proceed() {
        try( org.sql2o.Connection c = PaymentsDAO.getSql2o().beginTransaction() ) {
            LOGGER.info("Debt processor in progress...");

            String processingDate = PaymentConfig.getProcessingDate();

            PaymentsDAO.deleteBalanceForDay(c, processingDate);

            initializeQueries(c);

            createBalance(c, processingDate);

            Table organizationsDebts = PaymentsDAO.getOrganizationsDebts(c, processingDate);

            if (!organizationsDebts.rows().isEmpty()) {

                processDebts(c, organizationsDebts, processingDate);

                flushResult();

                c.commit();

                LOGGER.info("Debt processor finished...");
            }
        } catch (Exception e) {
            LOGGER.error("FAILED DebtProcessor:", e);
        }
    }

    private static void processDebts(Connection c, Table organizationsDebts, String processedDate) throws SQLException {
        MutableDouble retainedDebt = new MutableDouble();
        TIntHashSet usedPayments = new TIntHashSet();
        for (Row r : organizationsDebts.rows()) {
            retainedDebt.setValue(0.0);
            Double sum = subtractingDebt(c, r, retainedDebt, usedPayments);
            if( sum != -1 ) {
                addBatchInsertBalance(r, retainedDebt.doubleValue(), processedDate);
            }
        }
    }

    //TODO: make lazy initialization for queries below
    private static void initializeQueries(Connection c) throws SQLException {
        updatePaymentQuery = PaymentsDAO.createPaymentUpdateQuery(c);
        insertBalanceQuery = PaymentsDAO.createBalanceInsertQuery(c);
    }

    /**
     *
     * @param sum
     * @param transferValue
     * @param limitPercent
     * @param retainedDebt
     * @return
     */
    private static Double calculateTransferValueAndSum(Double sum, MutableDouble transferValue, Double limitPercent, MutableDouble retainedDebt ) {
        Double startSum = sum;
        double debtPart = PaymentMath.round(transferValue.toDouble() * (limitPercent / 100));
        if (sum >= debtPart) {
            startSum -= debtPart;
            transferValue.setValue( PaymentMath.round(transferValue.doubleValue() - debtPart));
            retainedDebt.add(debtPart);
        } else {
            transferValue.setValue( PaymentMath.round(transferValue.doubleValue() - sum) );
            retainedDebt.add(sum);
            startSum = .0;
        }
        return startSum;
    }

    private static Double subtractingDebt(Connection c, Row r, MutableDouble retainDebtResult, TIntHashSet usedPayments) throws SQLException {

        ScalarResult<Table> result = PaymentsDAO.getOrganizationPayments(c, r.getInteger("organization_id"), r.getInteger("bank_id"));
        Table organizationPayments = result.get();
        if (!PaymentsDAO.validateTable(organizationPayments)) {
            LOGGER.error("FAILED: subtractingDebt: NULLs occured: " + result.getSql());
            return -1.0;
        }

        if (organizationPayments.rows().isEmpty()) {
            return -1.0;
        }

        Double sum = r.getDouble("saldo");
        double limitPercent = r.getDouble("limit_percent");
        MutableDouble transferValue = new MutableDouble();

        for (Row row : organizationPayments.rows()) {
            transferValue.setValue(row.getDouble("transfer_value"));
            if (sum <= 0.00) {
                break;
            }
            if (usedPayments.add(row.getInteger("payment_id"))) {
                sum = calculateTransferValueAndSum(sum, transferValue, limitPercent, retainDebtResult);
                addBatchUpdatePayment(row, transferValue.doubleValue());
            }
        }
        return sum;
    }

    private static void addBatchUpdatePayment( Row row, Double transferValue ) throws SQLException {
        updatePaymentQuery.addParameter("transferValue", PaymentMath.round(transferValue))
                            .addParameter("paymentId", row.getInteger("payment_id"))
                            .addToBatch();
    }

    private static void addBatchInsertBalance(Row r, Double retainedDebt, String processedDate) throws SQLException {
        insertBalanceQuery.addParameter("debtId", r.getInteger("debt_id"))
                            .addParameter("saldo", r.getDouble("saldo"))
                            .addParameter("credit", retainedDebt)
                            .addParameter("processedDate", processedDate)
                            .addParameter("creationDateTime", DateHelper.timestamp())
                            .addToBatch();
    }

    private static void flushResult() throws SQLException {
        LOGGER.info("Update payments...");
        updatePaymentQuery.executeBatch();
        LOGGER.info("Flushing retained debt...");
        insertBalanceQuery.executeBatch();
    }
}
