package ua;

import com.google.common.base.*;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;
import org.sql2o.data.Column;
import org.sql2o.data.Row;
import org.sql2o.data.Table;
import ua.data.*;
import ua.utils.OptionalConsumer;

import java.lang.reflect.Field;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ekh on 26.11.15.
 */
public class PaymentsDAO {

    // показник формули щоденного утримання відсотка
    public static final boolean FORMULA_IS_MONTH = false;
    // відсіювання формул з нульвим відсотком
    public static final double FORMULA_PERCENT = 0.00;
    // статус запису в m_history
    public static final boolean M_HISTORY_STATYS = false;
    // типи договорів
    public static final int COMMISSION_TYPE_NONE = 0;
    public static final int COMMISSION_TYPE_PAYMENTS = 1;
    public static final int COMMISSION_TYPE_BILLING_SERVICE = 2;
    // типи організацій при утриманні комісій
    public static final int ORGANIZATION_TYPE_NONE = 0;
    public static final int ORGANIZATION_TYPE_CMSU = 1;
    public static final int ORGANIZATION_TYPE_BANK = 2;
    public static final int ORGANIZATION_TYPE_ZHEK = 4;

    private static final int CMSU_ID = 9999;


    static Logger LOGGER = LoggerFactory.getLogger(PaymentsDAO.class);

    /**
     * Параметри для підключення до бази даних
     */
    public static final String USER = PaymentConfig.getDatabasedUser();
    public static final String PASSWORD = PaymentConfig.getDatabasePassword();
    public static final String URL = String.format( "jdbc:mysql://%s:%d",
            PaymentConfig.getDatabaseServer(), PaymentConfig.getDatabasePort() );

    private static final boolean USE_HIKARI = true;

    //TODO: make that one thread do not use more than two connections
    public static Sql2o getSql2o() {
        return sql2o.get();
    }

    public static Sql2o createSql2oSimple() {
        return new Sql2o( URL, USER, PASSWORD );
    }

    private static Sql2o createSql2oWithHikari() {
        HikariConfig cfg = new HikariConfig();
        cfg.setMaximumPoolSize( 2 * Runtime.getRuntime().availableProcessors() );
        cfg.setJdbcUrl(  URL );
        cfg.setUsername( USER );
        cfg.setPassword( PASSWORD );
        cfg.addDataSourceProperty("cachePrepStmts", "true");
        cfg.addDataSourceProperty("prepStmtCacheSize", "250");
        cfg.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        cfg.setConnectionTestQuery("select 1");

        return new Sql2o( new HikariDataSource(cfg) );
    }

    private static final com.google.common.base.Supplier<Sql2o> sql2o =
            Suppliers.memoize(() -> USE_HIKARI ? createSql2oWithHikari() : createSql2oSimple());

    /**
     * Отримуємо усіх касирів (які приймали платежі за певний розрахунковий
     * період) згрупованих по організаціям, а також отримання графіків  по
     * організаціям згідно яким необхідно корегувати розрахунковий період
     * PyamentContext ( getPeriodBegin|getPeriodEnd|sql2o )
     */
    public static FetchResult<Cashier> getCashiersAndSchedules( PaymentsContext ctx ) throws Sql2oException {
        try( org.sql2o.Connection c = ctx.getSql2o().open() ) {
            Query q = c.createQuery(getCashierAndSchedulersSql())
                    .addParameter("beginDate", ctx.getPeriodBegin().toString())
                    .addParameter("endDate", ctx.getPeriodEnd().toString());
            return FetchResult.of( q.executeAndFetch(Cashier.class), getQuerySqlStatement(q) );
        }
    }

    private static String getCashierAndSchedulersSql() {
        return "SELECT DISTINCT user.organization_id AS ORGANIZATION_ID, "
                + "transaction.user_id AS USER_ID, " + "timetable.begin_time, " + "timetable.end_time "
                + "FROM online.tbl_user AS user " + "RIGHT OUTER JOIN online.tbl_transaction AS transaction "
                + "ON user.id=transaction.user_id " + "LEFT OUTER JOIN process.tbl_tranzaction_mode AS timetable "
                + "ON user.organization_id = timetable.organization_id " + "WHERE transaction.dt BETWEEN :beginDate AND :endDate "
                + "ORDER BY user.organization_id";
    }

    /**
     * Отримати головну орг по orgId
     * PaymentsContext(sql2o)
     * @param orgId
     * @return
     * @throws Sql2oException
     */
    public static int getMainOrgId( int orgId) throws Sql2oException {
        String findMainOrgIdSql = "{call main_new.find_main_org_id(?, ?)}";
        try (org.sql2o.Connection c = getSql2o().open()) {
            CallableStatement s = c.getJdbcConnection().prepareCall(findMainOrgIdSql);
            s.setInt("p_org_id", orgId);
            s.executeUpdate();
            return s.getInt("p_main_org_id");
        } catch ( SQLException e ) {
            LOGGER.error(e.getMessage());
            throw new Sql2oException(e);
        }
    }

    public static int getBalanceOrgId( int orgId) throws Sql2oException {
        String findMainOrgIdSql = "{?= CALL main_new.get_balance_org_id(?)}";
        try (org.sql2o.Connection c = getSql2o().open()) {
            CallableStatement s = c.getJdbcConnection().prepareCall(findMainOrgIdSql);
            s.registerOutParameter(1, Types.INTEGER);
            s.setInt(2, orgId);
            s.executeUpdate();
            return s.getInt(1);
        } catch ( SQLException e ) {
            LOGGER.error(e.getMessage());
            throw new Sql2oException(e);
        }
    }


    /**
     * Створення інсерт стейтмента для збереження комісій
     *
     * @param c Коннект до б/д (JDBCConnection)
     * @return
     * @throws SQLException
     */

    public static PreparedStatement createRetentionStoreStatement(Connection c) throws SQLException {
        String sql = "INSERT INTO process.payments_commissions (payment_id, comission_acceptor_org_id, comission_acceptor_type,"
                + "service_id, formula_id, comission) " + "VALUES (?, ?, ?, ?, ?, ?);";
        return c.prepareStatement(sql);
    }

    /**
     * Створення інсерт стейтмента для збереження оплат
     * @param c Коннект до б/д (JDBCConnection)
     * @return
     * @throws SQLException
     */
    public static PreparedStatement createPaymentStoreStatement(Connection c) throws SQLException {
        // збереження оплат
        // TODO: to mark each parameter by cooment like /*debt*/
        String sql = "INSERT INTO process.processed_payments " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        return c.prepareStatement(sql);
    }


    /**
     * Отримуємо усі транзакції по конкретному касиру за певний розрахунковий
     * період
     *
     * @param ctx (Payment context) ( getBeginPeriod|getEndPeriod|getSql2o)
     * @param cashier
     * @return FetchResult<PaymentTransaction>
     * @throws SQLException
     */

    static public FetchResult<PaymentTransaction> getTransactionByCashierAndPeriod(PaymentsContext ctx, Cashier cashier) throws Sql2oException {
        String sql = "SELECT id, home_id, user_id, dt, type_cash " + "FROM online.tbl_transaction AS t "
                + "WHERE t.user_id = :user_id " + "AND t.dt BETWEEN :start AND :end AND t.cancelled = false "
                + "AND t.home_id != 0;";


        try( org.sql2o.Connection c = ctx.getSql2o().open() ) {

            String start = obtainSchedule(ctx.getPeriodBegin(), ctx.getPeriodBegin(), cashier.begin_time != null ? cashier.begin_time.toLocalTime() : null , 1).toString();
            String end =  obtainSchedule(ctx.getPeriodEnd(), ctx.getPeriodBegin(), cashier.end_time != null ? cashier.end_time.toLocalTime() : null, 0).toString();

            Query q = c.createQuery(sql)
                    .addParameter("user_id", cashier.user_id)
                    .addParameter("start", start)
                    .addParameter("end", end);

            return FetchResult.of(q.executeAndFetch(PaymentTransaction.class), getQuerySqlStatement(q));
        } catch ( Exception e ) {
            LOGGER.error("getTransactionByCashierAndPeriod", e.getMessage());
            throw e;
        }
    }

    /**
     * Повертає список операцій по даній транзакції
     * PaymentContext(getSql2o)
     * @param ctx Payment context
     * @param id  Transaction Id
     * @return FetchResult<PaymentOperation>
     */
    static public FetchResult<PaymentOperation> getOperationsByTransactionId(PaymentsContext ctx, int id) throws Sql2oException {

        String sql = "SELECT " +
                "p.ID, " +
                "p.no, " +
                "p.zhek, " +
                "p.account, " +
                "a.home_id, " +
                "p.service_id, " +
                "p.value, " +
                "s.supplier_org_id, " +
                "s.acceptor_org_id "
                + "FROM online.tbl_payment AS p " + "LEFT JOIN main_new.m_account AS a "
                + "ON p.account = a.account AND p.zhek = a.zhek_code "
                + "LEFT JOIN main_new.m_supplier AS s "
                + "ON a.id = s.account_id AND p.service_id = s.service_id "
                + "WHERE p.transaction_id = :id";
        try( org.sql2o.Connection c = ctx.getSql2o().open() ) {
            Query q = c.createQuery(sql).addParameter("id", id);
            FetchResult<PaymentOperation> result = FetchResult.of(q.executeAndFetch(PaymentOperation.class), getQuerySqlStatement(q));
            result.getStream().forEach( (po) -> po.main_org_id = ctx.getMainOrgId());
            return result;
        } catch (Exception e) {
            LOGGER.error("getOperationsByTransactionId", e.getMessage());
            throw e;
        }
    }

    private static String getFormulesQuery() {
        return "SELECT c.*, f.*, o.zhek, t.type_id "
                + "FROM main_new.m_comission AS c "
                + "LEFT JOIN main_new.m_comission_formulae AS f "
                + "ON c.comission_formula_id = f.id "
                + "LEFT JOIN main_new.m_org_cmsu AS o "
                + "ON c.comission_acceptor_org_id = o.org_id "
                + "LEFT JOIN `online`.`tbl_organization` AS t "
                + "ON c.comission_acceptor_org_id = t.id "
                + "LEFT JOIN main_new.m_history AS h "
                + "ON c.history_id = h.id "
                + "WHERE c.service_id = :serviceId "
                + "AND c.supplier_org_id = :supplierOrgId "
                + "AND c.bank_org_id = :bankOrgId " // bankOrgId == mainOrgId
                + "AND f.is_month = :isMonth "
                + "AND f.percent != :percent "
                + "AND h.out_date > :outDate "
                + "AND h.cancelled = :cancelled";
    }

    static Cache<String, FetchResult<Formula>> formulasCache = CacheBuilder.newBuilder()
            .initialCapacity(150000).build();

    static public FetchResult<Formula> getFormulas( int serviceId, int supplierOrgId, int mainOrgId) {
        String key = Integer.toString(serviceId) + Integer.toString(supplierOrgId) + String.valueOf(mainOrgId);
        FetchResult<Formula> result = null;
        try { result =  formulasCache.get( key, () -> getFormulasInt( serviceId, supplierOrgId, mainOrgId) );
        } catch (ExecutionException e) {
            LOGGER.error( "FAILED getFormulas", e );
        }
        return result;
    }

    static public FetchResult<Formula> getFormulasInt( int serviceId, int supplierOrgId, int mainOrgId ) {
        String sql = getFormulesQuery();
        try(org.sql2o.Connection c = getSql2o().open() ){
            Query q = c.createQuery(sql)
                    .addParameter("serviceId", serviceId)
                    .addParameter("supplierOrgId", supplierOrgId)
                    .addParameter("bankOrgId", mainOrgId )
                    .addParameter("isMonth", FORMULA_IS_MONTH)
                    .addParameter("percent", FORMULA_PERCENT)
                    .addParameter("outDate", PaymentConfig.getProcessingDate())
                    .addParameter("cancelled", M_HISTORY_STATYS);

            return FetchResult.of( q.executeAndFetch(Formula.class), getQuerySqlStatement(q) );

        } catch (Sql2oException e) {
            LOGGER.error("FAILED getFormules:" + e.getMessage() );
            throw  e;
        }

    }


    /**
     * Should return 1 item normally
     * @param orgId
     * @return
     */
    static public ScalarResult<Table> getMoneyRedirectRulesInternal(org.sql2o.Connection c, int orgId ) {
        String sql = "SELECT r.account, a.rule_attribute_type, a.rule_attribute_value " +
                "FROM process.tbl_rules AS r " +
                "LEFT JOIN process.tbl_rules_attributes AS a " +
                "ON r.id = a.rule_id " +
                "WHERE r.organization_id = :orgId " +
                "AND r.stopped != 1";
            Query q = c.createQuery(sql)
                    .addParameter("orgId", orgId);
            return ScalarResult.of( q.executeAndFetchTable(), getQuerySqlStatement(q) );
    }

    static public ScalarResult<Table> getZhekInterests(PaymentsContext ctx ) {
        String sql = "SELECT * " +
                "FROM main_new.m_zhek_interest " +
                "WHERE main_new.m_zhek_interest.zhek_code = :zhek " +
                "AND main_new.m_zhek_interest.active = true;";
        try(org.sql2o.Connection c = ctx.getSql2o().open()) {
            Query q = c.createQuery(sql);
                q.addParameter("zhek", ctx.getZhek());

            Table result = q.executeAndFetchTable();
            return ScalarResult.of( result, getQuerySqlStatement(q));

        } catch ( Sql2oException e ) {
            LOGGER.error("FAILED getRentaSplittings:" + e.getMessage());
            throw e;
        }
    }

    private static Cache<Integer, ScalarResult<Boolean>> serviceLocationsCache =
            CacheBuilder.newBuilder().maximumSize(100000).build();

    public static ScalarResult<Boolean> isServiceLocations(PaymentsContext ctx, int addserviceId) {
        Optional<ScalarResult<Boolean>> result = Optional.empty();
        int key = Objects.hash(ctx.getZhek(), addserviceId);
        try {
            result =  Optional.of(serviceLocationsCache.get(key,
                    () -> isServiceLocationsExists(ctx, addserviceId)));
        } catch (ExecutionException e) {
            LOGGER.error( "FAILED isServiceLocationsExists", e );
        }
        return result.get();
    }


    public static ScalarResult<Boolean> isServiceLocationsExists(PaymentsContext ctx, int addserviceId) {
        String sql = "SELECT EXISTS( " +
                                    "SELECT * FROM main_new.m_addservice_supplier " +
                                    "WHERE main_new.m_addservice_supplier.zhek_code = :zhek " +
                                    "AND main_new.m_addservice_supplier.addservice_id = :addServiceId) " +
                    "AS result;";
        try(org.sql2o.Connection c = ctx.getSql2o().open() ) {
            Query q = c.createQuery(sql)
                    .addParameter("zhek", ctx.getZhek() )
                    .addParameter("addServiceId", addserviceId );
            Boolean result = q.executeScalar(Boolean.class);
            return ScalarResult.of(result, getQuerySqlStatement(q));
        } catch ( Sql2oException e ) {
            LOGGER.error("FAILED isServiceLocationsExists:" + e.getMessage());
            throw e;
        }
    }

    public static ScalarResult<Boolean> isServiceExists(PaymentsContext ctx, Row r) {
        String sql = "SELECT EXISTS( " +
                                    "SELECT * FROM main_new.m_addservice_supplier " +
                                    "WHERE main_new.m_addservice_supplier.zhek_code = :zhek " +
                                    "AND main_new.m_addservice_supplier.house_id = (SELECT main_new.m_home.house_id " +
                                                                                    "FROM main_new.m_home " +
                                                                                    "WHERE main_new.m_home.id = :homeId) " +
                                    "AND main_new.m_addservice_supplier.addservice_id = :addServiceId " +
                                    "AND main_new.m_addservice_supplier.supplier_org_id = :supplierOrgId " +
                                    "AND main_new.m_addservice_supplier.acceptor_org_id = :acceptorOrgId) " +
                    "AS result;";
        try(org.sql2o.Connection c = ctx.getSql2o().open() ) {
            Query q = c.createQuery(sql)
                    .addParameter("zhek", ctx.getZhek() )
                    .addParameter("homeId", ctx.getHomeId())
                    .addParameter("addServiceId", r.getInteger("addservice_id"))
                    .addParameter("supplierOrgId", r.getInteger("supplier_org_id"))
                    .addParameter("acceptorOrgId", r.getInteger("acceptor_org_id"));
            Boolean result = q.executeScalar(Boolean.class);
            return ScalarResult.of(result, getQuerySqlStatement(q));
        } catch ( Sql2oException e ) {
            LOGGER.error("FAILED isServiceLocationsExists:" + e.getMessage());
            throw e;
        }
    }

    public static ScalarResult<Double> getTotalHomeRentCoefficient(PaymentsContext ctx) {
        String sql = "SELECT main_new.m_tarkv_home_total.total " +
                "FROM main_new.m_tarkv_home_total " +
                "WHERE main_new.m_tarkv_home_total.home_id = :homeId " +
                "ORDER BY k DESC LIMIT 1;";
        try( org.sql2o.Connection c = ctx.getSql2o().open() ) {
            Query q = c.createQuery(sql)
                .addParameter("homeId", ctx.getHomeId() );
            Double result = q.executeScalar(Double.class);
            return ScalarResult.of( result, getQuerySqlStatement(q));
        } catch ( Sql2oException e ) {
            LOGGER.error("FAILED getTotalHomeRentCoefficient:" + e.getMessage());
            throw e;
        }
    }

    public static ScalarResult<Double> getService(PaymentsContext ctx, Integer addserviceId) {
        String sql = "SELECT main_new.m_tarkv_home.value " +
                "FROM main_new.m_tarkv_home " +
                "WHERE main_new.m_tarkv_home.home_id = :homeId " +
                "AND main_new.m_tarkv_home.addservice_id = :addServiceId " +
                "AND  main_new.m_tarkv_home.dt = (SELECT MAX(dt) " +
                                                 "FROM main_new.m_tarkv_home " +
                                                 "WHERE main_new.m_tarkv_home.home_id = :homeIdMaxDate);";

        try(org.sql2o.Connection c = ctx.getSql2o().open() ) {
            Query q = c.createQuery(sql)
                    .addParameter("homeId", ctx.getHomeId() )
                    .addParameter("addServiceId", addserviceId)
                    .addParameter("homeIdMaxDate", ctx.getHomeId() );

            Double result = q.executeScalar(Double.class);
            return ScalarResult.of( result, getQuerySqlStatement(q));
        } catch ( Sql2oException e ) {
            LOGGER.error("FAILED getService:" + e.getMessage());
            throw e;
        }
    }

    private static String getQuerySqlStatement( Query q ) {
        String result = "";
        try {
            Field f = q.getClass().getDeclaredField("statement");
            f.setAccessible(true);
            PreparedStatement s =  (PreparedStatement)f.get(q);
            result = s.toString();
            result = result.substring( result.indexOf("SELECT") );
            f.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException  e) {
            LOGGER.error("FAILED getQuerySqlStatement ",  e );
        }
        return result;
    }

    public static boolean isNullPresent(final Table t, final Row r ) {
        return t.columns().stream().anyMatch( (c) ->  r.getObject(c.getName()) == null );
    }

    public static boolean validateTable(final Table t ) {
        return t.rows().isEmpty() || t.rows().stream().noneMatch((r) -> isNullPresent(t, r));
    }

    static String getRowValue( final Table t, final Row r  ) {
        Stream<Integer> columns = t.columns().stream().map( Column::getIndex );
        return columns.map( i -> t.columns().get(i).getName() + "=" + r.getObject(i) )
                .collect(Collectors.joining("| ","[", "]"));
    }

    public static String getTableErrors( final Table t) {
        Stream<Row> rows =  t.rows().stream().filter( r -> isNullPresent(t, r) );
        return rows.map( r -> getRowValue(t,r) ).collect( Collectors.joining("\n") );
    }

    /**
     * враховує період обробки тарнзакцій відмінний від календарного дня
     * @param defaultDate стандартне значення дати (за відсутності графіку роботи установи)
     * @param evaluateDate дата для розрахунків
     * @param time час що необхідно додати до розрахункової дати
     * @param days дні що необхідно відняти від розрахункової дати
     * @return LocalDateTime дата з врахованим графіком роботи установи, або стандартне значення дати
     */
    public static LocalDateTime obtainSchedule( final LocalDateTime defaultDate,  final LocalDateTime evaluateDate,  final LocalTime time, final int days) {
        return time != null
                ? evaluateDate.minusDays(days)
                        .plusHours(time.getHour())
                        .plusMinutes(time.getMinute())
                        .plusSeconds(time.getSecond())
                : defaultDate;
    }


    public static Query createPaymentUpdateQuery(org.sql2o.Connection c) throws SQLException {
        String sql = "UPDATE process.processed_payments " +
                "SET process.processed_payments.transfer_value = :transferValue " +
                "WHERE process.processed_payments.payment_id = :paymentId;";
        return c.createQuery(sql);
    }


    public static ScalarResult<Table> getOrganizationPayments(org.sql2o.Connection c, Integer orgId, Integer bankId) {
        String sql = "SELECT p.payment_id, p.transfer_value " +
                "FROM process.processed_payments AS p " +
                "JOIN process.processed_days AS d " +
                "ON p.processed_day_id = d.id " +
                "WHERE p.acceptor_org_id = :supplierOrgId " +
                "AND p.balance_org_id = :balanceOrgId " +
                "AND transfer_value > 0 " +
                "AND d.processed_date = :processingDate;";

        try {
            Query q = c.createQuery(sql)
                    .addParameter("supplierOrgId", orgId)
                    .addParameter("balanceOrgId", bankId)
                    .addParameter("processingDate", PaymentConfig.getProcessingDate());

            return ScalarResult.of(q.executeAndFetchTable(), getQuerySqlStatement(q));
        } catch (Sql2oException e) {
            LOGGER.error("FAILED getOrganizationPayments ", e);
            throw e;
        }
    }

    public static Table getOrganizationsDebts(org.sql2o.Connection c, String processingDate) {
        String sql = "SELECT d.id AS debt_id, " +
                        "d.organization_id, " +
                        "d.bank_id, " +
                        "d.limit_percent, " +
                        "(b.organization_saldo - b.organization_credit)  AS saldo " +
                        "FROM process.`organizations_debts` AS d " +
                        "LEFT JOIN process.`organizations_debts_balance` AS b " +
                        "ON d.id = b.organization_debt_id " +
                        "WHERE b.id = (SELECT balance.id FROM process.`organizations_debts_balance` AS balance " +
                                        "WHERE balance.organization_debt_id = d.id " +
                                        "ORDER BY balance.id DESC LIMIT 1) " +
                        "AND b.organization_saldo - b.organization_credit != 0.00 " +
                        "AND processing_date <= :processingDate;";
        return c.createQuery(sql).addParameter("processingDate", processingDate).executeAndFetchTable();
    }

    private static Cache<Integer, ScalarResult<Table>> moneyRedirectRulesCache =
            CacheBuilder.newBuilder().maximumSize(100000).build();

    public static Optional<AcceptorRequisites> getAcceptorRequisites(final PaymentsContext ctx, PaymentOperation po) {
        try (org.sql2o.Connection c = ctx.getSql2o().open()) {
            ScalarResult<Table> mrr = PaymentsDAO.getMoneyRedirectRules(c, po.acceptor_org_id);
            boolean isRedirection = mrr.get().rows().isEmpty() ? false : mrr.get().rows().stream().anyMatch((r) -> isRedirection(po, r));
            List<Row> filteredMRR = mrr.get().rows().stream().filter((r) -> isRedirection(po, r)).collect(Collectors.toList());
            if (filteredMRR.size() > 1) {
                LOGGER.error("More than one money redirection rule. Payment id = {}. Organization id = {}.", po.ID, po.acceptor_org_id);
                throw new IllegalArgumentException();
            }
            int accountId = isRedirection ? filteredMRR.stream().findFirst().get().getInteger("account") : 0;

            OptionalConsumer consumer = OptionalConsumer.of( getAcceptorRequisites(po.acceptor_org_id, accountId, isRedirection, c) )
                    .ifNotPresent( () -> LOGGER.error("Can't find Acceptor requisites for acceptor_org_id = {}, account_id = {}. redir = {} ",
                            po.acceptor_org_id, accountId, isRedirection  ) );

            return consumer.getOptional();

        } catch (Sql2oException e) {
            LOGGER.error("FAILED getAcceptorRequisitesExternal:" + e.getMessage());
            throw e;
        }
    }

    public static ScalarResult<Table> getMoneyRedirectRules(org.sql2o.Connection c, Integer acceptorOrgId) {
        Optional<ScalarResult<Table>> result = Optional.empty();
        try {
            result =  Optional.of(moneyRedirectRulesCache.get(acceptorOrgId,
                    () -> getMoneyRedirectRulesInternal(c, acceptorOrgId)));
        } catch (ExecutionException e) {
            LOGGER.error( "FAILED getMoneyRedirectRules", e );
        }
        return result.get();
    }

    private static boolean isRedirection(PaymentOperation po, final Row r) {
        BiFunction<PaymentOperation, Row, Boolean> f = redirMap.get().get( r.getInteger("rule_attribute_type") );
        checkNotNull( f, "isRedirection:" + "No result in money redirect" );
        return f.apply(po, r);
    }

    private static final
    Supplier<Map<Integer, BiFunction<PaymentOperation, Row, Boolean>>>
            redirMap = Suppliers.memoize( () -> {
        Map<Integer, BiFunction<PaymentOperation, Row, Boolean>> redirMap = new HashMap<>();
        redirMap.put( 1, (po,r) -> po.main_org_id.equals(r.getInteger("rule_attribute_value")) );
        redirMap.put( 2, (po,r) -> po.zhek.equals(r.getInteger("rule_attribute_value")) );
        redirMap.put( 4, (po,r) -> po.service_id.equals(r.getInteger("rule_attribute_value")) );
        redirMap.put( 5, (po,r) -> po.home_id.equals(r.getInteger("rule_attribute_value")) );
        return redirMap;
    });


    public static Cache<Integer, Integer> acceptorTypeCache = CacheBuilder.newBuilder()
            .initialCapacity(150000).build();

    public static int getAcceptorType(PaymentsContext ctx) {
        Integer key = Objects.hash( ctx.getComissionAcceptorOrgId(), ctx.getComissionTypeId(), 47 );
        int result = 0;
        try {
            result =  acceptorTypeCache.get(key, () -> getAcceptorTypeInternal(ctx));
        } catch (ExecutionException e) {
            LOGGER.error( "FAILED getAcceptorType", e );
        }
        return result;
    }

    private static int getAcceptorTypeInternal(PaymentsContext ctx) {
        Function<PaymentsContext, Integer> f = acceptorTypeMap.get().get(ctx.getComissionTypeId());
        checkNotNull( f, "getAcceptorTypeInternal: No result in acceptor types" );
        return f.apply(ctx);
    }

    private static final
    Supplier<Map<Integer, Function<PaymentsContext, Integer>>>
            acceptorTypeMap = Suppliers.memoize( () -> {
        Map<Integer, Function<PaymentsContext, Integer>> commissionAcceptorTypeMap = new HashMap<>();
        commissionAcceptorTypeMap.put(COMMISSION_TYPE_NONE, (ctx) -> ORGANIZATION_TYPE_NONE);
        commissionAcceptorTypeMap.put(COMMISSION_TYPE_PAYMENTS, (ctx) ->
                                ctx.getComissionAcceptorOrgId() == CMSU_ID ? ORGANIZATION_TYPE_CMSU : ORGANIZATION_TYPE_BANK);
        commissionAcceptorTypeMap.put(COMMISSION_TYPE_BILLING_SERVICE, (ctx) -> ORGANIZATION_TYPE_ZHEK);
        return commissionAcceptorTypeMap;
    });

    private static Cache<Integer, Optional<AcceptorRequisites>> acceptorRequisitesCache =
            CacheBuilder.newBuilder().maximumSize(100000).build();

    public static Optional<AcceptorRequisites> getAcceptorRequisites(Integer acceptorOrgId, int accountId, boolean redirection,
                                                                     org.sql2o.Connection c) {
        Optional<AcceptorRequisites> result = Optional.empty();
        try {
            result =  acceptorRequisitesCache.get(Objects.hash(acceptorOrgId, accountId),
                    () -> Optional.ofNullable(getAcceptorRequisitesInternal(acceptorOrgId, accountId, redirection, c)));
        } catch (ExecutionException e) {
            LOGGER.error( "FAILED getAcceptorRequisites", e );
        }
        return result;
    }

    public static AcceptorRequisites getAcceptorRequisitesInternal(Integer acceptorOrgId, int accountId, boolean redirection,
                                                                              org.sql2o.Connection c) {
        String select = "SELECT account, mfo  FROM main_new.m_org_accounts ";
        String main = select
                + "WHERE main_new.m_org_accounts.org_id = :p1 "
                + "AND main_new.m_org_accounts.priority = 0";
        String redirect = select + "WHERE main_new.m_org_accounts.id = :p1";

        return c.createQuery(redirection ? redirect : main)
                .withParams(redirection ? accountId : acceptorOrgId)
                .executeAndFetchFirst(AcceptorRequisites.class);
    }

    public static Table getNewDebts(org.sql2o.Connection c, String processingDate) {
        String sql = "SELECT d.id AS debt_id, " +
                "d.organization_debt AS saldo " +
                "FROM process.`organizations_debts` AS d " +
                "WHERE d.id NOT IN (SELECT DISTINCT organization_debt_id FROM process.`organizations_debts_balance`) " +
                "AND processing_date <= :processingDate;";
        return c.createQuery(sql).addParameter("processingDate", processingDate).executeAndFetchTable();
    }

    public static Query createBalanceInsertQuery(org.sql2o.Connection c) {
        String sql = "INSERT INTO process.`organizations_debts_balance` " +
                "(organization_debt_id, organization_saldo, organization_credit, processed_date, creation_date_time) " +
                "VALUES (:debtId, :saldo, :credit, :processedDate, :creationDateTime);";
        return c.createQuery(sql);
    }

    public static void deleteBalanceForDay(org.sql2o.Connection c, String processingDate) {
        String sql = "DELETE FROM process.`organizations_debts_balance` " +
                "WHERE processed_date = :processingDate";
        c.createQuery(sql).addParameter("processingDate", processingDate).executeUpdate();
    }

    public static Boolean isProcessingDayExists(org.sql2o.Connection c) {
        String sql = "SELECT EXISTS ( " +
                "SELECT * FROM process.processed_days " +
                "WHERE process.processed_days.processed_date = :processingDate " +
                ") AS result;";
        Query q = c.createQuery(sql).addParameter("processingDate", PaymentConfig.getProcessingDate());
        return q.executeScalar(Boolean.class);
    }

    public static void insertProcessingDay(org.sql2o.Connection c) {
        String sql = "INSERT INTO `process`.`processed_days` (`processed_date`, `operator_id`, `day_status_id`, `comment`) " +
                "VALUES (:processingDate, :operatorId, :dayStatusId, :comment);";
        c.createQuery(sql).addParameter("processingDate", PaymentConfig.getProcessingDate())
                .addParameter("operatorId", PaymentConfig.getOperatorId())
                .addParameter("dayStatusId", DayStatus.OPEN.ordinal())
                .addParameter("comment", PaymentConfig.getComment())
                .executeUpdate();
    }

    public static int getProcessingDayId(org.sql2o.Connection c) {
        String sql = "SELECT MAX(d.id) AS id " +
                "FROM process.processed_days AS d;";
        return c.createQuery(sql).executeScalar(Integer.class);
    }

    public static void deleteProcessingDay(org.sql2o.Connection c) {
        String sql = "DELETE " +
                "FROM process.processed_days " +
                "WHERE process.processed_days.processed_date = :processingDate " +
                "AND process.processed_days.day_status_id != :dayStatusId;";
        c.createQuery(sql).addParameter("processingDate", PaymentConfig.getProcessingDate())
                .addParameter("dayStatusId", DayStatus.CLOSED.ordinal())
                .executeUpdate();
    }

    public static boolean isNotClosedProcessingDays(org.sql2o.Connection c) {
        String sql = "SELECT EXISTS (SELECT * " +
                "FROM process.processed_days AS d " +
                "WHERE d.day_status_id != :dayStatusId) AS result;";
        return c.createQuery(sql).addParameter("dayStatusId", DayStatus.CLOSED.ordinal())
                .executeScalar(Boolean.class);
    }

    public static void updateProcessingDay(int processingDayId, int dayStatusId) {
        String sql = "UPDATE process.processed_days AS d " +
                "SET d.day_status_id = :dayStatusId " +
                "WHERE d.id = :processingDayId;";
        try(org.sql2o.Connection c = PaymentsDAO.getSql2o().open() ) {
            c.createQuery(sql)
                    .addParameter("dayStatusId", dayStatusId)
                    .addParameter("processingDayId", processingDayId)
                    .executeUpdate();
        } catch ( Sql2oException e ) {
            LOGGER.error("FAILED updateProcessingDay:", e);
            throw e;
        }
    }

    public static Table getSpecificPayments(org.sql2o.Connection c, String beginDate, String endDate) {
        String sql = "SELECT u.organization_id, " +
                             "t.user_id, " +
                             "t.id AS transaction_id, " +
                             "p.ID AS payment_id, " +
                             "p.no AS quittance_number, " +
                             "t.type_cash AS cash_type_id, " +
                             "p.value, " +
                             "t.dt, t.dt_our " +
                     "FROM `online`.`tbl_transaction` AS t " +
                     "JOIN `online`.`tbl_payment` AS p " +
                     "on t.id  = p.transaction_id " +
                     "JOIN `online`.`tbl_user` AS u " +
                     "ON t.user_id = u.id " +
                     "WHERE t.home_id = 0 " +
                     "AND t.cancelled = false " +
                     "AND t.dt BETWEEN :beginDate AND :endDate " +
                     "AND p.value > 0 " +
                     "AND u.organization_id IN (:cashRegisterOne, :cashRegisterTwo);";
        return c.createQuery(sql).addParameter("beginDate", beginDate)
                                 .addParameter("endDate", endDate)
                                 .addParameter("cashRegisterOne", SpecificPaymentProcessor.VODOKANAL_CASH_REGISTER_1)
                                 .addParameter("cashRegisterTwo", SpecificPaymentProcessor.VODOKANAL_CASH_REGISTER_2)
                                 .executeAndFetchTable();
    }
}
