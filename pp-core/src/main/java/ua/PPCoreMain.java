package ua;

import com.google.common.base.Stopwatch;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.configuration.ConfigurationException;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import ua.data.Cashier;
import ua.data.FetchResult;
import ua.events.ProcessingBegin;
import ua.events.ProcessingEnd;
import ua.events.ProcessingErrorEvent;
import ua.events.ProcessingMsgEvent;

import java.sql.SQLException;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class PPCoreMain {

    org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PPCoreMain.class);
    String lastError;

    public boolean isDebuger() {
        return java.lang.management.ManagementFactory.getRuntimeMXBean().
                getInputArguments().toString().contains("jdwp");
    }

    private Optional<ua.events.PaymentProcessingListener> listener = Optional.empty();

    private PPCoreMain() {}

    private static Supplier<PPCoreMain> instance_ = Suppliers.memoize(PPCoreMain::new);

    public static PPCoreMain getInstance() {
        return instance_.get();
    }

    private EventBus paymentProcessingEventBus = new EventBus();

    public void registerProgressListener(Object listener) {
        paymentProcessingEventBus.register(listener);
    }

    public void setPPListener(ua.events.PaymentProcessingListener listener ) {
        this.listener = Optional.of(listener);
    }

    public void removePPListener() {
        listener = null;
    }

    private void fireStartProcessing() {
        paymentProcessingEventBus.post( new ProcessingBegin() );
        listener.ifPresent( (l) -> l.onStart() );
    }

    private void fireEndProcessing() {
        paymentProcessingEventBus.post( new ProcessingEnd() );
        listener.ifPresent( (l) -> l.onFinish() );
    }

    private void fireOnProgressProcessing(double progress) {
        listener.ifPresent((l) -> l.onProgress(progress));
    }

    private void fireOnMessage(String msg) {
        listener.ifPresent((l) -> l.onMessage(msg));
    }

    private void fireOnError(String msg) {
        listener.ifPresent((l) -> l.onError(msg));
    }


    /**
     * Event comming from PaymentProcessor side
     * @param percents
     */
    @Subscribe
    @AllowConcurrentEvents
    public synchronized void onPPProgress(Double percents) {
        this.fireOnProgressProcessing(percents);
    }

    /**
     * Event comming from PaymentProcessor side
     * @param msg
     */
    @Subscribe
    @AllowConcurrentEvents
    public synchronized void onPPMessage(ProcessingMsgEvent msg) { this.fireOnMessage(msg.getMsg()); }


    @Subscribe
    @AllowConcurrentEvents
    public synchronized void onPPError(ProcessingErrorEvent msg) { this.fireOnError(msg.getErrorMsg()); }

    /**
     * Entry point
     * @param numberOfThreads
     */
    public void proceed( int numberOfThreads, boolean loadConfiguration ) {

        paymentProcessingEventBus.register(this);

        fireStartProcessing();

        logInfo("Payment processing started");
        Stopwatch sw = Stopwatch.createStarted();

        // Обробка
        try {
            loadConfig(loadConfiguration);
            LOGGER.info("SQl Server:" + PaymentConfig.getDatabaseServer());
            fireOnMessage("SQl Server:" + PaymentConfig.getDatabaseServer());
            LOGGER.info("Processing Date: [{}]", PaymentConfig.getProcessingDate());
            fireOnMessage(("Processing Date: [{" + PaymentConfig.getProcessingDate() + "}]"));
            LOGGER.info("Threads used:{}", numberOfThreads);
            fireOnMessage("Threads used: " + numberOfThreads);

            launchThreads(PaymentsDAO.getSql2o(), numberOfThreads, createProcessingDay(),
                            new PaymentProcessingListenerInternal(0));
        } catch( DateTimeParseException dtpe ) {
            logError("Invalid processing date/time");
            fireOnError("Invalid processing date/time");
        }
        catch (Exception e) {
            logError(e);
        } finally {
            LOGGER.info("Payment processing finished:" + "Elapsed Time:" + sw.stop());
            fireOnMessage("Payment processing finished:" + "Elapsed Time:" + sw.toString());
            fireEndProcessing();
            paymentProcessingEventBus.unregister(this);
            removePPListener();
        }
    }

    public String getLastError() {
        return lastError;
    }

    /**
     * Load default config
     * @throws ConfigurationException
     */
    private void loadConfig(boolean load) throws ConfigurationException {
        PaymentConfig.loadConfig(load);
    }

    public void proceed( Integer operatorId, String startDate, String endDate, String comment ) {
        try {
            loadConfig(true);
            PaymentConfig.setStartEndProcessing(startDate, endDate);
            PaymentConfig.setComment(comment);
            PaymentConfig.setOperatorId(operatorId);
        } catch ( Exception e ) {
            logError(e);
        }
        proceed( Runtime.getRuntime().availableProcessors(), false );
    }

    private void logError(Exception e) {
        lastError = "FAILED: " + e.getMessage() + ":" + e.getCause();
        LOGGER.error( lastError );
        fireOnError( lastError );
    }

    private void logError(String error) {
        lastError = error;
        LOGGER.error( lastError );
        fireOnError( lastError );
    }

    private void logError(String msg, Exception e) {
        lastError = msg + " "+ e.getMessage() + ":" + e.getCause();;
        LOGGER.error( lastError );
    }

    private void logInfo(String msg) {
        LOGGER.info( msg );
    }

    // payment context builder
    public static PaymentsContext buildPaymentsContext(Sql2o sql2o) {
        return PaymentsContext.create().setSql2o(sql2o);
    }

    private void launchThreads(Sql2o sql2o, int numberOfThreads, int processingDayId, PaymentProcessingListenerInternal internalListener) {

        int cores = numberOfThreads == 0 ? Runtime.getRuntime().availableProcessors() : numberOfThreads;

        PaymentsContext ctx = buildPaymentsContext(sql2o);
        FetchResult<Cashier> cashiersResult = PaymentsDAO.getCashiersAndSchedules(ctx);

        // cashiers validation
        List<Cashier> cashiers = cashiersResult.getResultList();

        internalListener.setTotal( cashiersResult.getResultList().size() );
        internalListener.setAnotherEventBus(paymentProcessingEventBus);

        final ExecutorService executorService = Executors.newFixedThreadPool(cores);

        int count = cashiers.size() / cores;
        int tail = cashiers.size() % cores;

        List<List<Cashier>> chunks = sliceUpCashiers(cores, cashiers, count, tail);

        Boolean isErrors = false;
        try( Connection connect = sql2o.beginTransaction() ) {
            List<PaymentProcessor> processors = new ArrayList<>();
            chunks.stream().forEach((c) -> {
                processors.add(PaymentProcessor.Of(FetchResult.of(c, cashiersResult.getSql()), processingDayId));
                PaymentProcessor pp = processors.get(processors.size() - 1);
                pp.setTransactedConnection(connect);
                pp.registerPaymentProcessingListener(internalListener);
                executorService.execute(pp);
            });

            executorService.shutdown();
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

            LOGGER.info("\n");
            isErrors = processors.stream().anyMatch(PaymentProcessor::isErrors);
            if( !isErrors ) {
                processors.stream().forEach(p -> {
                        try { p.commit();}
                        catch (SQLException e) {
                            logError("Failed during commit", e);
                        }
                    });
                connect.commit();
                PaymentsDAO.updateProcessingDay(processingDayId, DayStatus.PROCESSED.ordinal());
            } else {
                logError("FAILED Payment Processor(s), see logs for info");
                connect.rollback(true);
                PaymentsDAO.updateProcessingDay(processingDayId, DayStatus.ERROR.ordinal());
            }
        } catch (Exception e) {
            LOGGER.error("FAILED launchThreads:", e);
        }

        if (!isErrors) {
            DebtProcessor.proceed();
            SpecificPaymentProcessor.proceed(processingDayId);
        }
    }

    private static List<List<Cashier>> sliceUpCashiers(int cores, List<Cashier> cashiers, int countPerCore, int tail) {
        List<List<Cashier>> chunks = new ArrayList<>();
        if (countPerCore > 0) {
            IntStream.range(0, cores).forEach( i -> {
                int from = i * countPerCore;
                chunks.add(cashiers.subList(from, from + countPerCore));
            });
        }
        if( tail > 0 ) {
            ArrayList tailList = new ArrayList<>();
            tailList.addAll(cashiers.subList(cashiers.size() - tail, cashiers.size()));
            chunks.add(tailList);
        }
        return chunks;
    }

    private int createProcessingDay() throws Exception {
        try (Connection c = PaymentsDAO.getSql2o().open()) {
            PaymentsDAO.deleteProcessingDay(c);
            if (isInvalidProcessingDay(c)) {
                throw new Exception("No closed day present or processing date already closed !");
            }
            PaymentsDAO.insertProcessingDay(c);
            return PaymentsDAO.getProcessingDayId(c);
        } catch (Exception e) {
            logError("Creation of processing day FAILED", e);
            throw e;
        }
    }

    private static boolean isInvalidProcessingDay(org.sql2o.Connection c) {
        return PaymentsDAO.isProcessingDayExists(c)
                || PaymentsDAO.isNotClosedProcessingDays(c);
    }
}