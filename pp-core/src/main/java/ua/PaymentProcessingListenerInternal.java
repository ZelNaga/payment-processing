package ua;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.events.ProcessingErrorEvent;
import ua.events.ProcessingMsgEvent;
import ua.events.ProgressEvent;

/**
 * Created by ekh on 18.11.15.
 */
class PaymentProcessingListenerInternal {

    static Logger LOGGER = LoggerFactory.getLogger("Total Progress");
    int total;
    EventBus anotherEventBus;
    Double lastPercent = .0;
    volatile int current;

    public PaymentProcessingListenerInternal(int totalProgressValue) {
        this.total = totalProgressValue;
    }

    @Subscribe
    public synchronized void onProgress(ProgressEvent progressEvent) {
        current++;
        int percent = (int)((double)current/(double)(total) * 100);
        if( lastPercent != percent ) {
            lastPercent = (double) percent;
            anotherEventBus.post(lastPercent);
        }
    }

    @Subscribe
    public synchronized void onMessage( ProcessingMsgEvent msg ) {
        anotherEventBus.post(msg);
    }

    @Subscribe
    public synchronized void onError( ProcessingErrorEvent msg ) {
        anotherEventBus.post(msg);
    }

    public void setAnotherEventBus(EventBus eventBus) {
        this.anotherEventBus = eventBus;
    }
    public void setTotal(int total) {
        this.total = total;
    }

}
