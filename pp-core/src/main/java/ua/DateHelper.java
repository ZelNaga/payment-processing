package ua;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by zelnaga on 14.12.15.
 */
public class DateHelper {
    private static DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static LocalDateTime createLocalDateTime(String date) {
        return LocalDateTime.parse(date, DATETIME_FORMATTER);
    }

    public static String timestamp() {
        return LocalDateTime.now().format(DATETIME_FORMATTER);
    }
}
