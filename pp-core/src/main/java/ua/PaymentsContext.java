package ua;

import com.google.common.base.MoreObjects;
import org.sql2o.Sql2o;
import org.sql2o.data.Row;
import ua.data.*;

import java.time.LocalDateTime;

import static ua.PaymentsDAO.COMMISSION_TYPE_NONE;

// class for sharing data and etc
public class PaymentsContext {
    private LocalDateTime periodBegin; // begin processing period
    private LocalDateTime periodEnd; // end processing period
    private int totalPaymentsToProcess; // total payments for processing used to indicate progress
    private int transactionId;
    private int organizationId;
    private int mainOrgId; // main organization id
    private int balanceOrgId; // balance organization id
    private int serviceId; // service id
    private int supplierOrgId;// supplier organization id
    private int paymentId; // payment id
    private long mainAccount; // main account
    private int mfo;  // mfo
    private double value; // summa value
    private double transfer;
    private int comissionTypeId;
    private int comissionAcceptorOrgId;
    private int formulaId;
    private double comission;
    private Integer zhek; // from payment operation (zhek)
    private Integer homeId;
    private Integer account;
    private Integer acceptorOrgId;
    private int userId; // cashier id
    private int quittanceNumber; // номер квитанції
    private int typeCash; // вид розрахунку
    private String transactionDate; //TODO: find out if necessary this date
    private int processingDayId;

    private Sql2o sql2o;

    private PaymentsContext() {
        buildPeriod();
    }

    public Sql2o getSql2o() {
        return sql2o;
    }

    public PaymentsContext setSql2o(Sql2o sql2o) {
        this.sql2o = sql2o;
        return this;
    }

    static public PaymentsContext create() {
        return new PaymentsContext();
    }

    public LocalDateTime getPeriodBegin() {
        return periodBegin;
    }

    public PaymentsContext setPeriodBegin(LocalDateTime periodBegin) {
        this.periodBegin = periodBegin;
        return this;
    }

    public LocalDateTime getPeriodEnd() {
        return periodEnd;
    }

    public PaymentsContext setPeriodEnd(LocalDateTime periodEnd) {
        this.periodEnd = periodEnd;
        return this;

    }

    public int getTotalPaymentsToProcess() {
        return totalPaymentsToProcess;
    }

    public PaymentsContext setTotalPaymentsToProcess(int totalPaymentsToProcess) {
        this.totalPaymentsToProcess = totalPaymentsToProcess;
        return this;
    }

    public int getMainOrgId() {
        return mainOrgId;
    }

    public PaymentsContext setMainOrgId(int mainOrgId) {
        this.mainOrgId = mainOrgId;
        return this;
    }

    public int getServiceId() {
        return serviceId;
    }

    public PaymentsContext setServiceId(int serviceId) {
        this.serviceId = serviceId;
        return this;
    }

    public int getSupplierOrgId() {
        return supplierOrgId;
    }

    public PaymentsContext setSupplierOrgId(int supplierOrgId) {
        this.supplierOrgId = supplierOrgId;
        return this;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public PaymentsContext setPaymentId(int paymentId) {
        this.paymentId = paymentId;
        return this;
    }

    public long getMainAccount() {
        return mainAccount;
    }

    public PaymentsContext setMainAccount(long l) {
        this.mainAccount = l;
        return this;
    }

    public int getMfo() {
        return mfo;
    }

    public PaymentsContext setMfo(int mfo) {
        this.mfo = mfo;
        return this;
    }

    public double getValue() {
        return value;
    }

    public PaymentsContext setValue(double value) {
        this.value = value;
        return this;
    }

    public double getTransferValue() {
        return transfer;
    }

    public PaymentsContext setTransferValue(double transfer) {
        this.transfer = transfer;
        return this;
    }

    public int getComissionAcceptorOrgId() {
        return comissionAcceptorOrgId;
    }

    public PaymentsContext setComissionAcceptorOrgId(int comissionAcceptorOrgId) {
        this.comissionAcceptorOrgId = comissionAcceptorOrgId;
        return this;
    }

    public int getFormulaId() {
        return formulaId;
    }

    public PaymentsContext setFormulaId(int formulaId) {
        this.formulaId = formulaId;
        return this;
    }

    public double getComission() {
        return comission;
    }

    public PaymentsContext setComission(double comission2) {
        this.comission = comission2;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("periodBegin", periodBegin)
                .add("periodEnd", periodEnd)
                .add("totalPaymentsToProcess", totalPaymentsToProcess)
                .add("mainOrgId", mainOrgId)
                .add("serviceId", serviceId)
                .add("supplierOrgId", supplierOrgId)
                .add("paymentId", paymentId)
                .add("mainAccount", mainAccount)
                .add("mfo", mfo)
                .add("value", value)
                .add("transfer", transfer)
                .add("comissionAcceptorOrgId", comissionAcceptorOrgId)
                .add("formulaId", formulaId)
                .add("comission", comission)
                .add("zhek", zhek)
                .add("homeId", homeId)
                .add("account", account)
                .add("sql2o", sql2o)
                .toString();
    }

    public Integer getZhek() {
        return zhek;
    }

    public PaymentsContext setZhek(Integer z ) {
        this.zhek = z;
        return this;
    }

    public double getTransfer() {
        return transfer;
    }

    public Integer getHomeId() {
        return homeId;
    }

    public Integer getAccount() {
        return account;
    }

    public PaymentsContext setHomeId(Integer homeId) {
        this.homeId = homeId;
        return this;
    }

    public PaymentsContext setAccount(Integer account) {
        this.account = account;
        return this;
    }

    public PaymentsContext setAcceptorOrgId(Integer acceptorOrgId) {
        this.acceptorOrgId = acceptorOrgId;
        return this;
    }

    public Integer getAcceptorOrgId() {
        return acceptorOrgId;
    }

    public int getTransactionId() {
        return this.transactionId;
    }

    public PaymentsContext setTransactionId(int transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public PaymentsContext setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public PaymentsContext setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public int getQuittanceNumber() {
        return quittanceNumber;
    }

    public PaymentsContext setQuittanceNumber(int quittanceNumber) {
        this.quittanceNumber = quittanceNumber;
        return this;
    }

    public int getTypeCash() {
        return typeCash;
    }

    public PaymentsContext setTypeCash(int typeCash) {
        this.typeCash = typeCash;
        return this;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public PaymentsContext setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public int getProcessingDayId() {
        return processingDayId;
    }

    public PaymentsContext setProcessingDayId(int processingDayId) {
        this.processingDayId = processingDayId;
        return this;
    }

    private void buildPeriod() {
        setPeriodBegin(DateHelper.createLocalDateTime(PaymentConfig.getPeriodBegin()));
        setPeriodEnd(DateHelper.createLocalDateTime(PaymentConfig.getPeriodEnd()));
    }

    public void buildPaymentsProcessParameters(final PaymentOperation o, final AcceptorRequisites requisites ) {
        this.setPaymentId(o.ID)
                .setQuittanceNumber(o.no)
                .setZhek(o.zhek)
                .setAccount(o.account)
                .setHomeId(o.home_id)
                .setServiceId(o.service_id)
                .setValue(o.value)
                .setSupplierOrgId(o.supplier_org_id)
                .setAcceptorOrgId(o.acceptor_org_id)
                .setMainAccount(requisites.account)
                .setMfo(requisites.mfo);
    }

    public void buildCashierParameters(Cashier cashier) {
        this.setUserId(cashier.user_id)
                .setOrganizationId(cashier.organization_id)
                .setMainOrgId(PaymentsDAO.getMainOrgId(cashier.organization_id))
                .setBalanceOrgId(PaymentsDAO.getBalanceOrgId(cashier.organization_id));
        }

    public void buildTransactionParameters(PaymentTransaction pt) {
        this.setTransactionId(pt.id)
                .setTypeCash(pt.type_cash)
                .setTransactionDate(pt.dt.toString());

    }

    public void buildRentaSplittingParams(final Double rentaPart, Double transferValue, Integer newAcceptorOrgId) {
        this.setComissionAcceptorOrgId(newAcceptorOrgId)
            .setFormulaId(0)
            .setComission(rentaPart)
            .setTransferValue(transferValue)
            .setComissionTypeId(COMMISSION_TYPE_NONE);
    }

    public void buildProcessretentionsParameters( Formula f, Double commission) {
        this.setComissionAcceptorOrgId(f.comission_acceptor_org_id)
                .setFormulaId(f.id)
                .setComission(commission)
                .setComissionTypeId(f.comission_type_id);
    }

    public int getComissionTypeId() {
        return comissionTypeId;
    }

    public PaymentsContext setComissionTypeId(int comissionTypeId) {
        this.comissionTypeId = comissionTypeId;
        return this;
    }

    public int getBalanceOrgId() {
        return balanceOrgId;
    }

    public PaymentsContext setBalanceOrgId(int balanceOrgId) {
        this.balanceOrgId = balanceOrgId;
        return this;
    }
}
