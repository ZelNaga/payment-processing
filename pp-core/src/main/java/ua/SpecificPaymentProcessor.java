package ua;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.data.Row;
import ua.utils.FluentPreparedStatement;

import java.util.List;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * That class process only special payments connected with  Vinitsya Vodokanal
 * and thous payments were made through two cash registers that belong to Vinitsya Vodokanal
 * Created by zelnaga on 19.05.16.
 */
class SpecificPaymentProcessor {

    // Two cash registers that belong to Vinitsya Vodokanal
    static final int VODOKANAL_CASH_REGISTER_1 = 9063;
    static final int VODOKANAL_CASH_REGISTER_2 = 7296;

    // id of organization to whom these payments were made
    private static final int VODOKANAL_ID = 5143;
    private static final int EMPTY_VALUE = 0;
    private static Logger LOGGER = LoggerFactory.getLogger(SpecificPaymentProcessor.class);

    private SpecificPaymentProcessor() {
    }

    static void proceed(int processingDayId) {
        try(Connection c = PaymentsDAO.getSql2o().beginTransaction();
            FluentPreparedStatement pstm = new FluentPreparedStatement(PaymentsDAO.createPaymentStoreStatement(c.getJdbcConnection()))) {

                LOGGER.info("SpecificPaymentProcessor in progress...");

                List<Row> specificPayments = PaymentsDAO.getSpecificPayments(c, PaymentConfig.getPeriodBegin(),
                                                                                  PaymentConfig.getPeriodEnd()).rows();
                if (!specificPayments.isEmpty()){

                    for (Row r : specificPayments) {
                        addBatchPayment(pstm, r, processingDayId);
                    }

                    LOGGER.info("Flushing specific payments...");

                    pstm.executeBatch();

                    c.commit();

                    LOGGER.info("SpecificPaymentProcessor finished...");
                }
        } catch (Exception e) {
            LOGGER.error("FAILED SpecificPaymentProcessor:", e);
        }
    }

    private static void addBatchPayment(FluentPreparedStatement pstm, Row r, int processingDayId) throws SQLException {
        pstm.setInt(1, r.getInteger("payment_id"))
        .setInt(2, r.getInteger("transaction_id"))
        .setDouble(3, r.getDouble("value"))
        .setDouble(4, r.getDouble("value"))
        .setLong(5, EMPTY_VALUE)
        .setInt(6, EMPTY_VALUE)
        .setInt(7, EMPTY_VALUE)
        .setInt(8, VODOKANAL_ID)
        .setInt(9, VODOKANAL_ID)
        .setInt(10, EMPTY_VALUE)
        .setInt(11, r.getInteger("organization_id"))
        .setInt(12, EMPTY_VALUE)
        .setInt(13, r.getInteger("user_id"))
        .setInt(14, EMPTY_VALUE)
        .setInt(15, r.getInteger("quittance_number"))
        .setInt(16, r.getInteger("cash_type_id"))
        .setString(17, DateHelper.timestamp())
        .setInt(18, processingDayId)
        .setInt(19, r.getInteger("organization_id"))
        .setInt(20, r.getInteger("organization_id"))
        .addBatch();
    }
}
