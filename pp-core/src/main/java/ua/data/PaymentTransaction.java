package ua.data;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by ekh on 20.11.15.
 */
public class PaymentTransaction extends BaseEntity {

    public Integer id;
    //public String external_id;
    //public String external_place;
    //public Integer session_id;
    public Integer home_id;
    public Integer user_id;
    //public Time period_begin;
    //public Time period_end;
    //public Time dt;
    public Date dt;
    //public Integer type;
    //public Integer bdate_id;
    //public Boolean cancelled;
    //public Boolean status;
    public Integer type_cash;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentTransaction)) return false;
        PaymentTransaction that = (PaymentTransaction) o;
        return com.google.common.base.Objects.equal(id, that.id) &&
                com.google.common.base.Objects.equal(home_id, that.home_id) &&
                com.google.common.base.Objects.equal(user_id, that.user_id) &&
                com.google.common.base.Objects.equal(dt, that.dt) &&
                com.google.common.base.Objects.equal(type_cash, that.type_cash);
    }

    @Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(id, home_id, user_id, dt, type_cash);
    }

    @Override
    public boolean validate() {
        return !Arrays.asList( id, home_id, user_id, dt, type_cash )
                .parallelStream().anyMatch( o -> o == null );
    }
}
