package ua.data;

import com.google.common.base.Preconditions;
import ua.utils.PaymentMath;

/**
 * Created by ekh on 22.12.15.
 */
public class RetentionCalculator {
    private static double TOTALLY_RETAINED_PAYMENT = 0.00; // completely deducted money from payment
    private double transferValue = .0;

    /**
     * Caclulate commission
     * @param value Initial value
     * @param percent Percent (0..100)
     * @param transferValue Initial transfer value
     * @return commission value
     */
    public Double calculateCommission( double value, double percent, double transferValue  ) {
        validateArguments(value, percent, transferValue);
        double commission = PaymentMath.round(value * (percent / 100));
        // handles situation where a commission can be >= than incoming argument transferValue
        if (commission >= transferValue) {
            this.transferValue = TOTALLY_RETAINED_PAYMENT;
            commission = transferValue;
        } else {
            this.transferValue = PaymentMath.round(transferValue - commission);
        }
        return commission;
    }

    /**
     * Validate incoming arguments
     * @param value Initial value
     * @param percent Percent (0..100)
     * @param transferValue Initial transfer value
     */
    private void validateArguments(double value, double percent, double transferValue) {
        Preconditions.checkArgument(value >= 0.00 && percent >=0.00 && transferValue >= 0.00,
                "Negative value in calculateCommission: %s, %s, %s", value, percent, transferValue);
        Preconditions.checkArgument(percent <= 100.00,
                "Percentage of commission payment can not exceed 100%: %s%", percent);
    }

    /**
     * Returns new transfer value
     * @return New transfer value after commission calculated
     */
    public Double getNewTransferValue() { return this.transferValue; }
}
