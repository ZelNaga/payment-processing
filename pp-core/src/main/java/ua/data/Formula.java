package ua.data;

import java.util.Objects;

/**
 * Created by ekh on 24.11.15.
 */
public class Formula extends BaseEntity {

    // main_new.m_commission
    public Long k;
    public Long bank_org_id;
    public Long supplier_org_id;
    public Long service_id;
    public Integer comission_type_id;
    public Integer comission_acceptor_org_id; //11
    public Long comission_formula_id;
    public Long comission_reason_id;
    public Long history_id;

    // m_commission_formula
    public Integer id; //1
    public Double  minimum;
    public Double percent; //1
    public Double  maximum;
    public Boolean is_month;
    // main_new.m_org_cmsu
    public Integer zhek;
    // online.tbl_organization
    public Integer type_id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Formula formula = (Formula) o;
        return Objects.equals(k, formula.k) &&
                Objects.equals(bank_org_id, formula.bank_org_id) &&
                Objects.equals(supplier_org_id, formula.supplier_org_id) &&
                Objects.equals(service_id, formula.service_id) &&
                Objects.equals(comission_type_id, formula.comission_type_id) &&
                Objects.equals(comission_acceptor_org_id, formula.comission_acceptor_org_id) &&
                Objects.equals(comission_formula_id, formula.comission_formula_id) &&
                Objects.equals(comission_reason_id, formula.comission_reason_id) &&
                Objects.equals(history_id, formula.history_id) &&
                Objects.equals(id, formula.id) &&
                Objects.equals(minimum, formula.minimum) &&
                Objects.equals(percent, formula.percent) &&
                Objects.equals(maximum, formula.maximum) &&
                Objects.equals(is_month, formula.is_month)&&
                Objects.equals(zhek, formula.zhek)&&
                Objects.equals(type_id, formula.type_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(k, bank_org_id, supplier_org_id, service_id, comission_type_id, comission_acceptor_org_id,
                comission_formula_id, comission_reason_id, history_id, id, minimum, percent, maximum, is_month, zhek, type_id);
    }

}
