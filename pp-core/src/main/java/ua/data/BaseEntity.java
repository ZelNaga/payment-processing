package ua.data;

import org.slf4j.*;

import java.lang.reflect.Field;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ekh on 26.11.15.
 */
public abstract class BaseEntity {

    private static Logger LOGGER = LoggerFactory.getLogger(BaseEntity.class);

    public boolean validate() {
        Field[] fields = this.getClass().getDeclaredFields();
        return !Stream.of(fields).parallel().anyMatch(this::isNullField);
    }

    public boolean isInvalid() {
        return !validate();
    }

    private boolean isNullField(Field f) {
        try {
            return f.get(this) == null;
        } catch (IllegalAccessException e) {
            LOGGER.error("BaseEntity::validate", e);
            return false;
        }
    }

    public String getErrors() {
        String result =
        Stream.of(this.getClass().getDeclaredFields()).parallel()
                .filter(f -> (isNullField(f)))
                .map( Field::getName )
                .collect(Collectors.joining(", ", "[", "]"));
        return "Нет данных:" + result;
    }
}
