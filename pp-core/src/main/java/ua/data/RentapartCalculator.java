package ua.data;

import com.google.common.base.Preconditions;
import ua.utils.PaymentMath;

/**
 * Created by ekh on 22.12.15.
 */
public class RentapartCalculator {
    private static double TOTALLY_SPLITED_RENTA = 0.00; // completely splitted rent
    private double newTransferValue;

    /**
     * Calculate rent part
     * @param rentStartValue Start value of renta
     * @param totalHomeRentCoefficient Home renta coefifcient
     * @param interest Side interest
     * @param transferValue Initial transfer value
     * @return Rent part calculated
     */
    public double calculateRentPart(double rentStartValue, double totalHomeRentCoefficient,
                                    double interest, double transferValue) throws IllegalArgumentException {

        //validateArguments(rentStartValue, totalHomeRentCoefficient, interest, transferValue);

        double percentage = (interest * 100 / totalHomeRentCoefficient) / 100;
        Preconditions.checkArgument((percentage * 100) <= 100.00,
                "Percentage of commission payment can't exceed 100%: %s%", percentage * 100);

        double rentPart = PaymentMath.round(rentStartValue * percentage);
        // handles situation where a commission can be >= than incoming argument transferValue
        if (rentPart >= transferValue) {
            this.newTransferValue = TOTALLY_SPLITED_RENTA;
            rentPart = transferValue;
        } else {
            this.newTransferValue = PaymentMath.round(transferValue - rentPart);
        }
        return rentPart;
    }

    /**
     * Validate incoming arguments
     * @param rentStartValue Start value of renta
     * @param totalHomeRentCoefficient Renta cofficient
     * @param interest Zheck interest
     * @param transferValue Initial transfer value
     */
    private void validateArguments(double rentStartValue,
                                   double totalHomeRentCoefficient,
                                   double interest,
                                   double transferValue) {

        boolean valid = rentStartValue >= 0.00
                && totalHomeRentCoefficient > 0.00
                && interest >= 0.00
                && transferValue >= 0;

        /* TODO: to turn on in production
        Preconditions.checkArgument( valid,
                "Invalid values : rentStartValue=%s, rentCoefficient=%s, interest=%s, transferValue=%s",
                rentStartValue, totalHomeRentCoefficient, interest, transferValue);
                */

    }

    /**
     * Returns new tansfer value after rentapart caclulation
     * @return New transfer value
     */
    public Double getNewTransferValue() {
        return newTransferValue;
    }
}
