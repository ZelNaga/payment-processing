package ua.data;

import com.google.common.base.MoreObjects;
import com.google.common.base.Suppliers;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static ua.PaymentsDAO.getFormulas;

/**
 * Created by ekh on 24.11.15.
 */
public class PaymentOperation extends BaseEntity {

    public Integer ID;
    public Integer no;
    public Integer zhek;
    public Integer account;
    public Integer home_id;
    public Integer service_id;
    public Double value;
    public Integer supplier_org_id;
    public Integer acceptor_org_id;
    public Integer main_org_id;

    public com.google.common.base.Supplier<List<Formula>> formules =
            Suppliers.memoize( () -> getFormulas( service_id, supplier_org_id, main_org_id ).getResultList() );

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentOperation that = (PaymentOperation) o;
        return Objects.equals(ID, that.ID) &&
                Objects.equals(no, that.no) &&
                Objects.equals(zhek, that.zhek) &&
                Objects.equals(account, that.account) &&
                Objects.equals(home_id, that.home_id) &&
                Objects.equals(service_id, that.service_id) &&
                Objects.equals(value, that.value) &&
                Objects.equals(supplier_org_id, that.supplier_org_id) &&
                Objects.equals(acceptor_org_id, that.acceptor_org_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, no, zhek, account, home_id, service_id, value, supplier_org_id, acceptor_org_id );
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("ID", ID)
                .add("no", no)
                .add("zhek", zhek)
                .add("account", account)
                .add("home_id", home_id)
                .add("service_id", service_id)
                .add("value", value)
                .add("supplier_org_id", supplier_org_id)
                .add("acceptor_org_id", acceptor_org_id)
                .toString();
    }

    @Override
    public boolean validate() {
        boolean result = Arrays.asList( ID,
                no, zhek, account, home_id,
                service_id, value,
                supplier_org_id, acceptor_org_id )
                .stream().anyMatch( o -> o == null );

        boolean invalidFormules = true;
        if(!result) {
            invalidFormules = formules != null && formules.get().parallelStream().anyMatch(f -> !f.validate());
        }


        return !result || !invalidFormules;
    }
}
