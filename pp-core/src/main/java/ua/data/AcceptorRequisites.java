package ua.data;

import com.google.common.base.Objects;

/**
 * Created by ekh on 07.12.15.
 */
public class AcceptorRequisites extends BaseEntity {
    public Long account;
    public Integer mfo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AcceptorRequisites)) return false;
        AcceptorRequisites that = (AcceptorRequisites) o;
        return Objects.equal(account, that.account) &&
                Objects.equal(mfo, that.mfo);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(account, mfo);
    }
}
