package ua.data;

/**
 * Created by ekh on 15.12.15.
 */
public class ScalarResult<T> {
    T result;
    String sql;

    private ScalarResult( T data, String sql  ) {
        result = data;
        this.sql = sql;
    }

    public static <T> ScalarResult of (T data, String sql ) { return new ScalarResult<>(data, sql ); }
    public T get() { return  result; }
    public String getSql() { return sql; }
}
