package ua.data;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by ekh on 26.11.15.
 */
public final class FetchResult<T> {
    private final List<T> result;
    private final String sql;

    private FetchResult( List<T> r, String sql ) {
        this.result  = r;
        this.sql = sql;
    }

    public static <T> FetchResult<T> of( List<T> result, String sql) {
        return new FetchResult<>( result, sql );
    }

    public List<T> getResultList() {
        return result;
    }

    public Stream<T> getStream() {
        return result.parallelStream();
    }

    public String getSql() {
        return sql;
    }
}