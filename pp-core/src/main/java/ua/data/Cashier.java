package ua.data;

import java.sql.Time;
import java.util.Objects;

/**
 * Created by ekh on 20.11.15.
 */
public class Cashier extends BaseEntity
{
    public Integer organization_id;
    public Integer user_id; // cashier id
    // schedulers

    public Time begin_time;

    public Time end_time;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cashier cashier = (Cashier) o;
        return Objects.equals(organization_id, cashier.organization_id) &&
                Objects.equals(user_id, cashier.user_id) &&
                Objects.equals(begin_time, cashier.begin_time) &&
                Objects.equals(end_time, cashier.end_time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(organization_id, user_id, begin_time, end_time);
    }

    @Override
    public boolean validate() {
        return user_id != null && organization_id != null;
    }
}
