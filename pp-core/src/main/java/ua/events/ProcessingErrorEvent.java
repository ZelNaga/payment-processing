package ua.events;

/**
 * Created by ekh on 14.03.16.
 */
public class ProcessingErrorEvent {
    private final String errorMsg;
    public ProcessingErrorEvent(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    public String getErrorMsg() {
        return errorMsg;
    }
}
