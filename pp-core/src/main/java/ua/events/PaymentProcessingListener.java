package ua.events;

/**
 * Created by ekh on 02.03.16.
 */
public interface PaymentProcessingListener {
    void onStart();
    void onFinish();
    void onProgress( double progress );
    void onMessage( String msg );
    void onError(String msg);
}
