package ua.events;

/**
 * Created by ekh on 14.03.16.
 */
public class ProcessingMsgEvent {
    private final String msg;
    public ProcessingMsgEvent(String msg) {
        this.msg = msg;
    }
    public String getMsg() {return msg;}
}
