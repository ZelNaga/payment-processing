package ua.events;

/**
 * Created by ekh on 18.11.15.
 */
public class ProgressEvent {
    final int progress;
    final int total;
    final long threadId;

    public ProgressEvent( long thId,  int progress, int total ) {
        this.progress = progress;
        this.total = total;
        this.threadId = thId;
    }

    public int getProgress() {
        return progress;
    }
    public int getTotal() {
        return total;
    }

    public long getThreadId() {
        return threadId;
    }
}
