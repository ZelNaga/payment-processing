package ua;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.LoggerFactory;

import java.time.format.DateTimeParseException;

/**
 * Created by ekh on 16.12.15.
 */
public class PaymentConfig {

    static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PaymentConfig.class);
    private static final String DATABASE_SERVER = "database.server";
    private static final String DATABSE_PORT = "database.port";
    private static final String DATABASE_USER = "database.user";
    private static final String DATABASE_PWD = "database.pwd";
    private static final String PROCESSING_PERIOD_BEGIN = "processing.PeriodBegin";
    private static final String PROCESSING_PERIOD_END = "processing.PeriodEnd";
    private static final String CHECK_DATABASE_INTEGRITY = "processing.checkDatabaseIntegrity";
    private static final String GENERATE_SUMMARY_REPORT = "processing.generateSummaryReport";
    private static final String OPERATOR_ID = "processing.operatorId";
    private static final String COMMENT = "processing.comment";


    private static Configuration cfg = null;

    public static synchronized Configuration loadConfig( boolean reload ) throws ConfigurationException {
        if ( cfg == null || reload ) {
            FileConfiguration fc = new PropertiesConfiguration();
            fc.setAutoSave(false);
            fc.load("conf/paymentprocessing.properties");
            cfg = fc;
        }
        return cfg;
    }

    public static String getDatabasedUser() {
        return cfg.getString(DATABASE_USER);
    }

    public static String getDatabaseServer()  {
        return cfg.getString(DATABASE_SERVER);
    }

    public static Integer getDatabasePort() {
        return cfg.getInteger(DATABSE_PORT, 3306);
    }

    public static String getDatabasePassword() {
        return cfg.getString(DATABASE_PWD);
    }

    public static String getPeriodBegin()  {
        return cfg.getString(PROCESSING_PERIOD_BEGIN);
    }

    public static String getPeriodEnd() {
        return cfg.getString(PROCESSING_PERIOD_END);
    }

    public static Boolean getCheckDatabaseIntegrity() {
        return cfg.getBoolean(CHECK_DATABASE_INTEGRITY, true);
    }

    public static Boolean getGenerateSummaryReport() {
        return cfg.getBoolean(GENERATE_SUMMARY_REPORT, true);
    }

    public static Integer getOperatorId() {
        return cfg.getInteger(OPERATOR_ID, 000);
    }

    public static String getComment() {
        return cfg.getString(COMMENT);
    }

    public static void setProperty(String key, Object value) {
        cfg.setProperty(key, value);
    }

    public static void setOperatorId(Integer id) {
        cfg.setProperty(OPERATOR_ID, id);
    }

    public static void setComment( String comment ) {
        cfg.setProperty( COMMENT, comment );
    }

    public static void setStartEndProcessing( String start, String end ) {

        cfg.setProperty( PROCESSING_PERIOD_BEGIN, start );
        cfg.setProperty( PROCESSING_PERIOD_END, end );
    }

    public static String getProcessingDate() throws DateTimeParseException {
        return DateHelper.createLocalDateTime(PaymentConfig.getPeriodEnd()).toLocalDate().toString();
    }
}
