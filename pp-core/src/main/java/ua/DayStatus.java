package ua;

/**
 * Created by zelnaga on 19.02.16.
 */
public enum DayStatus {
    OPEN,
    ERROR,
    PROCESSED,
    CLOSED
}
