package servlet;

import eu.webtoolkit.jwt.WApplication;
import eu.webtoolkit.jwt.WEnvironment;
import eu.webtoolkit.jwt.WtServlet;

/**
 * Created by asv on 14.11.16.
 */

public class PPRServlet extends WtServlet {
    private static final long serialVersionUID = 1L;

    public PPRServlet() {
        super();
    }

    @Override
    public WApplication createApplication(WEnvironment env) {
        /*
         * You could read information from the environment to decide whether the
         * user has permission to start a new application
         */
        return new PPRApplication(env);
    }
}
