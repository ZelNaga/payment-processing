package servlet;


import eu.webtoolkit.jwt.*;
import tasks.PPRTask;
import ua.events.ReportProgressEvent;
import ua.events.ReportsBuildingListener;

/**
 * Created by asv on 14.11.16.
 */
public class PPRApplication extends WApplication {
    private PPRServletEventListener progressListener = new PPRServletEventListener();
    private final WProgressBar totalProgressBar = new WProgressBar();
    private final WProgressBar reportProgressBar = new WProgressBar();
    private final WText infoBox = new WText("", TextFormat.XHTMLUnsafeText);
    private final WPanel reportProgressBarPanel = new WPanel();

    public PPRApplication(WEnvironment env) {
        super(env);
        this.setTheme(new WBootstrapTheme());
        this.useStyleSheet(new WLink("css/style.css"));
        infoBox.setWordWrap(false);
        addWidgets();
    }

    private void addWidgets() {
        setTitle("PPRServlet");

        final WDateEdit startDate = new WDateEdit();
        startDate.setDate(WDate.getCurrentServerDate().addDays(-1));
        startDate.setFormat("yyyy-MM-dd");

        final WDateEdit endDate = new WDateEdit();
        endDate.setDate(WDate.getCurrentServerDate().addDays(-1));
        startDate.setTop(endDate.getDate());
        endDate.setFormat("yyyy-MM-dd");
        endDate.setBottom(startDate.getDate());

        final WPushButton startButton = new WPushButton("Start");
        startButton.setStyleClass("btn btn-primary");

        WGroupBox appArgsBox = new WGroupBox(getRoot());
        appArgsBox.setTitle("Application arguments.");
        appArgsBox.addWidget(new WText("Start date "));
        appArgsBox.addWidget(startDate);
        appArgsBox.addWidget(new WText("End date "));
        appArgsBox.addWidget(endDate);
        appArgsBox.addWidget(startButton);

        getRoot().addWidget(new WBreak());

        totalProgressBar.setRange(0, 100);

        WPanel totalProgressBarPanel = new WPanel(getRoot());
        totalProgressBarPanel.setTitle("Application total progress.");
        totalProgressBarPanel.hide();
        totalProgressBarPanel.setCentralWidget(totalProgressBar);

        getRoot().addWidget(new WBreak());

        reportProgressBar.setRange(0, 100);

        getRoot().addWidget(reportProgressBarPanel);
        reportProgressBarPanel.setTitle("Application some report progress.");
        reportProgressBarPanel.hide();
        reportProgressBarPanel.setCentralWidget(reportProgressBar);

        getRoot().addWidget(new WBreak());

        WPanel infoBoxPanel = new WPanel(getRoot());
        infoBoxPanel.setTitle("Application logout.");
        infoBoxPanel.hide();
        infoBoxPanel.setCentralWidget(infoBox);
        infoBox.addStyleClass("info-box");

        final WTimer intervalTimer = new WTimer(getRoot()); //TODO: should be replaced by WApplication#enableUpdates()
        intervalTimer.setInterval(500);

        startDate.changed().addListener(this, () -> {
            if (startDate.validate() == WValidator.State.Valid) {
                endDate.setBottom(startDate.getDate());
            }
        });

        endDate.changed().addListener(this, () -> {
            if (startDate.validate() == WValidator.State.Valid) {
                startDate.setTop(endDate.getDate());
            }
        });

        startButton.clicked().addListener(this, () -> {
            appArgsBox.hide();
            intervalTimer.start();
            startButton.disable();
            PPRTask.execute(progressListener, startDate.getText(),
                            endDate.getText());
            reportProgressBarPanel.show();
            totalProgressBarPanel.show();
            infoBoxPanel.show();
        });

        intervalTimer.timeout().addListener(this, () -> {
            if (!progressListener.getAppStatus()) {
                reportProgressBarPanel.hide();
                totalProgressBarPanel.hide();
                intervalTimer.stop();
                startButton.enable();
                appArgsBox.show();
            }
        });
    }

    private class PPRServletEventListener implements ReportsBuildingListener {

        private volatile boolean appRun = false;

        public synchronized boolean getAppStatus() {
            return this.appRun;
        }

        @Override
        public synchronized void onStart() {
            attachThread();
            this.appRun = true;
            totalProgressBar.setValue(0.0);
            reportProgressBarPanel.setTitle("");
            reportProgressBar.setValue(0.0);
            infoBox.setText("");
        }

        @Override
        public synchronized void onFinish() {
            attachThread();
            this.appRun = false;
        }

        @Override
        public synchronized void onTotalProgress(double progress) {
            attachThread();
            totalProgressBar.setValue(progress);
        }

        @Override
        public synchronized void onReportProgress(ReportProgressEvent event) {
            attachThread();
            reportProgressBarPanel.setTitle("Current report " + event.getReportName());
            reportProgressBar.setValue(event.getReportProgress());
        }

        @Override
        public synchronized void onMessage(String msg) {
            attachThread();
            infoBox.setText(new WString("{1}<p>{2}</p>").arg(infoBox.getText()).arg(msg));
        }

        @Override
        public synchronized void onError(String msg) {
            attachThread();
            infoBox.setText(new WString("{1}<p style=\"color : red\">{2}</p>").arg(infoBox.getText()).arg(msg));
        }
    }
}
