package tasks;


import ua.PPRCoreMain;
import ua.events.ReportsBuildingListener;

/**
 * Created by zelnaga on 24.11.16.
 */
public class PPRTask {
    public static void execute(ReportsBuildingListener progressListener, String startDate, String endDate) {
        new Thread(() -> {
            PPRCoreMain.getInstance().setPPRListener(progressListener);
            PPRCoreMain.getInstance().proceed(startDate, endDate);
        }).start();
    }
}
